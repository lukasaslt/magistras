package org.inspection_plusplus.service.v2;
public class TestTestSuitedeleteInspectionPlan
{
	@org.junit.Test
	public void testtestDeleteInspectionPlan1() throws Throwable
	{
		org.inspection_plusplus.service.v2.WebServiceImpl testObject = new org.inspection_plusplus.service.v2.WebServiceImpl();
		org.inspection_plusplus.ID4Referencing iD4Referencing0 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing0.setClassID("aaaaaaaaaaaaa");
		iD4Referencing0.setUuid("aaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing0, "extension", extension1);
		org.inspection_plusplus.Sender sender2 = new org.inspection_plusplus.Sender();
		sender2.setAppID("aaaaaa");
		sender2.setDomain("aaaa");
		org.inspection_plusplus.Extension extension3 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any4 = new java.util.ArrayList<Object>();
		any4.add(new Object());
		any4.add(new Object());
		any4.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3, "any", any4);
		extension3.setVendor("aaaaaaaaaaaaaaaa");

		org.inspection_plusplus.ReturnStatus res = testObject.deleteInspectionPlan(iD4Referencing0, sender2, extension3);
		junit.framework.Assert.fail();
	};

	@org.junit.Test
	public void testtestDeleteInspectionPlan2() throws Throwable
	{
		org.inspection_plusplus.service.v2.WebServiceImpl testObject = new org.inspection_plusplus.service.v2.WebServiceImpl();
		org.inspection_plusplus.ID4Referencing iD4Referencing5 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing5.setClassID("");
		iD4Referencing5.setUuid("aaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension6 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing5, "extension", extension6);
		org.inspection_plusplus.Sender sender7 = new org.inspection_plusplus.Sender();
		sender7.setAppID("aaaaa");
		sender7.setDomain("aaaaa");
		org.inspection_plusplus.Extension extension8 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any9 = new java.util.ArrayList<Object>();
		any9.add(new Object());
		any9.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension8, "any", any9);
		extension8.setVendor("");

		org.inspection_plusplus.ReturnStatus res = testObject.deleteInspectionPlan(iD4Referencing5, sender7, extension8);
		junit.framework.Assert.fail();
	};

	@org.junit.Test
	public void testtestDeleteInspectionPlan3() throws Throwable
	{
		org.inspection_plusplus.service.v2.WebServiceImpl testObject = new org.inspection_plusplus.service.v2.WebServiceImpl();
		org.inspection_plusplus.ID4Referencing iD4Referencing10 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing10.setClassID("");
		iD4Referencing10.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension11 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing10, "extension", extension11);
		org.inspection_plusplus.Sender sender12 = new org.inspection_plusplus.Sender();
		sender12.setAppID("");
		sender12.setDomain("aaaaaaaaaaa");
		org.inspection_plusplus.Extension extension13 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any14 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension13, "any", any14);
		extension13.setVendor("");

		org.inspection_plusplus.ReturnStatus res = testObject.deleteInspectionPlan(iD4Referencing10, sender12, extension13);
		junit.framework.Assert.fail();
	};

	@org.junit.Test
	public void testtestDeleteInspectionPlan4() throws Throwable
	{
		org.inspection_plusplus.service.v2.WebServiceImpl testObject = new org.inspection_plusplus.service.v2.WebServiceImpl();
		org.inspection_plusplus.ID4Referencing iD4Referencing15 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing15.setClassID("aaaaaaaaaaaa");
		iD4Referencing15.setUuid("aaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension16 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension17 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any18 = new java.util.ArrayList<Object>();
		any18.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension17, "any", any18);
		extension17.setVendor("aaaaaaaaaaaaaaaaa");

		extension16.add(extension17);
		org.inspection_plusplus.Extension extension19 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any20 = new java.util.ArrayList<Object>();
		any20.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension19, "any", any20);
		extension19.setVendor("");

		extension16.add(extension19);
		org.inspection_plusplus.Extension extension21 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any22 = new java.util.ArrayList<Object>();
		any22.add(new Object());
		any22.add(new Object());
		any22.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension21, "any", any22);
		extension21.setVendor("");

		extension16.add(extension21);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing15, "extension", extension16);
		org.inspection_plusplus.Sender sender23 = new org.inspection_plusplus.Sender();
		sender23.setAppID("");
		sender23.setDomain("aaaaaaaaaaaaaaaaaa");
		org.inspection_plusplus.Extension extension24 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any25 = new java.util.ArrayList<Object>();
		any25.add(new Object());
		any25.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension24, "any", any25);
		extension24.setVendor("");

		org.inspection_plusplus.ReturnStatus res = testObject.deleteInspectionPlan(iD4Referencing15, sender23, extension24);
		junit.framework.Assert.fail();
	};

	@org.junit.Test
	public void testtestDeleteInspectionPlan5() throws Throwable
	{
		org.inspection_plusplus.service.v2.WebServiceImpl testObject = new org.inspection_plusplus.service.v2.WebServiceImpl();
		org.inspection_plusplus.ID4Referencing iD4Referencing26 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing26.setClassID("aaaaaaaaaa");
		iD4Referencing26.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension27 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing26, "extension", extension27);
		org.inspection_plusplus.Sender sender28 = new org.inspection_plusplus.Sender();
		sender28.setAppID("");
		sender28.setDomain("aaaaaaaaaa");
		org.inspection_plusplus.Extension extension29 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any30 = new java.util.ArrayList<Object>();
		any30.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension29, "any", any30);
		extension29.setVendor("");

		org.inspection_plusplus.ReturnStatus res = testObject.deleteInspectionPlan(iD4Referencing26, sender28, extension29);
		junit.framework.Assert.fail();
	};

	@org.junit.Test
	public void testtestDeleteInspectionPlan6() throws Throwable
	{
		org.inspection_plusplus.service.v2.WebServiceImpl testObject = new org.inspection_plusplus.service.v2.WebServiceImpl();
		org.inspection_plusplus.ID4Referencing iD4Referencing31 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing31.setClassID("");
		iD4Referencing31.setUuid("aaaaaaaaaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension32 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension33 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any34 = new java.util.ArrayList<Object>();
		any34.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension33, "any", any34);
		extension33.setVendor("");

		extension32.add(extension33);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing31, "extension", extension32);
		org.inspection_plusplus.Sender sender35 = new org.inspection_plusplus.Sender();
		sender35.setAppID("aaaaaaaaaa");
		sender35.setDomain("aaaaaa");
		org.inspection_plusplus.Extension extension36 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any37 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension36, "any", any37);
		extension36.setVendor("aaaaaaaaaaaaaaa");

		org.inspection_plusplus.ReturnStatus res = testObject.deleteInspectionPlan(iD4Referencing31, sender35, extension36);
		junit.framework.Assert.fail();
	};

	@org.junit.Test
	public void testtestDeleteInspectionPlan7() throws Throwable
	{
		org.inspection_plusplus.service.v2.WebServiceImpl testObject = new org.inspection_plusplus.service.v2.WebServiceImpl();
		org.inspection_plusplus.ID4Referencing iD4Referencing38 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing38.setClassID("aa");
		iD4Referencing38.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension39 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension40 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any41 = new java.util.ArrayList<Object>();
		any41.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension40, "any", any41);
		extension40.setVendor("aaaaaaaaaa");

		extension39.add(extension40);
		org.inspection_plusplus.Extension extension42 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any43 = new java.util.ArrayList<Object>();
		any43.add(new Object());
		any43.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension42, "any", any43);
		extension42.setVendor("");

		extension39.add(extension42);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing38, "extension", extension39);
		org.inspection_plusplus.Sender sender44 = new org.inspection_plusplus.Sender();
		sender44.setAppID("aaaaaaaaaaaaaaaaa");
		sender44.setDomain("aaaaaaa");
		org.inspection_plusplus.Extension extension45 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any46 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension45, "any", any46);
		extension45.setVendor("");

		org.inspection_plusplus.ReturnStatus res = testObject.deleteInspectionPlan(iD4Referencing38, sender44, extension45);
		junit.framework.Assert.fail();
	};

	@org.junit.Test
	public void testtestDeleteInspectionPlan8() throws Throwable
	{
		org.inspection_plusplus.service.v2.WebServiceImpl testObject = new org.inspection_plusplus.service.v2.WebServiceImpl();
		org.inspection_plusplus.ID4Referencing iD4Referencing47 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing47.setClassID("aaaaaaaaaaaaaaa");
		iD4Referencing47.setUuid("aaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension48 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing47, "extension", extension48);
		org.inspection_plusplus.Sender sender49 = new org.inspection_plusplus.Sender();
		sender49.setAppID("");
		sender49.setDomain("aaaaaa");
		org.inspection_plusplus.Extension extension50 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any51 = new java.util.ArrayList<Object>();
		any51.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension50, "any", any51);
		extension50.setVendor("aaaa");

		org.inspection_plusplus.ReturnStatus res = testObject.deleteInspectionPlan(iD4Referencing47, sender49, extension50);
		junit.framework.Assert.fail();
	};

	@org.junit.Test
	public void testtestDeleteInspectionPlan9() throws Throwable
	{
		org.inspection_plusplus.service.v2.WebServiceImpl testObject = new org.inspection_plusplus.service.v2.WebServiceImpl();
		org.inspection_plusplus.ID4Referencing iD4Referencing52 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing52.setClassID("");
		iD4Referencing52.setUuid("aaaaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension53 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension54 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any55 = new java.util.ArrayList<Object>();
		any55.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension54, "any", any55);
		extension54.setVendor("aaaaa");

		extension53.add(extension54);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing52, "extension", extension53);
		org.inspection_plusplus.Sender sender56 = new org.inspection_plusplus.Sender();
		sender56.setAppID("");
		sender56.setDomain("");
		org.inspection_plusplus.Extension extension57 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any58 = new java.util.ArrayList<Object>();
		any58.add(new Object());
		any58.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension57, "any", any58);
		extension57.setVendor("");

		org.inspection_plusplus.ReturnStatus res = testObject.deleteInspectionPlan(iD4Referencing52, sender56, extension57);
		junit.framework.Assert.fail();
	};

	@org.junit.Test
	public void testtestDeleteInspectionPlan10() throws Throwable
	{
		org.inspection_plusplus.service.v2.WebServiceImpl testObject = new org.inspection_plusplus.service.v2.WebServiceImpl();
		org.inspection_plusplus.ID4Referencing iD4Referencing59 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing59.setClassID("a");
		iD4Referencing59.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension60 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension61 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any62 = new java.util.ArrayList<Object>();
		any62.add(new Object());
		any62.add(new Object());
		any62.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension61, "any", any62);
		extension61.setVendor("aaaaaaaa");

		extension60.add(extension61);
		org.inspection_plusplus.Extension extension63 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any64 = new java.util.ArrayList<Object>();
		any64.add(new Object());
		any64.add(new Object());
		any64.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension63, "any", any64);
		extension63.setVendor("");

		extension60.add(extension63);
		org.inspection_plusplus.Extension extension65 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any66 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension65, "any", any66);
		extension65.setVendor("aaaaaa");

		extension60.add(extension65);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing59, "extension", extension60);
		org.inspection_plusplus.Sender sender67 = new org.inspection_plusplus.Sender();
		sender67.setAppID("");
		sender67.setDomain("aaaaaaaaaa");
		org.inspection_plusplus.Extension extension68 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any69 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension68, "any", any69);
		extension68.setVendor("aaaaa");

		org.inspection_plusplus.ReturnStatus res = testObject.deleteInspectionPlan(iD4Referencing59, sender67, extension68);
		junit.framework.Assert.fail();
	};

	@org.junit.Test
	public void testtestDeleteInspectionPlan11() throws Throwable
	{
		org.inspection_plusplus.service.v2.WebServiceImpl testObject = new org.inspection_plusplus.service.v2.WebServiceImpl();
		org.inspection_plusplus.ID4Referencing iD4Referencing70 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing70.setClassID("");
		iD4Referencing70.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension71 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension72 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any73 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension72, "any", any73);
		extension72.setVendor("aaaaaaaa");

		extension71.add(extension72);
		org.inspection_plusplus.Extension extension74 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any75 = new java.util.ArrayList<Object>();
		any75.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension74, "any", any75);
		extension74.setVendor("");

		extension71.add(extension74);
		org.inspection_plusplus.Extension extension76 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any77 = new java.util.ArrayList<Object>();
		any77.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension76, "any", any77);
		extension76.setVendor("");

		extension71.add(extension76);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing70, "extension", extension71);
		org.inspection_plusplus.Sender sender78 = new org.inspection_plusplus.Sender();
		sender78.setAppID("aaaaaaaaaaa");
		sender78.setDomain("a");
		org.inspection_plusplus.Extension extension79 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any80 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension79, "any", any80);
		extension79.setVendor("aaaaaa");

		org.inspection_plusplus.ReturnStatus res = testObject.deleteInspectionPlan(iD4Referencing70, sender78, extension79);
		junit.framework.Assert.fail();
	};

};
