package org.inspection_plusplus.service.v2;
public class TestTestSuitestoreInspectionPlan
{
	@org.junit.Test
	public void testtestStoreInspectionPlan1() throws Throwable
	{
		org.inspection_plusplus.service.v2.WebServiceImpl testObject = new org.inspection_plusplus.service.v2.WebServiceImpl();
		org.inspection_plusplus.InspectionPlan inspectionPlan0 = new org.inspection_plusplus.InspectionPlan();
		inspectionPlan0.setInspectionCategories("aaaaaaaaaaaaaaaaaaa");
		inspectionPlan0.setIPsymmetry("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
		inspectionPlan0.setPartQAversion("aaaaaaaaaaaa");
		eu.sarunas.junit.TestsHelper.set(inspectionPlan0, "pdMmaturity", "");
		inspectionPlan0.setQAmaturity("");
		inspectionPlan0.setQAversion("");
		java.util.ArrayList<org.inspection_plusplus.Comment> comment1 = new java.util.ArrayList<org.inspection_plusplus.Comment>();
		org.inspection_plusplus.Comment comment2 = new org.inspection_plusplus.Comment();
		comment2.setAuthor("");
		comment2.setReceiver("");
		comment2.setSubject("");
		comment2.setText("aaaaaaaaa");
		comment2.setName("aa");
		org.inspection_plusplus.ID4Objects iD4Objects3 = new org.inspection_plusplus.ID4Objects();
		iD4Objects3.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension4 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension5 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any6 = new java.util.ArrayList<Object>();
		any6.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension5, "any", any6);
		extension5.setVendor("aaaa");

		extension4.add(extension5);
		org.inspection_plusplus.Extension extension7 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any8 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension7, "any", any8);
		extension7.setVendor("aaa");

		extension4.add(extension7);
		eu.sarunas.junit.TestsHelper.set(iD4Objects3, "extension", extension4);

		comment2.setSystemID(iD4Objects3);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef9 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(comment2, "commentRef", commentRef9);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension10 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension11 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any12 = new java.util.ArrayList<Object>();
		any12.add(new Object());
		any12.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension11, "any", any12);
		extension11.setVendor("aaaaaaaaaaaaaaaaaaaaaa");

		extension10.add(extension11);
		org.inspection_plusplus.Extension extension13 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any14 = new java.util.ArrayList<Object>();
		any14.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension13, "any", any14);
		extension13.setVendor("");

		extension10.add(extension13);
		eu.sarunas.junit.TestsHelper.set(comment2, "extension", extension10);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID15 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID16 = new org.inspection_plusplus.DisplayID();
		displayID16.setLanguage("");
		displayID16.setText("");

		displayID15.add(displayID16);
		eu.sarunas.junit.TestsHelper.set(comment2, "displayID", displayID15);
		java.util.ArrayList<org.inspection_plusplus.History> history17 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history18 = new org.inspection_plusplus.History();
		history18.setAction("aaaaaaaaaaaaaaaaaaaa");
		history18.setAuthor("");
		history18.setComment("aaa");
		history18.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 0, 28)));

		history17.add(history18);
		eu.sarunas.junit.TestsHelper.set(comment2, "history", history17);

		comment1.add(comment2);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan0, "comment", comment1);
		java.util.ArrayList<org.inspection_plusplus.Tolerance> tolerance19 = new java.util.ArrayList<org.inspection_plusplus.Tolerance>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan0, "tolerance", tolerance19);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> partRef20 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing21 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing21.setClassID("");
		iD4Referencing21.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension22 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing21, "extension", extension22);

		partRef20.add(iD4Referencing21);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan0, "partRef", partRef20);
		java.util.ArrayList<org.inspection_plusplus.IPE> ipe23 = new java.util.ArrayList<org.inspection_plusplus.IPE>();
		org.inspection_plusplus.IPEAngle iPEAngle24 = new org.inspection_plusplus.IPEAngle();
		iPEAngle24.setAngle(-312.59);
		iPEAngle24.setAxisAngleX(8.21);
		iPEAngle24.setAxisAngleY(81.66);
		iPEAngle24.setAxisAngleZ(54.91);
		iPEAngle24.setPlaneAngleXY(-139.87);
		iPEAngle24.setPlaneAngleXZ(191.86);
		iPEAngle24.setPlaneAngleYZ(-36.74);
		iPEAngle24.setAuxiliaryElement(false);
		iPEAngle24.setCalculatedElement(false);
		iPEAngle24.setUseLeft(true);
		iPEAngle24.setUseRight(false);
		org.inspection_plusplus.ID4Referencing iD4Referencing25 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing25.setClassID("");
		iD4Referencing25.setUuid("a");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension26 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension27 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any28 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension27, "any", any28);
		extension27.setVendor("");

		extension26.add(extension27);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing25, "extension", extension26);

		iPEAngle24.setGeometricObjectRef(iD4Referencing25);
		java.util.ArrayList<org.inspection_plusplus.CalculationNominalElement> calculationNominalElement29 = new java.util.ArrayList<org.inspection_plusplus.CalculationNominalElement>();
		org.inspection_plusplus.CalculationNominalElement calculationNominalElement30 = new org.inspection_plusplus.CalculationNominalElement();
		calculationNominalElement30.setOperation("aaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.OperationParameter> operationParameter31 = new java.util.ArrayList<org.inspection_plusplus.OperationParameter>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement30, "operationParameter", operationParameter31);
		java.util.ArrayList<org.inspection_plusplus.Operand> operand32 = new java.util.ArrayList<org.inspection_plusplus.Operand>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement30, "operand", operand32);
		calculationNominalElement30.setName("aaaaaaaaaaaaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects33 = new org.inspection_plusplus.ID4Objects();
		iD4Objects33.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension34 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension35 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any36 = new java.util.ArrayList<Object>();
		any36.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension35, "any", any36);
		extension35.setVendor("aaa");

		extension34.add(extension35);
		eu.sarunas.junit.TestsHelper.set(iD4Objects33, "extension", extension34);

		calculationNominalElement30.setSystemID(iD4Objects33);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef37 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement30, "commentRef", commentRef37);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension38 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement30, "extension", extension38);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID39 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID40 = new org.inspection_plusplus.DisplayID();
		displayID40.setLanguage("");
		displayID40.setText("");

		displayID39.add(displayID40);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement30, "displayID", displayID39);
		java.util.ArrayList<org.inspection_plusplus.History> history41 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement30, "history", history41);

		calculationNominalElement29.add(calculationNominalElement30);
		org.inspection_plusplus.CalculationNominalElement calculationNominalElement42 = new org.inspection_plusplus.CalculationNominalElement();
		calculationNominalElement42.setOperation("");
		java.util.ArrayList<org.inspection_plusplus.OperationParameter> operationParameter43 = new java.util.ArrayList<org.inspection_plusplus.OperationParameter>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement42, "operationParameter", operationParameter43);
		java.util.ArrayList<org.inspection_plusplus.Operand> operand44 = new java.util.ArrayList<org.inspection_plusplus.Operand>();
		org.inspection_plusplus.Operand operand45 = new org.inspection_plusplus.Operand();
		operand45.setIndex(new java.math.BigInteger("-263"));
		operand45.setRole("");
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> referenceSystemRef46 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing47 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing47.setClassID("");
		iD4Referencing47.setUuid("aaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension48 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing47, "extension", extension48);

		referenceSystemRef46.add(iD4Referencing47);
		eu.sarunas.junit.TestsHelper.set(operand45, "referenceSystemRef", referenceSystemRef46);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeGeometricRef49 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(operand45, "ipeGeometricRef", ipeGeometricRef49);

		operand44.add(operand45);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement42, "operand", operand44);
		calculationNominalElement42.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects50 = new org.inspection_plusplus.ID4Objects();
		iD4Objects50.setUuid("aaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension51 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension52 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any53 = new java.util.ArrayList<Object>();
		any53.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension52, "any", any53);
		extension52.setVendor("");

		extension51.add(extension52);
		org.inspection_plusplus.Extension extension54 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any55 = new java.util.ArrayList<Object>();
		any55.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension54, "any", any55);
		extension54.setVendor("aa");

		extension51.add(extension54);
		eu.sarunas.junit.TestsHelper.set(iD4Objects50, "extension", extension51);

		calculationNominalElement42.setSystemID(iD4Objects50);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef56 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing57 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing57.setClassID("aaaaaa");
		iD4Referencing57.setUuid("aaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension58 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension59 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any60 = new java.util.ArrayList<Object>();
		any60.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension59, "any", any60);
		extension59.setVendor("aaaaaaaa");

		extension58.add(extension59);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing57, "extension", extension58);

		commentRef56.add(iD4Referencing57);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement42, "commentRef", commentRef56);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension61 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension62 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any63 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension62, "any", any63);
		extension62.setVendor("aaaaaaa");

		extension61.add(extension62);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement42, "extension", extension61);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID64 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID65 = new org.inspection_plusplus.DisplayID();
		displayID65.setLanguage("");
		displayID65.setText("aaaaa");

		displayID64.add(displayID65);
		org.inspection_plusplus.DisplayID displayID66 = new org.inspection_plusplus.DisplayID();
		displayID66.setLanguage("aaaaaaaaa");
		displayID66.setText("");

		displayID64.add(displayID66);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement42, "displayID", displayID64);
		java.util.ArrayList<org.inspection_plusplus.History> history67 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history68 = new org.inspection_plusplus.History();
		history68.setAction("");
		history68.setAuthor("");
		history68.setComment("aaa");
		history68.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 23, 11)));

		history67.add(history68);
		org.inspection_plusplus.History history69 = new org.inspection_plusplus.History();
		history69.setAction("aaaa");
		history69.setAuthor("");
		history69.setComment("");
		history69.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 39, 44)));

		history67.add(history69);
		org.inspection_plusplus.History history70 = new org.inspection_plusplus.History();
		history70.setAction("");
		history70.setAuthor("");
		history70.setComment("");
		history70.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 31, 14)));

		history67.add(history70);
		org.inspection_plusplus.History history71 = new org.inspection_plusplus.History();
		history71.setAction("");
		history71.setAuthor("");
		history71.setComment("a");
		history71.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 3, 2, 8)));

		history67.add(history71);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement42, "history", history67);

		calculationNominalElement29.add(calculationNominalElement42);
		eu.sarunas.junit.TestsHelper.set(iPEAngle24, "calculationNominalElement", calculationNominalElement29);
		iPEAngle24.setDescription("aaaaaaaaaaaaa");
		iPEAngle24.setFunction("");
		iPEAngle24.setName("aa");
		org.inspection_plusplus.ID4Objects iD4Objects72 = new org.inspection_plusplus.ID4Objects();
		iD4Objects72.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension73 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects72, "extension", extension73);

		iPEAngle24.setSystemID(iD4Objects72);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef74 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(iPEAngle24, "commentRef", commentRef74);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension75 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension76 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any77 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension76, "any", any77);
		extension76.setVendor("a");

		extension75.add(extension76);
		eu.sarunas.junit.TestsHelper.set(iPEAngle24, "extension", extension75);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID78 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID79 = new org.inspection_plusplus.DisplayID();
		displayID79.setLanguage("aaaaaaaaaaaaaaaaaaaaaaaa");
		displayID79.setText("aaaaaaaaaaaaaaa");

		displayID78.add(displayID79);
		org.inspection_plusplus.DisplayID displayID80 = new org.inspection_plusplus.DisplayID();
		displayID80.setLanguage("");
		displayID80.setText("");

		displayID78.add(displayID80);
		eu.sarunas.junit.TestsHelper.set(iPEAngle24, "displayID", displayID78);
		java.util.ArrayList<org.inspection_plusplus.History> history81 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(iPEAngle24, "history", history81);

		ipe23.add(iPEAngle24);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan0, "ipe", ipe23);
		java.util.ArrayList<org.inspection_plusplus.ReferenceSystem> referenceSystem82 = new java.util.ArrayList<org.inspection_plusplus.ReferenceSystem>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan0, "referenceSystem", referenceSystem82);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> inspectionTaskRef83 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing84 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing84.setClassID("");
		iD4Referencing84.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension85 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension86 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any87 = new java.util.ArrayList<Object>();
		any87.add(new Object());
		any87.add(new Object());
		any87.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension86, "any", any87);
		extension86.setVendor("");

		extension85.add(extension86);
		org.inspection_plusplus.Extension extension88 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any89 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension88, "any", any89);
		extension88.setVendor("");

		extension85.add(extension88);
		org.inspection_plusplus.Extension extension90 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any91 = new java.util.ArrayList<Object>();
		any91.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension90, "any", any91);
		extension90.setVendor("aaaaaaaa");

		extension85.add(extension90);
		org.inspection_plusplus.Extension extension92 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any93 = new java.util.ArrayList<Object>();
		any93.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension92, "any", any93);
		extension92.setVendor("aaaaaaaaa");

		extension85.add(extension92);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing84, "extension", extension85);

		inspectionTaskRef83.add(iD4Referencing84);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan0, "inspectionTaskRef", inspectionTaskRef83);
		java.util.ArrayList<org.inspection_plusplus.QC> qc94 = new java.util.ArrayList<org.inspection_plusplus.QC>();
		org.inspection_plusplus.QCSingle qCSingle95 = new org.inspection_plusplus.QCSingle();
		qCSingle95.setGeoObjectDetail("");
		org.inspection_plusplus.ID4Referencing iD4Referencing96 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing96.setClassID("");
		iD4Referencing96.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension97 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing96, "extension", extension97);

		qCSingle95.setToleranceRef(iD4Referencing96);
		qCSingle95.setAllAssembliesReferenced(true);
		qCSingle95.setDescription("aa");
		qCSingle95.setFunction("aaaa");
		qCSingle95.setPartQAversion("");
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> partRef98 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(qCSingle95, "partRef", partRef98);
		java.util.ArrayList<Object> calculationActualElementDetectionRef99 = new java.util.ArrayList<Object>();
		calculationActualElementDetectionRef99.add(new Object());
		calculationActualElementDetectionRef99.add(new Object());
		eu.sarunas.junit.TestsHelper.set(qCSingle95, "calculationActualElementDetectionRef", calculationActualElementDetectionRef99);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeRef100 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing101 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing101.setClassID("");
		iD4Referencing101.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension102 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension103 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any104 = new java.util.ArrayList<Object>();
		any104.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension103, "any", any104);
		extension103.setVendor("aaa");

		extension102.add(extension103);
		org.inspection_plusplus.Extension extension105 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any106 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension105, "any", any106);
		extension105.setVendor("");

		extension102.add(extension105);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing101, "extension", extension102);

		ipeRef100.add(iD4Referencing101);
		eu.sarunas.junit.TestsHelper.set(qCSingle95, "ipeRef", ipeRef100);
		qCSingle95.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects107 = new org.inspection_plusplus.ID4Objects();
		iD4Objects107.setUuid("aaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension108 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension109 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any110 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension109, "any", any110);
		extension109.setVendor("");

		extension108.add(extension109);
		eu.sarunas.junit.TestsHelper.set(iD4Objects107, "extension", extension108);

		qCSingle95.setSystemID(iD4Objects107);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef111 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(qCSingle95, "commentRef", commentRef111);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension112 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(qCSingle95, "extension", extension112);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID113 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(qCSingle95, "displayID", displayID113);
		java.util.ArrayList<org.inspection_plusplus.History> history114 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history115 = new org.inspection_plusplus.History();
		history115.setAction("");
		history115.setAuthor("aa");
		history115.setComment("");
		history115.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 20, 28)));

		history114.add(history115);
		org.inspection_plusplus.History history116 = new org.inspection_plusplus.History();
		history116.setAction("aaaaaaaaaaaaaaaaaaaaa");
		history116.setAuthor("aaaaaaaa");
		history116.setComment("");
		history116.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 48, 34)));

		history114.add(history116);
		org.inspection_plusplus.History history117 = new org.inspection_plusplus.History();
		history117.setAction("aaaaaaaaa");
		history117.setAuthor("");
		history117.setComment("aaaaaaaaa");
		history117.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 10, 41)));

		history114.add(history117);
		eu.sarunas.junit.TestsHelper.set(qCSingle95, "history", history114);

		qc94.add(qCSingle95);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan0, "qc", qc94);
		inspectionPlan0.setName("aaaaaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects118 = new org.inspection_plusplus.ID4Objects();
		iD4Objects118.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension119 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension120 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any121 = new java.util.ArrayList<Object>();
		any121.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension120, "any", any121);
		extension120.setVendor("aaaaaa");

		extension119.add(extension120);
		org.inspection_plusplus.Extension extension122 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any123 = new java.util.ArrayList<Object>();
		any123.add(new Object());
		any123.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension122, "any", any123);
		extension122.setVendor("aaaaaaaa");

		extension119.add(extension122);
		eu.sarunas.junit.TestsHelper.set(iD4Objects118, "extension", extension119);

		inspectionPlan0.setSystemID(iD4Objects118);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef124 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing125 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing125.setClassID("");
		iD4Referencing125.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension126 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension127 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any128 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension127, "any", any128);
		extension127.setVendor("aaaaa");

		extension126.add(extension127);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing125, "extension", extension126);

		commentRef124.add(iD4Referencing125);
		org.inspection_plusplus.ID4Referencing iD4Referencing129 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing129.setClassID("");
		iD4Referencing129.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension130 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing129, "extension", extension130);

		commentRef124.add(iD4Referencing129);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan0, "commentRef", commentRef124);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension131 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan0, "extension", extension131);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID132 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID133 = new org.inspection_plusplus.DisplayID();
		displayID133.setLanguage("");
		displayID133.setText("");

		displayID132.add(displayID133);
		org.inspection_plusplus.DisplayID displayID134 = new org.inspection_plusplus.DisplayID();
		displayID134.setLanguage("");
		displayID134.setText("");

		displayID132.add(displayID134);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan0, "displayID", displayID132);
		java.util.ArrayList<org.inspection_plusplus.History> history135 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history136 = new org.inspection_plusplus.History();
		history136.setAction("");
		history136.setAuthor("");
		history136.setComment("aaaaaaaaaaaaaaaaa");
		history136.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 52, 18)));

		history135.add(history136);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan0, "history", history135);
		org.inspection_plusplus.Sender sender137 = new org.inspection_plusplus.Sender();
		sender137.setAppID("");
		sender137.setDomain("aaaaaa");
		org.inspection_plusplus.Extension extension138 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any139 = new java.util.ArrayList<Object>();
		any139.add(new Object());
		any139.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension138, "any", any139);
		extension138.setVendor("aa");

		org.inspection_plusplus.ReturnStatus res = testObject.storeInspectionPlan(inspectionPlan0, "", "", sender137, extension138);
		junit.framework.Assert.fail();
	};

	@org.junit.Test
	public void testtestStoreInspectionPlan2() throws Throwable
	{
		org.inspection_plusplus.service.v2.WebServiceImpl testObject = new org.inspection_plusplus.service.v2.WebServiceImpl();
		org.inspection_plusplus.InspectionPlan inspectionPlan140 = new org.inspection_plusplus.InspectionPlan();
		inspectionPlan140.setInspectionCategories("aaaaaaaaaaaaaaa");
		inspectionPlan140.setIPsymmetry("");
		inspectionPlan140.setPartQAversion("");
		eu.sarunas.junit.TestsHelper.set(inspectionPlan140, "pdMmaturity", "");
		inspectionPlan140.setQAmaturity("");
		inspectionPlan140.setQAversion("");
		java.util.ArrayList<org.inspection_plusplus.Comment> comment141 = new java.util.ArrayList<org.inspection_plusplus.Comment>();
		org.inspection_plusplus.Comment comment142 = new org.inspection_plusplus.Comment();
		comment142.setAuthor("aaaaaaaaaaa");
		comment142.setReceiver("aaaaaaaaaaaaaaaaaa");
		comment142.setSubject("aaaaa");
		comment142.setText("");
		comment142.setName("aaaaaaaaaaaaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects143 = new org.inspection_plusplus.ID4Objects();
		iD4Objects143.setUuid("aaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension144 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects143, "extension", extension144);

		comment142.setSystemID(iD4Objects143);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef145 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(comment142, "commentRef", commentRef145);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension146 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension147 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any148 = new java.util.ArrayList<Object>();
		any148.add(new Object());
		any148.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension147, "any", any148);
		extension147.setVendor("");

		extension146.add(extension147);
		eu.sarunas.junit.TestsHelper.set(comment142, "extension", extension146);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID149 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID150 = new org.inspection_plusplus.DisplayID();
		displayID150.setLanguage("");
		displayID150.setText("");

		displayID149.add(displayID150);
		org.inspection_plusplus.DisplayID displayID151 = new org.inspection_plusplus.DisplayID();
		displayID151.setLanguage("aa");
		displayID151.setText("aaaaaaaaaaaaaaaa");

		displayID149.add(displayID151);
		eu.sarunas.junit.TestsHelper.set(comment142, "displayID", displayID149);
		java.util.ArrayList<org.inspection_plusplus.History> history152 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(comment142, "history", history152);

		comment141.add(comment142);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan140, "comment", comment141);
		java.util.ArrayList<org.inspection_plusplus.Tolerance> tolerance153 = new java.util.ArrayList<org.inspection_plusplus.Tolerance>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan140, "tolerance", tolerance153);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> partRef154 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing155 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing155.setClassID("aaaaaaaaa");
		iD4Referencing155.setUuid("aaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension156 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension157 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any158 = new java.util.ArrayList<Object>();
		any158.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension157, "any", any158);
		extension157.setVendor("");

		extension156.add(extension157);
		org.inspection_plusplus.Extension extension159 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any160 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension159, "any", any160);
		extension159.setVendor("aa");

		extension156.add(extension159);
		org.inspection_plusplus.Extension extension161 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any162 = new java.util.ArrayList<Object>();
		any162.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension161, "any", any162);
		extension161.setVendor("");

		extension156.add(extension161);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing155, "extension", extension156);

		partRef154.add(iD4Referencing155);
		org.inspection_plusplus.ID4Referencing iD4Referencing163 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing163.setClassID("");
		iD4Referencing163.setUuid("aaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension164 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing163, "extension", extension164);

		partRef154.add(iD4Referencing163);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan140, "partRef", partRef154);
		java.util.ArrayList<org.inspection_plusplus.IPE> ipe165 = new java.util.ArrayList<org.inspection_plusplus.IPE>();
		org.inspection_plusplus.IPESurfacePoint iPESurfacePoint166 = new org.inspection_plusplus.IPESurfacePoint();
		org.inspection_plusplus.Vector3D vector3D167 = new org.inspection_plusplus.Vector3D();
		vector3D167.setX(-45.18);
		vector3D167.setY(-221.20);
		vector3D167.setZ(-257.38);

		iPESurfacePoint166.setMainAxis(vector3D167);
		iPESurfacePoint166.setMaterialThickness(-419.60);
		iPESurfacePoint166.setMoveByMaterialThickness(true);
		iPESurfacePoint166.setOrientation(null);
		iPESurfacePoint166.setOrigin(null);
		iPESurfacePoint166.setTouchDirection(null);
		iPESurfacePoint166.setAuxiliaryElement(false);
		iPESurfacePoint166.setCalculatedElement(false);
		iPESurfacePoint166.setUseLeft(false);
		iPESurfacePoint166.setUseRight(false);
		org.inspection_plusplus.ID4Referencing iD4Referencing168 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing168.setClassID("aaa");
		iD4Referencing168.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension169 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension170 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any171 = new java.util.ArrayList<Object>();
		any171.add(new Object());
		any171.add(new Object());
		any171.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension170, "any", any171);
		extension170.setVendor("");

		extension169.add(extension170);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing168, "extension", extension169);

		iPESurfacePoint166.setGeometricObjectRef(iD4Referencing168);
		java.util.ArrayList<org.inspection_plusplus.CalculationNominalElement> calculationNominalElement172 = new java.util.ArrayList<org.inspection_plusplus.CalculationNominalElement>();
		org.inspection_plusplus.CalculationNominalElement calculationNominalElement173 = new org.inspection_plusplus.CalculationNominalElement();
		calculationNominalElement173.setOperation("");
		java.util.ArrayList<org.inspection_plusplus.OperationParameter> operationParameter174 = new java.util.ArrayList<org.inspection_plusplus.OperationParameter>();
		org.inspection_plusplus.OperationParameter operationParameter175 = new org.inspection_plusplus.OperationParameter();
		org.inspection_plusplus.KeyValueOperatorList keyValueOperatorList176 = new org.inspection_plusplus.KeyValueOperatorList();
		java.util.ArrayList<org.inspection_plusplus.KeyValueOperator> keyValueOperator177 = new java.util.ArrayList<org.inspection_plusplus.KeyValueOperator>();
		org.inspection_plusplus.KeyValueOperator keyValueOperator178 = new org.inspection_plusplus.KeyValueOperator();
		keyValueOperator178.setKey("aa");
		keyValueOperator178.setOperator("");
		keyValueOperator178.setValue("aaaaaaaa");

		keyValueOperator177.add(keyValueOperator178);
		org.inspection_plusplus.KeyValueOperator keyValueOperator179 = new org.inspection_plusplus.KeyValueOperator();
		keyValueOperator179.setKey("aaa");
		keyValueOperator179.setOperator("a");
		keyValueOperator179.setValue("aaaaaaaaaa");

		keyValueOperator177.add(keyValueOperator179);
		org.inspection_plusplus.KeyValueOperator keyValueOperator180 = new org.inspection_plusplus.KeyValueOperator();
		keyValueOperator180.setKey("aaaaaaa");
		keyValueOperator180.setOperator("");
		keyValueOperator180.setValue("a");

		keyValueOperator177.add(keyValueOperator180);
		eu.sarunas.junit.TestsHelper.set(keyValueOperatorList176, "keyValueOperator", keyValueOperator177);

		operationParameter175.setKeyValuePairs(keyValueOperatorList176);

		operationParameter174.add(operationParameter175);
		org.inspection_plusplus.OperationParameter operationParameter181 = new org.inspection_plusplus.OperationParameter();
		org.inspection_plusplus.KeyValueOperatorList keyValueOperatorList182 = new org.inspection_plusplus.KeyValueOperatorList();
		java.util.ArrayList<org.inspection_plusplus.KeyValueOperator> keyValueOperator183 = new java.util.ArrayList<org.inspection_plusplus.KeyValueOperator>();
		eu.sarunas.junit.TestsHelper.set(keyValueOperatorList182, "keyValueOperator", keyValueOperator183);

		operationParameter181.setKeyValuePairs(keyValueOperatorList182);

		operationParameter174.add(operationParameter181);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement173, "operationParameter", operationParameter174);
		java.util.ArrayList<org.inspection_plusplus.Operand> operand184 = new java.util.ArrayList<org.inspection_plusplus.Operand>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement173, "operand", operand184);
		calculationNominalElement173.setName("a");
		org.inspection_plusplus.ID4Objects iD4Objects185 = new org.inspection_plusplus.ID4Objects();
		iD4Objects185.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension186 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects185, "extension", extension186);

		calculationNominalElement173.setSystemID(iD4Objects185);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef187 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing188 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing188.setClassID("");
		iD4Referencing188.setUuid("aaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension189 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension190 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any191 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension190, "any", any191);
		extension190.setVendor("");

		extension189.add(extension190);
		org.inspection_plusplus.Extension extension192 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any193 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension192, "any", any193);
		extension192.setVendor("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");

		extension189.add(extension192);
		org.inspection_plusplus.Extension extension194 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any195 = new java.util.ArrayList<Object>();
		any195.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension194, "any", any195);
		extension194.setVendor("aaaaa");

		extension189.add(extension194);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing188, "extension", extension189);

		commentRef187.add(iD4Referencing188);
		org.inspection_plusplus.ID4Referencing iD4Referencing196 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing196.setClassID("aaaaaaaaaaaaaaaaaaaaaaa");
		iD4Referencing196.setUuid("aaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension197 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing196, "extension", extension197);

		commentRef187.add(iD4Referencing196);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement173, "commentRef", commentRef187);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension198 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension199 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any200 = new java.util.ArrayList<Object>();
		any200.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension199, "any", any200);
		extension199.setVendor("aaaaaaaaaaaaaaaaaaaaaaa");

		extension198.add(extension199);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement173, "extension", extension198);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID201 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID202 = new org.inspection_plusplus.DisplayID();
		displayID202.setLanguage("aa");
		displayID202.setText("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");

		displayID201.add(displayID202);
		org.inspection_plusplus.DisplayID displayID203 = new org.inspection_plusplus.DisplayID();
		displayID203.setLanguage("");
		displayID203.setText("aaa");

		displayID201.add(displayID203);
		org.inspection_plusplus.DisplayID displayID204 = new org.inspection_plusplus.DisplayID();
		displayID204.setLanguage("");
		displayID204.setText("aaaaaaaaaaa");

		displayID201.add(displayID204);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement173, "displayID", displayID201);
		java.util.ArrayList<org.inspection_plusplus.History> history205 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history206 = new org.inspection_plusplus.History();
		history206.setAction("");
		history206.setAuthor("");
		history206.setComment("");
		history206.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 33, 58)));

		history205.add(history206);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement173, "history", history205);

		calculationNominalElement172.add(calculationNominalElement173);
		org.inspection_plusplus.CalculationNominalElement calculationNominalElement207 = new org.inspection_plusplus.CalculationNominalElement();
		calculationNominalElement207.setOperation("aaaaa");
		java.util.ArrayList<org.inspection_plusplus.OperationParameter> operationParameter208 = new java.util.ArrayList<org.inspection_plusplus.OperationParameter>();
		org.inspection_plusplus.OperationParameter operationParameter209 = new org.inspection_plusplus.OperationParameter();
		org.inspection_plusplus.KeyValueOperatorList keyValueOperatorList210 = new org.inspection_plusplus.KeyValueOperatorList();
		java.util.ArrayList<org.inspection_plusplus.KeyValueOperator> keyValueOperator211 = new java.util.ArrayList<org.inspection_plusplus.KeyValueOperator>();
		eu.sarunas.junit.TestsHelper.set(keyValueOperatorList210, "keyValueOperator", keyValueOperator211);

		operationParameter209.setKeyValuePairs(keyValueOperatorList210);

		operationParameter208.add(operationParameter209);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement207, "operationParameter", operationParameter208);
		java.util.ArrayList<org.inspection_plusplus.Operand> operand212 = new java.util.ArrayList<org.inspection_plusplus.Operand>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement207, "operand", operand212);
		calculationNominalElement207.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects213 = new org.inspection_plusplus.ID4Objects();
		iD4Objects213.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension214 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension215 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any216 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension215, "any", any216);
		extension215.setVendor("aaaa");

		extension214.add(extension215);
		org.inspection_plusplus.Extension extension217 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any218 = new java.util.ArrayList<Object>();
		any218.add(new Object());
		any218.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension217, "any", any218);
		extension217.setVendor("");

		extension214.add(extension217);
		eu.sarunas.junit.TestsHelper.set(iD4Objects213, "extension", extension214);

		calculationNominalElement207.setSystemID(iD4Objects213);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef219 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing220 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing220.setClassID("");
		iD4Referencing220.setUuid("aaaaaaaaaaaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension221 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension222 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any223 = new java.util.ArrayList<Object>();
		any223.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension222, "any", any223);
		extension222.setVendor("");

		extension221.add(extension222);
		org.inspection_plusplus.Extension extension224 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any225 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension224, "any", any225);
		extension224.setVendor("");

		extension221.add(extension224);
		org.inspection_plusplus.Extension extension226 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any227 = new java.util.ArrayList<Object>();
		any227.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension226, "any", any227);
		extension226.setVendor("aaaa");

		extension221.add(extension226);
		org.inspection_plusplus.Extension extension228 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any229 = new java.util.ArrayList<Object>();
		any229.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension228, "any", any229);
		extension228.setVendor("");

		extension221.add(extension228);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing220, "extension", extension221);

		commentRef219.add(iD4Referencing220);
		org.inspection_plusplus.ID4Referencing iD4Referencing230 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing230.setClassID("");
		iD4Referencing230.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension231 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension232 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any233 = new java.util.ArrayList<Object>();
		any233.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension232, "any", any233);
		extension232.setVendor("");

		extension231.add(extension232);
		org.inspection_plusplus.Extension extension234 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any235 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension234, "any", any235);
		extension234.setVendor("aa");

		extension231.add(extension234);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing230, "extension", extension231);

		commentRef219.add(iD4Referencing230);
		org.inspection_plusplus.ID4Referencing iD4Referencing236 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing236.setClassID("");
		iD4Referencing236.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension237 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension238 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any239 = new java.util.ArrayList<Object>();
		any239.add(new Object());
		any239.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension238, "any", any239);
		extension238.setVendor("");

		extension237.add(extension238);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing236, "extension", extension237);

		commentRef219.add(iD4Referencing236);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement207, "commentRef", commentRef219);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension240 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement207, "extension", extension240);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID241 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID242 = new org.inspection_plusplus.DisplayID();
		displayID242.setLanguage("aaaaaaaaa");
		displayID242.setText("");

		displayID241.add(displayID242);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement207, "displayID", displayID241);
		java.util.ArrayList<org.inspection_plusplus.History> history243 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history244 = new org.inspection_plusplus.History();
		history244.setAction("aaaaaaa");
		history244.setAuthor("aaaaaaaaaaa");
		history244.setComment("aaaaaaaaaaa");
		history244.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 6, 35)));

		history243.add(history244);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement207, "history", history243);

		calculationNominalElement172.add(calculationNominalElement207);
		org.inspection_plusplus.CalculationNominalElement calculationNominalElement245 = new org.inspection_plusplus.CalculationNominalElement();
		calculationNominalElement245.setOperation("");
		java.util.ArrayList<org.inspection_plusplus.OperationParameter> operationParameter246 = new java.util.ArrayList<org.inspection_plusplus.OperationParameter>();
		org.inspection_plusplus.OperationParameter operationParameter247 = new org.inspection_plusplus.OperationParameter();
		org.inspection_plusplus.KeyValueOperatorList keyValueOperatorList248 = new org.inspection_plusplus.KeyValueOperatorList();
		java.util.ArrayList<org.inspection_plusplus.KeyValueOperator> keyValueOperator249 = new java.util.ArrayList<org.inspection_plusplus.KeyValueOperator>();
		org.inspection_plusplus.KeyValueOperator keyValueOperator250 = new org.inspection_plusplus.KeyValueOperator();
		keyValueOperator250.setKey("");
		keyValueOperator250.setOperator("aaaaaaaa");
		keyValueOperator250.setValue("");

		keyValueOperator249.add(keyValueOperator250);
		org.inspection_plusplus.KeyValueOperator keyValueOperator251 = new org.inspection_plusplus.KeyValueOperator();
		keyValueOperator251.setKey("");
		keyValueOperator251.setOperator("aa");
		keyValueOperator251.setValue("");

		keyValueOperator249.add(keyValueOperator251);
		org.inspection_plusplus.KeyValueOperator keyValueOperator252 = new org.inspection_plusplus.KeyValueOperator();
		keyValueOperator252.setKey("");
		keyValueOperator252.setOperator("");
		keyValueOperator252.setValue("");

		keyValueOperator249.add(keyValueOperator252);
		org.inspection_plusplus.KeyValueOperator keyValueOperator253 = new org.inspection_plusplus.KeyValueOperator();
		keyValueOperator253.setKey("");
		keyValueOperator253.setOperator("aaaaa");
		keyValueOperator253.setValue("aaaaaaaaaa");

		keyValueOperator249.add(keyValueOperator253);
		eu.sarunas.junit.TestsHelper.set(keyValueOperatorList248, "keyValueOperator", keyValueOperator249);

		operationParameter247.setKeyValuePairs(keyValueOperatorList248);

		operationParameter246.add(operationParameter247);
		org.inspection_plusplus.OperationParameter operationParameter254 = new org.inspection_plusplus.OperationParameter();
		org.inspection_plusplus.KeyValueOperatorList keyValueOperatorList255 = new org.inspection_plusplus.KeyValueOperatorList();
		java.util.ArrayList<org.inspection_plusplus.KeyValueOperator> keyValueOperator256 = new java.util.ArrayList<org.inspection_plusplus.KeyValueOperator>();
		org.inspection_plusplus.KeyValueOperator keyValueOperator257 = new org.inspection_plusplus.KeyValueOperator();
		keyValueOperator257.setKey("");
		keyValueOperator257.setOperator("aaaaaaaa");
		keyValueOperator257.setValue("aaaaaaaa");

		keyValueOperator256.add(keyValueOperator257);
		eu.sarunas.junit.TestsHelper.set(keyValueOperatorList255, "keyValueOperator", keyValueOperator256);

		operationParameter254.setKeyValuePairs(keyValueOperatorList255);

		operationParameter246.add(operationParameter254);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement245, "operationParameter", operationParameter246);
		java.util.ArrayList<org.inspection_plusplus.Operand> operand258 = new java.util.ArrayList<org.inspection_plusplus.Operand>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement245, "operand", operand258);
		calculationNominalElement245.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects259 = new org.inspection_plusplus.ID4Objects();
		iD4Objects259.setUuid("aaaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension260 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension261 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any262 = new java.util.ArrayList<Object>();
		any262.add(new Object());
		any262.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension261, "any", any262);
		extension261.setVendor("");

		extension260.add(extension261);
		org.inspection_plusplus.Extension extension263 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any264 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension263, "any", any264);
		extension263.setVendor("");

		extension260.add(extension263);
		eu.sarunas.junit.TestsHelper.set(iD4Objects259, "extension", extension260);

		calculationNominalElement245.setSystemID(iD4Objects259);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef265 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing266 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing266.setClassID("aa");
		iD4Referencing266.setUuid("aaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension267 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing266, "extension", extension267);

		commentRef265.add(iD4Referencing266);
		org.inspection_plusplus.ID4Referencing iD4Referencing268 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing268.setClassID("aaaaa");
		iD4Referencing268.setUuid("aa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension269 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing268, "extension", extension269);

		commentRef265.add(iD4Referencing268);
		org.inspection_plusplus.ID4Referencing iD4Referencing270 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing270.setClassID("aa");
		iD4Referencing270.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension271 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension272 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any273 = new java.util.ArrayList<Object>();
		any273.add(new Object());
		any273.add(new Object());
		any273.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension272, "any", any273);
		extension272.setVendor("a");

		extension271.add(extension272);
		org.inspection_plusplus.Extension extension274 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any275 = new java.util.ArrayList<Object>();
		any275.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension274, "any", any275);
		extension274.setVendor("");

		extension271.add(extension274);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing270, "extension", extension271);

		commentRef265.add(iD4Referencing270);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement245, "commentRef", commentRef265);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension276 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension277 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any278 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension277, "any", any278);
		extension277.setVendor("");

		extension276.add(extension277);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement245, "extension", extension276);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID279 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID280 = new org.inspection_plusplus.DisplayID();
		displayID280.setLanguage("");
		displayID280.setText("");

		displayID279.add(displayID280);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement245, "displayID", displayID279);
		java.util.ArrayList<org.inspection_plusplus.History> history281 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement245, "history", history281);

		calculationNominalElement172.add(calculationNominalElement245);
		org.inspection_plusplus.CalculationNominalElement calculationNominalElement282 = new org.inspection_plusplus.CalculationNominalElement();
		calculationNominalElement282.setOperation("");
		java.util.ArrayList<org.inspection_plusplus.OperationParameter> operationParameter283 = new java.util.ArrayList<org.inspection_plusplus.OperationParameter>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement282, "operationParameter", operationParameter283);
		java.util.ArrayList<org.inspection_plusplus.Operand> operand284 = new java.util.ArrayList<org.inspection_plusplus.Operand>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement282, "operand", operand284);
		calculationNominalElement282.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects285 = new org.inspection_plusplus.ID4Objects();
		iD4Objects285.setUuid("aa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension286 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension287 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any288 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension287, "any", any288);
		extension287.setVendor("");

		extension286.add(extension287);
		org.inspection_plusplus.Extension extension289 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any290 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension289, "any", any290);
		extension289.setVendor("aaaaaaa");

		extension286.add(extension289);
		org.inspection_plusplus.Extension extension291 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any292 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension291, "any", any292);
		extension291.setVendor("");

		extension286.add(extension291);
		eu.sarunas.junit.TestsHelper.set(iD4Objects285, "extension", extension286);

		calculationNominalElement282.setSystemID(iD4Objects285);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef293 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement282, "commentRef", commentRef293);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension294 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension295 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any296 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension295, "any", any296);
		extension295.setVendor("");

		extension294.add(extension295);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement282, "extension", extension294);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID297 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement282, "displayID", displayID297);
		java.util.ArrayList<org.inspection_plusplus.History> history298 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history299 = new org.inspection_plusplus.History();
		history299.setAction("aaaaa");
		history299.setAuthor("");
		history299.setComment("");
		history299.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 0, 53, 52)));

		history298.add(history299);
		org.inspection_plusplus.History history300 = new org.inspection_plusplus.History();
		history300.setAction("");
		history300.setAuthor("aaaaaaaaaa");
		history300.setComment("");
		history300.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 17, 48)));

		history298.add(history300);
		org.inspection_plusplus.History history301 = new org.inspection_plusplus.History();
		history301.setAction("aaaaaaaaaaaaaa");
		history301.setAuthor("");
		history301.setComment("");
		history301.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 11, 7)));

		history298.add(history301);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement282, "history", history298);

		calculationNominalElement172.add(calculationNominalElement282);
		eu.sarunas.junit.TestsHelper.set(iPESurfacePoint166, "calculationNominalElement", calculationNominalElement172);
		iPESurfacePoint166.setDescription("");
		iPESurfacePoint166.setFunction("");
		iPESurfacePoint166.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects302 = new org.inspection_plusplus.ID4Objects();
		iD4Objects302.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension303 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension304 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any305 = new java.util.ArrayList<Object>();
		any305.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension304, "any", any305);
		extension304.setVendor("");

		extension303.add(extension304);
		org.inspection_plusplus.Extension extension306 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any307 = new java.util.ArrayList<Object>();
		any307.add(new Object());
		any307.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension306, "any", any307);
		extension306.setVendor("");

		extension303.add(extension306);
		org.inspection_plusplus.Extension extension308 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any309 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension308, "any", any309);
		extension308.setVendor("aaaaaaaaaaaaaa");

		extension303.add(extension308);
		eu.sarunas.junit.TestsHelper.set(iD4Objects302, "extension", extension303);

		iPESurfacePoint166.setSystemID(iD4Objects302);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef310 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(iPESurfacePoint166, "commentRef", commentRef310);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension311 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension312 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any313 = new java.util.ArrayList<Object>();
		any313.add(new Object());
		any313.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension312, "any", any313);
		extension312.setVendor("");

		extension311.add(extension312);
		org.inspection_plusplus.Extension extension314 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any315 = new java.util.ArrayList<Object>();
		any315.add(new Object());
		any315.add(new Object());
		any315.add(new Object());
		any315.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension314, "any", any315);
		extension314.setVendor("");

		extension311.add(extension314);
		org.inspection_plusplus.Extension extension316 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any317 = new java.util.ArrayList<Object>();
		any317.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension316, "any", any317);
		extension316.setVendor("");

		extension311.add(extension316);
		org.inspection_plusplus.Extension extension318 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any319 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension318, "any", any319);
		extension318.setVendor("");

		extension311.add(extension318);
		eu.sarunas.junit.TestsHelper.set(iPESurfacePoint166, "extension", extension311);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID320 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID321 = new org.inspection_plusplus.DisplayID();
		displayID321.setLanguage("");
		displayID321.setText("");

		displayID320.add(displayID321);
		org.inspection_plusplus.DisplayID displayID322 = new org.inspection_plusplus.DisplayID();
		displayID322.setLanguage("");
		displayID322.setText("aaaaaaaaaaa");

		displayID320.add(displayID322);
		org.inspection_plusplus.DisplayID displayID323 = new org.inspection_plusplus.DisplayID();
		displayID323.setLanguage("");
		displayID323.setText("");

		displayID320.add(displayID323);
		eu.sarunas.junit.TestsHelper.set(iPESurfacePoint166, "displayID", displayID320);
		java.util.ArrayList<org.inspection_plusplus.History> history324 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(iPESurfacePoint166, "history", history324);

		ipe165.add(iPESurfacePoint166);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan140, "ipe", ipe165);
		java.util.ArrayList<org.inspection_plusplus.ReferenceSystem> referenceSystem325 = new java.util.ArrayList<org.inspection_plusplus.ReferenceSystem>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan140, "referenceSystem", referenceSystem325);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> inspectionTaskRef326 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing327 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing327.setClassID("");
		iD4Referencing327.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension328 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension329 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any330 = new java.util.ArrayList<Object>();
		any330.add(new Object());
		any330.add(new Object());
		any330.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension329, "any", any330);
		extension329.setVendor("aa");

		extension328.add(extension329);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing327, "extension", extension328);

		inspectionTaskRef326.add(iD4Referencing327);
		org.inspection_plusplus.ID4Referencing iD4Referencing331 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing331.setClassID("");
		iD4Referencing331.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension332 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension333 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any334 = new java.util.ArrayList<Object>();
		any334.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension333, "any", any334);
		extension333.setVendor("");

		extension332.add(extension333);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing331, "extension", extension332);

		inspectionTaskRef326.add(iD4Referencing331);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan140, "inspectionTaskRef", inspectionTaskRef326);
		java.util.ArrayList<org.inspection_plusplus.QC> qc335 = new java.util.ArrayList<org.inspection_plusplus.QC>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan140, "qc", qc335);
		inspectionPlan140.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects336 = new org.inspection_plusplus.ID4Objects();
		iD4Objects336.setUuid("aaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension337 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension338 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any339 = new java.util.ArrayList<Object>();
		any339.add(new Object());
		any339.add(new Object());
		any339.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension338, "any", any339);
		extension338.setVendor("");

		extension337.add(extension338);
		eu.sarunas.junit.TestsHelper.set(iD4Objects336, "extension", extension337);

		inspectionPlan140.setSystemID(iD4Objects336);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef340 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing341 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing341.setClassID("");
		iD4Referencing341.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension342 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension343 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any344 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension343, "any", any344);
		extension343.setVendor("");

		extension342.add(extension343);
		org.inspection_plusplus.Extension extension345 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any346 = new java.util.ArrayList<Object>();
		any346.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension345, "any", any346);
		extension345.setVendor("");

		extension342.add(extension345);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing341, "extension", extension342);

		commentRef340.add(iD4Referencing341);
		org.inspection_plusplus.ID4Referencing iD4Referencing347 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing347.setClassID("aaaaaaaaaaa");
		iD4Referencing347.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension348 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension349 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any350 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension349, "any", any350);
		extension349.setVendor("aaaa");

		extension348.add(extension349);
		org.inspection_plusplus.Extension extension351 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any352 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension351, "any", any352);
		extension351.setVendor("aaaa");

		extension348.add(extension351);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing347, "extension", extension348);

		commentRef340.add(iD4Referencing347);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan140, "commentRef", commentRef340);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension353 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension354 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any355 = new java.util.ArrayList<Object>();
		any355.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension354, "any", any355);
		extension354.setVendor("");

		extension353.add(extension354);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan140, "extension", extension353);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID356 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID357 = new org.inspection_plusplus.DisplayID();
		displayID357.setLanguage("");
		displayID357.setText("");

		displayID356.add(displayID357);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan140, "displayID", displayID356);
		java.util.ArrayList<org.inspection_plusplus.History> history358 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan140, "history", history358);
		org.inspection_plusplus.Sender sender359 = new org.inspection_plusplus.Sender();
		sender359.setAppID("aaaa");
		sender359.setDomain("aaaaaaaaaa");
		org.inspection_plusplus.Extension extension360 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any361 = new java.util.ArrayList<Object>();
		any361.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension360, "any", any361);
		extension360.setVendor("");

		org.inspection_plusplus.ReturnStatus res = testObject.storeInspectionPlan(inspectionPlan140, "", "aaa", sender359, extension360);
		junit.framework.Assert.fail();
	};

	@org.junit.Test
	public void testtestStoreInspectionPlan3() throws Throwable
	{
		org.inspection_plusplus.service.v2.WebServiceImpl testObject = new org.inspection_plusplus.service.v2.WebServiceImpl();
		org.inspection_plusplus.InspectionPlan inspectionPlan362 = new org.inspection_plusplus.InspectionPlan();
		inspectionPlan362.setInspectionCategories("aaa");
		inspectionPlan362.setIPsymmetry("aaaaaaaaaa");
		inspectionPlan362.setPartQAversion("");
		eu.sarunas.junit.TestsHelper.set(inspectionPlan362, "pdMmaturity", "");
		inspectionPlan362.setQAmaturity("");
		inspectionPlan362.setQAversion("");
		java.util.ArrayList<org.inspection_plusplus.Comment> comment363 = new java.util.ArrayList<org.inspection_plusplus.Comment>();
		org.inspection_plusplus.Comment comment364 = new org.inspection_plusplus.Comment();
		comment364.setAuthor("");
		comment364.setReceiver("aaaa");
		comment364.setSubject("");
		comment364.setText("");
		comment364.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects365 = new org.inspection_plusplus.ID4Objects();
		iD4Objects365.setUuid("aaaaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension366 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension367 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any368 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension367, "any", any368);
		extension367.setVendor("aaaaaaaaaa");

		extension366.add(extension367);
		eu.sarunas.junit.TestsHelper.set(iD4Objects365, "extension", extension366);

		comment364.setSystemID(iD4Objects365);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef369 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing370 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing370.setClassID("");
		iD4Referencing370.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension371 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension372 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any373 = new java.util.ArrayList<Object>();
		any373.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension372, "any", any373);
		extension372.setVendor("");

		extension371.add(extension372);
		org.inspection_plusplus.Extension extension374 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any375 = new java.util.ArrayList<Object>();
		any375.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension374, "any", any375);
		extension374.setVendor("aaaaaaaaa");

		extension371.add(extension374);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing370, "extension", extension371);

		commentRef369.add(iD4Referencing370);
		org.inspection_plusplus.ID4Referencing iD4Referencing376 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing376.setClassID("");
		iD4Referencing376.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension377 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension378 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any379 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension378, "any", any379);
		extension378.setVendor("");

		extension377.add(extension378);
		org.inspection_plusplus.Extension extension380 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any381 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension380, "any", any381);
		extension380.setVendor("");

		extension377.add(extension380);
		org.inspection_plusplus.Extension extension382 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any383 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension382, "any", any383);
		extension382.setVendor("");

		extension377.add(extension382);
		org.inspection_plusplus.Extension extension384 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any385 = new java.util.ArrayList<Object>();
		any385.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension384, "any", any385);
		extension384.setVendor("");

		extension377.add(extension384);
		org.inspection_plusplus.Extension extension386 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any387 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension386, "any", any387);
		extension386.setVendor("aaaaaaaaaaaaaaaa");

		extension377.add(extension386);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing376, "extension", extension377);

		commentRef369.add(iD4Referencing376);
		org.inspection_plusplus.ID4Referencing iD4Referencing388 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing388.setClassID("");
		iD4Referencing388.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension389 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension390 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any391 = new java.util.ArrayList<Object>();
		any391.add(new Object());
		any391.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension390, "any", any391);
		extension390.setVendor("");

		extension389.add(extension390);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing388, "extension", extension389);

		commentRef369.add(iD4Referencing388);
		eu.sarunas.junit.TestsHelper.set(comment364, "commentRef", commentRef369);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension392 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension393 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any394 = new java.util.ArrayList<Object>();
		any394.add(new Object());
		any394.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension393, "any", any394);
		extension393.setVendor("aaaaaaaaaaaaa");

		extension392.add(extension393);
		org.inspection_plusplus.Extension extension395 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any396 = new java.util.ArrayList<Object>();
		any396.add(new Object());
		any396.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension395, "any", any396);
		extension395.setVendor("a");

		extension392.add(extension395);
		eu.sarunas.junit.TestsHelper.set(comment364, "extension", extension392);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID397 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID398 = new org.inspection_plusplus.DisplayID();
		displayID398.setLanguage("aaa");
		displayID398.setText("");

		displayID397.add(displayID398);
		org.inspection_plusplus.DisplayID displayID399 = new org.inspection_plusplus.DisplayID();
		displayID399.setLanguage("");
		displayID399.setText("");

		displayID397.add(displayID399);
		org.inspection_plusplus.DisplayID displayID400 = new org.inspection_plusplus.DisplayID();
		displayID400.setLanguage("aaaaaaaaaaaaaaa");
		displayID400.setText("aaaa");

		displayID397.add(displayID400);
		eu.sarunas.junit.TestsHelper.set(comment364, "displayID", displayID397);
		java.util.ArrayList<org.inspection_plusplus.History> history401 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history402 = new org.inspection_plusplus.History();
		history402.setAction("a");
		history402.setAuthor("aaa");
		history402.setComment("");
		history402.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 42, 17)));

		history401.add(history402);
		org.inspection_plusplus.History history403 = new org.inspection_plusplus.History();
		history403.setAction("");
		history403.setAuthor("");
		history403.setComment("");
		history403.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 32, 54)));

		history401.add(history403);
		org.inspection_plusplus.History history404 = new org.inspection_plusplus.History();
		history404.setAction("aaa");
		history404.setAuthor("");
		history404.setComment("");
		history404.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 39, 59)));

		history401.add(history404);
		eu.sarunas.junit.TestsHelper.set(comment364, "history", history401);

		comment363.add(comment364);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan362, "comment", comment363);
		java.util.ArrayList<org.inspection_plusplus.Tolerance> tolerance405 = new java.util.ArrayList<org.inspection_plusplus.Tolerance>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan362, "tolerance", tolerance405);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> partRef406 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan362, "partRef", partRef406);
		java.util.ArrayList<org.inspection_plusplus.IPE> ipe407 = new java.util.ArrayList<org.inspection_plusplus.IPE>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan362, "ipe", ipe407);
		java.util.ArrayList<org.inspection_plusplus.ReferenceSystem> referenceSystem408 = new java.util.ArrayList<org.inspection_plusplus.ReferenceSystem>();
		org.inspection_plusplus.ReferenceSystem referenceSystem409 = new org.inspection_plusplus.ReferenceSystem();
		java.util.ArrayList<org.inspection_plusplus.Datum> datum410 = new java.util.ArrayList<org.inspection_plusplus.Datum>();
		org.inspection_plusplus.Datum datum411 = new org.inspection_plusplus.Datum();
		org.inspection_plusplus.Vector3D vector3D412 = new org.inspection_plusplus.Vector3D();
		vector3D412.setX(-190.74);
		vector3D412.setY(-180.69);
		vector3D412.setZ(145.40);

		datum411.setDisplacement(vector3D412);
		datum411.setRole("");
		java.util.ArrayList<org.inspection_plusplus.DatumTarget> datumTarget413 = new java.util.ArrayList<org.inspection_plusplus.DatumTarget>();
		org.inspection_plusplus.DatumTarget datumTarget414 = new org.inspection_plusplus.DatumTarget();
		datumTarget414.setRole("");
		org.inspection_plusplus.ID4Referencing iD4Referencing415 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing415.setClassID("");
		iD4Referencing415.setUuid("aaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension416 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing415, "extension", extension416);

		eu.sarunas.junit.TestsHelper.set(datumTarget414, "ipeGeometricRef", iD4Referencing415);
		datumTarget414.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects417 = new org.inspection_plusplus.ID4Objects();
		iD4Objects417.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension418 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension419 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any420 = new java.util.ArrayList<Object>();
		any420.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension419, "any", any420);
		extension419.setVendor("aaaaaaaaaaaaaaaaaa");

		extension418.add(extension419);
		eu.sarunas.junit.TestsHelper.set(iD4Objects417, "extension", extension418);

		datumTarget414.setSystemID(iD4Objects417);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef421 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing422 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing422.setClassID("");
		iD4Referencing422.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension423 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension424 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any425 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension424, "any", any425);
		extension424.setVendor("aaaa");

		extension423.add(extension424);
		org.inspection_plusplus.Extension extension426 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any427 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension426, "any", any427);
		extension426.setVendor("");

		extension423.add(extension426);
		org.inspection_plusplus.Extension extension428 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any429 = new java.util.ArrayList<Object>();
		any429.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension428, "any", any429);
		extension428.setVendor("");

		extension423.add(extension428);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing422, "extension", extension423);

		commentRef421.add(iD4Referencing422);
		eu.sarunas.junit.TestsHelper.set(datumTarget414, "commentRef", commentRef421);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension430 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(datumTarget414, "extension", extension430);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID431 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID432 = new org.inspection_plusplus.DisplayID();
		displayID432.setLanguage("");
		displayID432.setText("");

		displayID431.add(displayID432);
		eu.sarunas.junit.TestsHelper.set(datumTarget414, "displayID", displayID431);
		java.util.ArrayList<org.inspection_plusplus.History> history433 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history434 = new org.inspection_plusplus.History();
		history434.setAction("");
		history434.setAuthor("");
		history434.setComment("aaaaaa");
		history434.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 2, 28)));

		history433.add(history434);
		eu.sarunas.junit.TestsHelper.set(datumTarget414, "history", history433);

		datumTarget413.add(datumTarget414);
		org.inspection_plusplus.DatumTarget datumTarget435 = new org.inspection_plusplus.DatumTarget();
		datumTarget435.setRole("");
		org.inspection_plusplus.ID4Referencing iD4Referencing436 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing436.setClassID("");
		iD4Referencing436.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension437 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension438 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any439 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension438, "any", any439);
		extension438.setVendor("a");

		extension437.add(extension438);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing436, "extension", extension437);

		eu.sarunas.junit.TestsHelper.set(datumTarget435, "ipeGeometricRef", iD4Referencing436);
		datumTarget435.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects440 = new org.inspection_plusplus.ID4Objects();
		iD4Objects440.setUuid("aaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension441 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension442 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any443 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension442, "any", any443);
		extension442.setVendor("a");

		extension441.add(extension442);
		org.inspection_plusplus.Extension extension444 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any445 = new java.util.ArrayList<Object>();
		any445.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension444, "any", any445);
		extension444.setVendor("");

		extension441.add(extension444);
		eu.sarunas.junit.TestsHelper.set(iD4Objects440, "extension", extension441);

		datumTarget435.setSystemID(iD4Objects440);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef446 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing447 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing447.setClassID("");
		iD4Referencing447.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension448 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension449 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any450 = new java.util.ArrayList<Object>();
		any450.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension449, "any", any450);
		extension449.setVendor("aaaaaaa");

		extension448.add(extension449);
		org.inspection_plusplus.Extension extension451 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any452 = new java.util.ArrayList<Object>();
		any452.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension451, "any", any452);
		extension451.setVendor("");

		extension448.add(extension451);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing447, "extension", extension448);

		commentRef446.add(iD4Referencing447);
		org.inspection_plusplus.ID4Referencing iD4Referencing453 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing453.setClassID("aaaaaaaaaaa");
		iD4Referencing453.setUuid("aaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension454 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension455 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any456 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension455, "any", any456);
		extension455.setVendor("aaaaaaaaaaaaaaa");

		extension454.add(extension455);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing453, "extension", extension454);

		commentRef446.add(iD4Referencing453);
		eu.sarunas.junit.TestsHelper.set(datumTarget435, "commentRef", commentRef446);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension457 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(datumTarget435, "extension", extension457);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID458 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(datumTarget435, "displayID", displayID458);
		java.util.ArrayList<org.inspection_plusplus.History> history459 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(datumTarget435, "history", history459);

		datumTarget413.add(datumTarget435);
		eu.sarunas.junit.TestsHelper.set(datum411, "datumTarget", datumTarget413);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeGeometricRef460 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(datum411, "ipeGeometricRef", ipeGeometricRef460);
		datum411.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects461 = new org.inspection_plusplus.ID4Objects();
		iD4Objects461.setUuid("aaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension462 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects461, "extension", extension462);

		datum411.setSystemID(iD4Objects461);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef463 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing464 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing464.setClassID("");
		iD4Referencing464.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension465 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing464, "extension", extension465);

		commentRef463.add(iD4Referencing464);
		eu.sarunas.junit.TestsHelper.set(datum411, "commentRef", commentRef463);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension466 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(datum411, "extension", extension466);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID467 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(datum411, "displayID", displayID467);
		java.util.ArrayList<org.inspection_plusplus.History> history468 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history469 = new org.inspection_plusplus.History();
		history469.setAction("aa");
		history469.setAuthor("");
		history469.setComment("");
		history469.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 22, 57)));

		history468.add(history469);
		org.inspection_plusplus.History history470 = new org.inspection_plusplus.History();
		history470.setAction("");
		history470.setAuthor("aaaaaaaaaaaaaaaaa");
		history470.setComment("aaaa");
		history470.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 51, 21)));

		history468.add(history470);
		org.inspection_plusplus.History history471 = new org.inspection_plusplus.History();
		history471.setAction("aaaaaaaaaaaaaaaaaaaaaaaa");
		history471.setAuthor("aaaaa");
		history471.setComment("");
		history471.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 33, 14)));

		history468.add(history471);
		eu.sarunas.junit.TestsHelper.set(datum411, "history", history468);

		datum410.add(datum411);
		eu.sarunas.junit.TestsHelper.set(referenceSystem409, "datum", datum410);
		java.util.ArrayList<org.inspection_plusplus.AlignmentStrategy> alignmentStrategy472 = new java.util.ArrayList<org.inspection_plusplus.AlignmentStrategy>();
		org.inspection_plusplus.AlignmentFSS alignmentFSS473 = new org.inspection_plusplus.AlignmentFSS();
		alignmentFSS473.setTerminatingCondition(-172.13);
		alignmentFSS473.setName("aaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects474 = new org.inspection_plusplus.ID4Objects();
		iD4Objects474.setUuid("aaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension475 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension476 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any477 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension476, "any", any477);
		extension476.setVendor("aaaaaaaaaaaa");

		extension475.add(extension476);
		org.inspection_plusplus.Extension extension478 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any479 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension478, "any", any479);
		extension478.setVendor("");

		extension475.add(extension478);
		eu.sarunas.junit.TestsHelper.set(iD4Objects474, "extension", extension475);

		alignmentFSS473.setSystemID(iD4Objects474);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef480 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(alignmentFSS473, "commentRef", commentRef480);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension481 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension482 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any483 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension482, "any", any483);
		extension482.setVendor("aaa");

		extension481.add(extension482);
		org.inspection_plusplus.Extension extension484 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any485 = new java.util.ArrayList<Object>();
		any485.add(new Object());
		any485.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension484, "any", any485);
		extension484.setVendor("aa");

		extension481.add(extension484);
		org.inspection_plusplus.Extension extension486 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any487 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension486, "any", any487);
		extension486.setVendor("aaaaaa");

		extension481.add(extension486);
		eu.sarunas.junit.TestsHelper.set(alignmentFSS473, "extension", extension481);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID488 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID489 = new org.inspection_plusplus.DisplayID();
		displayID489.setLanguage("");
		displayID489.setText("");

		displayID488.add(displayID489);
		eu.sarunas.junit.TestsHelper.set(alignmentFSS473, "displayID", displayID488);
		java.util.ArrayList<org.inspection_plusplus.History> history490 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history491 = new org.inspection_plusplus.History();
		history491.setAction("");
		history491.setAuthor("");
		history491.setComment("aa");
		history491.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 33, 27)));

		history490.add(history491);
		eu.sarunas.junit.TestsHelper.set(alignmentFSS473, "history", history490);

		alignmentStrategy472.add(alignmentFSS473);
		org.inspection_plusplus.AlignmentBestfit alignmentBestfit492 = new org.inspection_plusplus.AlignmentBestfit();
		alignmentBestfit492.setTerminatingCondition(-181.36);
		alignmentBestfit492.setName("a");
		org.inspection_plusplus.ID4Objects iD4Objects493 = new org.inspection_plusplus.ID4Objects();
		iD4Objects493.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension494 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension495 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any496 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension495, "any", any496);
		extension495.setVendor("aaaaa");

		extension494.add(extension495);
		org.inspection_plusplus.Extension extension497 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any498 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension497, "any", any498);
		extension497.setVendor("aaaaaaaa");

		extension494.add(extension497);
		eu.sarunas.junit.TestsHelper.set(iD4Objects493, "extension", extension494);

		alignmentBestfit492.setSystemID(iD4Objects493);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef499 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(alignmentBestfit492, "commentRef", commentRef499);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension500 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(alignmentBestfit492, "extension", extension500);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID501 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(alignmentBestfit492, "displayID", displayID501);
		java.util.ArrayList<org.inspection_plusplus.History> history502 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history503 = new org.inspection_plusplus.History();
		history503.setAction("");
		history503.setAuthor("");
		history503.setComment("aaa");
		history503.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 46, 16)));

		history502.add(history503);
		eu.sarunas.junit.TestsHelper.set(alignmentBestfit492, "history", history502);

		alignmentStrategy472.add(alignmentBestfit492);
		eu.sarunas.junit.TestsHelper.set(referenceSystem409, "alignmentStrategy", alignmentStrategy472);
		referenceSystem409.setName("a");
		org.inspection_plusplus.ID4Objects iD4Objects504 = new org.inspection_plusplus.ID4Objects();
		iD4Objects504.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension505 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects504, "extension", extension505);

		referenceSystem409.setSystemID(iD4Objects504);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef506 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing507 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing507.setClassID("aaaaaaaaaaaaaaaaaaaa");
		iD4Referencing507.setUuid("aaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension508 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing507, "extension", extension508);

		commentRef506.add(iD4Referencing507);
		eu.sarunas.junit.TestsHelper.set(referenceSystem409, "commentRef", commentRef506);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension509 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(referenceSystem409, "extension", extension509);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID510 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID511 = new org.inspection_plusplus.DisplayID();
		displayID511.setLanguage("aaaaaaaaaaaaaaaaaaaaaa");
		displayID511.setText("");

		displayID510.add(displayID511);
		eu.sarunas.junit.TestsHelper.set(referenceSystem409, "displayID", displayID510);
		java.util.ArrayList<org.inspection_plusplus.History> history512 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history513 = new org.inspection_plusplus.History();
		history513.setAction("aaa");
		history513.setAuthor("aaaaaa");
		history513.setComment("aaaaaaaaaaaaaaaaaaaaa");
		history513.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 53, 6)));

		history512.add(history513);
		eu.sarunas.junit.TestsHelper.set(referenceSystem409, "history", history512);

		referenceSystem408.add(referenceSystem409);
		org.inspection_plusplus.ReferenceSystem referenceSystem514 = new org.inspection_plusplus.ReferenceSystem();
		java.util.ArrayList<org.inspection_plusplus.Datum> datum515 = new java.util.ArrayList<org.inspection_plusplus.Datum>();
		org.inspection_plusplus.Datum datum516 = new org.inspection_plusplus.Datum();
		org.inspection_plusplus.Vector3D vector3D517 = new org.inspection_plusplus.Vector3D();
		vector3D517.setX(318.66);
		vector3D517.setY(11.39);
		vector3D517.setZ(-309.80);

		datum516.setDisplacement(vector3D517);
		datum516.setRole("");
		java.util.ArrayList<org.inspection_plusplus.DatumTarget> datumTarget518 = new java.util.ArrayList<org.inspection_plusplus.DatumTarget>();
		eu.sarunas.junit.TestsHelper.set(datum516, "datumTarget", datumTarget518);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeGeometricRef519 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(datum516, "ipeGeometricRef", ipeGeometricRef519);
		datum516.setName("aaa");
		org.inspection_plusplus.ID4Objects iD4Objects520 = new org.inspection_plusplus.ID4Objects();
		iD4Objects520.setUuid("aaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension521 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension522 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any523 = new java.util.ArrayList<Object>();
		any523.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension522, "any", any523);
		extension522.setVendor("aaaaaaaaa");

		extension521.add(extension522);
		eu.sarunas.junit.TestsHelper.set(iD4Objects520, "extension", extension521);

		datum516.setSystemID(iD4Objects520);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef524 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing525 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing525.setClassID("aaaaaaaaaaaaa");
		iD4Referencing525.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension526 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing525, "extension", extension526);

		commentRef524.add(iD4Referencing525);
		eu.sarunas.junit.TestsHelper.set(datum516, "commentRef", commentRef524);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension527 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(datum516, "extension", extension527);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID528 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID529 = new org.inspection_plusplus.DisplayID();
		displayID529.setLanguage("");
		displayID529.setText("aaaaaa");

		displayID528.add(displayID529);
		eu.sarunas.junit.TestsHelper.set(datum516, "displayID", displayID528);
		java.util.ArrayList<org.inspection_plusplus.History> history530 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history531 = new org.inspection_plusplus.History();
		history531.setAction("");
		history531.setAuthor("aaaaaaa");
		history531.setComment("aaaaaaaaaaaaaa");
		history531.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 42, 15)));

		history530.add(history531);
		org.inspection_plusplus.History history532 = new org.inspection_plusplus.History();
		history532.setAction("");
		history532.setAuthor("");
		history532.setComment("");
		history532.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 15, 22)));

		history530.add(history532);
		eu.sarunas.junit.TestsHelper.set(datum516, "history", history530);

		datum515.add(datum516);
		eu.sarunas.junit.TestsHelper.set(referenceSystem514, "datum", datum515);
		java.util.ArrayList<org.inspection_plusplus.AlignmentStrategy> alignmentStrategy533 = new java.util.ArrayList<org.inspection_plusplus.AlignmentStrategy>();
		eu.sarunas.junit.TestsHelper.set(referenceSystem514, "alignmentStrategy", alignmentStrategy533);
		referenceSystem514.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects534 = new org.inspection_plusplus.ID4Objects();
		iD4Objects534.setUuid("aaaaaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension535 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects534, "extension", extension535);

		referenceSystem514.setSystemID(iD4Objects534);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef536 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(referenceSystem514, "commentRef", commentRef536);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension537 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension538 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any539 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension538, "any", any539);
		extension538.setVendor("");

		extension537.add(extension538);
		eu.sarunas.junit.TestsHelper.set(referenceSystem514, "extension", extension537);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID540 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID541 = new org.inspection_plusplus.DisplayID();
		displayID541.setLanguage("aaaaaaaaaa");
		displayID541.setText("aaaa");

		displayID540.add(displayID541);
		eu.sarunas.junit.TestsHelper.set(referenceSystem514, "displayID", displayID540);
		java.util.ArrayList<org.inspection_plusplus.History> history542 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(referenceSystem514, "history", history542);

		referenceSystem408.add(referenceSystem514);
		org.inspection_plusplus.ReferenceSystem referenceSystem543 = new org.inspection_plusplus.ReferenceSystem();
		java.util.ArrayList<org.inspection_plusplus.Datum> datum544 = new java.util.ArrayList<org.inspection_plusplus.Datum>();
		org.inspection_plusplus.Datum datum545 = new org.inspection_plusplus.Datum();
		org.inspection_plusplus.Vector3D vector3D546 = new org.inspection_plusplus.Vector3D();
		vector3D546.setX(19.17);
		vector3D546.setY(180.60);
		vector3D546.setZ(-164.54);

		datum545.setDisplacement(vector3D546);
		datum545.setRole("aaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.DatumTarget> datumTarget547 = new java.util.ArrayList<org.inspection_plusplus.DatumTarget>();
		eu.sarunas.junit.TestsHelper.set(datum545, "datumTarget", datumTarget547);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeGeometricRef548 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(datum545, "ipeGeometricRef", ipeGeometricRef548);
		datum545.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects549 = new org.inspection_plusplus.ID4Objects();
		iD4Objects549.setUuid("aaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension550 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension551 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any552 = new java.util.ArrayList<Object>();
		any552.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension551, "any", any552);
		extension551.setVendor("aaa");

		extension550.add(extension551);
		org.inspection_plusplus.Extension extension553 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any554 = new java.util.ArrayList<Object>();
		any554.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension553, "any", any554);
		extension553.setVendor("");

		extension550.add(extension553);
		eu.sarunas.junit.TestsHelper.set(iD4Objects549, "extension", extension550);

		datum545.setSystemID(iD4Objects549);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef555 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing556 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing556.setClassID("a");
		iD4Referencing556.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension557 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension558 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any559 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension558, "any", any559);
		extension558.setVendor("aaaaaaaaaaaaaaaaaa");

		extension557.add(extension558);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing556, "extension", extension557);

		commentRef555.add(iD4Referencing556);
		org.inspection_plusplus.ID4Referencing iD4Referencing560 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing560.setClassID("");
		iD4Referencing560.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension561 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension562 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any563 = new java.util.ArrayList<Object>();
		any563.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension562, "any", any563);
		extension562.setVendor("aaa");

		extension561.add(extension562);
		org.inspection_plusplus.Extension extension564 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any565 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension564, "any", any565);
		extension564.setVendor("");

		extension561.add(extension564);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing560, "extension", extension561);

		commentRef555.add(iD4Referencing560);
		org.inspection_plusplus.ID4Referencing iD4Referencing566 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing566.setClassID("");
		iD4Referencing566.setUuid("aaaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension567 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension568 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any569 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension568, "any", any569);
		extension568.setVendor("");

		extension567.add(extension568);
		org.inspection_plusplus.Extension extension570 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any571 = new java.util.ArrayList<Object>();
		any571.add(new Object());
		any571.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension570, "any", any571);
		extension570.setVendor("");

		extension567.add(extension570);
		org.inspection_plusplus.Extension extension572 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any573 = new java.util.ArrayList<Object>();
		any573.add(new Object());
		any573.add(new Object());
		any573.add(new Object());
		any573.add(new Object());
		any573.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension572, "any", any573);
		extension572.setVendor("aaaaaaaaaaaaaaaaaaaa");

		extension567.add(extension572);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing566, "extension", extension567);

		commentRef555.add(iD4Referencing566);
		eu.sarunas.junit.TestsHelper.set(datum545, "commentRef", commentRef555);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension574 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(datum545, "extension", extension574);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID575 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(datum545, "displayID", displayID575);
		java.util.ArrayList<org.inspection_plusplus.History> history576 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(datum545, "history", history576);

		datum544.add(datum545);
		org.inspection_plusplus.Datum datum577 = new org.inspection_plusplus.Datum();
		org.inspection_plusplus.Vector3D vector3D578 = new org.inspection_plusplus.Vector3D();
		vector3D578.setX(-338.35);
		vector3D578.setY(156.05);
		vector3D578.setZ(-78.04);

		datum577.setDisplacement(vector3D578);
		datum577.setRole("");
		java.util.ArrayList<org.inspection_plusplus.DatumTarget> datumTarget579 = new java.util.ArrayList<org.inspection_plusplus.DatumTarget>();
		eu.sarunas.junit.TestsHelper.set(datum577, "datumTarget", datumTarget579);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeGeometricRef580 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(datum577, "ipeGeometricRef", ipeGeometricRef580);
		datum577.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects581 = new org.inspection_plusplus.ID4Objects();
		iD4Objects581.setUuid("aaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension582 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects581, "extension", extension582);

		datum577.setSystemID(iD4Objects581);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef583 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(datum577, "commentRef", commentRef583);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension584 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(datum577, "extension", extension584);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID585 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID586 = new org.inspection_plusplus.DisplayID();
		displayID586.setLanguage("");
		displayID586.setText("aaaaaaa");

		displayID585.add(displayID586);
		org.inspection_plusplus.DisplayID displayID587 = new org.inspection_plusplus.DisplayID();
		displayID587.setLanguage("");
		displayID587.setText("");

		displayID585.add(displayID587);
		org.inspection_plusplus.DisplayID displayID588 = new org.inspection_plusplus.DisplayID();
		displayID588.setLanguage("");
		displayID588.setText("aa");

		displayID585.add(displayID588);
		org.inspection_plusplus.DisplayID displayID589 = new org.inspection_plusplus.DisplayID();
		displayID589.setLanguage("");
		displayID589.setText("");

		displayID585.add(displayID589);
		eu.sarunas.junit.TestsHelper.set(datum577, "displayID", displayID585);
		java.util.ArrayList<org.inspection_plusplus.History> history590 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history591 = new org.inspection_plusplus.History();
		history591.setAction("");
		history591.setAuthor("");
		history591.setComment("");
		history591.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 40, 53)));

		history590.add(history591);
		eu.sarunas.junit.TestsHelper.set(datum577, "history", history590);

		datum544.add(datum577);
		eu.sarunas.junit.TestsHelper.set(referenceSystem543, "datum", datum544);
		java.util.ArrayList<org.inspection_plusplus.AlignmentStrategy> alignmentStrategy592 = new java.util.ArrayList<org.inspection_plusplus.AlignmentStrategy>();
		org.inspection_plusplus.AlignmentFSS alignmentFSS593 = new org.inspection_plusplus.AlignmentFSS();
		alignmentFSS593.setTerminatingCondition(-376.53);
		alignmentFSS593.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects594 = new org.inspection_plusplus.ID4Objects();
		iD4Objects594.setUuid("aaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension595 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension596 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any597 = new java.util.ArrayList<Object>();
		any597.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension596, "any", any597);
		extension596.setVendor("aaaaaa");

		extension595.add(extension596);
		eu.sarunas.junit.TestsHelper.set(iD4Objects594, "extension", extension595);

		alignmentFSS593.setSystemID(iD4Objects594);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef598 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing599 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing599.setClassID("aa");
		iD4Referencing599.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension600 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension601 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any602 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension601, "any", any602);
		extension601.setVendor("");

		extension600.add(extension601);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing599, "extension", extension600);

		commentRef598.add(iD4Referencing599);
		org.inspection_plusplus.ID4Referencing iD4Referencing603 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing603.setClassID("aaaa");
		iD4Referencing603.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension604 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension605 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any606 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension605, "any", any606);
		extension605.setVendor("");

		extension604.add(extension605);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing603, "extension", extension604);

		commentRef598.add(iD4Referencing603);
		eu.sarunas.junit.TestsHelper.set(alignmentFSS593, "commentRef", commentRef598);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension607 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(alignmentFSS593, "extension", extension607);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID608 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(alignmentFSS593, "displayID", displayID608);
		java.util.ArrayList<org.inspection_plusplus.History> history609 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history610 = new org.inspection_plusplus.History();
		history610.setAction("aaa");
		history610.setAuthor("aaaa");
		history610.setComment("aaaaaaaaa");
		history610.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 28, 53)));

		history609.add(history610);
		org.inspection_plusplus.History history611 = new org.inspection_plusplus.History();
		history611.setAction("");
		history611.setAuthor("");
		history611.setComment("");
		history611.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 48, 37)));

		history609.add(history611);
		org.inspection_plusplus.History history612 = new org.inspection_plusplus.History();
		history612.setAction("");
		history612.setAuthor("");
		history612.setComment("aaaaaaaaaaaaaaaa");
		history612.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 59, 55)));

		history609.add(history612);
		org.inspection_plusplus.History history613 = new org.inspection_plusplus.History();
		history613.setAction("aaaa");
		history613.setAuthor("aaaaaaaaaaaa");
		history613.setComment("aaaaaaaaaaa");
		history613.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 53, 29)));

		history609.add(history613);
		eu.sarunas.junit.TestsHelper.set(alignmentFSS593, "history", history609);

		alignmentStrategy592.add(alignmentFSS593);
		eu.sarunas.junit.TestsHelper.set(referenceSystem543, "alignmentStrategy", alignmentStrategy592);
		referenceSystem543.setName("aaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects614 = new org.inspection_plusplus.ID4Objects();
		iD4Objects614.setUuid("aaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension615 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects614, "extension", extension615);

		referenceSystem543.setSystemID(iD4Objects614);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef616 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing617 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing617.setClassID("aaaaaaaaaaaaaaaaaaa");
		iD4Referencing617.setUuid("aaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension618 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension619 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any620 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension619, "any", any620);
		extension619.setVendor("");

		extension618.add(extension619);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing617, "extension", extension618);

		commentRef616.add(iD4Referencing617);
		org.inspection_plusplus.ID4Referencing iD4Referencing621 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing621.setClassID("aaaaaaa");
		iD4Referencing621.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension622 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension623 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any624 = new java.util.ArrayList<Object>();
		any624.add(new Object());
		any624.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension623, "any", any624);
		extension623.setVendor("aaaa");

		extension622.add(extension623);
		org.inspection_plusplus.Extension extension625 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any626 = new java.util.ArrayList<Object>();
		any626.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension625, "any", any626);
		extension625.setVendor("aaaa");

		extension622.add(extension625);
		org.inspection_plusplus.Extension extension627 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any628 = new java.util.ArrayList<Object>();
		any628.add(new Object());
		any628.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension627, "any", any628);
		extension627.setVendor("");

		extension622.add(extension627);
		org.inspection_plusplus.Extension extension629 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any630 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension629, "any", any630);
		extension629.setVendor("aa");

		extension622.add(extension629);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing621, "extension", extension622);

		commentRef616.add(iD4Referencing621);
		eu.sarunas.junit.TestsHelper.set(referenceSystem543, "commentRef", commentRef616);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension631 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(referenceSystem543, "extension", extension631);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID632 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(referenceSystem543, "displayID", displayID632);
		java.util.ArrayList<org.inspection_plusplus.History> history633 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(referenceSystem543, "history", history633);

		referenceSystem408.add(referenceSystem543);
		org.inspection_plusplus.ReferenceSystem referenceSystem634 = new org.inspection_plusplus.ReferenceSystem();
		java.util.ArrayList<org.inspection_plusplus.Datum> datum635 = new java.util.ArrayList<org.inspection_plusplus.Datum>();
		org.inspection_plusplus.Datum datum636 = new org.inspection_plusplus.Datum();
		org.inspection_plusplus.Vector3D vector3D637 = new org.inspection_plusplus.Vector3D();
		vector3D637.setX(-428.33);
		vector3D637.setY(-187.44);
		vector3D637.setZ(-46.75);

		datum636.setDisplacement(vector3D637);
		datum636.setRole("");
		java.util.ArrayList<org.inspection_plusplus.DatumTarget> datumTarget638 = new java.util.ArrayList<org.inspection_plusplus.DatumTarget>();
		eu.sarunas.junit.TestsHelper.set(datum636, "datumTarget", datumTarget638);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeGeometricRef639 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing640 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing640.setClassID("aaaaaaaaaaaaaaa");
		iD4Referencing640.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension641 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension642 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any643 = new java.util.ArrayList<Object>();
		any643.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension642, "any", any643);
		extension642.setVendor("");

		extension641.add(extension642);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing640, "extension", extension641);

		ipeGeometricRef639.add(iD4Referencing640);
		eu.sarunas.junit.TestsHelper.set(datum636, "ipeGeometricRef", ipeGeometricRef639);
		datum636.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects644 = new org.inspection_plusplus.ID4Objects();
		iD4Objects644.setUuid("aaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension645 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension646 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any647 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension646, "any", any647);
		extension646.setVendor("");

		extension645.add(extension646);
		org.inspection_plusplus.Extension extension648 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any649 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension648, "any", any649);
		extension648.setVendor("");

		extension645.add(extension648);
		eu.sarunas.junit.TestsHelper.set(iD4Objects644, "extension", extension645);

		datum636.setSystemID(iD4Objects644);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef650 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing651 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing651.setClassID("");
		iD4Referencing651.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension652 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension653 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any654 = new java.util.ArrayList<Object>();
		any654.add(new Object());
		any654.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension653, "any", any654);
		extension653.setVendor("aaaaaaaaaa");

		extension652.add(extension653);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing651, "extension", extension652);

		commentRef650.add(iD4Referencing651);
		org.inspection_plusplus.ID4Referencing iD4Referencing655 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing655.setClassID("aaa");
		iD4Referencing655.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension656 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing655, "extension", extension656);

		commentRef650.add(iD4Referencing655);
		eu.sarunas.junit.TestsHelper.set(datum636, "commentRef", commentRef650);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension657 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(datum636, "extension", extension657);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID658 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID659 = new org.inspection_plusplus.DisplayID();
		displayID659.setLanguage("");
		displayID659.setText("");

		displayID658.add(displayID659);
		eu.sarunas.junit.TestsHelper.set(datum636, "displayID", displayID658);
		java.util.ArrayList<org.inspection_plusplus.History> history660 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history661 = new org.inspection_plusplus.History();
		history661.setAction("");
		history661.setAuthor("aaaaaaaaaaaaaaaaa");
		history661.setComment("aaaaa");
		history661.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 20, 57)));

		history660.add(history661);
		org.inspection_plusplus.History history662 = new org.inspection_plusplus.History();
		history662.setAction("aaaa");
		history662.setAuthor("");
		history662.setComment("");
		history662.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 29, 31)));

		history660.add(history662);
		eu.sarunas.junit.TestsHelper.set(datum636, "history", history660);

		datum635.add(datum636);
		org.inspection_plusplus.Datum datum663 = new org.inspection_plusplus.Datum();
		org.inspection_plusplus.Vector3D vector3D664 = new org.inspection_plusplus.Vector3D();
		vector3D664.setX(-75.25);
		vector3D664.setY(-372.25);
		vector3D664.setZ(-107.43);

		datum663.setDisplacement(vector3D664);
		datum663.setRole("");
		java.util.ArrayList<org.inspection_plusplus.DatumTarget> datumTarget665 = new java.util.ArrayList<org.inspection_plusplus.DatumTarget>();
		org.inspection_plusplus.DatumTarget datumTarget666 = new org.inspection_plusplus.DatumTarget();
		datumTarget666.setRole("");
		org.inspection_plusplus.ID4Referencing iD4Referencing667 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing667.setClassID("aa");
		iD4Referencing667.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension668 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension669 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any670 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension669, "any", any670);
		extension669.setVendor("");

		extension668.add(extension669);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing667, "extension", extension668);

		eu.sarunas.junit.TestsHelper.set(datumTarget666, "ipeGeometricRef", iD4Referencing667);
		datumTarget666.setName("aaa");
		org.inspection_plusplus.ID4Objects iD4Objects671 = new org.inspection_plusplus.ID4Objects();
		iD4Objects671.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension672 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension673 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any674 = new java.util.ArrayList<Object>();
		any674.add(new Object());
		any674.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension673, "any", any674);
		extension673.setVendor("aaaa");

		extension672.add(extension673);
		org.inspection_plusplus.Extension extension675 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any676 = new java.util.ArrayList<Object>();
		any676.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension675, "any", any676);
		extension675.setVendor("");

		extension672.add(extension675);
		eu.sarunas.junit.TestsHelper.set(iD4Objects671, "extension", extension672);

		datumTarget666.setSystemID(iD4Objects671);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef677 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(datumTarget666, "commentRef", commentRef677);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension678 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension679 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any680 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension679, "any", any680);
		extension679.setVendor("aaaaaaaaaaaaaaaa");

		extension678.add(extension679);
		org.inspection_plusplus.Extension extension681 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any682 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension681, "any", any682);
		extension681.setVendor("aaaaaaaaaaa");

		extension678.add(extension681);
		org.inspection_plusplus.Extension extension683 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any684 = new java.util.ArrayList<Object>();
		any684.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension683, "any", any684);
		extension683.setVendor("aaaaaaaaa");

		extension678.add(extension683);
		eu.sarunas.junit.TestsHelper.set(datumTarget666, "extension", extension678);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID685 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID686 = new org.inspection_plusplus.DisplayID();
		displayID686.setLanguage("aaa");
		displayID686.setText("aaa");

		displayID685.add(displayID686);
		eu.sarunas.junit.TestsHelper.set(datumTarget666, "displayID", displayID685);
		java.util.ArrayList<org.inspection_plusplus.History> history687 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(datumTarget666, "history", history687);

		datumTarget665.add(datumTarget666);
		eu.sarunas.junit.TestsHelper.set(datum663, "datumTarget", datumTarget665);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeGeometricRef688 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing689 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing689.setClassID("aaa");
		iD4Referencing689.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension690 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension691 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any692 = new java.util.ArrayList<Object>();
		any692.add(new Object());
		any692.add(new Object());
		any692.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension691, "any", any692);
		extension691.setVendor("aaaaaaaaaaaaaaaaaaaaaa");

		extension690.add(extension691);
		org.inspection_plusplus.Extension extension693 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any694 = new java.util.ArrayList<Object>();
		any694.add(new Object());
		any694.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension693, "any", any694);
		extension693.setVendor("");

		extension690.add(extension693);
		org.inspection_plusplus.Extension extension695 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any696 = new java.util.ArrayList<Object>();
		any696.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension695, "any", any696);
		extension695.setVendor("");

		extension690.add(extension695);
		org.inspection_plusplus.Extension extension697 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any698 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension697, "any", any698);
		extension697.setVendor("aaaaaaa");

		extension690.add(extension697);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing689, "extension", extension690);

		ipeGeometricRef688.add(iD4Referencing689);
		eu.sarunas.junit.TestsHelper.set(datum663, "ipeGeometricRef", ipeGeometricRef688);
		datum663.setName("aaaaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects699 = new org.inspection_plusplus.ID4Objects();
		iD4Objects699.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension700 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension701 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any702 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension701, "any", any702);
		extension701.setVendor("");

		extension700.add(extension701);
		eu.sarunas.junit.TestsHelper.set(iD4Objects699, "extension", extension700);

		datum663.setSystemID(iD4Objects699);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef703 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing704 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing704.setClassID("aaaaa");
		iD4Referencing704.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension705 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension706 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any707 = new java.util.ArrayList<Object>();
		any707.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension706, "any", any707);
		extension706.setVendor("");

		extension705.add(extension706);
		org.inspection_plusplus.Extension extension708 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any709 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension708, "any", any709);
		extension708.setVendor("");

		extension705.add(extension708);
		org.inspection_plusplus.Extension extension710 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any711 = new java.util.ArrayList<Object>();
		any711.add(new Object());
		any711.add(new Object());
		any711.add(new Object());
		any711.add(new Object());
		any711.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension710, "any", any711);
		extension710.setVendor("");

		extension705.add(extension710);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing704, "extension", extension705);

		commentRef703.add(iD4Referencing704);
		org.inspection_plusplus.ID4Referencing iD4Referencing712 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing712.setClassID("aaa");
		iD4Referencing712.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension713 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension714 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any715 = new java.util.ArrayList<Object>();
		any715.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension714, "any", any715);
		extension714.setVendor("aaaaaa");

		extension713.add(extension714);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing712, "extension", extension713);

		commentRef703.add(iD4Referencing712);
		org.inspection_plusplus.ID4Referencing iD4Referencing716 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing716.setClassID("");
		iD4Referencing716.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension717 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing716, "extension", extension717);

		commentRef703.add(iD4Referencing716);
		org.inspection_plusplus.ID4Referencing iD4Referencing718 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing718.setClassID("aaaaa");
		iD4Referencing718.setUuid("a");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension719 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension720 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any721 = new java.util.ArrayList<Object>();
		any721.add(new Object());
		any721.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension720, "any", any721);
		extension720.setVendor("");

		extension719.add(extension720);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing718, "extension", extension719);

		commentRef703.add(iD4Referencing718);
		eu.sarunas.junit.TestsHelper.set(datum663, "commentRef", commentRef703);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension722 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(datum663, "extension", extension722);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID723 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID724 = new org.inspection_plusplus.DisplayID();
		displayID724.setLanguage("");
		displayID724.setText("");

		displayID723.add(displayID724);
		eu.sarunas.junit.TestsHelper.set(datum663, "displayID", displayID723);
		java.util.ArrayList<org.inspection_plusplus.History> history725 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history726 = new org.inspection_plusplus.History();
		history726.setAction("aaa");
		history726.setAuthor("aaaaaaa");
		history726.setComment("aaaaaaaaaaaaaaa");
		history726.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 29, 56)));

		history725.add(history726);
		eu.sarunas.junit.TestsHelper.set(datum663, "history", history725);

		datum635.add(datum663);
		eu.sarunas.junit.TestsHelper.set(referenceSystem634, "datum", datum635);
		java.util.ArrayList<org.inspection_plusplus.AlignmentStrategy> alignmentStrategy727 = new java.util.ArrayList<org.inspection_plusplus.AlignmentStrategy>();
		org.inspection_plusplus.AlignmentFSS alignmentFSS728 = new org.inspection_plusplus.AlignmentFSS();
		alignmentFSS728.setTerminatingCondition(-105.61);
		alignmentFSS728.setName("aaaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects729 = new org.inspection_plusplus.ID4Objects();
		iD4Objects729.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension730 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension731 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any732 = new java.util.ArrayList<Object>();
		any732.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension731, "any", any732);
		extension731.setVendor("");

		extension730.add(extension731);
		eu.sarunas.junit.TestsHelper.set(iD4Objects729, "extension", extension730);

		alignmentFSS728.setSystemID(iD4Objects729);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef733 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing734 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing734.setClassID("aaaa");
		iD4Referencing734.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension735 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing734, "extension", extension735);

		commentRef733.add(iD4Referencing734);
		eu.sarunas.junit.TestsHelper.set(alignmentFSS728, "commentRef", commentRef733);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension736 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(alignmentFSS728, "extension", extension736);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID737 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(alignmentFSS728, "displayID", displayID737);
		java.util.ArrayList<org.inspection_plusplus.History> history738 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history739 = new org.inspection_plusplus.History();
		history739.setAction("");
		history739.setAuthor("");
		history739.setComment("");
		history739.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 3, 50)));

		history738.add(history739);
		org.inspection_plusplus.History history740 = new org.inspection_plusplus.History();
		history740.setAction("");
		history740.setAuthor("");
		history740.setComment("aaaaaaa");
		history740.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 55, 51)));

		history738.add(history740);
		eu.sarunas.junit.TestsHelper.set(alignmentFSS728, "history", history738);

		alignmentStrategy727.add(alignmentFSS728);
		org.inspection_plusplus.AlignmentRPS alignmentRPS741 = new org.inspection_plusplus.AlignmentRPS();
		alignmentRPS741.setMaxNrOfIterations(new java.math.BigInteger("-292"));
		alignmentRPS741.setName("aaaaaaaaaaaaaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects742 = new org.inspection_plusplus.ID4Objects();
		iD4Objects742.setUuid("aaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension743 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension744 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any745 = new java.util.ArrayList<Object>();
		any745.add(new Object());
		any745.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension744, "any", any745);
		extension744.setVendor("");

		extension743.add(extension744);
		org.inspection_plusplus.Extension extension746 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any747 = new java.util.ArrayList<Object>();
		any747.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension746, "any", any747);
		extension746.setVendor("aaaaaaaaaaaaaaaaaaaaaa");

		extension743.add(extension746);
		eu.sarunas.junit.TestsHelper.set(iD4Objects742, "extension", extension743);

		alignmentRPS741.setSystemID(iD4Objects742);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef748 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing749 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing749.setClassID("aaaaaa");
		iD4Referencing749.setUuid("aaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension750 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing749, "extension", extension750);

		commentRef748.add(iD4Referencing749);
		eu.sarunas.junit.TestsHelper.set(alignmentRPS741, "commentRef", commentRef748);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension751 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension752 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any753 = new java.util.ArrayList<Object>();
		any753.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension752, "any", any753);
		extension752.setVendor("");

		extension751.add(extension752);
		eu.sarunas.junit.TestsHelper.set(alignmentRPS741, "extension", extension751);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID754 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID755 = new org.inspection_plusplus.DisplayID();
		displayID755.setLanguage("");
		displayID755.setText("aaaaaaa");

		displayID754.add(displayID755);
		eu.sarunas.junit.TestsHelper.set(alignmentRPS741, "displayID", displayID754);
		java.util.ArrayList<org.inspection_plusplus.History> history756 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history757 = new org.inspection_plusplus.History();
		history757.setAction("aa");
		history757.setAuthor("aaaaaaa");
		history757.setComment("");
		history757.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 43, 9)));

		history756.add(history757);
		org.inspection_plusplus.History history758 = new org.inspection_plusplus.History();
		history758.setAction("");
		history758.setAuthor("");
		history758.setComment("a");
		history758.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 29, 57)));

		history756.add(history758);
		org.inspection_plusplus.History history759 = new org.inspection_plusplus.History();
		history759.setAction("aaaaaaa");
		history759.setAuthor("aaaaaaaaa");
		history759.setComment("");
		history759.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 22, 10)));

		history756.add(history759);
		eu.sarunas.junit.TestsHelper.set(alignmentRPS741, "history", history756);

		alignmentStrategy727.add(alignmentRPS741);
		org.inspection_plusplus.AlignmentRPS alignmentRPS760 = new org.inspection_plusplus.AlignmentRPS();
		alignmentRPS760.setMaxNrOfIterations(new java.math.BigInteger("-65"));
		alignmentRPS760.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects761 = new org.inspection_plusplus.ID4Objects();
		iD4Objects761.setUuid("aaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension762 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension763 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any764 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension763, "any", any764);
		extension763.setVendor("");

		extension762.add(extension763);
		org.inspection_plusplus.Extension extension765 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any766 = new java.util.ArrayList<Object>();
		any766.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension765, "any", any766);
		extension765.setVendor("a");

		extension762.add(extension765);
		eu.sarunas.junit.TestsHelper.set(iD4Objects761, "extension", extension762);

		alignmentRPS760.setSystemID(iD4Objects761);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef767 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing768 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing768.setClassID("");
		iD4Referencing768.setUuid("aaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension769 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension770 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any771 = new java.util.ArrayList<Object>();
		any771.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension770, "any", any771);
		extension770.setVendor("aaaaaaaaaaaaaaaaa");

		extension769.add(extension770);
		org.inspection_plusplus.Extension extension772 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any773 = new java.util.ArrayList<Object>();
		any773.add(new Object());
		any773.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension772, "any", any773);
		extension772.setVendor("aaaaa");

		extension769.add(extension772);
		org.inspection_plusplus.Extension extension774 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any775 = new java.util.ArrayList<Object>();
		any775.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension774, "any", any775);
		extension774.setVendor("");

		extension769.add(extension774);
		org.inspection_plusplus.Extension extension776 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any777 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension776, "any", any777);
		extension776.setVendor("aaaaaaaaaaaaa");

		extension769.add(extension776);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing768, "extension", extension769);

		commentRef767.add(iD4Referencing768);
		eu.sarunas.junit.TestsHelper.set(alignmentRPS760, "commentRef", commentRef767);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension778 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension779 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any780 = new java.util.ArrayList<Object>();
		any780.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension779, "any", any780);
		extension779.setVendor("aaaaaaaaaaaaa");

		extension778.add(extension779);
		eu.sarunas.junit.TestsHelper.set(alignmentRPS760, "extension", extension778);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID781 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID782 = new org.inspection_plusplus.DisplayID();
		displayID782.setLanguage("");
		displayID782.setText("");

		displayID781.add(displayID782);
		org.inspection_plusplus.DisplayID displayID783 = new org.inspection_plusplus.DisplayID();
		displayID783.setLanguage("");
		displayID783.setText("");

		displayID781.add(displayID783);
		org.inspection_plusplus.DisplayID displayID784 = new org.inspection_plusplus.DisplayID();
		displayID784.setLanguage("a");
		displayID784.setText("");

		displayID781.add(displayID784);
		org.inspection_plusplus.DisplayID displayID785 = new org.inspection_plusplus.DisplayID();
		displayID785.setLanguage("aaaaa");
		displayID785.setText("");

		displayID781.add(displayID785);
		eu.sarunas.junit.TestsHelper.set(alignmentRPS760, "displayID", displayID781);
		java.util.ArrayList<org.inspection_plusplus.History> history786 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(alignmentRPS760, "history", history786);

		alignmentStrategy727.add(alignmentRPS760);
		org.inspection_plusplus.AlignmentRPS alignmentRPS787 = new org.inspection_plusplus.AlignmentRPS();
		alignmentRPS787.setMaxNrOfIterations(new java.math.BigInteger("-137"));
		alignmentRPS787.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects788 = new org.inspection_plusplus.ID4Objects();
		iD4Objects788.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension789 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension790 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any791 = new java.util.ArrayList<Object>();
		any791.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension790, "any", any791);
		extension790.setVendor("aaaaaaaaaaaaaaaaaa");

		extension789.add(extension790);
		eu.sarunas.junit.TestsHelper.set(iD4Objects788, "extension", extension789);

		alignmentRPS787.setSystemID(iD4Objects788);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef792 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing793 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing793.setClassID("a");
		iD4Referencing793.setUuid("aaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension794 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension795 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any796 = new java.util.ArrayList<Object>();
		any796.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension795, "any", any796);
		extension795.setVendor("");

		extension794.add(extension795);
		org.inspection_plusplus.Extension extension797 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any798 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension797, "any", any798);
		extension797.setVendor("");

		extension794.add(extension797);
		org.inspection_plusplus.Extension extension799 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any800 = new java.util.ArrayList<Object>();
		any800.add(new Object());
		any800.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension799, "any", any800);
		extension799.setVendor("");

		extension794.add(extension799);
		org.inspection_plusplus.Extension extension801 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any802 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension801, "any", any802);
		extension801.setVendor("");

		extension794.add(extension801);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing793, "extension", extension794);

		commentRef792.add(iD4Referencing793);
		org.inspection_plusplus.ID4Referencing iD4Referencing803 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing803.setClassID("");
		iD4Referencing803.setUuid("aaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension804 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing803, "extension", extension804);

		commentRef792.add(iD4Referencing803);
		eu.sarunas.junit.TestsHelper.set(alignmentRPS787, "commentRef", commentRef792);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension805 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(alignmentRPS787, "extension", extension805);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID806 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID807 = new org.inspection_plusplus.DisplayID();
		displayID807.setLanguage("");
		displayID807.setText("");

		displayID806.add(displayID807);
		org.inspection_plusplus.DisplayID displayID808 = new org.inspection_plusplus.DisplayID();
		displayID808.setLanguage("");
		displayID808.setText("");

		displayID806.add(displayID808);
		eu.sarunas.junit.TestsHelper.set(alignmentRPS787, "displayID", displayID806);
		java.util.ArrayList<org.inspection_plusplus.History> history809 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history810 = new org.inspection_plusplus.History();
		history810.setAction("aaaaaaa");
		history810.setAuthor("");
		history810.setComment("");
		history810.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 35, 27)));

		history809.add(history810);
		eu.sarunas.junit.TestsHelper.set(alignmentRPS787, "history", history809);

		alignmentStrategy727.add(alignmentRPS787);
		eu.sarunas.junit.TestsHelper.set(referenceSystem634, "alignmentStrategy", alignmentStrategy727);
		referenceSystem634.setName("aaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects811 = new org.inspection_plusplus.ID4Objects();
		iD4Objects811.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension812 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension813 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any814 = new java.util.ArrayList<Object>();
		any814.add(new Object());
		any814.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension813, "any", any814);
		extension813.setVendor("aaaaaaaaaa");

		extension812.add(extension813);
		org.inspection_plusplus.Extension extension815 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any816 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension815, "any", any816);
		extension815.setVendor("aaa");

		extension812.add(extension815);
		org.inspection_plusplus.Extension extension817 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any818 = new java.util.ArrayList<Object>();
		any818.add(new Object());
		any818.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension817, "any", any818);
		extension817.setVendor("aaaaaaa");

		extension812.add(extension817);
		eu.sarunas.junit.TestsHelper.set(iD4Objects811, "extension", extension812);

		referenceSystem634.setSystemID(iD4Objects811);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef819 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(referenceSystem634, "commentRef", commentRef819);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension820 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(referenceSystem634, "extension", extension820);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID821 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID822 = new org.inspection_plusplus.DisplayID();
		displayID822.setLanguage("aaaaaa");
		displayID822.setText("aaaaaa");

		displayID821.add(displayID822);
		org.inspection_plusplus.DisplayID displayID823 = new org.inspection_plusplus.DisplayID();
		displayID823.setLanguage("");
		displayID823.setText("aaaaaaaaaaaaaaaa");

		displayID821.add(displayID823);
		eu.sarunas.junit.TestsHelper.set(referenceSystem634, "displayID", displayID821);
		java.util.ArrayList<org.inspection_plusplus.History> history824 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history825 = new org.inspection_plusplus.History();
		history825.setAction("aaaa");
		history825.setAuthor("aaaaaaaaaaaaa");
		history825.setComment("aaa");
		history825.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 11, 42)));

		history824.add(history825);
		org.inspection_plusplus.History history826 = new org.inspection_plusplus.History();
		history826.setAction("aa");
		history826.setAuthor("");
		history826.setComment("");
		history826.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 43, 20)));

		history824.add(history826);
		org.inspection_plusplus.History history827 = new org.inspection_plusplus.History();
		history827.setAction("aaaaaaaaaaaa");
		history827.setAuthor("");
		history827.setComment("aaa");
		history827.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 41, 46)));

		history824.add(history827);
		eu.sarunas.junit.TestsHelper.set(referenceSystem634, "history", history824);

		referenceSystem408.add(referenceSystem634);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan362, "referenceSystem", referenceSystem408);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> inspectionTaskRef828 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing829 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing829.setClassID("aaaa");
		iD4Referencing829.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension830 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension831 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any832 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension831, "any", any832);
		extension831.setVendor("");

		extension830.add(extension831);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing829, "extension", extension830);

		inspectionTaskRef828.add(iD4Referencing829);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan362, "inspectionTaskRef", inspectionTaskRef828);
		java.util.ArrayList<org.inspection_plusplus.QC> qc833 = new java.util.ArrayList<org.inspection_plusplus.QC>();
		org.inspection_plusplus.QCSingle qCSingle834 = new org.inspection_plusplus.QCSingle();
		qCSingle834.setGeoObjectDetail("");
		org.inspection_plusplus.ID4Referencing iD4Referencing835 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing835.setClassID("");
		iD4Referencing835.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension836 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing835, "extension", extension836);

		qCSingle834.setToleranceRef(iD4Referencing835);
		qCSingle834.setAllAssembliesReferenced(true);
		qCSingle834.setDescription("a");
		qCSingle834.setFunction("");
		qCSingle834.setPartQAversion("");
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> partRef837 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing838 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing838.setClassID("");
		iD4Referencing838.setUuid("aaaaaaaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension839 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing838, "extension", extension839);

		partRef837.add(iD4Referencing838);
		eu.sarunas.junit.TestsHelper.set(qCSingle834, "partRef", partRef837);
		java.util.ArrayList<Object> calculationActualElementDetectionRef840 = new java.util.ArrayList<Object>();
		calculationActualElementDetectionRef840.add(new Object());
		eu.sarunas.junit.TestsHelper.set(qCSingle834, "calculationActualElementDetectionRef", calculationActualElementDetectionRef840);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeRef841 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing842 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing842.setClassID("aaaaaaaaaaa");
		iD4Referencing842.setUuid("aaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension843 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension844 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any845 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension844, "any", any845);
		extension844.setVendor("");

		extension843.add(extension844);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing842, "extension", extension843);

		ipeRef841.add(iD4Referencing842);
		eu.sarunas.junit.TestsHelper.set(qCSingle834, "ipeRef", ipeRef841);
		qCSingle834.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects846 = new org.inspection_plusplus.ID4Objects();
		iD4Objects846.setUuid("aaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension847 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension848 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any849 = new java.util.ArrayList<Object>();
		any849.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension848, "any", any849);
		extension848.setVendor("aaaaaaaaaaaaaaaaaaaaa");

		extension847.add(extension848);
		org.inspection_plusplus.Extension extension850 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any851 = new java.util.ArrayList<Object>();
		any851.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension850, "any", any851);
		extension850.setVendor("aaaaaaaa");

		extension847.add(extension850);
		eu.sarunas.junit.TestsHelper.set(iD4Objects846, "extension", extension847);

		qCSingle834.setSystemID(iD4Objects846);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef852 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(qCSingle834, "commentRef", commentRef852);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension853 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension854 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any855 = new java.util.ArrayList<Object>();
		any855.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension854, "any", any855);
		extension854.setVendor("aaaaaaaaa");

		extension853.add(extension854);
		eu.sarunas.junit.TestsHelper.set(qCSingle834, "extension", extension853);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID856 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(qCSingle834, "displayID", displayID856);
		java.util.ArrayList<org.inspection_plusplus.History> history857 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(qCSingle834, "history", history857);

		qc833.add(qCSingle834);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan362, "qc", qc833);
		inspectionPlan362.setName("aaaaaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects858 = new org.inspection_plusplus.ID4Objects();
		iD4Objects858.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension859 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects858, "extension", extension859);

		inspectionPlan362.setSystemID(iD4Objects858);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef860 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing861 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing861.setClassID("");
		iD4Referencing861.setUuid("a");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension862 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension863 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any864 = new java.util.ArrayList<Object>();
		any864.add(new Object());
		any864.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension863, "any", any864);
		extension863.setVendor("aaaaaaaaaaaaaaaaaaaaaaaaaaa");

		extension862.add(extension863);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing861, "extension", extension862);

		commentRef860.add(iD4Referencing861);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan362, "commentRef", commentRef860);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension865 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan362, "extension", extension865);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID866 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan362, "displayID", displayID866);
		java.util.ArrayList<org.inspection_plusplus.History> history867 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history868 = new org.inspection_plusplus.History();
		history868.setAction("");
		history868.setAuthor("aaaaaa");
		history868.setComment("");
		history868.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 34, 7)));

		history867.add(history868);
		org.inspection_plusplus.History history869 = new org.inspection_plusplus.History();
		history869.setAction("");
		history869.setAuthor("aaaaaaaaaaaaaaaaa");
		history869.setComment("");
		history869.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 21, 5)));

		history867.add(history869);
		org.inspection_plusplus.History history870 = new org.inspection_plusplus.History();
		history870.setAction("aaaaaaaaaaaaaaaaaaa");
		history870.setAuthor("aaaaa");
		history870.setComment("");
		history870.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 38, 37)));

		history867.add(history870);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan362, "history", history867);
		org.inspection_plusplus.Sender sender871 = new org.inspection_plusplus.Sender();
		sender871.setAppID("");
		sender871.setDomain("aaaaaaaa");
		org.inspection_plusplus.Extension extension872 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any873 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension872, "any", any873);
		extension872.setVendor("");

		org.inspection_plusplus.ReturnStatus res = testObject.storeInspectionPlan(inspectionPlan362, "", "", sender871, extension872);
		junit.framework.Assert.fail();
	};

	@org.junit.Test
	public void testtestStoreInspectionPlan4() throws Throwable
	{
		org.inspection_plusplus.service.v2.WebServiceImpl testObject = new org.inspection_plusplus.service.v2.WebServiceImpl();
		org.inspection_plusplus.InspectionPlan inspectionPlan874 = new org.inspection_plusplus.InspectionPlan();
		inspectionPlan874.setInspectionCategories("aaaaaaaaaa");
		inspectionPlan874.setIPsymmetry("aaaa");
		inspectionPlan874.setPartQAversion("");
		eu.sarunas.junit.TestsHelper.set(inspectionPlan874, "pdMmaturity", "");
		inspectionPlan874.setQAmaturity("aaa");
		inspectionPlan874.setQAversion("");
		java.util.ArrayList<org.inspection_plusplus.Comment> comment875 = new java.util.ArrayList<org.inspection_plusplus.Comment>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan874, "comment", comment875);
		java.util.ArrayList<org.inspection_plusplus.Tolerance> tolerance876 = new java.util.ArrayList<org.inspection_plusplus.Tolerance>();
		org.inspection_plusplus.TolAxisAngleY tolAxisAngleY877 = new org.inspection_plusplus.TolAxisAngleY();
		org.inspection_plusplus.IntervalToleranceCriterion intervalToleranceCriterion878 = new org.inspection_plusplus.IntervalToleranceCriterion();
		intervalToleranceCriterion878.setInOut("aaaaa");
		intervalToleranceCriterion878.setLowerToleranceValue(new java.math.BigDecimal(-332.78));
		intervalToleranceCriterion878.setUpperToleranceValue(new java.math.BigDecimal(-165.42));

		tolAxisAngleY877.setCriterion(intervalToleranceCriterion878);
		org.inspection_plusplus.ID4Referencing iD4Referencing879 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing879.setClassID("");
		iD4Referencing879.setUuid("aaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension880 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension881 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any882 = new java.util.ArrayList<Object>();
		any882.add(new Object());
		any882.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension881, "any", any882);
		extension881.setVendor("aaaaaaaaa");

		extension880.add(extension881);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing879, "extension", extension880);

		tolAxisAngleY877.setReferenceSystemRef(iD4Referencing879);
		tolAxisAngleY877.setStandardConformity("");
		org.inspection_plusplus.ToleranceSource toleranceSource883 = new org.inspection_plusplus.ToleranceSource();
		toleranceSource883.setTyp("aaaaaaaaaa");
		toleranceSource883.setValidFrom(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 36, 53)));
		toleranceSource883.setValidTo(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 15, 40)));
		toleranceSource883.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects884 = new org.inspection_plusplus.ID4Objects();
		iD4Objects884.setUuid("aaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension885 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension886 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any887 = new java.util.ArrayList<Object>();
		any887.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension886, "any", any887);
		extension886.setVendor("");

		extension885.add(extension886);
		org.inspection_plusplus.Extension extension888 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any889 = new java.util.ArrayList<Object>();
		any889.add(new Object());
		any889.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension888, "any", any889);
		extension888.setVendor("");

		extension885.add(extension888);
		org.inspection_plusplus.Extension extension890 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any891 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension890, "any", any891);
		extension890.setVendor("");

		extension885.add(extension890);
		org.inspection_plusplus.Extension extension892 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any893 = new java.util.ArrayList<Object>();
		any893.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension892, "any", any893);
		extension892.setVendor("");

		extension885.add(extension892);
		eu.sarunas.junit.TestsHelper.set(iD4Objects884, "extension", extension885);

		toleranceSource883.setSystemID(iD4Objects884);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef894 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(toleranceSource883, "commentRef", commentRef894);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension895 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(toleranceSource883, "extension", extension895);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID896 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID897 = new org.inspection_plusplus.DisplayID();
		displayID897.setLanguage("");
		displayID897.setText("a");

		displayID896.add(displayID897);
		eu.sarunas.junit.TestsHelper.set(toleranceSource883, "displayID", displayID896);
		java.util.ArrayList<org.inspection_plusplus.History> history898 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history899 = new org.inspection_plusplus.History();
		history899.setAction("aaaaaaa");
		history899.setAuthor("");
		history899.setComment("aaa");
		history899.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 39, 22)));

		history898.add(history899);
		eu.sarunas.junit.TestsHelper.set(toleranceSource883, "history", history898);

		tolAxisAngleY877.setToleranceSource(toleranceSource883);
		tolAxisAngleY877.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects900 = new org.inspection_plusplus.ID4Objects();
		iD4Objects900.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension901 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension902 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any903 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension902, "any", any903);
		extension902.setVendor("aaaaaaaaaaaaaaaaa");

		extension901.add(extension902);
		org.inspection_plusplus.Extension extension904 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any905 = new java.util.ArrayList<Object>();
		any905.add(new Object());
		any905.add(new Object());
		any905.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension904, "any", any905);
		extension904.setVendor("");

		extension901.add(extension904);
		org.inspection_plusplus.Extension extension906 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any907 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension906, "any", any907);
		extension906.setVendor("");

		extension901.add(extension906);
		eu.sarunas.junit.TestsHelper.set(iD4Objects900, "extension", extension901);

		tolAxisAngleY877.setSystemID(iD4Objects900);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef908 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing909 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing909.setClassID("aaaaaaaaaaaaa");
		iD4Referencing909.setUuid("a");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension910 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension911 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any912 = new java.util.ArrayList<Object>();
		any912.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension911, "any", any912);
		extension911.setVendor("aaaaaaaaaaaaaaa");

		extension910.add(extension911);
		org.inspection_plusplus.Extension extension913 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any914 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension913, "any", any914);
		extension913.setVendor("");

		extension910.add(extension913);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing909, "extension", extension910);

		commentRef908.add(iD4Referencing909);
		org.inspection_plusplus.ID4Referencing iD4Referencing915 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing915.setClassID("");
		iD4Referencing915.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension916 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension917 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any918 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension917, "any", any918);
		extension917.setVendor("aa");

		extension916.add(extension917);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing915, "extension", extension916);

		commentRef908.add(iD4Referencing915);
		org.inspection_plusplus.ID4Referencing iD4Referencing919 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing919.setClassID("");
		iD4Referencing919.setUuid("aaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension920 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing919, "extension", extension920);

		commentRef908.add(iD4Referencing919);
		eu.sarunas.junit.TestsHelper.set(tolAxisAngleY877, "commentRef", commentRef908);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension921 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(tolAxisAngleY877, "extension", extension921);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID922 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID923 = new org.inspection_plusplus.DisplayID();
		displayID923.setLanguage("");
		displayID923.setText("aaaaaaaaaaaa");

		displayID922.add(displayID923);
		eu.sarunas.junit.TestsHelper.set(tolAxisAngleY877, "displayID", displayID922);
		java.util.ArrayList<org.inspection_plusplus.History> history924 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(tolAxisAngleY877, "history", history924);

		tolerance876.add(tolAxisAngleY877);
		org.inspection_plusplus.TolAngle tolAngle925 = new org.inspection_plusplus.TolAngle();
		org.inspection_plusplus.IntervalToleranceCriterion intervalToleranceCriterion926 = new org.inspection_plusplus.IntervalToleranceCriterion();
		intervalToleranceCriterion926.setInOut("aaaaaaaaaaaaa");
		intervalToleranceCriterion926.setLowerToleranceValue(new java.math.BigDecimal(-451.82));
		intervalToleranceCriterion926.setUpperToleranceValue(new java.math.BigDecimal(-146.34));

		tolAngle925.setCriterion(intervalToleranceCriterion926);
		tolAngle925.setStandardConformity("");
		org.inspection_plusplus.ToleranceSource toleranceSource927 = new org.inspection_plusplus.ToleranceSource();
		toleranceSource927.setTyp("a");
		toleranceSource927.setValidFrom(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 35, 17)));
		toleranceSource927.setValidTo(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 25, 30)));
		toleranceSource927.setName("aaaaaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects928 = new org.inspection_plusplus.ID4Objects();
		iD4Objects928.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension929 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects928, "extension", extension929);

		toleranceSource927.setSystemID(iD4Objects928);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef930 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(toleranceSource927, "commentRef", commentRef930);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension931 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(toleranceSource927, "extension", extension931);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID932 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID933 = new org.inspection_plusplus.DisplayID();
		displayID933.setLanguage("aaaaaaaaaaa");
		displayID933.setText("aa");

		displayID932.add(displayID933);
		eu.sarunas.junit.TestsHelper.set(toleranceSource927, "displayID", displayID932);
		java.util.ArrayList<org.inspection_plusplus.History> history934 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history935 = new org.inspection_plusplus.History();
		history935.setAction("");
		history935.setAuthor("aaaaaaaaaaaaaa");
		history935.setComment("");
		history935.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 46, 23)));

		history934.add(history935);
		eu.sarunas.junit.TestsHelper.set(toleranceSource927, "history", history934);

		tolAngle925.setToleranceSource(toleranceSource927);
		tolAngle925.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects936 = new org.inspection_plusplus.ID4Objects();
		iD4Objects936.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension937 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension938 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any939 = new java.util.ArrayList<Object>();
		any939.add(new Object());
		any939.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension938, "any", any939);
		extension938.setVendor("");

		extension937.add(extension938);
		eu.sarunas.junit.TestsHelper.set(iD4Objects936, "extension", extension937);

		tolAngle925.setSystemID(iD4Objects936);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef940 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing941 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing941.setClassID("");
		iD4Referencing941.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension942 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension943 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any944 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension943, "any", any944);
		extension943.setVendor("");

		extension942.add(extension943);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing941, "extension", extension942);

		commentRef940.add(iD4Referencing941);
		eu.sarunas.junit.TestsHelper.set(tolAngle925, "commentRef", commentRef940);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension945 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension946 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any947 = new java.util.ArrayList<Object>();
		any947.add(new Object());
		any947.add(new Object());
		any947.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension946, "any", any947);
		extension946.setVendor("aaaaaaa");

		extension945.add(extension946);
		org.inspection_plusplus.Extension extension948 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any949 = new java.util.ArrayList<Object>();
		any949.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension948, "any", any949);
		extension948.setVendor("aaaa");

		extension945.add(extension948);
		eu.sarunas.junit.TestsHelper.set(tolAngle925, "extension", extension945);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID950 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(tolAngle925, "displayID", displayID950);
		java.util.ArrayList<org.inspection_plusplus.History> history951 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history952 = new org.inspection_plusplus.History();
		history952.setAction("aaaaaaaaaa");
		history952.setAuthor("aaa");
		history952.setComment("");
		history952.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 39, 7)));

		history951.add(history952);
		eu.sarunas.junit.TestsHelper.set(tolAngle925, "history", history951);

		tolerance876.add(tolAngle925);
		org.inspection_plusplus.TolDistance tolDistance953 = new org.inspection_plusplus.TolDistance();
		org.inspection_plusplus.IntervalToleranceCriterion intervalToleranceCriterion954 = new org.inspection_plusplus.IntervalToleranceCriterion();
		intervalToleranceCriterion954.setInOut("aa");
		intervalToleranceCriterion954.setLowerToleranceValue(new java.math.BigDecimal(-622.41));
		intervalToleranceCriterion954.setUpperToleranceValue(new java.math.BigDecimal(-201.75));

		tolDistance953.setCriterion(intervalToleranceCriterion954);
		tolDistance953.setStandardConformity("aaaaaaaaaaaaaa");
		org.inspection_plusplus.ToleranceSource toleranceSource955 = new org.inspection_plusplus.ToleranceSource();
		toleranceSource955.setTyp("");
		toleranceSource955.setValidFrom(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 55, 0)));
		toleranceSource955.setValidTo(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 44, 8)));
		toleranceSource955.setName("aaa");
		org.inspection_plusplus.ID4Objects iD4Objects956 = new org.inspection_plusplus.ID4Objects();
		iD4Objects956.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension957 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects956, "extension", extension957);

		toleranceSource955.setSystemID(iD4Objects956);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef958 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(toleranceSource955, "commentRef", commentRef958);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension959 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension960 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any961 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension960, "any", any961);
		extension960.setVendor("aaaaaaaaa");

		extension959.add(extension960);
		org.inspection_plusplus.Extension extension962 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any963 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension962, "any", any963);
		extension962.setVendor("");

		extension959.add(extension962);
		eu.sarunas.junit.TestsHelper.set(toleranceSource955, "extension", extension959);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID964 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID965 = new org.inspection_plusplus.DisplayID();
		displayID965.setLanguage("");
		displayID965.setText("aaaaa");

		displayID964.add(displayID965);
		org.inspection_plusplus.DisplayID displayID966 = new org.inspection_plusplus.DisplayID();
		displayID966.setLanguage("");
		displayID966.setText("aaaaaa");

		displayID964.add(displayID966);
		eu.sarunas.junit.TestsHelper.set(toleranceSource955, "displayID", displayID964);
		java.util.ArrayList<org.inspection_plusplus.History> history967 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history968 = new org.inspection_plusplus.History();
		history968.setAction("aa");
		history968.setAuthor("aaaaaaaaaa");
		history968.setComment("");
		history968.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 31, 36)));

		history967.add(history968);
		eu.sarunas.junit.TestsHelper.set(toleranceSource955, "history", history967);

		tolDistance953.setToleranceSource(toleranceSource955);
		tolDistance953.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects969 = new org.inspection_plusplus.ID4Objects();
		iD4Objects969.setUuid("aaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension970 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects969, "extension", extension970);

		tolDistance953.setSystemID(iD4Objects969);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef971 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing972 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing972.setClassID("");
		iD4Referencing972.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension973 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension974 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any975 = new java.util.ArrayList<Object>();
		any975.add(new Object());
		any975.add(new Object());
		any975.add(new Object());
		any975.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension974, "any", any975);
		extension974.setVendor("aaaa");

		extension973.add(extension974);
		org.inspection_plusplus.Extension extension976 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any977 = new java.util.ArrayList<Object>();
		any977.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension976, "any", any977);
		extension976.setVendor("aaaaaaaaaa");

		extension973.add(extension976);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing972, "extension", extension973);

		commentRef971.add(iD4Referencing972);
		eu.sarunas.junit.TestsHelper.set(tolDistance953, "commentRef", commentRef971);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension978 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension979 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any980 = new java.util.ArrayList<Object>();
		any980.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension979, "any", any980);
		extension979.setVendor("");

		extension978.add(extension979);
		org.inspection_plusplus.Extension extension981 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any982 = new java.util.ArrayList<Object>();
		any982.add(new Object());
		any982.add(new Object());
		any982.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension981, "any", any982);
		extension981.setVendor("aaaaaa");

		extension978.add(extension981);
		eu.sarunas.junit.TestsHelper.set(tolDistance953, "extension", extension978);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID983 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(tolDistance953, "displayID", displayID983);
		java.util.ArrayList<org.inspection_plusplus.History> history984 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history985 = new org.inspection_plusplus.History();
		history985.setAction("aaaaaa");
		history985.setAuthor("aaaaaaaaaaaaaaa");
		history985.setComment("aaaaaaa");
		history985.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 44, 28)));

		history984.add(history985);
		eu.sarunas.junit.TestsHelper.set(tolDistance953, "history", history984);

		tolerance876.add(tolDistance953);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan874, "tolerance", tolerance876);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> partRef986 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing987 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing987.setClassID("aaa");
		iD4Referencing987.setUuid("aaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension988 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension989 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any990 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension989, "any", any990);
		extension989.setVendor("");

		extension988.add(extension989);
		org.inspection_plusplus.Extension extension991 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any992 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension991, "any", any992);
		extension991.setVendor("");

		extension988.add(extension991);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing987, "extension", extension988);

		partRef986.add(iD4Referencing987);
		org.inspection_plusplus.ID4Referencing iD4Referencing993 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing993.setClassID("");
		iD4Referencing993.setUuid("aaaaaaaaaaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension994 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension995 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any996 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension995, "any", any996);
		extension995.setVendor("aaaaaaaaaaaaaa");

		extension994.add(extension995);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing993, "extension", extension994);

		partRef986.add(iD4Referencing993);
		org.inspection_plusplus.ID4Referencing iD4Referencing997 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing997.setClassID("");
		iD4Referencing997.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension998 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension999 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1000 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension999, "any", any1000);
		extension999.setVendor("");

		extension998.add(extension999);
		org.inspection_plusplus.Extension extension1001 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1002 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1001, "any", any1002);
		extension1001.setVendor("");

		extension998.add(extension1001);
		org.inspection_plusplus.Extension extension1003 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1004 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1003, "any", any1004);
		extension1003.setVendor("");

		extension998.add(extension1003);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing997, "extension", extension998);

		partRef986.add(iD4Referencing997);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan874, "partRef", partRef986);
		java.util.ArrayList<org.inspection_plusplus.IPE> ipe1005 = new java.util.ArrayList<org.inspection_plusplus.IPE>();
		org.inspection_plusplus.IPEPoint iPEPoint1006 = new org.inspection_plusplus.IPEPoint();
		org.inspection_plusplus.Vector3D vector3D1007 = new org.inspection_plusplus.Vector3D();
		vector3D1007.setX(-29.34);
		vector3D1007.setY(-301.62);
		vector3D1007.setZ(-145.84);

		iPEPoint1006.setMainAxis(vector3D1007);
		iPEPoint1006.setMaterialThickness(-52.02);
		iPEPoint1006.setMoveByMaterialThickness(true);
		iPEPoint1006.setOrientation(null);
		iPEPoint1006.setOrigin(null);
		iPEPoint1006.setTouchDirection(null);
		iPEPoint1006.setAuxiliaryElement(true);
		iPEPoint1006.setCalculatedElement(true);
		iPEPoint1006.setUseLeft(false);
		iPEPoint1006.setUseRight(false);
		org.inspection_plusplus.ID4Referencing iD4Referencing1008 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1008.setClassID("");
		iD4Referencing1008.setUuid("aaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1009 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1010 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1011 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1010, "any", any1011);
		extension1010.setVendor("aaaaaaaaaaaaaaaaaaaa");

		extension1009.add(extension1010);
		org.inspection_plusplus.Extension extension1012 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1013 = new java.util.ArrayList<Object>();
		any1013.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1012, "any", any1013);
		extension1012.setVendor("");

		extension1009.add(extension1012);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1008, "extension", extension1009);

		iPEPoint1006.setGeometricObjectRef(iD4Referencing1008);
		java.util.ArrayList<org.inspection_plusplus.CalculationNominalElement> calculationNominalElement1014 = new java.util.ArrayList<org.inspection_plusplus.CalculationNominalElement>();
		org.inspection_plusplus.CalculationNominalElement calculationNominalElement1015 = new org.inspection_plusplus.CalculationNominalElement();
		calculationNominalElement1015.setOperation("");
		java.util.ArrayList<org.inspection_plusplus.OperationParameter> operationParameter1016 = new java.util.ArrayList<org.inspection_plusplus.OperationParameter>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1015, "operationParameter", operationParameter1016);
		java.util.ArrayList<org.inspection_plusplus.Operand> operand1017 = new java.util.ArrayList<org.inspection_plusplus.Operand>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1015, "operand", operand1017);
		calculationNominalElement1015.setName("aaaaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects1018 = new org.inspection_plusplus.ID4Objects();
		iD4Objects1018.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1019 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects1018, "extension", extension1019);

		calculationNominalElement1015.setSystemID(iD4Objects1018);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef1020 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1015, "commentRef", commentRef1020);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1021 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1022 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1023 = new java.util.ArrayList<Object>();
		any1023.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1022, "any", any1023);
		extension1022.setVendor("aaaaaaa");

		extension1021.add(extension1022);
		org.inspection_plusplus.Extension extension1024 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1025 = new java.util.ArrayList<Object>();
		any1025.add(new Object());
		any1025.add(new Object());
		any1025.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1024, "any", any1025);
		extension1024.setVendor("");

		extension1021.add(extension1024);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1015, "extension", extension1021);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID1026 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID1027 = new org.inspection_plusplus.DisplayID();
		displayID1027.setLanguage("aaa");
		displayID1027.setText("");

		displayID1026.add(displayID1027);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1015, "displayID", displayID1026);
		java.util.ArrayList<org.inspection_plusplus.History> history1028 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history1029 = new org.inspection_plusplus.History();
		history1029.setAction("");
		history1029.setAuthor("");
		history1029.setComment("aaaaa");
		history1029.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 59, 58)));

		history1028.add(history1029);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1015, "history", history1028);

		calculationNominalElement1014.add(calculationNominalElement1015);
		org.inspection_plusplus.CalculationNominalElement calculationNominalElement1030 = new org.inspection_plusplus.CalculationNominalElement();
		calculationNominalElement1030.setOperation("");
		java.util.ArrayList<org.inspection_plusplus.OperationParameter> operationParameter1031 = new java.util.ArrayList<org.inspection_plusplus.OperationParameter>();
		org.inspection_plusplus.OperationParameter operationParameter1032 = new org.inspection_plusplus.OperationParameter();
		org.inspection_plusplus.KeyValueOperatorList keyValueOperatorList1033 = new org.inspection_plusplus.KeyValueOperatorList();
		java.util.ArrayList<org.inspection_plusplus.KeyValueOperator> keyValueOperator1034 = new java.util.ArrayList<org.inspection_plusplus.KeyValueOperator>();
		org.inspection_plusplus.KeyValueOperator keyValueOperator1035 = new org.inspection_plusplus.KeyValueOperator();
		keyValueOperator1035.setKey("");
		keyValueOperator1035.setOperator("aaaaaaaaaaaaaaaaaaa");
		keyValueOperator1035.setValue("aaaaaaaa");

		keyValueOperator1034.add(keyValueOperator1035);
		eu.sarunas.junit.TestsHelper.set(keyValueOperatorList1033, "keyValueOperator", keyValueOperator1034);

		operationParameter1032.setKeyValuePairs(keyValueOperatorList1033);

		operationParameter1031.add(operationParameter1032);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1030, "operationParameter", operationParameter1031);
		java.util.ArrayList<org.inspection_plusplus.Operand> operand1036 = new java.util.ArrayList<org.inspection_plusplus.Operand>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1030, "operand", operand1036);
		calculationNominalElement1030.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects1037 = new org.inspection_plusplus.ID4Objects();
		iD4Objects1037.setUuid("aaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1038 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects1037, "extension", extension1038);

		calculationNominalElement1030.setSystemID(iD4Objects1037);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef1039 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1040 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1040.setClassID("");
		iD4Referencing1040.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1041 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1042 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1043 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1042, "any", any1043);
		extension1042.setVendor("");

		extension1041.add(extension1042);
		org.inspection_plusplus.Extension extension1044 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1045 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1044, "any", any1045);
		extension1044.setVendor("aa");

		extension1041.add(extension1044);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1040, "extension", extension1041);

		commentRef1039.add(iD4Referencing1040);
		org.inspection_plusplus.ID4Referencing iD4Referencing1046 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1046.setClassID("aaaaaaaaaaaaaaa");
		iD4Referencing1046.setUuid("aaaaaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1047 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1048 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1049 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1048, "any", any1049);
		extension1048.setVendor("aa");

		extension1047.add(extension1048);
		org.inspection_plusplus.Extension extension1050 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1051 = new java.util.ArrayList<Object>();
		any1051.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1050, "any", any1051);
		extension1050.setVendor("aaaa");

		extension1047.add(extension1050);
		org.inspection_plusplus.Extension extension1052 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1053 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1052, "any", any1053);
		extension1052.setVendor("aa");

		extension1047.add(extension1052);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1046, "extension", extension1047);

		commentRef1039.add(iD4Referencing1046);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1030, "commentRef", commentRef1039);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1054 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1030, "extension", extension1054);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID1055 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID1056 = new org.inspection_plusplus.DisplayID();
		displayID1056.setLanguage("a");
		displayID1056.setText("");

		displayID1055.add(displayID1056);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1030, "displayID", displayID1055);
		java.util.ArrayList<org.inspection_plusplus.History> history1057 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1030, "history", history1057);

		calculationNominalElement1014.add(calculationNominalElement1030);
		eu.sarunas.junit.TestsHelper.set(iPEPoint1006, "calculationNominalElement", calculationNominalElement1014);
		iPEPoint1006.setDescription("aaaaaaaaa");
		iPEPoint1006.setFunction("aaaaaaaaa");
		iPEPoint1006.setName("a");
		org.inspection_plusplus.ID4Objects iD4Objects1058 = new org.inspection_plusplus.ID4Objects();
		iD4Objects1058.setUuid("aa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1059 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects1058, "extension", extension1059);

		iPEPoint1006.setSystemID(iD4Objects1058);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef1060 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1061 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1061.setClassID("aaaa");
		iD4Referencing1061.setUuid("aaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1062 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1061, "extension", extension1062);

		commentRef1060.add(iD4Referencing1061);
		eu.sarunas.junit.TestsHelper.set(iPEPoint1006, "commentRef", commentRef1060);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1063 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1064 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1065 = new java.util.ArrayList<Object>();
		any1065.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1064, "any", any1065);
		extension1064.setVendor("aaaaaaaaa");

		extension1063.add(extension1064);
		eu.sarunas.junit.TestsHelper.set(iPEPoint1006, "extension", extension1063);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID1066 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID1067 = new org.inspection_plusplus.DisplayID();
		displayID1067.setLanguage("");
		displayID1067.setText("");

		displayID1066.add(displayID1067);
		eu.sarunas.junit.TestsHelper.set(iPEPoint1006, "displayID", displayID1066);
		java.util.ArrayList<org.inspection_plusplus.History> history1068 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history1069 = new org.inspection_plusplus.History();
		history1069.setAction("aaaaaaaaaaaaaaaaaaaa");
		history1069.setAuthor("aaaa");
		history1069.setComment("");
		history1069.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 38, 19)));

		history1068.add(history1069);
		eu.sarunas.junit.TestsHelper.set(iPEPoint1006, "history", history1068);

		ipe1005.add(iPEPoint1006);
		org.inspection_plusplus.IPEPoint iPEPoint1070 = new org.inspection_plusplus.IPEPoint();
		org.inspection_plusplus.Vector3D vector3D1071 = new org.inspection_plusplus.Vector3D();
		vector3D1071.setX(-295.47);
		vector3D1071.setY(42.99);
		vector3D1071.setZ(-129.77);

		iPEPoint1070.setMainAxis(vector3D1071);
		iPEPoint1070.setMaterialThickness(-346.92);
		iPEPoint1070.setMoveByMaterialThickness(true);
		iPEPoint1070.setOrientation(null);
		iPEPoint1070.setOrigin(null);
		iPEPoint1070.setTouchDirection(null);
		iPEPoint1070.setAuxiliaryElement(false);
		iPEPoint1070.setCalculatedElement(true);
		iPEPoint1070.setUseLeft(false);
		iPEPoint1070.setUseRight(false);
		org.inspection_plusplus.ID4Referencing iD4Referencing1072 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1072.setClassID("aaaaaaaaa");
		iD4Referencing1072.setUuid("aaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1073 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1074 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1075 = new java.util.ArrayList<Object>();
		any1075.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1074, "any", any1075);
		extension1074.setVendor("");

		extension1073.add(extension1074);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1072, "extension", extension1073);

		iPEPoint1070.setGeometricObjectRef(iD4Referencing1072);
		java.util.ArrayList<org.inspection_plusplus.CalculationNominalElement> calculationNominalElement1076 = new java.util.ArrayList<org.inspection_plusplus.CalculationNominalElement>();
		eu.sarunas.junit.TestsHelper.set(iPEPoint1070, "calculationNominalElement", calculationNominalElement1076);
		iPEPoint1070.setDescription("");
		iPEPoint1070.setFunction("aaaaa");
		iPEPoint1070.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects1077 = new org.inspection_plusplus.ID4Objects();
		iD4Objects1077.setUuid("aaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1078 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects1077, "extension", extension1078);

		iPEPoint1070.setSystemID(iD4Objects1077);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef1079 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(iPEPoint1070, "commentRef", commentRef1079);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1080 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iPEPoint1070, "extension", extension1080);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID1081 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID1082 = new org.inspection_plusplus.DisplayID();
		displayID1082.setLanguage("a");
		displayID1082.setText("");

		displayID1081.add(displayID1082);
		org.inspection_plusplus.DisplayID displayID1083 = new org.inspection_plusplus.DisplayID();
		displayID1083.setLanguage("aaaaaaaa");
		displayID1083.setText("aaaaaaaaaaaaaaaaaaa");

		displayID1081.add(displayID1083);
		org.inspection_plusplus.DisplayID displayID1084 = new org.inspection_plusplus.DisplayID();
		displayID1084.setLanguage("");
		displayID1084.setText("aaaaaaaa");

		displayID1081.add(displayID1084);
		eu.sarunas.junit.TestsHelper.set(iPEPoint1070, "displayID", displayID1081);
		java.util.ArrayList<org.inspection_plusplus.History> history1085 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(iPEPoint1070, "history", history1085);

		ipe1005.add(iPEPoint1070);
		org.inspection_plusplus.IPESurfacePoint iPESurfacePoint1086 = new org.inspection_plusplus.IPESurfacePoint();
		org.inspection_plusplus.Vector3D vector3D1087 = new org.inspection_plusplus.Vector3D();
		vector3D1087.setX(-462.46);
		vector3D1087.setY(-263.76);
		vector3D1087.setZ(-113.13);

		iPESurfacePoint1086.setMainAxis(vector3D1087);
		iPESurfacePoint1086.setMaterialThickness(-566.79);
		iPESurfacePoint1086.setMoveByMaterialThickness(false);
		iPESurfacePoint1086.setOrientation(null);
		iPESurfacePoint1086.setOrigin(null);
		iPESurfacePoint1086.setTouchDirection(null);
		iPESurfacePoint1086.setAuxiliaryElement(false);
		iPESurfacePoint1086.setCalculatedElement(false);
		iPESurfacePoint1086.setUseLeft(true);
		iPESurfacePoint1086.setUseRight(false);
		org.inspection_plusplus.ID4Referencing iD4Referencing1088 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1088.setClassID("aaaa");
		iD4Referencing1088.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1089 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1090 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1091 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1090, "any", any1091);
		extension1090.setVendor("aaaa");

		extension1089.add(extension1090);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1088, "extension", extension1089);

		iPESurfacePoint1086.setGeometricObjectRef(iD4Referencing1088);
		java.util.ArrayList<org.inspection_plusplus.CalculationNominalElement> calculationNominalElement1092 = new java.util.ArrayList<org.inspection_plusplus.CalculationNominalElement>();
		org.inspection_plusplus.CalculationNominalElement calculationNominalElement1093 = new org.inspection_plusplus.CalculationNominalElement();
		calculationNominalElement1093.setOperation("aaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.OperationParameter> operationParameter1094 = new java.util.ArrayList<org.inspection_plusplus.OperationParameter>();
		org.inspection_plusplus.OperationParameter operationParameter1095 = new org.inspection_plusplus.OperationParameter();
		org.inspection_plusplus.KeyValueOperatorList keyValueOperatorList1096 = new org.inspection_plusplus.KeyValueOperatorList();
		java.util.ArrayList<org.inspection_plusplus.KeyValueOperator> keyValueOperator1097 = new java.util.ArrayList<org.inspection_plusplus.KeyValueOperator>();
		eu.sarunas.junit.TestsHelper.set(keyValueOperatorList1096, "keyValueOperator", keyValueOperator1097);

		operationParameter1095.setKeyValuePairs(keyValueOperatorList1096);

		operationParameter1094.add(operationParameter1095);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1093, "operationParameter", operationParameter1094);
		java.util.ArrayList<org.inspection_plusplus.Operand> operand1098 = new java.util.ArrayList<org.inspection_plusplus.Operand>();
		org.inspection_plusplus.Operand operand1099 = new org.inspection_plusplus.Operand();
		operand1099.setIndex(new java.math.BigInteger("-91"));
		operand1099.setRole("aaaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> referenceSystemRef1100 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1101 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1101.setClassID("aaaaaaaaaaa");
		iD4Referencing1101.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1102 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1101, "extension", extension1102);

		referenceSystemRef1100.add(iD4Referencing1101);
		org.inspection_plusplus.ID4Referencing iD4Referencing1103 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1103.setClassID("aa");
		iD4Referencing1103.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1104 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1105 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1106 = new java.util.ArrayList<Object>();
		any1106.add(new Object());
		any1106.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1105, "any", any1106);
		extension1105.setVendor("aa");

		extension1104.add(extension1105);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1103, "extension", extension1104);

		referenceSystemRef1100.add(iD4Referencing1103);
		org.inspection_plusplus.ID4Referencing iD4Referencing1107 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1107.setClassID("aaaaaaaa");
		iD4Referencing1107.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1108 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1109 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1110 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1109, "any", any1110);
		extension1109.setVendor("");

		extension1108.add(extension1109);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1107, "extension", extension1108);

		referenceSystemRef1100.add(iD4Referencing1107);
		eu.sarunas.junit.TestsHelper.set(operand1099, "referenceSystemRef", referenceSystemRef1100);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeGeometricRef1111 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1112 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1112.setClassID("");
		iD4Referencing1112.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1113 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1114 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1115 = new java.util.ArrayList<Object>();
		any1115.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1114, "any", any1115);
		extension1114.setVendor("");

		extension1113.add(extension1114);
		org.inspection_plusplus.Extension extension1116 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1117 = new java.util.ArrayList<Object>();
		any1117.add(new Object());
		any1117.add(new Object());
		any1117.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1116, "any", any1117);
		extension1116.setVendor("");

		extension1113.add(extension1116);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1112, "extension", extension1113);

		ipeGeometricRef1111.add(iD4Referencing1112);
		org.inspection_plusplus.ID4Referencing iD4Referencing1118 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1118.setClassID("");
		iD4Referencing1118.setUuid("aaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1119 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1120 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1121 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1120, "any", any1121);
		extension1120.setVendor("aaa");

		extension1119.add(extension1120);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1118, "extension", extension1119);

		ipeGeometricRef1111.add(iD4Referencing1118);
		eu.sarunas.junit.TestsHelper.set(operand1099, "ipeGeometricRef", ipeGeometricRef1111);

		operand1098.add(operand1099);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1093, "operand", operand1098);
		calculationNominalElement1093.setName("aa");
		org.inspection_plusplus.ID4Objects iD4Objects1122 = new org.inspection_plusplus.ID4Objects();
		iD4Objects1122.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1123 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1124 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1125 = new java.util.ArrayList<Object>();
		any1125.add(new Object());
		any1125.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1124, "any", any1125);
		extension1124.setVendor("");

		extension1123.add(extension1124);
		eu.sarunas.junit.TestsHelper.set(iD4Objects1122, "extension", extension1123);

		calculationNominalElement1093.setSystemID(iD4Objects1122);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef1126 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1093, "commentRef", commentRef1126);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1127 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1093, "extension", extension1127);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID1128 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID1129 = new org.inspection_plusplus.DisplayID();
		displayID1129.setLanguage("aaaaaaaaaa");
		displayID1129.setText("aaaaaaaaaaaa");

		displayID1128.add(displayID1129);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1093, "displayID", displayID1128);
		java.util.ArrayList<org.inspection_plusplus.History> history1130 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history1131 = new org.inspection_plusplus.History();
		history1131.setAction("aaaaa");
		history1131.setAuthor("aaaaaaaaaaaaaaaaaaaaaa");
		history1131.setComment("");
		history1131.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 35, 54)));

		history1130.add(history1131);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1093, "history", history1130);

		calculationNominalElement1092.add(calculationNominalElement1093);
		org.inspection_plusplus.CalculationNominalElement calculationNominalElement1132 = new org.inspection_plusplus.CalculationNominalElement();
		calculationNominalElement1132.setOperation("aaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.OperationParameter> operationParameter1133 = new java.util.ArrayList<org.inspection_plusplus.OperationParameter>();
		org.inspection_plusplus.OperationParameter operationParameter1134 = new org.inspection_plusplus.OperationParameter();
		org.inspection_plusplus.KeyValueOperatorList keyValueOperatorList1135 = new org.inspection_plusplus.KeyValueOperatorList();
		java.util.ArrayList<org.inspection_plusplus.KeyValueOperator> keyValueOperator1136 = new java.util.ArrayList<org.inspection_plusplus.KeyValueOperator>();
		org.inspection_plusplus.KeyValueOperator keyValueOperator1137 = new org.inspection_plusplus.KeyValueOperator();
		keyValueOperator1137.setKey("a");
		keyValueOperator1137.setOperator("aaaaaaaaaaaaaa");
		keyValueOperator1137.setValue("aaaaaa");

		keyValueOperator1136.add(keyValueOperator1137);
		eu.sarunas.junit.TestsHelper.set(keyValueOperatorList1135, "keyValueOperator", keyValueOperator1136);

		operationParameter1134.setKeyValuePairs(keyValueOperatorList1135);

		operationParameter1133.add(operationParameter1134);
		org.inspection_plusplus.OperationParameter operationParameter1138 = new org.inspection_plusplus.OperationParameter();
		org.inspection_plusplus.KeyValueOperatorList keyValueOperatorList1139 = new org.inspection_plusplus.KeyValueOperatorList();
		java.util.ArrayList<org.inspection_plusplus.KeyValueOperator> keyValueOperator1140 = new java.util.ArrayList<org.inspection_plusplus.KeyValueOperator>();
		eu.sarunas.junit.TestsHelper.set(keyValueOperatorList1139, "keyValueOperator", keyValueOperator1140);

		operationParameter1138.setKeyValuePairs(keyValueOperatorList1139);

		operationParameter1133.add(operationParameter1138);
		org.inspection_plusplus.OperationParameter operationParameter1141 = new org.inspection_plusplus.OperationParameter();
		org.inspection_plusplus.KeyValueOperatorList keyValueOperatorList1142 = new org.inspection_plusplus.KeyValueOperatorList();
		java.util.ArrayList<org.inspection_plusplus.KeyValueOperator> keyValueOperator1143 = new java.util.ArrayList<org.inspection_plusplus.KeyValueOperator>();
		eu.sarunas.junit.TestsHelper.set(keyValueOperatorList1142, "keyValueOperator", keyValueOperator1143);

		operationParameter1141.setKeyValuePairs(keyValueOperatorList1142);

		operationParameter1133.add(operationParameter1141);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1132, "operationParameter", operationParameter1133);
		java.util.ArrayList<org.inspection_plusplus.Operand> operand1144 = new java.util.ArrayList<org.inspection_plusplus.Operand>();
		org.inspection_plusplus.Operand operand1145 = new org.inspection_plusplus.Operand();
		operand1145.setIndex(new java.math.BigInteger("-369"));
		operand1145.setRole("aaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> referenceSystemRef1146 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1147 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1147.setClassID("aaaaaaaaa");
		iD4Referencing1147.setUuid("aa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1148 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1147, "extension", extension1148);

		referenceSystemRef1146.add(iD4Referencing1147);
		org.inspection_plusplus.ID4Referencing iD4Referencing1149 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1149.setClassID("");
		iD4Referencing1149.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1150 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1151 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1152 = new java.util.ArrayList<Object>();
		any1152.add(new Object());
		any1152.add(new Object());
		any1152.add(new Object());
		any1152.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1151, "any", any1152);
		extension1151.setVendor("");

		extension1150.add(extension1151);
		org.inspection_plusplus.Extension extension1153 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1154 = new java.util.ArrayList<Object>();
		any1154.add(new Object());
		any1154.add(new Object());
		any1154.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1153, "any", any1154);
		extension1153.setVendor("aaa");

		extension1150.add(extension1153);
		org.inspection_plusplus.Extension extension1155 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1156 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1155, "any", any1156);
		extension1155.setVendor("");

		extension1150.add(extension1155);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1149, "extension", extension1150);

		referenceSystemRef1146.add(iD4Referencing1149);
		eu.sarunas.junit.TestsHelper.set(operand1145, "referenceSystemRef", referenceSystemRef1146);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeGeometricRef1157 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1158 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1158.setClassID("");
		iD4Referencing1158.setUuid("aaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1159 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1160 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1161 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1160, "any", any1161);
		extension1160.setVendor("");

		extension1159.add(extension1160);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1158, "extension", extension1159);

		ipeGeometricRef1157.add(iD4Referencing1158);
		eu.sarunas.junit.TestsHelper.set(operand1145, "ipeGeometricRef", ipeGeometricRef1157);

		operand1144.add(operand1145);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1132, "operand", operand1144);
		calculationNominalElement1132.setName("aaa");
		org.inspection_plusplus.ID4Objects iD4Objects1162 = new org.inspection_plusplus.ID4Objects();
		iD4Objects1162.setUuid("aaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1163 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1164 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1165 = new java.util.ArrayList<Object>();
		any1165.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1164, "any", any1165);
		extension1164.setVendor("");

		extension1163.add(extension1164);
		eu.sarunas.junit.TestsHelper.set(iD4Objects1162, "extension", extension1163);

		calculationNominalElement1132.setSystemID(iD4Objects1162);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef1166 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1132, "commentRef", commentRef1166);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1167 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1168 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1169 = new java.util.ArrayList<Object>();
		any1169.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1168, "any", any1169);
		extension1168.setVendor("");

		extension1167.add(extension1168);
		org.inspection_plusplus.Extension extension1170 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1171 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1170, "any", any1171);
		extension1170.setVendor("");

		extension1167.add(extension1170);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1132, "extension", extension1167);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID1172 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1132, "displayID", displayID1172);
		java.util.ArrayList<org.inspection_plusplus.History> history1173 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history1174 = new org.inspection_plusplus.History();
		history1174.setAction("");
		history1174.setAuthor("a");
		history1174.setComment("aaaaaaaaaaaaa");
		history1174.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 19, 11)));

		history1173.add(history1174);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1132, "history", history1173);

		calculationNominalElement1092.add(calculationNominalElement1132);
		eu.sarunas.junit.TestsHelper.set(iPESurfacePoint1086, "calculationNominalElement", calculationNominalElement1092);
		iPESurfacePoint1086.setDescription("aa");
		iPESurfacePoint1086.setFunction("aa");
		iPESurfacePoint1086.setName("aaaaaaaaaaaaaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects1175 = new org.inspection_plusplus.ID4Objects();
		iD4Objects1175.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1176 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1177 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1178 = new java.util.ArrayList<Object>();
		any1178.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1177, "any", any1178);
		extension1177.setVendor("aaaaaaa");

		extension1176.add(extension1177);
		eu.sarunas.junit.TestsHelper.set(iD4Objects1175, "extension", extension1176);

		iPESurfacePoint1086.setSystemID(iD4Objects1175);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef1179 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(iPESurfacePoint1086, "commentRef", commentRef1179);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1180 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1181 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1182 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1181, "any", any1182);
		extension1181.setVendor("a");

		extension1180.add(extension1181);
		eu.sarunas.junit.TestsHelper.set(iPESurfacePoint1086, "extension", extension1180);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID1183 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID1184 = new org.inspection_plusplus.DisplayID();
		displayID1184.setLanguage("");
		displayID1184.setText("");

		displayID1183.add(displayID1184);
		org.inspection_plusplus.DisplayID displayID1185 = new org.inspection_plusplus.DisplayID();
		displayID1185.setLanguage("");
		displayID1185.setText("aaaaaa");

		displayID1183.add(displayID1185);
		org.inspection_plusplus.DisplayID displayID1186 = new org.inspection_plusplus.DisplayID();
		displayID1186.setLanguage("");
		displayID1186.setText("aaaaaaaaaaaaaaaaa");

		displayID1183.add(displayID1186);
		eu.sarunas.junit.TestsHelper.set(iPESurfacePoint1086, "displayID", displayID1183);
		java.util.ArrayList<org.inspection_plusplus.History> history1187 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history1188 = new org.inspection_plusplus.History();
		history1188.setAction("");
		history1188.setAuthor("aaaa");
		history1188.setComment("aaaaaaaaa");
		history1188.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 8, 56)));

		history1187.add(history1188);
		org.inspection_plusplus.History history1189 = new org.inspection_plusplus.History();
		history1189.setAction("");
		history1189.setAuthor("");
		history1189.setComment("");
		history1189.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 50, 16)));

		history1187.add(history1189);
		org.inspection_plusplus.History history1190 = new org.inspection_plusplus.History();
		history1190.setAction("aa");
		history1190.setAuthor("aaaaaaaaaaaaaaaaaaa");
		history1190.setComment("aaaaaa");
		history1190.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 3, 6, 31)));

		history1187.add(history1190);
		eu.sarunas.junit.TestsHelper.set(iPESurfacePoint1086, "history", history1187);

		ipe1005.add(iPESurfacePoint1086);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan874, "ipe", ipe1005);
		java.util.ArrayList<org.inspection_plusplus.ReferenceSystem> referenceSystem1191 = new java.util.ArrayList<org.inspection_plusplus.ReferenceSystem>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan874, "referenceSystem", referenceSystem1191);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> inspectionTaskRef1192 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan874, "inspectionTaskRef", inspectionTaskRef1192);
		java.util.ArrayList<org.inspection_plusplus.QC> qc1193 = new java.util.ArrayList<org.inspection_plusplus.QC>();
		org.inspection_plusplus.QCSingle qCSingle1194 = new org.inspection_plusplus.QCSingle();
		qCSingle1194.setGeoObjectDetail("");
		org.inspection_plusplus.ID4Referencing iD4Referencing1195 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1195.setClassID("");
		iD4Referencing1195.setUuid("aaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1196 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1197 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1198 = new java.util.ArrayList<Object>();
		any1198.add(new Object());
		any1198.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1197, "any", any1198);
		extension1197.setVendor("aaaaaaaaaaaaaa");

		extension1196.add(extension1197);
		org.inspection_plusplus.Extension extension1199 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1200 = new java.util.ArrayList<Object>();
		any1200.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1199, "any", any1200);
		extension1199.setVendor("");

		extension1196.add(extension1199);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1195, "extension", extension1196);

		qCSingle1194.setToleranceRef(iD4Referencing1195);
		qCSingle1194.setAllAssembliesReferenced(false);
		qCSingle1194.setDescription("");
		qCSingle1194.setFunction("aaaaaaaaaaa");
		qCSingle1194.setPartQAversion("");
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> partRef1201 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1202 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1202.setClassID("aaaa");
		iD4Referencing1202.setUuid("aaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1203 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1202, "extension", extension1203);

		partRef1201.add(iD4Referencing1202);
		org.inspection_plusplus.ID4Referencing iD4Referencing1204 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1204.setClassID("");
		iD4Referencing1204.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1205 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1206 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1207 = new java.util.ArrayList<Object>();
		any1207.add(new Object());
		any1207.add(new Object());
		any1207.add(new Object());
		any1207.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1206, "any", any1207);
		extension1206.setVendor("aaa");

		extension1205.add(extension1206);
		org.inspection_plusplus.Extension extension1208 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1209 = new java.util.ArrayList<Object>();
		any1209.add(new Object());
		any1209.add(new Object());
		any1209.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1208, "any", any1209);
		extension1208.setVendor("aaaa");

		extension1205.add(extension1208);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1204, "extension", extension1205);

		partRef1201.add(iD4Referencing1204);
		org.inspection_plusplus.ID4Referencing iD4Referencing1210 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1210.setClassID("");
		iD4Referencing1210.setUuid("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1211 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1212 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1213 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1212, "any", any1213);
		extension1212.setVendor("");

		extension1211.add(extension1212);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1210, "extension", extension1211);

		partRef1201.add(iD4Referencing1210);
		org.inspection_plusplus.ID4Referencing iD4Referencing1214 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1214.setClassID("");
		iD4Referencing1214.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1215 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1216 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1217 = new java.util.ArrayList<Object>();
		any1217.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1216, "any", any1217);
		extension1216.setVendor("aaaaaaaaaaa");

		extension1215.add(extension1216);
		org.inspection_plusplus.Extension extension1218 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1219 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1218, "any", any1219);
		extension1218.setVendor("");

		extension1215.add(extension1218);
		org.inspection_plusplus.Extension extension1220 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1221 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1220, "any", any1221);
		extension1220.setVendor("");

		extension1215.add(extension1220);
		org.inspection_plusplus.Extension extension1222 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1223 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1222, "any", any1223);
		extension1222.setVendor("aaa");

		extension1215.add(extension1222);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1214, "extension", extension1215);

		partRef1201.add(iD4Referencing1214);
		org.inspection_plusplus.ID4Referencing iD4Referencing1224 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1224.setClassID("");
		iD4Referencing1224.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1225 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1226 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1227 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1226, "any", any1227);
		extension1226.setVendor("aaa");

		extension1225.add(extension1226);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1224, "extension", extension1225);

		partRef1201.add(iD4Referencing1224);
		org.inspection_plusplus.ID4Referencing iD4Referencing1228 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1228.setClassID("");
		iD4Referencing1228.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1229 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1230 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1231 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1230, "any", any1231);
		extension1230.setVendor("aaaaaaaaa");

		extension1229.add(extension1230);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1228, "extension", extension1229);

		partRef1201.add(iD4Referencing1228);
		eu.sarunas.junit.TestsHelper.set(qCSingle1194, "partRef", partRef1201);
		java.util.ArrayList<Object> calculationActualElementDetectionRef1232 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(qCSingle1194, "calculationActualElementDetectionRef", calculationActualElementDetectionRef1232);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeRef1233 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1234 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1234.setClassID("");
		iD4Referencing1234.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1235 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1236 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1237 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1236, "any", any1237);
		extension1236.setVendor("");

		extension1235.add(extension1236);
		org.inspection_plusplus.Extension extension1238 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1239 = new java.util.ArrayList<Object>();
		any1239.add(new Object());
		any1239.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1238, "any", any1239);
		extension1238.setVendor("");

		extension1235.add(extension1238);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1234, "extension", extension1235);

		ipeRef1233.add(iD4Referencing1234);
		eu.sarunas.junit.TestsHelper.set(qCSingle1194, "ipeRef", ipeRef1233);
		qCSingle1194.setName("aaaaaaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects1240 = new org.inspection_plusplus.ID4Objects();
		iD4Objects1240.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1241 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1242 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1243 = new java.util.ArrayList<Object>();
		any1243.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1242, "any", any1243);
		extension1242.setVendor("");

		extension1241.add(extension1242);
		org.inspection_plusplus.Extension extension1244 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1245 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1244, "any", any1245);
		extension1244.setVendor("");

		extension1241.add(extension1244);
		eu.sarunas.junit.TestsHelper.set(iD4Objects1240, "extension", extension1241);

		qCSingle1194.setSystemID(iD4Objects1240);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef1246 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1247 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1247.setClassID("aaaaaaa");
		iD4Referencing1247.setUuid("aaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1248 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1249 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1250 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1249, "any", any1250);
		extension1249.setVendor("");

		extension1248.add(extension1249);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1247, "extension", extension1248);

		commentRef1246.add(iD4Referencing1247);
		eu.sarunas.junit.TestsHelper.set(qCSingle1194, "commentRef", commentRef1246);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1251 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1252 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1253 = new java.util.ArrayList<Object>();
		any1253.add(new Object());
		any1253.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1252, "any", any1253);
		extension1252.setVendor("aaaaaaa");

		extension1251.add(extension1252);
		org.inspection_plusplus.Extension extension1254 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1255 = new java.util.ArrayList<Object>();
		any1255.add(new Object());
		any1255.add(new Object());
		any1255.add(new Object());
		any1255.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1254, "any", any1255);
		extension1254.setVendor("");

		extension1251.add(extension1254);
		eu.sarunas.junit.TestsHelper.set(qCSingle1194, "extension", extension1251);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID1256 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(qCSingle1194, "displayID", displayID1256);
		java.util.ArrayList<org.inspection_plusplus.History> history1257 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history1258 = new org.inspection_plusplus.History();
		history1258.setAction("aaaaaaaaaaaaaaaa");
		history1258.setAuthor("");
		history1258.setComment("");
		history1258.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 7, 0)));

		history1257.add(history1258);
		org.inspection_plusplus.History history1259 = new org.inspection_plusplus.History();
		history1259.setAction("aaaaaa");
		history1259.setAuthor("aaaaaaaaaa");
		history1259.setComment("");
		history1259.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 5, 53)));

		history1257.add(history1259);
		eu.sarunas.junit.TestsHelper.set(qCSingle1194, "history", history1257);

		qc1193.add(qCSingle1194);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan874, "qc", qc1193);
		inspectionPlan874.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects1260 = new org.inspection_plusplus.ID4Objects();
		iD4Objects1260.setUuid("aaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1261 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1262 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1263 = new java.util.ArrayList<Object>();
		any1263.add(new Object());
		any1263.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1262, "any", any1263);
		extension1262.setVendor("aaaaaaaa");

		extension1261.add(extension1262);
		org.inspection_plusplus.Extension extension1264 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1265 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1264, "any", any1265);
		extension1264.setVendor("a");

		extension1261.add(extension1264);
		eu.sarunas.junit.TestsHelper.set(iD4Objects1260, "extension", extension1261);

		inspectionPlan874.setSystemID(iD4Objects1260);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef1266 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1267 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1267.setClassID("a");
		iD4Referencing1267.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1268 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1269 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1270 = new java.util.ArrayList<Object>();
		any1270.add(new Object());
		any1270.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1269, "any", any1270);
		extension1269.setVendor("aaaaaaa");

		extension1268.add(extension1269);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1267, "extension", extension1268);

		commentRef1266.add(iD4Referencing1267);
		org.inspection_plusplus.ID4Referencing iD4Referencing1271 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1271.setClassID("");
		iD4Referencing1271.setUuid("aaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1272 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1271, "extension", extension1272);

		commentRef1266.add(iD4Referencing1271);
		org.inspection_plusplus.ID4Referencing iD4Referencing1273 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1273.setClassID("");
		iD4Referencing1273.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1274 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1273, "extension", extension1274);

		commentRef1266.add(iD4Referencing1273);
		org.inspection_plusplus.ID4Referencing iD4Referencing1275 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1275.setClassID("");
		iD4Referencing1275.setUuid("aaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1276 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1275, "extension", extension1276);

		commentRef1266.add(iD4Referencing1275);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan874, "commentRef", commentRef1266);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1277 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan874, "extension", extension1277);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID1278 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID1279 = new org.inspection_plusplus.DisplayID();
		displayID1279.setLanguage("aaaaaaa");
		displayID1279.setText("aaaaaaaaaaaaaa");

		displayID1278.add(displayID1279);
		org.inspection_plusplus.DisplayID displayID1280 = new org.inspection_plusplus.DisplayID();
		displayID1280.setLanguage("");
		displayID1280.setText("aa");

		displayID1278.add(displayID1280);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan874, "displayID", displayID1278);
		java.util.ArrayList<org.inspection_plusplus.History> history1281 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history1282 = new org.inspection_plusplus.History();
		history1282.setAction("aaaaaaaaa");
		history1282.setAuthor("");
		history1282.setComment("aaaaaaaa");
		history1282.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 18, 1)));

		history1281.add(history1282);
		org.inspection_plusplus.History history1283 = new org.inspection_plusplus.History();
		history1283.setAction("");
		history1283.setAuthor("");
		history1283.setComment("");
		history1283.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 53, 5)));

		history1281.add(history1283);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan874, "history", history1281);
		org.inspection_plusplus.Sender sender1284 = new org.inspection_plusplus.Sender();
		sender1284.setAppID("");
		sender1284.setDomain("aaaaa");
		org.inspection_plusplus.Extension extension1285 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1286 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1285, "any", any1286);
		extension1285.setVendor("");

		org.inspection_plusplus.ReturnStatus res = testObject.storeInspectionPlan(inspectionPlan874, "aaaaaa", "", sender1284, extension1285);
		junit.framework.Assert.fail();
	};

	@org.junit.Test
	public void testtestStoreInspectionPlan5() throws Throwable
	{
		org.inspection_plusplus.service.v2.WebServiceImpl testObject = new org.inspection_plusplus.service.v2.WebServiceImpl();
		org.inspection_plusplus.InspectionPlan inspectionPlan1287 = new org.inspection_plusplus.InspectionPlan();
		inspectionPlan1287.setInspectionCategories("aaaaaaa");
		inspectionPlan1287.setIPsymmetry("");
		inspectionPlan1287.setPartQAversion("aaaaa");
		eu.sarunas.junit.TestsHelper.set(inspectionPlan1287, "pdMmaturity", "aaaaa");
		inspectionPlan1287.setQAmaturity("aa");
		inspectionPlan1287.setQAversion("aaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Comment> comment1288 = new java.util.ArrayList<org.inspection_plusplus.Comment>();
		org.inspection_plusplus.Comment comment1289 = new org.inspection_plusplus.Comment();
		comment1289.setAuthor("aaaa");
		comment1289.setReceiver("");
		comment1289.setSubject("aaaaaaaaaaaa");
		comment1289.setText("aaaaaaaaa");
		comment1289.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects1290 = new org.inspection_plusplus.ID4Objects();
		iD4Objects1290.setUuid("aaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1291 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1292 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1293 = new java.util.ArrayList<Object>();
		any1293.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1292, "any", any1293);
		extension1292.setVendor("");

		extension1291.add(extension1292);
		eu.sarunas.junit.TestsHelper.set(iD4Objects1290, "extension", extension1291);

		comment1289.setSystemID(iD4Objects1290);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef1294 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1295 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1295.setClassID("");
		iD4Referencing1295.setUuid("a");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1296 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1297 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1298 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1297, "any", any1298);
		extension1297.setVendor("");

		extension1296.add(extension1297);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1295, "extension", extension1296);

		commentRef1294.add(iD4Referencing1295);
		eu.sarunas.junit.TestsHelper.set(comment1289, "commentRef", commentRef1294);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1299 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1300 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1301 = new java.util.ArrayList<Object>();
		any1301.add(new Object());
		any1301.add(new Object());
		any1301.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1300, "any", any1301);
		extension1300.setVendor("");

		extension1299.add(extension1300);
		eu.sarunas.junit.TestsHelper.set(comment1289, "extension", extension1299);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID1302 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID1303 = new org.inspection_plusplus.DisplayID();
		displayID1303.setLanguage("aaaaaaaaaa");
		displayID1303.setText("");

		displayID1302.add(displayID1303);
		eu.sarunas.junit.TestsHelper.set(comment1289, "displayID", displayID1302);
		java.util.ArrayList<org.inspection_plusplus.History> history1304 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history1305 = new org.inspection_plusplus.History();
		history1305.setAction("");
		history1305.setAuthor("");
		history1305.setComment("aaaaa");
		history1305.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 45, 23)));

		history1304.add(history1305);
		org.inspection_plusplus.History history1306 = new org.inspection_plusplus.History();
		history1306.setAction("aaaaaaaaaaaaaaaaaaa");
		history1306.setAuthor("");
		history1306.setComment("aaaaaaaaaa");
		history1306.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 8, 51)));

		history1304.add(history1306);
		eu.sarunas.junit.TestsHelper.set(comment1289, "history", history1304);

		comment1288.add(comment1289);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan1287, "comment", comment1288);
		java.util.ArrayList<org.inspection_plusplus.Tolerance> tolerance1307 = new java.util.ArrayList<org.inspection_plusplus.Tolerance>();
		org.inspection_plusplus.TolPosition tolPosition1308 = new org.inspection_plusplus.TolPosition();
		org.inspection_plusplus.IntervalToleranceCriterion intervalToleranceCriterion1309 = new org.inspection_plusplus.IntervalToleranceCriterion();
		intervalToleranceCriterion1309.setInOut("");
		intervalToleranceCriterion1309.setLowerToleranceValue(new java.math.BigDecimal(-177.47));
		intervalToleranceCriterion1309.setUpperToleranceValue(new java.math.BigDecimal(98.67));

		tolPosition1308.setCriterion(intervalToleranceCriterion1309);
		org.inspection_plusplus.ReferenceSystem referenceSystem1310 = new org.inspection_plusplus.ReferenceSystem();
		java.util.ArrayList<org.inspection_plusplus.Datum> datum1311 = new java.util.ArrayList<org.inspection_plusplus.Datum>();
		eu.sarunas.junit.TestsHelper.set(referenceSystem1310, "datum", datum1311);
		java.util.ArrayList<org.inspection_plusplus.AlignmentStrategy> alignmentStrategy1312 = new java.util.ArrayList<org.inspection_plusplus.AlignmentStrategy>();
		org.inspection_plusplus.Alignment321 alignment3211313 = new org.inspection_plusplus.Alignment321();
		alignment3211313.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects1314 = new org.inspection_plusplus.ID4Objects();
		iD4Objects1314.setUuid("aaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1315 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects1314, "extension", extension1315);

		alignment3211313.setSystemID(iD4Objects1314);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef1316 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1317 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1317.setClassID("aaaaaaaaaaaaa");
		iD4Referencing1317.setUuid("a");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1318 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1319 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1320 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1319, "any", any1320);
		extension1319.setVendor("aaaaaa");

		extension1318.add(extension1319);
		org.inspection_plusplus.Extension extension1321 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1322 = new java.util.ArrayList<Object>();
		any1322.add(new Object());
		any1322.add(new Object());
		any1322.add(new Object());
		any1322.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1321, "any", any1322);
		extension1321.setVendor("");

		extension1318.add(extension1321);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1317, "extension", extension1318);

		commentRef1316.add(iD4Referencing1317);
		eu.sarunas.junit.TestsHelper.set(alignment3211313, "commentRef", commentRef1316);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1323 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1324 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1325 = new java.util.ArrayList<Object>();
		any1325.add(new Object());
		any1325.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1324, "any", any1325);
		extension1324.setVendor("aaaaaaaaaaaa");

		extension1323.add(extension1324);
		eu.sarunas.junit.TestsHelper.set(alignment3211313, "extension", extension1323);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID1326 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID1327 = new org.inspection_plusplus.DisplayID();
		displayID1327.setLanguage("aaaaaaaa");
		displayID1327.setText("aaaaaaaaaaaa");

		displayID1326.add(displayID1327);
		eu.sarunas.junit.TestsHelper.set(alignment3211313, "displayID", displayID1326);
		java.util.ArrayList<org.inspection_plusplus.History> history1328 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(alignment3211313, "history", history1328);

		alignmentStrategy1312.add(alignment3211313);
		eu.sarunas.junit.TestsHelper.set(referenceSystem1310, "alignmentStrategy", alignmentStrategy1312);
		referenceSystem1310.setName("aaaaaaaaaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects1329 = new org.inspection_plusplus.ID4Objects();
		iD4Objects1329.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1330 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects1329, "extension", extension1330);

		referenceSystem1310.setSystemID(iD4Objects1329);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef1331 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(referenceSystem1310, "commentRef", commentRef1331);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1332 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(referenceSystem1310, "extension", extension1332);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID1333 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID1334 = new org.inspection_plusplus.DisplayID();
		displayID1334.setLanguage("");
		displayID1334.setText("aaaaaa");

		displayID1333.add(displayID1334);
		org.inspection_plusplus.DisplayID displayID1335 = new org.inspection_plusplus.DisplayID();
		displayID1335.setLanguage("aaaaaaaaaaa");
		displayID1335.setText("");

		displayID1333.add(displayID1335);
		eu.sarunas.junit.TestsHelper.set(referenceSystem1310, "displayID", displayID1333);
		java.util.ArrayList<org.inspection_plusplus.History> history1336 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history1337 = new org.inspection_plusplus.History();
		history1337.setAction("");
		history1337.setAuthor("");
		history1337.setComment("aaaaaaaaaaaaaaaaaaaaa");
		history1337.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 56, 20)));

		history1336.add(history1337);
		eu.sarunas.junit.TestsHelper.set(referenceSystem1310, "history", history1336);

		tolPosition1308.setReferenceSystem(referenceSystem1310);
		tolPosition1308.setStandardConformity("");
		org.inspection_plusplus.ToleranceSource toleranceSource1338 = new org.inspection_plusplus.ToleranceSource();
		toleranceSource1338.setTyp("aaaaaaaaaaaaa");
		toleranceSource1338.setValidFrom(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 18, 19)));
		toleranceSource1338.setValidTo(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 4, 54)));
		toleranceSource1338.setName("aaaaaaaaaaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects1339 = new org.inspection_plusplus.ID4Objects();
		iD4Objects1339.setUuid("aaaaaaaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1340 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1341 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1342 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1341, "any", any1342);
		extension1341.setVendor("");

		extension1340.add(extension1341);
		eu.sarunas.junit.TestsHelper.set(iD4Objects1339, "extension", extension1340);

		toleranceSource1338.setSystemID(iD4Objects1339);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef1343 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1344 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1344.setClassID("");
		iD4Referencing1344.setUuid("aaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1345 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1344, "extension", extension1345);

		commentRef1343.add(iD4Referencing1344);
		org.inspection_plusplus.ID4Referencing iD4Referencing1346 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1346.setClassID("aa");
		iD4Referencing1346.setUuid("aaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1347 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1348 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1349 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1348, "any", any1349);
		extension1348.setVendor("");

		extension1347.add(extension1348);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1346, "extension", extension1347);

		commentRef1343.add(iD4Referencing1346);
		org.inspection_plusplus.ID4Referencing iD4Referencing1350 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1350.setClassID("");
		iD4Referencing1350.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1351 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1352 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1353 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1352, "any", any1353);
		extension1352.setVendor("");

		extension1351.add(extension1352);
		org.inspection_plusplus.Extension extension1354 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1355 = new java.util.ArrayList<Object>();
		any1355.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1354, "any", any1355);
		extension1354.setVendor("");

		extension1351.add(extension1354);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1350, "extension", extension1351);

		commentRef1343.add(iD4Referencing1350);
		eu.sarunas.junit.TestsHelper.set(toleranceSource1338, "commentRef", commentRef1343);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1356 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(toleranceSource1338, "extension", extension1356);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID1357 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID1358 = new org.inspection_plusplus.DisplayID();
		displayID1358.setLanguage("aaaaaa");
		displayID1358.setText("aaaaaaaaaaaaa");

		displayID1357.add(displayID1358);
		eu.sarunas.junit.TestsHelper.set(toleranceSource1338, "displayID", displayID1357);
		java.util.ArrayList<org.inspection_plusplus.History> history1359 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history1360 = new org.inspection_plusplus.History();
		history1360.setAction("");
		history1360.setAuthor("");
		history1360.setComment("aaaaaaa");
		history1360.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 21, 31)));

		history1359.add(history1360);
		org.inspection_plusplus.History history1361 = new org.inspection_plusplus.History();
		history1361.setAction("");
		history1361.setAuthor("aaaaa");
		history1361.setComment("aaaaaaaaa");
		history1361.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 50, 31)));

		history1359.add(history1361);
		org.inspection_plusplus.History history1362 = new org.inspection_plusplus.History();
		history1362.setAction("a");
		history1362.setAuthor("");
		history1362.setComment("a");
		history1362.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 46, 40)));

		history1359.add(history1362);
		org.inspection_plusplus.History history1363 = new org.inspection_plusplus.History();
		history1363.setAction("aa");
		history1363.setAuthor("");
		history1363.setComment("");
		history1363.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 28, 0)));

		history1359.add(history1363);
		eu.sarunas.junit.TestsHelper.set(toleranceSource1338, "history", history1359);

		tolPosition1308.setToleranceSource(toleranceSource1338);
		tolPosition1308.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects1364 = new org.inspection_plusplus.ID4Objects();
		iD4Objects1364.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1365 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1366 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1367 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1366, "any", any1367);
		extension1366.setVendor("");

		extension1365.add(extension1366);
		org.inspection_plusplus.Extension extension1368 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1369 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1368, "any", any1369);
		extension1368.setVendor("aaaaaaaaaaaaaaaa");

		extension1365.add(extension1368);
		eu.sarunas.junit.TestsHelper.set(iD4Objects1364, "extension", extension1365);

		tolPosition1308.setSystemID(iD4Objects1364);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef1370 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1371 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1371.setClassID("");
		iD4Referencing1371.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1372 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1371, "extension", extension1372);

		commentRef1370.add(iD4Referencing1371);
		org.inspection_plusplus.ID4Referencing iD4Referencing1373 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1373.setClassID("");
		iD4Referencing1373.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1374 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1375 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1376 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1375, "any", any1376);
		extension1375.setVendor("");

		extension1374.add(extension1375);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1373, "extension", extension1374);

		commentRef1370.add(iD4Referencing1373);
		eu.sarunas.junit.TestsHelper.set(tolPosition1308, "commentRef", commentRef1370);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1377 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1378 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1379 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1378, "any", any1379);
		extension1378.setVendor("aaaaaaaaaaaaaaa");

		extension1377.add(extension1378);
		eu.sarunas.junit.TestsHelper.set(tolPosition1308, "extension", extension1377);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID1380 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(tolPosition1308, "displayID", displayID1380);
		java.util.ArrayList<org.inspection_plusplus.History> history1381 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(tolPosition1308, "history", history1381);

		tolerance1307.add(tolPosition1308);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan1287, "tolerance", tolerance1307);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> partRef1382 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1383 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1383.setClassID("");
		iD4Referencing1383.setUuid("aaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1384 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1385 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1386 = new java.util.ArrayList<Object>();
		any1386.add(new Object());
		any1386.add(new Object());
		any1386.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1385, "any", any1386);
		extension1385.setVendor("");

		extension1384.add(extension1385);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1383, "extension", extension1384);

		partRef1382.add(iD4Referencing1383);
		org.inspection_plusplus.ID4Referencing iD4Referencing1387 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1387.setClassID("");
		iD4Referencing1387.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1388 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1387, "extension", extension1388);

		partRef1382.add(iD4Referencing1387);
		org.inspection_plusplus.ID4Referencing iD4Referencing1389 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1389.setClassID("");
		iD4Referencing1389.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1390 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1391 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1392 = new java.util.ArrayList<Object>();
		any1392.add(new Object());
		any1392.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1391, "any", any1392);
		extension1391.setVendor("");

		extension1390.add(extension1391);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1389, "extension", extension1390);

		partRef1382.add(iD4Referencing1389);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan1287, "partRef", partRef1382);
		java.util.ArrayList<org.inspection_plusplus.IPE> ipe1393 = new java.util.ArrayList<org.inspection_plusplus.IPE>();
		org.inspection_plusplus.IPEGeometric iPEGeometric1394 = new org.inspection_plusplus.IPEGeometric();
		iPEGeometric1394.setMaterialThickness(-10.83);
		iPEGeometric1394.setMoveByMaterialThickness(true);
		org.inspection_plusplus.Vector3D vector3D1395 = new org.inspection_plusplus.Vector3D();
		vector3D1395.setX(-150.21);
		vector3D1395.setY(-235.73);
		vector3D1395.setZ(264.73);

		iPEGeometric1394.setOrientation(vector3D1395);
		iPEGeometric1394.setOrigin(null);
		iPEGeometric1394.setTouchDirection(null);
		iPEGeometric1394.setAuxiliaryElement(false);
		iPEGeometric1394.setCalculatedElement(true);
		iPEGeometric1394.setUseLeft(false);
		iPEGeometric1394.setUseRight(false);
		org.inspection_plusplus.ID4Referencing iD4Referencing1396 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1396.setClassID("");
		iD4Referencing1396.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1397 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1396, "extension", extension1397);

		iPEGeometric1394.setGeometricObjectRef(iD4Referencing1396);
		java.util.ArrayList<org.inspection_plusplus.CalculationNominalElement> calculationNominalElement1398 = new java.util.ArrayList<org.inspection_plusplus.CalculationNominalElement>();
		org.inspection_plusplus.CalculationNominalElement calculationNominalElement1399 = new org.inspection_plusplus.CalculationNominalElement();
		calculationNominalElement1399.setOperation("");
		java.util.ArrayList<org.inspection_plusplus.OperationParameter> operationParameter1400 = new java.util.ArrayList<org.inspection_plusplus.OperationParameter>();
		org.inspection_plusplus.OperationParameter operationParameter1401 = new org.inspection_plusplus.OperationParameter();
		org.inspection_plusplus.KeyValueOperatorList keyValueOperatorList1402 = new org.inspection_plusplus.KeyValueOperatorList();
		java.util.ArrayList<org.inspection_plusplus.KeyValueOperator> keyValueOperator1403 = new java.util.ArrayList<org.inspection_plusplus.KeyValueOperator>();
		eu.sarunas.junit.TestsHelper.set(keyValueOperatorList1402, "keyValueOperator", keyValueOperator1403);

		operationParameter1401.setKeyValuePairs(keyValueOperatorList1402);

		operationParameter1400.add(operationParameter1401);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1399, "operationParameter", operationParameter1400);
		java.util.ArrayList<org.inspection_plusplus.Operand> operand1404 = new java.util.ArrayList<org.inspection_plusplus.Operand>();
		org.inspection_plusplus.Operand operand1405 = new org.inspection_plusplus.Operand();
		operand1405.setIndex(new java.math.BigInteger("-512"));
		operand1405.setRole("aaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> referenceSystemRef1406 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1407 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1407.setClassID("aaaaaaaaaaaaaaa");
		iD4Referencing1407.setUuid("aaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1408 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1409 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1410 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1409, "any", any1410);
		extension1409.setVendor("aaaaaaaaaa");

		extension1408.add(extension1409);
		org.inspection_plusplus.Extension extension1411 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1412 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1411, "any", any1412);
		extension1411.setVendor("");

		extension1408.add(extension1411);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1407, "extension", extension1408);

		referenceSystemRef1406.add(iD4Referencing1407);
		eu.sarunas.junit.TestsHelper.set(operand1405, "referenceSystemRef", referenceSystemRef1406);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeGeometricRef1413 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1414 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1414.setClassID("a");
		iD4Referencing1414.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1415 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1414, "extension", extension1415);

		ipeGeometricRef1413.add(iD4Referencing1414);
		org.inspection_plusplus.ID4Referencing iD4Referencing1416 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1416.setClassID("aaaa");
		iD4Referencing1416.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1417 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1418 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1419 = new java.util.ArrayList<Object>();
		any1419.add(new Object());
		any1419.add(new Object());
		any1419.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1418, "any", any1419);
		extension1418.setVendor("aaaa");

		extension1417.add(extension1418);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1416, "extension", extension1417);

		ipeGeometricRef1413.add(iD4Referencing1416);
		org.inspection_plusplus.ID4Referencing iD4Referencing1420 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1420.setClassID("a");
		iD4Referencing1420.setUuid("aaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1421 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1422 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1423 = new java.util.ArrayList<Object>();
		any1423.add(new Object());
		any1423.add(new Object());
		any1423.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1422, "any", any1423);
		extension1422.setVendor("");

		extension1421.add(extension1422);
		org.inspection_plusplus.Extension extension1424 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1425 = new java.util.ArrayList<Object>();
		any1425.add(new Object());
		any1425.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1424, "any", any1425);
		extension1424.setVendor("aaaa");

		extension1421.add(extension1424);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1420, "extension", extension1421);

		ipeGeometricRef1413.add(iD4Referencing1420);
		eu.sarunas.junit.TestsHelper.set(operand1405, "ipeGeometricRef", ipeGeometricRef1413);

		operand1404.add(operand1405);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1399, "operand", operand1404);
		calculationNominalElement1399.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects1426 = new org.inspection_plusplus.ID4Objects();
		iD4Objects1426.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1427 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects1426, "extension", extension1427);

		calculationNominalElement1399.setSystemID(iD4Objects1426);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef1428 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1399, "commentRef", commentRef1428);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1429 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1399, "extension", extension1429);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID1430 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID1431 = new org.inspection_plusplus.DisplayID();
		displayID1431.setLanguage("");
		displayID1431.setText("aaaaaaaaaaaaaaaaaa");

		displayID1430.add(displayID1431);
		org.inspection_plusplus.DisplayID displayID1432 = new org.inspection_plusplus.DisplayID();
		displayID1432.setLanguage("aaaa");
		displayID1432.setText("aaaaaa");

		displayID1430.add(displayID1432);
		org.inspection_plusplus.DisplayID displayID1433 = new org.inspection_plusplus.DisplayID();
		displayID1433.setLanguage("");
		displayID1433.setText("");

		displayID1430.add(displayID1433);
		org.inspection_plusplus.DisplayID displayID1434 = new org.inspection_plusplus.DisplayID();
		displayID1434.setLanguage("");
		displayID1434.setText("");

		displayID1430.add(displayID1434);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1399, "displayID", displayID1430);
		java.util.ArrayList<org.inspection_plusplus.History> history1435 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history1436 = new org.inspection_plusplus.History();
		history1436.setAction("a");
		history1436.setAuthor("");
		history1436.setComment("");
		history1436.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 43, 24)));

		history1435.add(history1436);
		org.inspection_plusplus.History history1437 = new org.inspection_plusplus.History();
		history1437.setAction("aaaa");
		history1437.setAuthor("");
		history1437.setComment("");
		history1437.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 10, 44)));

		history1435.add(history1437);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1399, "history", history1435);

		calculationNominalElement1398.add(calculationNominalElement1399);
		org.inspection_plusplus.CalculationNominalElement calculationNominalElement1438 = new org.inspection_plusplus.CalculationNominalElement();
		calculationNominalElement1438.setOperation("a");
		java.util.ArrayList<org.inspection_plusplus.OperationParameter> operationParameter1439 = new java.util.ArrayList<org.inspection_plusplus.OperationParameter>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1438, "operationParameter", operationParameter1439);
		java.util.ArrayList<org.inspection_plusplus.Operand> operand1440 = new java.util.ArrayList<org.inspection_plusplus.Operand>();
		org.inspection_plusplus.Operand operand1441 = new org.inspection_plusplus.Operand();
		operand1441.setIndex(new java.math.BigInteger("-97"));
		operand1441.setRole("");
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> referenceSystemRef1442 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1443 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1443.setClassID("aaaaaaaaaaa");
		iD4Referencing1443.setUuid("aaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1444 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1445 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1446 = new java.util.ArrayList<Object>();
		any1446.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1445, "any", any1446);
		extension1445.setVendor("");

		extension1444.add(extension1445);
		org.inspection_plusplus.Extension extension1447 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1448 = new java.util.ArrayList<Object>();
		any1448.add(new Object());
		any1448.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1447, "any", any1448);
		extension1447.setVendor("aaaaaaaaaaaaaaaaaaaaa");

		extension1444.add(extension1447);
		org.inspection_plusplus.Extension extension1449 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1450 = new java.util.ArrayList<Object>();
		any1450.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1449, "any", any1450);
		extension1449.setVendor("aaaaaaaaaa");

		extension1444.add(extension1449);
		org.inspection_plusplus.Extension extension1451 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1452 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1451, "any", any1452);
		extension1451.setVendor("");

		extension1444.add(extension1451);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1443, "extension", extension1444);

		referenceSystemRef1442.add(iD4Referencing1443);
		eu.sarunas.junit.TestsHelper.set(operand1441, "referenceSystemRef", referenceSystemRef1442);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeGeometricRef1453 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1454 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1454.setClassID("aaaaa");
		iD4Referencing1454.setUuid("aaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1455 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1454, "extension", extension1455);

		ipeGeometricRef1453.add(iD4Referencing1454);
		eu.sarunas.junit.TestsHelper.set(operand1441, "ipeGeometricRef", ipeGeometricRef1453);

		operand1440.add(operand1441);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1438, "operand", operand1440);
		calculationNominalElement1438.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects1456 = new org.inspection_plusplus.ID4Objects();
		iD4Objects1456.setUuid("aaaaaaaaaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1457 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1458 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1459 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1458, "any", any1459);
		extension1458.setVendor("");

		extension1457.add(extension1458);
		eu.sarunas.junit.TestsHelper.set(iD4Objects1456, "extension", extension1457);

		calculationNominalElement1438.setSystemID(iD4Objects1456);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef1460 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1438, "commentRef", commentRef1460);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1461 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1438, "extension", extension1461);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID1462 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1438, "displayID", displayID1462);
		java.util.ArrayList<org.inspection_plusplus.History> history1463 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1438, "history", history1463);

		calculationNominalElement1398.add(calculationNominalElement1438);
		org.inspection_plusplus.CalculationNominalElement calculationNominalElement1464 = new org.inspection_plusplus.CalculationNominalElement();
		calculationNominalElement1464.setOperation("aaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.OperationParameter> operationParameter1465 = new java.util.ArrayList<org.inspection_plusplus.OperationParameter>();
		org.inspection_plusplus.OperationParameter operationParameter1466 = new org.inspection_plusplus.OperationParameter();
		org.inspection_plusplus.KeyValueOperatorList keyValueOperatorList1467 = new org.inspection_plusplus.KeyValueOperatorList();
		java.util.ArrayList<org.inspection_plusplus.KeyValueOperator> keyValueOperator1468 = new java.util.ArrayList<org.inspection_plusplus.KeyValueOperator>();
		org.inspection_plusplus.KeyValueOperator keyValueOperator1469 = new org.inspection_plusplus.KeyValueOperator();
		keyValueOperator1469.setKey("");
		keyValueOperator1469.setOperator("");
		keyValueOperator1469.setValue("aaaaaa");

		keyValueOperator1468.add(keyValueOperator1469);
		org.inspection_plusplus.KeyValueOperator keyValueOperator1470 = new org.inspection_plusplus.KeyValueOperator();
		keyValueOperator1470.setKey("");
		keyValueOperator1470.setOperator("aaaaaaaaaaaaa");
		keyValueOperator1470.setValue("aaaaaaaaaaaaaaa");

		keyValueOperator1468.add(keyValueOperator1470);
		eu.sarunas.junit.TestsHelper.set(keyValueOperatorList1467, "keyValueOperator", keyValueOperator1468);

		operationParameter1466.setKeyValuePairs(keyValueOperatorList1467);

		operationParameter1465.add(operationParameter1466);
		org.inspection_plusplus.OperationParameter operationParameter1471 = new org.inspection_plusplus.OperationParameter();
		org.inspection_plusplus.KeyValueOperatorList keyValueOperatorList1472 = new org.inspection_plusplus.KeyValueOperatorList();
		java.util.ArrayList<org.inspection_plusplus.KeyValueOperator> keyValueOperator1473 = new java.util.ArrayList<org.inspection_plusplus.KeyValueOperator>();
		org.inspection_plusplus.KeyValueOperator keyValueOperator1474 = new org.inspection_plusplus.KeyValueOperator();
		keyValueOperator1474.setKey("");
		keyValueOperator1474.setOperator("");
		keyValueOperator1474.setValue("");

		keyValueOperator1473.add(keyValueOperator1474);
		eu.sarunas.junit.TestsHelper.set(keyValueOperatorList1472, "keyValueOperator", keyValueOperator1473);

		operationParameter1471.setKeyValuePairs(keyValueOperatorList1472);

		operationParameter1465.add(operationParameter1471);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1464, "operationParameter", operationParameter1465);
		java.util.ArrayList<org.inspection_plusplus.Operand> operand1475 = new java.util.ArrayList<org.inspection_plusplus.Operand>();
		org.inspection_plusplus.Operand operand1476 = new org.inspection_plusplus.Operand();
		operand1476.setIndex(new java.math.BigInteger("-179"));
		operand1476.setRole("");
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> referenceSystemRef1477 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(operand1476, "referenceSystemRef", referenceSystemRef1477);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeGeometricRef1478 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(operand1476, "ipeGeometricRef", ipeGeometricRef1478);

		operand1475.add(operand1476);
		org.inspection_plusplus.Operand operand1479 = new org.inspection_plusplus.Operand();
		operand1479.setIndex(new java.math.BigInteger("-300"));
		operand1479.setRole("aaaaaaaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> referenceSystemRef1480 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1481 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1481.setClassID("aaaaaaaa");
		iD4Referencing1481.setUuid("a");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1482 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1483 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1484 = new java.util.ArrayList<Object>();
		any1484.add(new Object());
		any1484.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1483, "any", any1484);
		extension1483.setVendor("aaaaaaaaaaaaaaaaaa");

		extension1482.add(extension1483);
		org.inspection_plusplus.Extension extension1485 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1486 = new java.util.ArrayList<Object>();
		any1486.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1485, "any", any1486);
		extension1485.setVendor("");

		extension1482.add(extension1485);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1481, "extension", extension1482);

		referenceSystemRef1480.add(iD4Referencing1481);
		org.inspection_plusplus.ID4Referencing iD4Referencing1487 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1487.setClassID("");
		iD4Referencing1487.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1488 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1487, "extension", extension1488);

		referenceSystemRef1480.add(iD4Referencing1487);
		org.inspection_plusplus.ID4Referencing iD4Referencing1489 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1489.setClassID("aa");
		iD4Referencing1489.setUuid("aaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1490 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1491 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1492 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1491, "any", any1492);
		extension1491.setVendor("");

		extension1490.add(extension1491);
		org.inspection_plusplus.Extension extension1493 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1494 = new java.util.ArrayList<Object>();
		any1494.add(new Object());
		any1494.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1493, "any", any1494);
		extension1493.setVendor("aaaaaaaaa");

		extension1490.add(extension1493);
		org.inspection_plusplus.Extension extension1495 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1496 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1495, "any", any1496);
		extension1495.setVendor("");

		extension1490.add(extension1495);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1489, "extension", extension1490);

		referenceSystemRef1480.add(iD4Referencing1489);
		org.inspection_plusplus.ID4Referencing iD4Referencing1497 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1497.setClassID("");
		iD4Referencing1497.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1498 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1497, "extension", extension1498);

		referenceSystemRef1480.add(iD4Referencing1497);
		eu.sarunas.junit.TestsHelper.set(operand1479, "referenceSystemRef", referenceSystemRef1480);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeGeometricRef1499 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(operand1479, "ipeGeometricRef", ipeGeometricRef1499);

		operand1475.add(operand1479);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1464, "operand", operand1475);
		calculationNominalElement1464.setName("aaaaaaaaaaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects1500 = new org.inspection_plusplus.ID4Objects();
		iD4Objects1500.setUuid("a");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1501 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects1500, "extension", extension1501);

		calculationNominalElement1464.setSystemID(iD4Objects1500);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef1502 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1503 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1503.setClassID("aaaaaaa");
		iD4Referencing1503.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1504 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1503, "extension", extension1504);

		commentRef1502.add(iD4Referencing1503);
		org.inspection_plusplus.ID4Referencing iD4Referencing1505 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1505.setClassID("");
		iD4Referencing1505.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1506 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1507 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1508 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1507, "any", any1508);
		extension1507.setVendor("aaaaaaaaaaa");

		extension1506.add(extension1507);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1505, "extension", extension1506);

		commentRef1502.add(iD4Referencing1505);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1464, "commentRef", commentRef1502);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1509 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1510 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1511 = new java.util.ArrayList<Object>();
		any1511.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1510, "any", any1511);
		extension1510.setVendor("");

		extension1509.add(extension1510);
		org.inspection_plusplus.Extension extension1512 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1513 = new java.util.ArrayList<Object>();
		any1513.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1512, "any", any1513);
		extension1512.setVendor("");

		extension1509.add(extension1512);
		org.inspection_plusplus.Extension extension1514 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1515 = new java.util.ArrayList<Object>();
		any1515.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1514, "any", any1515);
		extension1514.setVendor("");

		extension1509.add(extension1514);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1464, "extension", extension1509);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID1516 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID1517 = new org.inspection_plusplus.DisplayID();
		displayID1517.setLanguage("aaaaaaaaaaa");
		displayID1517.setText("");

		displayID1516.add(displayID1517);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1464, "displayID", displayID1516);
		java.util.ArrayList<org.inspection_plusplus.History> history1518 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement1464, "history", history1518);

		calculationNominalElement1398.add(calculationNominalElement1464);
		eu.sarunas.junit.TestsHelper.set(iPEGeometric1394, "calculationNominalElement", calculationNominalElement1398);
		iPEGeometric1394.setDescription("");
		iPEGeometric1394.setFunction("");
		iPEGeometric1394.setName("aaaaaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects1519 = new org.inspection_plusplus.ID4Objects();
		iD4Objects1519.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1520 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects1519, "extension", extension1520);

		iPEGeometric1394.setSystemID(iD4Objects1519);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef1521 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(iPEGeometric1394, "commentRef", commentRef1521);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1522 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1523 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1524 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1523, "any", any1524);
		extension1523.setVendor("");

		extension1522.add(extension1523);
		org.inspection_plusplus.Extension extension1525 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1526 = new java.util.ArrayList<Object>();
		any1526.add(new Object());
		any1526.add(new Object());
		any1526.add(new Object());
		any1526.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1525, "any", any1526);
		extension1525.setVendor("a");

		extension1522.add(extension1525);
		org.inspection_plusplus.Extension extension1527 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1528 = new java.util.ArrayList<Object>();
		any1528.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1527, "any", any1528);
		extension1527.setVendor("aaaaaaaaaa");

		extension1522.add(extension1527);
		org.inspection_plusplus.Extension extension1529 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1530 = new java.util.ArrayList<Object>();
		any1530.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1529, "any", any1530);
		extension1529.setVendor("");

		extension1522.add(extension1529);
		eu.sarunas.junit.TestsHelper.set(iPEGeometric1394, "extension", extension1522);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID1531 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID1532 = new org.inspection_plusplus.DisplayID();
		displayID1532.setLanguage("aaaa");
		displayID1532.setText("aaaaaaa");

		displayID1531.add(displayID1532);
		eu.sarunas.junit.TestsHelper.set(iPEGeometric1394, "displayID", displayID1531);
		java.util.ArrayList<org.inspection_plusplus.History> history1533 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history1534 = new org.inspection_plusplus.History();
		history1534.setAction("aaaaaaa");
		history1534.setAuthor("");
		history1534.setComment("");
		history1534.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 45, 13)));

		history1533.add(history1534);
		org.inspection_plusplus.History history1535 = new org.inspection_plusplus.History();
		history1535.setAction("");
		history1535.setAuthor("aaaaaaaaaaaaaa");
		history1535.setComment("");
		history1535.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 10, 25)));

		history1533.add(history1535);
		eu.sarunas.junit.TestsHelper.set(iPEGeometric1394, "history", history1533);

		ipe1393.add(iPEGeometric1394);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan1287, "ipe", ipe1393);
		java.util.ArrayList<org.inspection_plusplus.ReferenceSystem> referenceSystem1536 = new java.util.ArrayList<org.inspection_plusplus.ReferenceSystem>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan1287, "referenceSystem", referenceSystem1536);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> inspectionTaskRef1537 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan1287, "inspectionTaskRef", inspectionTaskRef1537);
		java.util.ArrayList<org.inspection_plusplus.QC> qc1538 = new java.util.ArrayList<org.inspection_plusplus.QC>();
		org.inspection_plusplus.QCSingle qCSingle1539 = new org.inspection_plusplus.QCSingle();
		qCSingle1539.setGeoObjectDetail("");
		org.inspection_plusplus.ID4Referencing iD4Referencing1540 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1540.setClassID("a");
		iD4Referencing1540.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1541 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1542 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1543 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1542, "any", any1543);
		extension1542.setVendor("");

		extension1541.add(extension1542);
		org.inspection_plusplus.Extension extension1544 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1545 = new java.util.ArrayList<Object>();
		any1545.add(new Object());
		any1545.add(new Object());
		any1545.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1544, "any", any1545);
		extension1544.setVendor("");

		extension1541.add(extension1544);
		org.inspection_plusplus.Extension extension1546 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1547 = new java.util.ArrayList<Object>();
		any1547.add(new Object());
		any1547.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1546, "any", any1547);
		extension1546.setVendor("aaa");

		extension1541.add(extension1546);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1540, "extension", extension1541);

		qCSingle1539.setToleranceRef(iD4Referencing1540);
		qCSingle1539.setAllAssembliesReferenced(true);
		qCSingle1539.setDescription("");
		qCSingle1539.setFunction("");
		qCSingle1539.setPartQAversion("");
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> partRef1548 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1549 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1549.setClassID("aaaaaaaaa");
		iD4Referencing1549.setUuid("aaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1550 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1549, "extension", extension1550);

		partRef1548.add(iD4Referencing1549);
		org.inspection_plusplus.ID4Referencing iD4Referencing1551 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1551.setClassID("");
		iD4Referencing1551.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1552 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1553 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1554 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1553, "any", any1554);
		extension1553.setVendor("");

		extension1552.add(extension1553);
		org.inspection_plusplus.Extension extension1555 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1556 = new java.util.ArrayList<Object>();
		any1556.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1555, "any", any1556);
		extension1555.setVendor("aaaaaaaaaaaaaaaaa");

		extension1552.add(extension1555);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1551, "extension", extension1552);

		partRef1548.add(iD4Referencing1551);
		org.inspection_plusplus.ID4Referencing iD4Referencing1557 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1557.setClassID("");
		iD4Referencing1557.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1558 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1559 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1560 = new java.util.ArrayList<Object>();
		any1560.add(new Object());
		any1560.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1559, "any", any1560);
		extension1559.setVendor("");

		extension1558.add(extension1559);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1557, "extension", extension1558);

		partRef1548.add(iD4Referencing1557);
		eu.sarunas.junit.TestsHelper.set(qCSingle1539, "partRef", partRef1548);
		java.util.ArrayList<Object> calculationActualElementDetectionRef1561 = new java.util.ArrayList<Object>();
		calculationActualElementDetectionRef1561.add(new Object());
		calculationActualElementDetectionRef1561.add(new Object());
		eu.sarunas.junit.TestsHelper.set(qCSingle1539, "calculationActualElementDetectionRef", calculationActualElementDetectionRef1561);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeRef1562 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1563 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1563.setClassID("aa");
		iD4Referencing1563.setUuid("aaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1564 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1563, "extension", extension1564);

		ipeRef1562.add(iD4Referencing1563);
		eu.sarunas.junit.TestsHelper.set(qCSingle1539, "ipeRef", ipeRef1562);
		qCSingle1539.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects1565 = new org.inspection_plusplus.ID4Objects();
		iD4Objects1565.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1566 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1567 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1568 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1567, "any", any1568);
		extension1567.setVendor("");

		extension1566.add(extension1567);
		eu.sarunas.junit.TestsHelper.set(iD4Objects1565, "extension", extension1566);

		qCSingle1539.setSystemID(iD4Objects1565);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef1569 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(qCSingle1539, "commentRef", commentRef1569);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1570 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1571 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1572 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1571, "any", any1572);
		extension1571.setVendor("");

		extension1570.add(extension1571);
		eu.sarunas.junit.TestsHelper.set(qCSingle1539, "extension", extension1570);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID1573 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID1574 = new org.inspection_plusplus.DisplayID();
		displayID1574.setLanguage("aaaaa");
		displayID1574.setText("");

		displayID1573.add(displayID1574);
		org.inspection_plusplus.DisplayID displayID1575 = new org.inspection_plusplus.DisplayID();
		displayID1575.setLanguage("");
		displayID1575.setText("");

		displayID1573.add(displayID1575);
		eu.sarunas.junit.TestsHelper.set(qCSingle1539, "displayID", displayID1573);
		java.util.ArrayList<org.inspection_plusplus.History> history1576 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history1577 = new org.inspection_plusplus.History();
		history1577.setAction("aaaaaaaa");
		history1577.setAuthor("aaaaaaaaaaaaaaa");
		history1577.setComment("");
		history1577.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 3, 39)));

		history1576.add(history1577);
		eu.sarunas.junit.TestsHelper.set(qCSingle1539, "history", history1576);

		qc1538.add(qCSingle1539);
		org.inspection_plusplus.QCSingle qCSingle1578 = new org.inspection_plusplus.QCSingle();
		qCSingle1578.setGeoObjectDetail("aaaaaaaa");
		org.inspection_plusplus.ID4Referencing iD4Referencing1579 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1579.setClassID("");
		iD4Referencing1579.setUuid("aaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1580 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1581 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1582 = new java.util.ArrayList<Object>();
		any1582.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1581, "any", any1582);
		extension1581.setVendor("aaaaaaaaaaaaaaaa");

		extension1580.add(extension1581);
		org.inspection_plusplus.Extension extension1583 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1584 = new java.util.ArrayList<Object>();
		any1584.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1583, "any", any1584);
		extension1583.setVendor("");

		extension1580.add(extension1583);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1579, "extension", extension1580);

		qCSingle1578.setToleranceRef(iD4Referencing1579);
		qCSingle1578.setAllAssembliesReferenced(false);
		qCSingle1578.setDescription("aaaaaa");
		qCSingle1578.setFunction("");
		qCSingle1578.setPartQAversion("");
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> partRef1585 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1586 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1586.setClassID("");
		iD4Referencing1586.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1587 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1588 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1589 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1588, "any", any1589);
		extension1588.setVendor("");

		extension1587.add(extension1588);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1586, "extension", extension1587);

		partRef1585.add(iD4Referencing1586);
		eu.sarunas.junit.TestsHelper.set(qCSingle1578, "partRef", partRef1585);
		java.util.ArrayList<Object> calculationActualElementDetectionRef1590 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(qCSingle1578, "calculationActualElementDetectionRef", calculationActualElementDetectionRef1590);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeRef1591 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1592 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1592.setClassID("aaaaa");
		iD4Referencing1592.setUuid("aaaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1593 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1592, "extension", extension1593);

		ipeRef1591.add(iD4Referencing1592);
		eu.sarunas.junit.TestsHelper.set(qCSingle1578, "ipeRef", ipeRef1591);
		qCSingle1578.setName("aa");
		org.inspection_plusplus.ID4Objects iD4Objects1594 = new org.inspection_plusplus.ID4Objects();
		iD4Objects1594.setUuid("aaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1595 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1596 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1597 = new java.util.ArrayList<Object>();
		any1597.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1596, "any", any1597);
		extension1596.setVendor("");

		extension1595.add(extension1596);
		org.inspection_plusplus.Extension extension1598 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1599 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1598, "any", any1599);
		extension1598.setVendor("");

		extension1595.add(extension1598);
		eu.sarunas.junit.TestsHelper.set(iD4Objects1594, "extension", extension1595);

		qCSingle1578.setSystemID(iD4Objects1594);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef1600 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1601 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1601.setClassID("aaaa");
		iD4Referencing1601.setUuid("aaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1602 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1603 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1604 = new java.util.ArrayList<Object>();
		any1604.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1603, "any", any1604);
		extension1603.setVendor("aaaaa");

		extension1602.add(extension1603);
		org.inspection_plusplus.Extension extension1605 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1606 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1605, "any", any1606);
		extension1605.setVendor("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");

		extension1602.add(extension1605);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1601, "extension", extension1602);

		commentRef1600.add(iD4Referencing1601);
		org.inspection_plusplus.ID4Referencing iD4Referencing1607 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1607.setClassID("");
		iD4Referencing1607.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1608 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1607, "extension", extension1608);

		commentRef1600.add(iD4Referencing1607);
		eu.sarunas.junit.TestsHelper.set(qCSingle1578, "commentRef", commentRef1600);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1609 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1610 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1611 = new java.util.ArrayList<Object>();
		any1611.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1610, "any", any1611);
		extension1610.setVendor("aaaaaaaaaaa");

		extension1609.add(extension1610);
		org.inspection_plusplus.Extension extension1612 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1613 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1612, "any", any1613);
		extension1612.setVendor("");

		extension1609.add(extension1612);
		eu.sarunas.junit.TestsHelper.set(qCSingle1578, "extension", extension1609);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID1614 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID1615 = new org.inspection_plusplus.DisplayID();
		displayID1615.setLanguage("");
		displayID1615.setText("");

		displayID1614.add(displayID1615);
		eu.sarunas.junit.TestsHelper.set(qCSingle1578, "displayID", displayID1614);
		java.util.ArrayList<org.inspection_plusplus.History> history1616 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(qCSingle1578, "history", history1616);

		qc1538.add(qCSingle1578);
		org.inspection_plusplus.QCSingle qCSingle1617 = new org.inspection_plusplus.QCSingle();
		qCSingle1617.setGeoObjectDetail("aaa");
		org.inspection_plusplus.ID4Referencing iD4Referencing1618 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1618.setClassID("aaaaaa");
		iD4Referencing1618.setUuid("aaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1619 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1618, "extension", extension1619);

		qCSingle1617.setToleranceRef(iD4Referencing1618);
		qCSingle1617.setAllAssembliesReferenced(true);
		qCSingle1617.setDescription("");
		qCSingle1617.setFunction("aaaaaaaaa");
		qCSingle1617.setPartQAversion("aaaaaa");
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> partRef1620 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(qCSingle1617, "partRef", partRef1620);
		java.util.ArrayList<Object> calculationActualElementDetectionRef1621 = new java.util.ArrayList<Object>();
		calculationActualElementDetectionRef1621.add(new Object());
		eu.sarunas.junit.TestsHelper.set(qCSingle1617, "calculationActualElementDetectionRef", calculationActualElementDetectionRef1621);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeRef1622 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1623 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1623.setClassID("");
		iD4Referencing1623.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1624 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1623, "extension", extension1624);

		ipeRef1622.add(iD4Referencing1623);
		eu.sarunas.junit.TestsHelper.set(qCSingle1617, "ipeRef", ipeRef1622);
		qCSingle1617.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects1625 = new org.inspection_plusplus.ID4Objects();
		iD4Objects1625.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1626 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects1625, "extension", extension1626);

		qCSingle1617.setSystemID(iD4Objects1625);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef1627 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1628 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1628.setClassID("");
		iD4Referencing1628.setUuid("aaaaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1629 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1628, "extension", extension1629);

		commentRef1627.add(iD4Referencing1628);
		eu.sarunas.junit.TestsHelper.set(qCSingle1617, "commentRef", commentRef1627);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1630 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1631 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1632 = new java.util.ArrayList<Object>();
		any1632.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1631, "any", any1632);
		extension1631.setVendor("");

		extension1630.add(extension1631);
		org.inspection_plusplus.Extension extension1633 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1634 = new java.util.ArrayList<Object>();
		any1634.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1633, "any", any1634);
		extension1633.setVendor("a");

		extension1630.add(extension1633);
		eu.sarunas.junit.TestsHelper.set(qCSingle1617, "extension", extension1630);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID1635 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(qCSingle1617, "displayID", displayID1635);
		java.util.ArrayList<org.inspection_plusplus.History> history1636 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history1637 = new org.inspection_plusplus.History();
		history1637.setAction("");
		history1637.setAuthor("aaaaaaaaa");
		history1637.setComment("");
		history1637.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 20, 26)));

		history1636.add(history1637);
		eu.sarunas.junit.TestsHelper.set(qCSingle1617, "history", history1636);

		qc1538.add(qCSingle1617);
		org.inspection_plusplus.QCSingle qCSingle1638 = new org.inspection_plusplus.QCSingle();
		qCSingle1638.setGeoObjectDetail("");
		org.inspection_plusplus.ID4Referencing iD4Referencing1639 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1639.setClassID("aa");
		iD4Referencing1639.setUuid("aaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1640 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1641 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1642 = new java.util.ArrayList<Object>();
		any1642.add(new Object());
		any1642.add(new Object());
		any1642.add(new Object());
		any1642.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1641, "any", any1642);
		extension1641.setVendor("aa");

		extension1640.add(extension1641);
		org.inspection_plusplus.Extension extension1643 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1644 = new java.util.ArrayList<Object>();
		any1644.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1643, "any", any1644);
		extension1643.setVendor("");

		extension1640.add(extension1643);
		org.inspection_plusplus.Extension extension1645 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1646 = new java.util.ArrayList<Object>();
		any1646.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1645, "any", any1646);
		extension1645.setVendor("aaaaaaaaaaaaa");

		extension1640.add(extension1645);
		org.inspection_plusplus.Extension extension1647 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1648 = new java.util.ArrayList<Object>();
		any1648.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1647, "any", any1648);
		extension1647.setVendor("aaaaaaaaaaa");

		extension1640.add(extension1647);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1639, "extension", extension1640);

		qCSingle1638.setToleranceRef(iD4Referencing1639);
		qCSingle1638.setAllAssembliesReferenced(true);
		qCSingle1638.setDescription("aaaaaaaaaaaaa");
		qCSingle1638.setFunction("a");
		qCSingle1638.setPartQAversion("aaaaaaaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> partRef1649 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(qCSingle1638, "partRef", partRef1649);
		java.util.ArrayList<Object> calculationActualElementDetectionRef1650 = new java.util.ArrayList<Object>();
		calculationActualElementDetectionRef1650.add(new Object());
		eu.sarunas.junit.TestsHelper.set(qCSingle1638, "calculationActualElementDetectionRef", calculationActualElementDetectionRef1650);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeRef1651 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1652 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1652.setClassID("");
		iD4Referencing1652.setUuid("aa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1653 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1654 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1655 = new java.util.ArrayList<Object>();
		any1655.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1654, "any", any1655);
		extension1654.setVendor("");

		extension1653.add(extension1654);
		org.inspection_plusplus.Extension extension1656 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1657 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1656, "any", any1657);
		extension1656.setVendor("aaaaaaaaaaa");

		extension1653.add(extension1656);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1652, "extension", extension1653);

		ipeRef1651.add(iD4Referencing1652);
		eu.sarunas.junit.TestsHelper.set(qCSingle1638, "ipeRef", ipeRef1651);
		qCSingle1638.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects1658 = new org.inspection_plusplus.ID4Objects();
		iD4Objects1658.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1659 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects1658, "extension", extension1659);

		qCSingle1638.setSystemID(iD4Objects1658);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef1660 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1661 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1661.setClassID("aaaaaaaaaaaaaaa");
		iD4Referencing1661.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1662 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1661, "extension", extension1662);

		commentRef1660.add(iD4Referencing1661);
		eu.sarunas.junit.TestsHelper.set(qCSingle1638, "commentRef", commentRef1660);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1663 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1664 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1665 = new java.util.ArrayList<Object>();
		any1665.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1664, "any", any1665);
		extension1664.setVendor("aaaaa");

		extension1663.add(extension1664);
		org.inspection_plusplus.Extension extension1666 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1667 = new java.util.ArrayList<Object>();
		any1667.add(new Object());
		any1667.add(new Object());
		any1667.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1666, "any", any1667);
		extension1666.setVendor("aa");

		extension1663.add(extension1666);
		eu.sarunas.junit.TestsHelper.set(qCSingle1638, "extension", extension1663);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID1668 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID1669 = new org.inspection_plusplus.DisplayID();
		displayID1669.setLanguage("");
		displayID1669.setText("a");

		displayID1668.add(displayID1669);
		org.inspection_plusplus.DisplayID displayID1670 = new org.inspection_plusplus.DisplayID();
		displayID1670.setLanguage("aaa");
		displayID1670.setText("");

		displayID1668.add(displayID1670);
		org.inspection_plusplus.DisplayID displayID1671 = new org.inspection_plusplus.DisplayID();
		displayID1671.setLanguage("aaaaaaa");
		displayID1671.setText("aaaaa");

		displayID1668.add(displayID1671);
		eu.sarunas.junit.TestsHelper.set(qCSingle1638, "displayID", displayID1668);
		java.util.ArrayList<org.inspection_plusplus.History> history1672 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history1673 = new org.inspection_plusplus.History();
		history1673.setAction("aaaaaa");
		history1673.setAuthor("aa");
		history1673.setComment("aaaaaaaaaaaaaaaaaaa");
		history1673.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 26, 17)));

		history1672.add(history1673);
		eu.sarunas.junit.TestsHelper.set(qCSingle1638, "history", history1672);

		qc1538.add(qCSingle1638);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan1287, "qc", qc1538);
		inspectionPlan1287.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects1674 = new org.inspection_plusplus.ID4Objects();
		iD4Objects1674.setUuid("aaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1675 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1676 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1677 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1676, "any", any1677);
		extension1676.setVendor("aaaaaaaaaaaaa");

		extension1675.add(extension1676);
		org.inspection_plusplus.Extension extension1678 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1679 = new java.util.ArrayList<Object>();
		any1679.add(new Object());
		any1679.add(new Object());
		any1679.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1678, "any", any1679);
		extension1678.setVendor("");

		extension1675.add(extension1678);
		org.inspection_plusplus.Extension extension1680 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1681 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1680, "any", any1681);
		extension1680.setVendor("");

		extension1675.add(extension1680);
		eu.sarunas.junit.TestsHelper.set(iD4Objects1674, "extension", extension1675);

		inspectionPlan1287.setSystemID(iD4Objects1674);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef1682 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1683 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1683.setClassID("");
		iD4Referencing1683.setUuid("aaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1684 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1683, "extension", extension1684);

		commentRef1682.add(iD4Referencing1683);
		org.inspection_plusplus.ID4Referencing iD4Referencing1685 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1685.setClassID("");
		iD4Referencing1685.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1686 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1687 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1688 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1687, "any", any1688);
		extension1687.setVendor("");

		extension1686.add(extension1687);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1685, "extension", extension1686);

		commentRef1682.add(iD4Referencing1685);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan1287, "commentRef", commentRef1682);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1689 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1690 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1691 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1690, "any", any1691);
		extension1690.setVendor("");

		extension1689.add(extension1690);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan1287, "extension", extension1689);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID1692 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID1693 = new org.inspection_plusplus.DisplayID();
		displayID1693.setLanguage("");
		displayID1693.setText("aaaaaaaaaaa");

		displayID1692.add(displayID1693);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan1287, "displayID", displayID1692);
		java.util.ArrayList<org.inspection_plusplus.History> history1694 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan1287, "history", history1694);
		org.inspection_plusplus.Sender sender1695 = new org.inspection_plusplus.Sender();
		sender1695.setAppID("aaaaaaaaaa");
		sender1695.setDomain("");
		org.inspection_plusplus.Extension extension1696 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1697 = new java.util.ArrayList<Object>();
		any1697.add(new Object());
		any1697.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1696, "any", any1697);
		extension1696.setVendor("aaaa");

		org.inspection_plusplus.ReturnStatus res = testObject.storeInspectionPlan(inspectionPlan1287, "", "aaaaaaa", sender1695, extension1696);
		junit.framework.Assert.fail();
	};

	@org.junit.Test
	public void testtestStoreInspectionPlan6() throws Throwable
	{
		org.inspection_plusplus.service.v2.WebServiceImpl testObject = new org.inspection_plusplus.service.v2.WebServiceImpl();
		org.inspection_plusplus.InspectionPlan inspectionPlan1698 = new org.inspection_plusplus.InspectionPlan();
		inspectionPlan1698.setInspectionCategories("aaaaaaaaa");
		inspectionPlan1698.setIPsymmetry("");
		inspectionPlan1698.setPartQAversion("");
		eu.sarunas.junit.TestsHelper.set(inspectionPlan1698, "pdMmaturity", "aaaaa");
		inspectionPlan1698.setQAmaturity("aaaaaaaa");
		inspectionPlan1698.setQAversion("a");
		java.util.ArrayList<org.inspection_plusplus.Comment> comment1699 = new java.util.ArrayList<org.inspection_plusplus.Comment>();
		org.inspection_plusplus.Comment comment1700 = new org.inspection_plusplus.Comment();
		comment1700.setAuthor("aaaaaaaaaaaaaaaaaaaaaaa");
		comment1700.setReceiver("aa");
		comment1700.setSubject("aaaaaaaaaa");
		comment1700.setText("aaaaaaaaaaaaaa");
		comment1700.setName("aaaaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects1701 = new org.inspection_plusplus.ID4Objects();
		iD4Objects1701.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1702 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1703 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1704 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1703, "any", any1704);
		extension1703.setVendor("");

		extension1702.add(extension1703);
		eu.sarunas.junit.TestsHelper.set(iD4Objects1701, "extension", extension1702);

		comment1700.setSystemID(iD4Objects1701);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef1705 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(comment1700, "commentRef", commentRef1705);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1706 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1707 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1708 = new java.util.ArrayList<Object>();
		any1708.add(new Object());
		any1708.add(new Object());
		any1708.add(new Object());
		any1708.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1707, "any", any1708);
		extension1707.setVendor("");

		extension1706.add(extension1707);
		eu.sarunas.junit.TestsHelper.set(comment1700, "extension", extension1706);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID1709 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID1710 = new org.inspection_plusplus.DisplayID();
		displayID1710.setLanguage("aaaaaaaaa");
		displayID1710.setText("");

		displayID1709.add(displayID1710);
		org.inspection_plusplus.DisplayID displayID1711 = new org.inspection_plusplus.DisplayID();
		displayID1711.setLanguage("");
		displayID1711.setText("");

		displayID1709.add(displayID1711);
		org.inspection_plusplus.DisplayID displayID1712 = new org.inspection_plusplus.DisplayID();
		displayID1712.setLanguage("aaaaaaaaaa");
		displayID1712.setText("aaaaaaaaaaa");

		displayID1709.add(displayID1712);
		org.inspection_plusplus.DisplayID displayID1713 = new org.inspection_plusplus.DisplayID();
		displayID1713.setLanguage("aaaaaaaaaaaaaaaaa");
		displayID1713.setText("aaaaaaaaaaaaaaa");

		displayID1709.add(displayID1713);
		eu.sarunas.junit.TestsHelper.set(comment1700, "displayID", displayID1709);
		java.util.ArrayList<org.inspection_plusplus.History> history1714 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history1715 = new org.inspection_plusplus.History();
		history1715.setAction("");
		history1715.setAuthor("aaaaaaaaaaaaaaa");
		history1715.setComment("");
		history1715.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 49, 45)));

		history1714.add(history1715);
		eu.sarunas.junit.TestsHelper.set(comment1700, "history", history1714);

		comment1699.add(comment1700);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan1698, "comment", comment1699);
		java.util.ArrayList<org.inspection_plusplus.Tolerance> tolerance1716 = new java.util.ArrayList<org.inspection_plusplus.Tolerance>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan1698, "tolerance", tolerance1716);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> partRef1717 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1718 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1718.setClassID("");
		iD4Referencing1718.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1719 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1720 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1721 = new java.util.ArrayList<Object>();
		any1721.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1720, "any", any1721);
		extension1720.setVendor("aaaaaaaaa");

		extension1719.add(extension1720);
		org.inspection_plusplus.Extension extension1722 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1723 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1722, "any", any1723);
		extension1722.setVendor("");

		extension1719.add(extension1722);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1718, "extension", extension1719);

		partRef1717.add(iD4Referencing1718);
		org.inspection_plusplus.ID4Referencing iD4Referencing1724 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1724.setClassID("");
		iD4Referencing1724.setUuid("a");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1725 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1724, "extension", extension1725);

		partRef1717.add(iD4Referencing1724);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan1698, "partRef", partRef1717);
		java.util.ArrayList<org.inspection_plusplus.IPE> ipe1726 = new java.util.ArrayList<org.inspection_plusplus.IPE>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan1698, "ipe", ipe1726);
		java.util.ArrayList<org.inspection_plusplus.ReferenceSystem> referenceSystem1727 = new java.util.ArrayList<org.inspection_plusplus.ReferenceSystem>();
		org.inspection_plusplus.ReferenceSystem referenceSystem1728 = new org.inspection_plusplus.ReferenceSystem();
		java.util.ArrayList<org.inspection_plusplus.Datum> datum1729 = new java.util.ArrayList<org.inspection_plusplus.Datum>();
		eu.sarunas.junit.TestsHelper.set(referenceSystem1728, "datum", datum1729);
		java.util.ArrayList<org.inspection_plusplus.AlignmentStrategy> alignmentStrategy1730 = new java.util.ArrayList<org.inspection_plusplus.AlignmentStrategy>();
		org.inspection_plusplus.AlignmentRPS alignmentRPS1731 = new org.inspection_plusplus.AlignmentRPS();
		alignmentRPS1731.setMaxNrOfIterations(new java.math.BigInteger("-300"));
		alignmentRPS1731.setName("aaaaaaaaaaaaaaaaaaaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects1732 = new org.inspection_plusplus.ID4Objects();
		iD4Objects1732.setUuid("aaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1733 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1734 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1735 = new java.util.ArrayList<Object>();
		any1735.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1734, "any", any1735);
		extension1734.setVendor("");

		extension1733.add(extension1734);
		eu.sarunas.junit.TestsHelper.set(iD4Objects1732, "extension", extension1733);

		alignmentRPS1731.setSystemID(iD4Objects1732);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef1736 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(alignmentRPS1731, "commentRef", commentRef1736);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1737 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1738 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1739 = new java.util.ArrayList<Object>();
		any1739.add(new Object());
		any1739.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1738, "any", any1739);
		extension1738.setVendor("");

		extension1737.add(extension1738);
		org.inspection_plusplus.Extension extension1740 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1741 = new java.util.ArrayList<Object>();
		any1741.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1740, "any", any1741);
		extension1740.setVendor("");

		extension1737.add(extension1740);
		eu.sarunas.junit.TestsHelper.set(alignmentRPS1731, "extension", extension1737);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID1742 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID1743 = new org.inspection_plusplus.DisplayID();
		displayID1743.setLanguage("");
		displayID1743.setText("");

		displayID1742.add(displayID1743);
		eu.sarunas.junit.TestsHelper.set(alignmentRPS1731, "displayID", displayID1742);
		java.util.ArrayList<org.inspection_plusplus.History> history1744 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history1745 = new org.inspection_plusplus.History();
		history1745.setAction("aaaaa");
		history1745.setAuthor("");
		history1745.setComment("aaa");
		history1745.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 14, 1)));

		history1744.add(history1745);
		eu.sarunas.junit.TestsHelper.set(alignmentRPS1731, "history", history1744);

		alignmentStrategy1730.add(alignmentRPS1731);
		org.inspection_plusplus.AlignmentFSS alignmentFSS1746 = new org.inspection_plusplus.AlignmentFSS();
		alignmentFSS1746.setTerminatingCondition(-98.21);
		alignmentFSS1746.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects1747 = new org.inspection_plusplus.ID4Objects();
		iD4Objects1747.setUuid("aaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1748 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1749 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1750 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1749, "any", any1750);
		extension1749.setVendor("");

		extension1748.add(extension1749);
		org.inspection_plusplus.Extension extension1751 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1752 = new java.util.ArrayList<Object>();
		any1752.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1751, "any", any1752);
		extension1751.setVendor("aaaaaaaaaaaaaaaaa");

		extension1748.add(extension1751);
		org.inspection_plusplus.Extension extension1753 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1754 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1753, "any", any1754);
		extension1753.setVendor("");

		extension1748.add(extension1753);
		org.inspection_plusplus.Extension extension1755 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1756 = new java.util.ArrayList<Object>();
		any1756.add(new Object());
		any1756.add(new Object());
		any1756.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1755, "any", any1756);
		extension1755.setVendor("");

		extension1748.add(extension1755);
		eu.sarunas.junit.TestsHelper.set(iD4Objects1747, "extension", extension1748);

		alignmentFSS1746.setSystemID(iD4Objects1747);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef1757 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(alignmentFSS1746, "commentRef", commentRef1757);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1758 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1759 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1760 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1759, "any", any1760);
		extension1759.setVendor("");

		extension1758.add(extension1759);
		eu.sarunas.junit.TestsHelper.set(alignmentFSS1746, "extension", extension1758);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID1761 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID1762 = new org.inspection_plusplus.DisplayID();
		displayID1762.setLanguage("");
		displayID1762.setText("");

		displayID1761.add(displayID1762);
		eu.sarunas.junit.TestsHelper.set(alignmentFSS1746, "displayID", displayID1761);
		java.util.ArrayList<org.inspection_plusplus.History> history1763 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(alignmentFSS1746, "history", history1763);

		alignmentStrategy1730.add(alignmentFSS1746);
		org.inspection_plusplus.AlignmentFSS alignmentFSS1764 = new org.inspection_plusplus.AlignmentFSS();
		alignmentFSS1764.setTerminatingCondition(26.63);
		alignmentFSS1764.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects1765 = new org.inspection_plusplus.ID4Objects();
		iD4Objects1765.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1766 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1767 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1768 = new java.util.ArrayList<Object>();
		any1768.add(new Object());
		any1768.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1767, "any", any1768);
		extension1767.setVendor("");

		extension1766.add(extension1767);
		eu.sarunas.junit.TestsHelper.set(iD4Objects1765, "extension", extension1766);

		alignmentFSS1764.setSystemID(iD4Objects1765);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef1769 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1770 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1770.setClassID("");
		iD4Referencing1770.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1771 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1772 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1773 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1772, "any", any1773);
		extension1772.setVendor("aaaaaaaa");

		extension1771.add(extension1772);
		org.inspection_plusplus.Extension extension1774 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1775 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1774, "any", any1775);
		extension1774.setVendor("aaaaaaaaaaaaaaaaaaaaa");

		extension1771.add(extension1774);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1770, "extension", extension1771);

		commentRef1769.add(iD4Referencing1770);
		eu.sarunas.junit.TestsHelper.set(alignmentFSS1764, "commentRef", commentRef1769);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1776 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1777 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1778 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1777, "any", any1778);
		extension1777.setVendor("");

		extension1776.add(extension1777);
		eu.sarunas.junit.TestsHelper.set(alignmentFSS1764, "extension", extension1776);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID1779 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID1780 = new org.inspection_plusplus.DisplayID();
		displayID1780.setLanguage("aaaaaaa");
		displayID1780.setText("");

		displayID1779.add(displayID1780);
		eu.sarunas.junit.TestsHelper.set(alignmentFSS1764, "displayID", displayID1779);
		java.util.ArrayList<org.inspection_plusplus.History> history1781 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history1782 = new org.inspection_plusplus.History();
		history1782.setAction("");
		history1782.setAuthor("");
		history1782.setComment("");
		history1782.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 51, 38)));

		history1781.add(history1782);
		eu.sarunas.junit.TestsHelper.set(alignmentFSS1764, "history", history1781);

		alignmentStrategy1730.add(alignmentFSS1764);
		eu.sarunas.junit.TestsHelper.set(referenceSystem1728, "alignmentStrategy", alignmentStrategy1730);
		referenceSystem1728.setName("aaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects1783 = new org.inspection_plusplus.ID4Objects();
		iD4Objects1783.setUuid("aaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1784 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1785 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1786 = new java.util.ArrayList<Object>();
		any1786.add(new Object());
		any1786.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1785, "any", any1786);
		extension1785.setVendor("");

		extension1784.add(extension1785);
		org.inspection_plusplus.Extension extension1787 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1788 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1787, "any", any1788);
		extension1787.setVendor("");

		extension1784.add(extension1787);
		eu.sarunas.junit.TestsHelper.set(iD4Objects1783, "extension", extension1784);

		referenceSystem1728.setSystemID(iD4Objects1783);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef1789 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(referenceSystem1728, "commentRef", commentRef1789);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1790 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1791 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1792 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1791, "any", any1792);
		extension1791.setVendor("aa");

		extension1790.add(extension1791);
		eu.sarunas.junit.TestsHelper.set(referenceSystem1728, "extension", extension1790);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID1793 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID1794 = new org.inspection_plusplus.DisplayID();
		displayID1794.setLanguage("");
		displayID1794.setText("aaaaaaaaa");

		displayID1793.add(displayID1794);
		org.inspection_plusplus.DisplayID displayID1795 = new org.inspection_plusplus.DisplayID();
		displayID1795.setLanguage("aaaaa");
		displayID1795.setText("");

		displayID1793.add(displayID1795);
		org.inspection_plusplus.DisplayID displayID1796 = new org.inspection_plusplus.DisplayID();
		displayID1796.setLanguage("");
		displayID1796.setText("");

		displayID1793.add(displayID1796);
		org.inspection_plusplus.DisplayID displayID1797 = new org.inspection_plusplus.DisplayID();
		displayID1797.setLanguage("");
		displayID1797.setText("");

		displayID1793.add(displayID1797);
		eu.sarunas.junit.TestsHelper.set(referenceSystem1728, "displayID", displayID1793);
		java.util.ArrayList<org.inspection_plusplus.History> history1798 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history1799 = new org.inspection_plusplus.History();
		history1799.setAction("aaaaaaa");
		history1799.setAuthor("a");
		history1799.setComment("");
		history1799.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 30, 7)));

		history1798.add(history1799);
		eu.sarunas.junit.TestsHelper.set(referenceSystem1728, "history", history1798);

		referenceSystem1727.add(referenceSystem1728);
		org.inspection_plusplus.ReferenceSystem referenceSystem1800 = new org.inspection_plusplus.ReferenceSystem();
		java.util.ArrayList<org.inspection_plusplus.Datum> datum1801 = new java.util.ArrayList<org.inspection_plusplus.Datum>();
		org.inspection_plusplus.Datum datum1802 = new org.inspection_plusplus.Datum();
		org.inspection_plusplus.Vector3D vector3D1803 = new org.inspection_plusplus.Vector3D();
		vector3D1803.setX(187.15);
		vector3D1803.setY(-286.97);
		vector3D1803.setZ(127.25);

		datum1802.setDisplacement(vector3D1803);
		datum1802.setRole("");
		java.util.ArrayList<org.inspection_plusplus.DatumTarget> datumTarget1804 = new java.util.ArrayList<org.inspection_plusplus.DatumTarget>();
		org.inspection_plusplus.DatumTarget datumTarget1805 = new org.inspection_plusplus.DatumTarget();
		datumTarget1805.setRole("");
		org.inspection_plusplus.ID4Referencing iD4Referencing1806 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1806.setClassID("aaaaaaaaaaaaaa");
		iD4Referencing1806.setUuid("aaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1807 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1806, "extension", extension1807);

		eu.sarunas.junit.TestsHelper.set(datumTarget1805, "ipeGeometricRef", iD4Referencing1806);
		datumTarget1805.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects1808 = new org.inspection_plusplus.ID4Objects();
		iD4Objects1808.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1809 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects1808, "extension", extension1809);

		datumTarget1805.setSystemID(iD4Objects1808);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef1810 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1811 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1811.setClassID("");
		iD4Referencing1811.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1812 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1811, "extension", extension1812);

		commentRef1810.add(iD4Referencing1811);
		eu.sarunas.junit.TestsHelper.set(datumTarget1805, "commentRef", commentRef1810);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1813 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(datumTarget1805, "extension", extension1813);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID1814 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID1815 = new org.inspection_plusplus.DisplayID();
		displayID1815.setLanguage("aaaaaaaa");
		displayID1815.setText("");

		displayID1814.add(displayID1815);
		org.inspection_plusplus.DisplayID displayID1816 = new org.inspection_plusplus.DisplayID();
		displayID1816.setLanguage("aaaaaaa");
		displayID1816.setText("aaaaaaaaaaaaaa");

		displayID1814.add(displayID1816);
		eu.sarunas.junit.TestsHelper.set(datumTarget1805, "displayID", displayID1814);
		java.util.ArrayList<org.inspection_plusplus.History> history1817 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(datumTarget1805, "history", history1817);

		datumTarget1804.add(datumTarget1805);
		org.inspection_plusplus.DatumTarget datumTarget1818 = new org.inspection_plusplus.DatumTarget();
		datumTarget1818.setRole("");
		org.inspection_plusplus.ID4Referencing iD4Referencing1819 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1819.setClassID("");
		iD4Referencing1819.setUuid("aaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1820 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1821 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1822 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1821, "any", any1822);
		extension1821.setVendor("");

		extension1820.add(extension1821);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1819, "extension", extension1820);

		eu.sarunas.junit.TestsHelper.set(datumTarget1818, "ipeGeometricRef", iD4Referencing1819);
		datumTarget1818.setName("aaaa");
		org.inspection_plusplus.ID4Objects iD4Objects1823 = new org.inspection_plusplus.ID4Objects();
		iD4Objects1823.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1824 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects1823, "extension", extension1824);

		datumTarget1818.setSystemID(iD4Objects1823);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef1825 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(datumTarget1818, "commentRef", commentRef1825);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1826 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1827 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1828 = new java.util.ArrayList<Object>();
		any1828.add(new Object());
		any1828.add(new Object());
		any1828.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1827, "any", any1828);
		extension1827.setVendor("aaaaaaaaa");

		extension1826.add(extension1827);
		org.inspection_plusplus.Extension extension1829 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1830 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1829, "any", any1830);
		extension1829.setVendor("");

		extension1826.add(extension1829);
		eu.sarunas.junit.TestsHelper.set(datumTarget1818, "extension", extension1826);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID1831 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(datumTarget1818, "displayID", displayID1831);
		java.util.ArrayList<org.inspection_plusplus.History> history1832 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history1833 = new org.inspection_plusplus.History();
		history1833.setAction("aaaa");
		history1833.setAuthor("aaaaaaaaa");
		history1833.setComment("aaaaaaaaaaaaaa");
		history1833.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 13, 20)));

		history1832.add(history1833);
		eu.sarunas.junit.TestsHelper.set(datumTarget1818, "history", history1832);

		datumTarget1804.add(datumTarget1818);
		org.inspection_plusplus.DatumTarget datumTarget1834 = new org.inspection_plusplus.DatumTarget();
		datumTarget1834.setRole("");
		org.inspection_plusplus.ID4Referencing iD4Referencing1835 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1835.setClassID("");
		iD4Referencing1835.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1836 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1837 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1838 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1837, "any", any1838);
		extension1837.setVendor("");

		extension1836.add(extension1837);
		org.inspection_plusplus.Extension extension1839 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1840 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1839, "any", any1840);
		extension1839.setVendor("aaaaaaa");

		extension1836.add(extension1839);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1835, "extension", extension1836);

		eu.sarunas.junit.TestsHelper.set(datumTarget1834, "ipeGeometricRef", iD4Referencing1835);
		datumTarget1834.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects1841 = new org.inspection_plusplus.ID4Objects();
		iD4Objects1841.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1842 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1843 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1844 = new java.util.ArrayList<Object>();
		any1844.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1843, "any", any1844);
		extension1843.setVendor("");

		extension1842.add(extension1843);
		org.inspection_plusplus.Extension extension1845 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1846 = new java.util.ArrayList<Object>();
		any1846.add(new Object());
		any1846.add(new Object());
		any1846.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1845, "any", any1846);
		extension1845.setVendor("aaaaaaaaaa");

		extension1842.add(extension1845);
		org.inspection_plusplus.Extension extension1847 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1848 = new java.util.ArrayList<Object>();
		any1848.add(new Object());
		any1848.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1847, "any", any1848);
		extension1847.setVendor("");

		extension1842.add(extension1847);
		eu.sarunas.junit.TestsHelper.set(iD4Objects1841, "extension", extension1842);

		datumTarget1834.setSystemID(iD4Objects1841);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef1849 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1850 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1850.setClassID("");
		iD4Referencing1850.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1851 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1852 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1853 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1852, "any", any1853);
		extension1852.setVendor("aa");

		extension1851.add(extension1852);
		org.inspection_plusplus.Extension extension1854 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1855 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1854, "any", any1855);
		extension1854.setVendor("aaaaaaaaaaaaaaaaaaaaaa");

		extension1851.add(extension1854);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1850, "extension", extension1851);

		commentRef1849.add(iD4Referencing1850);
		eu.sarunas.junit.TestsHelper.set(datumTarget1834, "commentRef", commentRef1849);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1856 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(datumTarget1834, "extension", extension1856);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID1857 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID1858 = new org.inspection_plusplus.DisplayID();
		displayID1858.setLanguage("aaaaaaaaa");
		displayID1858.setText("");

		displayID1857.add(displayID1858);
		eu.sarunas.junit.TestsHelper.set(datumTarget1834, "displayID", displayID1857);
		java.util.ArrayList<org.inspection_plusplus.History> history1859 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history1860 = new org.inspection_plusplus.History();
		history1860.setAction("aaaaaa");
		history1860.setAuthor("");
		history1860.setComment("aaaa");
		history1860.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 53, 9)));

		history1859.add(history1860);
		org.inspection_plusplus.History history1861 = new org.inspection_plusplus.History();
		history1861.setAction("");
		history1861.setAuthor("");
		history1861.setComment("");
		history1861.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 2, 19)));

		history1859.add(history1861);
		eu.sarunas.junit.TestsHelper.set(datumTarget1834, "history", history1859);

		datumTarget1804.add(datumTarget1834);
		eu.sarunas.junit.TestsHelper.set(datum1802, "datumTarget", datumTarget1804);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeGeometricRef1862 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(datum1802, "ipeGeometricRef", ipeGeometricRef1862);
		datum1802.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects1863 = new org.inspection_plusplus.ID4Objects();
		iD4Objects1863.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1864 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1865 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1866 = new java.util.ArrayList<Object>();
		any1866.add(new Object());
		any1866.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1865, "any", any1866);
		extension1865.setVendor("aaaaa");

		extension1864.add(extension1865);
		org.inspection_plusplus.Extension extension1867 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1868 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1867, "any", any1868);
		extension1867.setVendor("");

		extension1864.add(extension1867);
		eu.sarunas.junit.TestsHelper.set(iD4Objects1863, "extension", extension1864);

		datum1802.setSystemID(iD4Objects1863);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef1869 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(datum1802, "commentRef", commentRef1869);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1870 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1871 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1872 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1871, "any", any1872);
		extension1871.setVendor("aaaaaaa");

		extension1870.add(extension1871);
		eu.sarunas.junit.TestsHelper.set(datum1802, "extension", extension1870);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID1873 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID1874 = new org.inspection_plusplus.DisplayID();
		displayID1874.setLanguage("");
		displayID1874.setText("aaaaaaaaaaaaaaaa");

		displayID1873.add(displayID1874);
		eu.sarunas.junit.TestsHelper.set(datum1802, "displayID", displayID1873);
		java.util.ArrayList<org.inspection_plusplus.History> history1875 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history1876 = new org.inspection_plusplus.History();
		history1876.setAction("");
		history1876.setAuthor("");
		history1876.setComment("");
		history1876.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 55, 26)));

		history1875.add(history1876);
		eu.sarunas.junit.TestsHelper.set(datum1802, "history", history1875);

		datum1801.add(datum1802);
		org.inspection_plusplus.Datum datum1877 = new org.inspection_plusplus.Datum();
		org.inspection_plusplus.Vector3D vector3D1878 = new org.inspection_plusplus.Vector3D();
		vector3D1878.setX(-327.27);
		vector3D1878.setY(114.43);
		vector3D1878.setZ(-111.49);

		datum1877.setDisplacement(vector3D1878);
		datum1877.setRole("");
		java.util.ArrayList<org.inspection_plusplus.DatumTarget> datumTarget1879 = new java.util.ArrayList<org.inspection_plusplus.DatumTarget>();
		org.inspection_plusplus.DatumTarget datumTarget1880 = new org.inspection_plusplus.DatumTarget();
		datumTarget1880.setRole("aaa");
		org.inspection_plusplus.ID4Referencing iD4Referencing1881 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1881.setClassID("");
		iD4Referencing1881.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1882 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1883 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1884 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1883, "any", any1884);
		extension1883.setVendor("aaaaaaaaaaa");

		extension1882.add(extension1883);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1881, "extension", extension1882);

		eu.sarunas.junit.TestsHelper.set(datumTarget1880, "ipeGeometricRef", iD4Referencing1881);
		datumTarget1880.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects1885 = new org.inspection_plusplus.ID4Objects();
		iD4Objects1885.setUuid("aaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1886 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects1885, "extension", extension1886);

		datumTarget1880.setSystemID(iD4Objects1885);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef1887 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1888 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1888.setClassID("");
		iD4Referencing1888.setUuid("aaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1889 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1890 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1891 = new java.util.ArrayList<Object>();
		any1891.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1890, "any", any1891);
		extension1890.setVendor("aa");

		extension1889.add(extension1890);
		org.inspection_plusplus.Extension extension1892 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1893 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1892, "any", any1893);
		extension1892.setVendor("");

		extension1889.add(extension1892);
		org.inspection_plusplus.Extension extension1894 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1895 = new java.util.ArrayList<Object>();
		any1895.add(new Object());
		any1895.add(new Object());
		any1895.add(new Object());
		any1895.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1894, "any", any1895);
		extension1894.setVendor("");

		extension1889.add(extension1894);
		org.inspection_plusplus.Extension extension1896 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1897 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1896, "any", any1897);
		extension1896.setVendor("");

		extension1889.add(extension1896);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1888, "extension", extension1889);

		commentRef1887.add(iD4Referencing1888);
		eu.sarunas.junit.TestsHelper.set(datumTarget1880, "commentRef", commentRef1887);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1898 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(datumTarget1880, "extension", extension1898);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID1899 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID1900 = new org.inspection_plusplus.DisplayID();
		displayID1900.setLanguage("");
		displayID1900.setText("aaaaaaaaaa");

		displayID1899.add(displayID1900);
		eu.sarunas.junit.TestsHelper.set(datumTarget1880, "displayID", displayID1899);
		java.util.ArrayList<org.inspection_plusplus.History> history1901 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(datumTarget1880, "history", history1901);

		datumTarget1879.add(datumTarget1880);
		org.inspection_plusplus.DatumTarget datumTarget1902 = new org.inspection_plusplus.DatumTarget();
		datumTarget1902.setRole("aaaaaaaaaaaa");
		org.inspection_plusplus.ID4Referencing iD4Referencing1903 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1903.setClassID("");
		iD4Referencing1903.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1904 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1903, "extension", extension1904);

		eu.sarunas.junit.TestsHelper.set(datumTarget1902, "ipeGeometricRef", iD4Referencing1903);
		datumTarget1902.setName("aa");
		org.inspection_plusplus.ID4Objects iD4Objects1905 = new org.inspection_plusplus.ID4Objects();
		iD4Objects1905.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1906 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1907 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1908 = new java.util.ArrayList<Object>();
		any1908.add(new Object());
		any1908.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1907, "any", any1908);
		extension1907.setVendor("a");

		extension1906.add(extension1907);
		org.inspection_plusplus.Extension extension1909 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1910 = new java.util.ArrayList<Object>();
		any1910.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1909, "any", any1910);
		extension1909.setVendor("");

		extension1906.add(extension1909);
		eu.sarunas.junit.TestsHelper.set(iD4Objects1905, "extension", extension1906);

		datumTarget1902.setSystemID(iD4Objects1905);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef1911 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1912 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1912.setClassID("aaaaaa");
		iD4Referencing1912.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1913 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1914 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1915 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1914, "any", any1915);
		extension1914.setVendor("aaaaaaaaaaaaaa");

		extension1913.add(extension1914);
		org.inspection_plusplus.Extension extension1916 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1917 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1916, "any", any1917);
		extension1916.setVendor("aaaaaaaaaa");

		extension1913.add(extension1916);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1912, "extension", extension1913);

		commentRef1911.add(iD4Referencing1912);
		org.inspection_plusplus.ID4Referencing iD4Referencing1918 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1918.setClassID("aaaaaaa");
		iD4Referencing1918.setUuid("aaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1919 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1920 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1921 = new java.util.ArrayList<Object>();
		any1921.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1920, "any", any1921);
		extension1920.setVendor("aaaaaaaaaaaaaaa");

		extension1919.add(extension1920);
		org.inspection_plusplus.Extension extension1922 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1923 = new java.util.ArrayList<Object>();
		any1923.add(new Object());
		any1923.add(new Object());
		any1923.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1922, "any", any1923);
		extension1922.setVendor("");

		extension1919.add(extension1922);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1918, "extension", extension1919);

		commentRef1911.add(iD4Referencing1918);
		eu.sarunas.junit.TestsHelper.set(datumTarget1902, "commentRef", commentRef1911);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1924 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(datumTarget1902, "extension", extension1924);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID1925 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID1926 = new org.inspection_plusplus.DisplayID();
		displayID1926.setLanguage("");
		displayID1926.setText("");

		displayID1925.add(displayID1926);
		org.inspection_plusplus.DisplayID displayID1927 = new org.inspection_plusplus.DisplayID();
		displayID1927.setLanguage("aaaaaaaaa");
		displayID1927.setText("");

		displayID1925.add(displayID1927);
		eu.sarunas.junit.TestsHelper.set(datumTarget1902, "displayID", displayID1925);
		java.util.ArrayList<org.inspection_plusplus.History> history1928 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(datumTarget1902, "history", history1928);

		datumTarget1879.add(datumTarget1902);
		org.inspection_plusplus.DatumTarget datumTarget1929 = new org.inspection_plusplus.DatumTarget();
		datumTarget1929.setRole("");
		org.inspection_plusplus.ID4Referencing iD4Referencing1930 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1930.setClassID("");
		iD4Referencing1930.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1931 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1932 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1933 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1932, "any", any1933);
		extension1932.setVendor("aaaaaaaaaaaaa");

		extension1931.add(extension1932);
		org.inspection_plusplus.Extension extension1934 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1935 = new java.util.ArrayList<Object>();
		any1935.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1934, "any", any1935);
		extension1934.setVendor("");

		extension1931.add(extension1934);
		org.inspection_plusplus.Extension extension1936 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1937 = new java.util.ArrayList<Object>();
		any1937.add(new Object());
		any1937.add(new Object());
		any1937.add(new Object());
		any1937.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1936, "any", any1937);
		extension1936.setVendor("aa");

		extension1931.add(extension1936);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1930, "extension", extension1931);

		eu.sarunas.junit.TestsHelper.set(datumTarget1929, "ipeGeometricRef", iD4Referencing1930);
		datumTarget1929.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects1938 = new org.inspection_plusplus.ID4Objects();
		iD4Objects1938.setUuid("aaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1939 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1940 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1941 = new java.util.ArrayList<Object>();
		any1941.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1940, "any", any1941);
		extension1940.setVendor("aaaaaaaaaaaaa");

		extension1939.add(extension1940);
		org.inspection_plusplus.Extension extension1942 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1943 = new java.util.ArrayList<Object>();
		any1943.add(new Object());
		any1943.add(new Object());
		any1943.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1942, "any", any1943);
		extension1942.setVendor("");

		extension1939.add(extension1942);
		org.inspection_plusplus.Extension extension1944 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1945 = new java.util.ArrayList<Object>();
		any1945.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1944, "any", any1945);
		extension1944.setVendor("aaaaaaa");

		extension1939.add(extension1944);
		eu.sarunas.junit.TestsHelper.set(iD4Objects1938, "extension", extension1939);

		datumTarget1929.setSystemID(iD4Objects1938);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef1946 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1947 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1947.setClassID("aa");
		iD4Referencing1947.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1948 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1949 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1950 = new java.util.ArrayList<Object>();
		any1950.add(new Object());
		any1950.add(new Object());
		any1950.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1949, "any", any1950);
		extension1949.setVendor("");

		extension1948.add(extension1949);
		org.inspection_plusplus.Extension extension1951 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1952 = new java.util.ArrayList<Object>();
		any1952.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1951, "any", any1952);
		extension1951.setVendor("");

		extension1948.add(extension1951);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1947, "extension", extension1948);

		commentRef1946.add(iD4Referencing1947);
		org.inspection_plusplus.ID4Referencing iD4Referencing1953 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1953.setClassID("aaaaaa");
		iD4Referencing1953.setUuid("aaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1954 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1955 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1956 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1955, "any", any1956);
		extension1955.setVendor("");

		extension1954.add(extension1955);
		org.inspection_plusplus.Extension extension1957 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1958 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1957, "any", any1958);
		extension1957.setVendor("aaaaaaaaaaaaa");

		extension1954.add(extension1957);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1953, "extension", extension1954);

		commentRef1946.add(iD4Referencing1953);
		eu.sarunas.junit.TestsHelper.set(datumTarget1929, "commentRef", commentRef1946);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1959 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(datumTarget1929, "extension", extension1959);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID1960 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(datumTarget1929, "displayID", displayID1960);
		java.util.ArrayList<org.inspection_plusplus.History> history1961 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(datumTarget1929, "history", history1961);

		datumTarget1879.add(datumTarget1929);
		eu.sarunas.junit.TestsHelper.set(datum1877, "datumTarget", datumTarget1879);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeGeometricRef1962 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1963 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1963.setClassID("");
		iD4Referencing1963.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1964 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1965 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1966 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1965, "any", any1966);
		extension1965.setVendor("a");

		extension1964.add(extension1965);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1963, "extension", extension1964);

		ipeGeometricRef1962.add(iD4Referencing1963);
		eu.sarunas.junit.TestsHelper.set(datum1877, "ipeGeometricRef", ipeGeometricRef1962);
		datum1877.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects1967 = new org.inspection_plusplus.ID4Objects();
		iD4Objects1967.setUuid("aaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1968 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects1967, "extension", extension1968);

		datum1877.setSystemID(iD4Objects1967);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef1969 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1970 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1970.setClassID("");
		iD4Referencing1970.setUuid("aaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1971 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1970, "extension", extension1971);

		commentRef1969.add(iD4Referencing1970);
		eu.sarunas.junit.TestsHelper.set(datum1877, "commentRef", commentRef1969);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1972 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1973 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1974 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1973, "any", any1974);
		extension1973.setVendor("");

		extension1972.add(extension1973);
		org.inspection_plusplus.Extension extension1975 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1976 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1975, "any", any1976);
		extension1975.setVendor("aaaaaaaaaaaaaaaaa");

		extension1972.add(extension1975);
		eu.sarunas.junit.TestsHelper.set(datum1877, "extension", extension1972);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID1977 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID1978 = new org.inspection_plusplus.DisplayID();
		displayID1978.setLanguage("");
		displayID1978.setText("");

		displayID1977.add(displayID1978);
		eu.sarunas.junit.TestsHelper.set(datum1877, "displayID", displayID1977);
		java.util.ArrayList<org.inspection_plusplus.History> history1979 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(datum1877, "history", history1979);

		datum1801.add(datum1877);
		org.inspection_plusplus.Datum datum1980 = new org.inspection_plusplus.Datum();
		org.inspection_plusplus.Vector3D vector3D1981 = new org.inspection_plusplus.Vector3D();
		vector3D1981.setX(225.05);
		vector3D1981.setY(75.47);
		vector3D1981.setZ(-235.65);

		datum1980.setDisplacement(vector3D1981);
		datum1980.setRole("");
		java.util.ArrayList<org.inspection_plusplus.DatumTarget> datumTarget1982 = new java.util.ArrayList<org.inspection_plusplus.DatumTarget>();
		org.inspection_plusplus.DatumTarget datumTarget1983 = new org.inspection_plusplus.DatumTarget();
		datumTarget1983.setRole("aaaa");
		org.inspection_plusplus.ID4Referencing iD4Referencing1984 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1984.setClassID("");
		iD4Referencing1984.setUuid("aaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1985 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1986 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1987 = new java.util.ArrayList<Object>();
		any1987.add(new Object());
		any1987.add(new Object());
		any1987.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension1986, "any", any1987);
		extension1986.setVendor("aaaaa");

		extension1985.add(extension1986);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1984, "extension", extension1985);

		eu.sarunas.junit.TestsHelper.set(datumTarget1983, "ipeGeometricRef", iD4Referencing1984);
		datumTarget1983.setName("aaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects1988 = new org.inspection_plusplus.ID4Objects();
		iD4Objects1988.setUuid("aaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1989 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects1988, "extension", extension1989);

		datumTarget1983.setSystemID(iD4Objects1988);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef1990 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing1991 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1991.setClassID("");
		iD4Referencing1991.setUuid("aaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1992 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1991, "extension", extension1992);

		commentRef1990.add(iD4Referencing1991);
		org.inspection_plusplus.ID4Referencing iD4Referencing1993 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1993.setClassID("");
		iD4Referencing1993.setUuid("aaaaaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1994 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1995 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any1996 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1995, "any", any1996);
		extension1995.setVendor("");

		extension1994.add(extension1995);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1993, "extension", extension1994);

		commentRef1990.add(iD4Referencing1993);
		org.inspection_plusplus.ID4Referencing iD4Referencing1997 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing1997.setClassID("aaaaaaa");
		iD4Referencing1997.setUuid("a");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension1998 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension1999 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2000 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension1999, "any", any2000);
		extension1999.setVendor("aaaaaaaaaaaaaaaaaaaaaaaa");

		extension1998.add(extension1999);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing1997, "extension", extension1998);

		commentRef1990.add(iD4Referencing1997);
		eu.sarunas.junit.TestsHelper.set(datumTarget1983, "commentRef", commentRef1990);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2001 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(datumTarget1983, "extension", extension2001);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2002 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID2003 = new org.inspection_plusplus.DisplayID();
		displayID2003.setLanguage("");
		displayID2003.setText("aaaaaaa");

		displayID2002.add(displayID2003);
		eu.sarunas.junit.TestsHelper.set(datumTarget1983, "displayID", displayID2002);
		java.util.ArrayList<org.inspection_plusplus.History> history2004 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(datumTarget1983, "history", history2004);

		datumTarget1982.add(datumTarget1983);
		org.inspection_plusplus.DatumTarget datumTarget2005 = new org.inspection_plusplus.DatumTarget();
		datumTarget2005.setRole("");
		org.inspection_plusplus.ID4Referencing iD4Referencing2006 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2006.setClassID("aaaaaaaaaaaaa");
		iD4Referencing2006.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2007 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2008 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2009 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2008, "any", any2009);
		extension2008.setVendor("aaaa");

		extension2007.add(extension2008);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2006, "extension", extension2007);

		eu.sarunas.junit.TestsHelper.set(datumTarget2005, "ipeGeometricRef", iD4Referencing2006);
		datumTarget2005.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects2010 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2010.setUuid("a");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2011 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects2010, "extension", extension2011);

		datumTarget2005.setSystemID(iD4Objects2010);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2012 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(datumTarget2005, "commentRef", commentRef2012);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2013 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(datumTarget2005, "extension", extension2013);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2014 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID2015 = new org.inspection_plusplus.DisplayID();
		displayID2015.setLanguage("");
		displayID2015.setText("aaaaaaaaaaaaa");

		displayID2014.add(displayID2015);
		org.inspection_plusplus.DisplayID displayID2016 = new org.inspection_plusplus.DisplayID();
		displayID2016.setLanguage("");
		displayID2016.setText("");

		displayID2014.add(displayID2016);
		eu.sarunas.junit.TestsHelper.set(datumTarget2005, "displayID", displayID2014);
		java.util.ArrayList<org.inspection_plusplus.History> history2017 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history2018 = new org.inspection_plusplus.History();
		history2018.setAction("aaaaaaaaaaaa");
		history2018.setAuthor("");
		history2018.setComment("a");
		history2018.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 20, 10)));

		history2017.add(history2018);
		eu.sarunas.junit.TestsHelper.set(datumTarget2005, "history", history2017);

		datumTarget1982.add(datumTarget2005);
		eu.sarunas.junit.TestsHelper.set(datum1980, "datumTarget", datumTarget1982);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeGeometricRef2019 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(datum1980, "ipeGeometricRef", ipeGeometricRef2019);
		datum1980.setName("aaaaaaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects2020 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2020.setUuid("aaaaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2021 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2022 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2023 = new java.util.ArrayList<Object>();
		any2023.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2022, "any", any2023);
		extension2022.setVendor("");

		extension2021.add(extension2022);
		org.inspection_plusplus.Extension extension2024 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2025 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2024, "any", any2025);
		extension2024.setVendor("");

		extension2021.add(extension2024);
		eu.sarunas.junit.TestsHelper.set(iD4Objects2020, "extension", extension2021);

		datum1980.setSystemID(iD4Objects2020);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2026 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(datum1980, "commentRef", commentRef2026);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2027 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2028 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2029 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2028, "any", any2029);
		extension2028.setVendor("");

		extension2027.add(extension2028);
		eu.sarunas.junit.TestsHelper.set(datum1980, "extension", extension2027);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2030 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(datum1980, "displayID", displayID2030);
		java.util.ArrayList<org.inspection_plusplus.History> history2031 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(datum1980, "history", history2031);

		datum1801.add(datum1980);
		eu.sarunas.junit.TestsHelper.set(referenceSystem1800, "datum", datum1801);
		java.util.ArrayList<org.inspection_plusplus.AlignmentStrategy> alignmentStrategy2032 = new java.util.ArrayList<org.inspection_plusplus.AlignmentStrategy>();
		org.inspection_plusplus.AlignmentBestfit alignmentBestfit2033 = new org.inspection_plusplus.AlignmentBestfit();
		alignmentBestfit2033.setTerminatingCondition(251.58);
		alignmentBestfit2033.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects2034 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2034.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2035 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects2034, "extension", extension2035);

		alignmentBestfit2033.setSystemID(iD4Objects2034);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2036 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing2037 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2037.setClassID("");
		iD4Referencing2037.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2038 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2037, "extension", extension2038);

		commentRef2036.add(iD4Referencing2037);
		eu.sarunas.junit.TestsHelper.set(alignmentBestfit2033, "commentRef", commentRef2036);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2039 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2040 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2041 = new java.util.ArrayList<Object>();
		any2041.add(new Object());
		any2041.add(new Object());
		any2041.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2040, "any", any2041);
		extension2040.setVendor("a");

		extension2039.add(extension2040);
		eu.sarunas.junit.TestsHelper.set(alignmentBestfit2033, "extension", extension2039);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2042 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID2043 = new org.inspection_plusplus.DisplayID();
		displayID2043.setLanguage("aaaaaaaaaa");
		displayID2043.setText("");

		displayID2042.add(displayID2043);
		org.inspection_plusplus.DisplayID displayID2044 = new org.inspection_plusplus.DisplayID();
		displayID2044.setLanguage("");
		displayID2044.setText("aaaaaa");

		displayID2042.add(displayID2044);
		eu.sarunas.junit.TestsHelper.set(alignmentBestfit2033, "displayID", displayID2042);
		java.util.ArrayList<org.inspection_plusplus.History> history2045 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(alignmentBestfit2033, "history", history2045);

		alignmentStrategy2032.add(alignmentBestfit2033);
		eu.sarunas.junit.TestsHelper.set(referenceSystem1800, "alignmentStrategy", alignmentStrategy2032);
		referenceSystem1800.setName("aaaaaaaaaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects2046 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2046.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2047 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects2046, "extension", extension2047);

		referenceSystem1800.setSystemID(iD4Objects2046);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2048 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing2049 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2049.setClassID("");
		iD4Referencing2049.setUuid("aaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2050 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2051 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2052 = new java.util.ArrayList<Object>();
		any2052.add(new Object());
		any2052.add(new Object());
		any2052.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2051, "any", any2052);
		extension2051.setVendor("");

		extension2050.add(extension2051);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2049, "extension", extension2050);

		commentRef2048.add(iD4Referencing2049);
		org.inspection_plusplus.ID4Referencing iD4Referencing2053 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2053.setClassID("aa");
		iD4Referencing2053.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2054 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2055 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2056 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2055, "any", any2056);
		extension2055.setVendor("aaaaa");

		extension2054.add(extension2055);
		org.inspection_plusplus.Extension extension2057 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2058 = new java.util.ArrayList<Object>();
		any2058.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2057, "any", any2058);
		extension2057.setVendor("");

		extension2054.add(extension2057);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2053, "extension", extension2054);

		commentRef2048.add(iD4Referencing2053);
		eu.sarunas.junit.TestsHelper.set(referenceSystem1800, "commentRef", commentRef2048);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2059 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2060 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2061 = new java.util.ArrayList<Object>();
		any2061.add(new Object());
		any2061.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2060, "any", any2061);
		extension2060.setVendor("aaaa");

		extension2059.add(extension2060);
		org.inspection_plusplus.Extension extension2062 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2063 = new java.util.ArrayList<Object>();
		any2063.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2062, "any", any2063);
		extension2062.setVendor("");

		extension2059.add(extension2062);
		org.inspection_plusplus.Extension extension2064 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2065 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2064, "any", any2065);
		extension2064.setVendor("aaaaa");

		extension2059.add(extension2064);
		eu.sarunas.junit.TestsHelper.set(referenceSystem1800, "extension", extension2059);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2066 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID2067 = new org.inspection_plusplus.DisplayID();
		displayID2067.setLanguage("");
		displayID2067.setText("");

		displayID2066.add(displayID2067);
		org.inspection_plusplus.DisplayID displayID2068 = new org.inspection_plusplus.DisplayID();
		displayID2068.setLanguage("");
		displayID2068.setText("aaaaaaaaaaaaaa");

		displayID2066.add(displayID2068);
		eu.sarunas.junit.TestsHelper.set(referenceSystem1800, "displayID", displayID2066);
		java.util.ArrayList<org.inspection_plusplus.History> history2069 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(referenceSystem1800, "history", history2069);

		referenceSystem1727.add(referenceSystem1800);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan1698, "referenceSystem", referenceSystem1727);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> inspectionTaskRef2070 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing2071 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2071.setClassID("");
		iD4Referencing2071.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2072 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2071, "extension", extension2072);

		inspectionTaskRef2070.add(iD4Referencing2071);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan1698, "inspectionTaskRef", inspectionTaskRef2070);
		java.util.ArrayList<org.inspection_plusplus.QC> qc2073 = new java.util.ArrayList<org.inspection_plusplus.QC>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan1698, "qc", qc2073);
		inspectionPlan1698.setName("aaaaaaaaaaaaaaaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects2074 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2074.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2075 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2076 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2077 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2076, "any", any2077);
		extension2076.setVendor("");

		extension2075.add(extension2076);
		org.inspection_plusplus.Extension extension2078 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2079 = new java.util.ArrayList<Object>();
		any2079.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2078, "any", any2079);
		extension2078.setVendor("aaaaaaaaaaaaaaaaaaa");

		extension2075.add(extension2078);
		eu.sarunas.junit.TestsHelper.set(iD4Objects2074, "extension", extension2075);

		inspectionPlan1698.setSystemID(iD4Objects2074);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2080 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing2081 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2081.setClassID("a");
		iD4Referencing2081.setUuid("aaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2082 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2083 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2084 = new java.util.ArrayList<Object>();
		any2084.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2083, "any", any2084);
		extension2083.setVendor("aaaa");

		extension2082.add(extension2083);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2081, "extension", extension2082);

		commentRef2080.add(iD4Referencing2081);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan1698, "commentRef", commentRef2080);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2085 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2086 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2087 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2086, "any", any2087);
		extension2086.setVendor("");

		extension2085.add(extension2086);
		org.inspection_plusplus.Extension extension2088 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2089 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2088, "any", any2089);
		extension2088.setVendor("");

		extension2085.add(extension2088);
		org.inspection_plusplus.Extension extension2090 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2091 = new java.util.ArrayList<Object>();
		any2091.add(new Object());
		any2091.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2090, "any", any2091);
		extension2090.setVendor("aaa");

		extension2085.add(extension2090);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan1698, "extension", extension2085);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2092 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID2093 = new org.inspection_plusplus.DisplayID();
		displayID2093.setLanguage("");
		displayID2093.setText("aaaaaaaaaa");

		displayID2092.add(displayID2093);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan1698, "displayID", displayID2092);
		java.util.ArrayList<org.inspection_plusplus.History> history2094 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history2095 = new org.inspection_plusplus.History();
		history2095.setAction("");
		history2095.setAuthor("");
		history2095.setComment("aaaa");
		history2095.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 20, 4)));

		history2094.add(history2095);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan1698, "history", history2094);
		org.inspection_plusplus.Sender sender2096 = new org.inspection_plusplus.Sender();
		sender2096.setAppID("aa");
		sender2096.setDomain("");
		org.inspection_plusplus.Extension extension2097 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2098 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2097, "any", any2098);
		extension2097.setVendor("");

		org.inspection_plusplus.ReturnStatus res = testObject.storeInspectionPlan(inspectionPlan1698, "", "aaaa", sender2096, extension2097);
		junit.framework.Assert.fail();
	};

	@org.junit.Test
	public void testtestStoreInspectionPlan7() throws Throwable
	{
		org.inspection_plusplus.service.v2.WebServiceImpl testObject = new org.inspection_plusplus.service.v2.WebServiceImpl();
		org.inspection_plusplus.InspectionPlan inspectionPlan2099 = new org.inspection_plusplus.InspectionPlan();
		inspectionPlan2099.setInspectionCategories("aaaaa");
		inspectionPlan2099.setIPsymmetry("");
		inspectionPlan2099.setPartQAversion("a");
		eu.sarunas.junit.TestsHelper.set(inspectionPlan2099, "pdMmaturity", "aaaaaaaaaaaa");
		inspectionPlan2099.setQAmaturity("aa");
		inspectionPlan2099.setQAversion("");
		java.util.ArrayList<org.inspection_plusplus.Comment> comment2100 = new java.util.ArrayList<org.inspection_plusplus.Comment>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan2099, "comment", comment2100);
		java.util.ArrayList<org.inspection_plusplus.Tolerance> tolerance2101 = new java.util.ArrayList<org.inspection_plusplus.Tolerance>();
		org.inspection_plusplus.TolAxisAngleX tolAxisAngleX2102 = new org.inspection_plusplus.TolAxisAngleX();
		org.inspection_plusplus.IntervalToleranceCriterion intervalToleranceCriterion2103 = new org.inspection_plusplus.IntervalToleranceCriterion();
		intervalToleranceCriterion2103.setInOut("");
		intervalToleranceCriterion2103.setLowerToleranceValue(new java.math.BigDecimal(-410.32));
		intervalToleranceCriterion2103.setUpperToleranceValue(new java.math.BigDecimal(-160.08));

		tolAxisAngleX2102.setCriterion(intervalToleranceCriterion2103);
		org.inspection_plusplus.ID4Referencing iD4Referencing2104 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2104.setClassID("");
		iD4Referencing2104.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2105 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2106 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2107 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2106, "any", any2107);
		extension2106.setVendor("");

		extension2105.add(extension2106);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2104, "extension", extension2105);

		tolAxisAngleX2102.setReferenceSystemRef(iD4Referencing2104);
		tolAxisAngleX2102.setStandardConformity("");
		org.inspection_plusplus.ToleranceSource toleranceSource2108 = new org.inspection_plusplus.ToleranceSource();
		toleranceSource2108.setTyp("a");
		toleranceSource2108.setValidFrom(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 22, 2)));
		toleranceSource2108.setValidTo(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 52, 25)));
		toleranceSource2108.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects2109 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2109.setUuid("aa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2110 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects2109, "extension", extension2110);

		toleranceSource2108.setSystemID(iD4Objects2109);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2111 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing2112 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2112.setClassID("");
		iD4Referencing2112.setUuid("a");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2113 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2114 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2115 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2114, "any", any2115);
		extension2114.setVendor("aaa");

		extension2113.add(extension2114);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2112, "extension", extension2113);

		commentRef2111.add(iD4Referencing2112);
		eu.sarunas.junit.TestsHelper.set(toleranceSource2108, "commentRef", commentRef2111);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2116 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(toleranceSource2108, "extension", extension2116);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2117 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID2118 = new org.inspection_plusplus.DisplayID();
		displayID2118.setLanguage("");
		displayID2118.setText("aa");

		displayID2117.add(displayID2118);
		eu.sarunas.junit.TestsHelper.set(toleranceSource2108, "displayID", displayID2117);
		java.util.ArrayList<org.inspection_plusplus.History> history2119 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(toleranceSource2108, "history", history2119);

		tolAxisAngleX2102.setToleranceSource(toleranceSource2108);
		tolAxisAngleX2102.setName("aa");
		org.inspection_plusplus.ID4Objects iD4Objects2120 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2120.setUuid("a");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2121 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects2120, "extension", extension2121);

		tolAxisAngleX2102.setSystemID(iD4Objects2120);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2122 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(tolAxisAngleX2102, "commentRef", commentRef2122);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2123 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(tolAxisAngleX2102, "extension", extension2123);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2124 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(tolAxisAngleX2102, "displayID", displayID2124);
		java.util.ArrayList<org.inspection_plusplus.History> history2125 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(tolAxisAngleX2102, "history", history2125);

		tolerance2101.add(tolAxisAngleX2102);
		org.inspection_plusplus.TolAngle tolAngle2126 = new org.inspection_plusplus.TolAngle();
		org.inspection_plusplus.IntervalToleranceCriterion intervalToleranceCriterion2127 = new org.inspection_plusplus.IntervalToleranceCriterion();
		intervalToleranceCriterion2127.setInOut("");
		intervalToleranceCriterion2127.setLowerToleranceValue(new java.math.BigDecimal(-254.14));
		intervalToleranceCriterion2127.setUpperToleranceValue(new java.math.BigDecimal(4.44));

		tolAngle2126.setCriterion(intervalToleranceCriterion2127);
		tolAngle2126.setStandardConformity("aaaaaaaaa");
		org.inspection_plusplus.ToleranceSource toleranceSource2128 = new org.inspection_plusplus.ToleranceSource();
		toleranceSource2128.setTyp("");
		toleranceSource2128.setValidFrom(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 25, 13)));
		toleranceSource2128.setValidTo(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 14, 43)));
		toleranceSource2128.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects2129 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2129.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2130 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2131 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2132 = new java.util.ArrayList<Object>();
		any2132.add(new Object());
		any2132.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2131, "any", any2132);
		extension2131.setVendor("");

		extension2130.add(extension2131);
		eu.sarunas.junit.TestsHelper.set(iD4Objects2129, "extension", extension2130);

		toleranceSource2128.setSystemID(iD4Objects2129);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2133 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(toleranceSource2128, "commentRef", commentRef2133);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2134 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2135 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2136 = new java.util.ArrayList<Object>();
		any2136.add(new Object());
		any2136.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2135, "any", any2136);
		extension2135.setVendor("aaaa");

		extension2134.add(extension2135);
		org.inspection_plusplus.Extension extension2137 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2138 = new java.util.ArrayList<Object>();
		any2138.add(new Object());
		any2138.add(new Object());
		any2138.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2137, "any", any2138);
		extension2137.setVendor("aaaa");

		extension2134.add(extension2137);
		org.inspection_plusplus.Extension extension2139 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2140 = new java.util.ArrayList<Object>();
		any2140.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2139, "any", any2140);
		extension2139.setVendor("aaa");

		extension2134.add(extension2139);
		eu.sarunas.junit.TestsHelper.set(toleranceSource2128, "extension", extension2134);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2141 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID2142 = new org.inspection_plusplus.DisplayID();
		displayID2142.setLanguage("aaaaa");
		displayID2142.setText("aaaaaa");

		displayID2141.add(displayID2142);
		eu.sarunas.junit.TestsHelper.set(toleranceSource2128, "displayID", displayID2141);
		java.util.ArrayList<org.inspection_plusplus.History> history2143 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(toleranceSource2128, "history", history2143);

		tolAngle2126.setToleranceSource(toleranceSource2128);
		tolAngle2126.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects2144 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2144.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2145 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects2144, "extension", extension2145);

		tolAngle2126.setSystemID(iD4Objects2144);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2146 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(tolAngle2126, "commentRef", commentRef2146);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2147 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2148 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2149 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2148, "any", any2149);
		extension2148.setVendor("aaaa");

		extension2147.add(extension2148);
		eu.sarunas.junit.TestsHelper.set(tolAngle2126, "extension", extension2147);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2150 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID2151 = new org.inspection_plusplus.DisplayID();
		displayID2151.setLanguage("aaaaaaaaa");
		displayID2151.setText("");

		displayID2150.add(displayID2151);
		org.inspection_plusplus.DisplayID displayID2152 = new org.inspection_plusplus.DisplayID();
		displayID2152.setLanguage("aaaaaaaaaa");
		displayID2152.setText("");

		displayID2150.add(displayID2152);
		eu.sarunas.junit.TestsHelper.set(tolAngle2126, "displayID", displayID2150);
		java.util.ArrayList<org.inspection_plusplus.History> history2153 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history2154 = new org.inspection_plusplus.History();
		history2154.setAction("a");
		history2154.setAuthor("aaaaa");
		history2154.setComment("");
		history2154.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 1, 22)));

		history2153.add(history2154);
		org.inspection_plusplus.History history2155 = new org.inspection_plusplus.History();
		history2155.setAction("aaaaaaaaaaaa");
		history2155.setAuthor("aaaaaaaa");
		history2155.setComment("");
		history2155.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 33, 11)));

		history2153.add(history2155);
		eu.sarunas.junit.TestsHelper.set(tolAngle2126, "history", history2153);

		tolerance2101.add(tolAngle2126);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan2099, "tolerance", tolerance2101);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> partRef2156 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan2099, "partRef", partRef2156);
		java.util.ArrayList<org.inspection_plusplus.IPE> ipe2157 = new java.util.ArrayList<org.inspection_plusplus.IPE>();
		org.inspection_plusplus.IPEGeometric iPEGeometric2158 = new org.inspection_plusplus.IPEGeometric();
		iPEGeometric2158.setMaterialThickness(-239.30);
		iPEGeometric2158.setMoveByMaterialThickness(true);
		org.inspection_plusplus.Vector3D vector3D2159 = new org.inspection_plusplus.Vector3D();
		vector3D2159.setX(-259.69);
		vector3D2159.setY(-42.65);
		vector3D2159.setZ(-59.95);

		iPEGeometric2158.setOrientation(vector3D2159);
		iPEGeometric2158.setOrigin(null);
		iPEGeometric2158.setTouchDirection(null);
		iPEGeometric2158.setAuxiliaryElement(true);
		iPEGeometric2158.setCalculatedElement(true);
		iPEGeometric2158.setUseLeft(true);
		iPEGeometric2158.setUseRight(true);
		org.inspection_plusplus.ID4Referencing iD4Referencing2160 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2160.setClassID("");
		iD4Referencing2160.setUuid("aaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2161 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2160, "extension", extension2161);

		iPEGeometric2158.setGeometricObjectRef(iD4Referencing2160);
		java.util.ArrayList<org.inspection_plusplus.CalculationNominalElement> calculationNominalElement2162 = new java.util.ArrayList<org.inspection_plusplus.CalculationNominalElement>();
		org.inspection_plusplus.CalculationNominalElement calculationNominalElement2163 = new org.inspection_plusplus.CalculationNominalElement();
		calculationNominalElement2163.setOperation("");
		java.util.ArrayList<org.inspection_plusplus.OperationParameter> operationParameter2164 = new java.util.ArrayList<org.inspection_plusplus.OperationParameter>();
		org.inspection_plusplus.OperationParameter operationParameter2165 = new org.inspection_plusplus.OperationParameter();
		org.inspection_plusplus.KeyValueOperatorList keyValueOperatorList2166 = new org.inspection_plusplus.KeyValueOperatorList();
		java.util.ArrayList<org.inspection_plusplus.KeyValueOperator> keyValueOperator2167 = new java.util.ArrayList<org.inspection_plusplus.KeyValueOperator>();
		org.inspection_plusplus.KeyValueOperator keyValueOperator2168 = new org.inspection_plusplus.KeyValueOperator();
		keyValueOperator2168.setKey("");
		keyValueOperator2168.setOperator("");
		keyValueOperator2168.setValue("");

		keyValueOperator2167.add(keyValueOperator2168);
		eu.sarunas.junit.TestsHelper.set(keyValueOperatorList2166, "keyValueOperator", keyValueOperator2167);

		operationParameter2165.setKeyValuePairs(keyValueOperatorList2166);

		operationParameter2164.add(operationParameter2165);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement2163, "operationParameter", operationParameter2164);
		java.util.ArrayList<org.inspection_plusplus.Operand> operand2169 = new java.util.ArrayList<org.inspection_plusplus.Operand>();
		org.inspection_plusplus.Operand operand2170 = new org.inspection_plusplus.Operand();
		operand2170.setIndex(new java.math.BigInteger("57"));
		operand2170.setRole("");
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> referenceSystemRef2171 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(operand2170, "referenceSystemRef", referenceSystemRef2171);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeGeometricRef2172 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(operand2170, "ipeGeometricRef", ipeGeometricRef2172);

		operand2169.add(operand2170);
		org.inspection_plusplus.Operand operand2173 = new org.inspection_plusplus.Operand();
		operand2173.setIndex(new java.math.BigInteger("152"));
		operand2173.setRole("");
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> referenceSystemRef2174 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing2175 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2175.setClassID("");
		iD4Referencing2175.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2176 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2177 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2178 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2177, "any", any2178);
		extension2177.setVendor("");

		extension2176.add(extension2177);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2175, "extension", extension2176);

		referenceSystemRef2174.add(iD4Referencing2175);
		org.inspection_plusplus.ID4Referencing iD4Referencing2179 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2179.setClassID("aaa");
		iD4Referencing2179.setUuid("aaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2180 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2181 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2182 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2181, "any", any2182);
		extension2181.setVendor("");

		extension2180.add(extension2181);
		org.inspection_plusplus.Extension extension2183 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2184 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2183, "any", any2184);
		extension2183.setVendor("");

		extension2180.add(extension2183);
		org.inspection_plusplus.Extension extension2185 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2186 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2185, "any", any2186);
		extension2185.setVendor("");

		extension2180.add(extension2185);
		org.inspection_plusplus.Extension extension2187 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2188 = new java.util.ArrayList<Object>();
		any2188.add(new Object());
		any2188.add(new Object());
		any2188.add(new Object());
		any2188.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2187, "any", any2188);
		extension2187.setVendor("aaaaaaaaaaaaaa");

		extension2180.add(extension2187);
		org.inspection_plusplus.Extension extension2189 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2190 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2189, "any", any2190);
		extension2189.setVendor("");

		extension2180.add(extension2189);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2179, "extension", extension2180);

		referenceSystemRef2174.add(iD4Referencing2179);
		eu.sarunas.junit.TestsHelper.set(operand2173, "referenceSystemRef", referenceSystemRef2174);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeGeometricRef2191 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(operand2173, "ipeGeometricRef", ipeGeometricRef2191);

		operand2169.add(operand2173);
		org.inspection_plusplus.Operand operand2192 = new org.inspection_plusplus.Operand();
		operand2192.setIndex(new java.math.BigInteger("-191"));
		operand2192.setRole("aaa");
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> referenceSystemRef2193 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(operand2192, "referenceSystemRef", referenceSystemRef2193);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeGeometricRef2194 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing2195 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2195.setClassID("aaaaaaaaaaaaaaaaa");
		iD4Referencing2195.setUuid("aaaaaaaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2196 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2195, "extension", extension2196);

		ipeGeometricRef2194.add(iD4Referencing2195);
		org.inspection_plusplus.ID4Referencing iD4Referencing2197 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2197.setClassID("");
		iD4Referencing2197.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2198 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2199 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2200 = new java.util.ArrayList<Object>();
		any2200.add(new Object());
		any2200.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2199, "any", any2200);
		extension2199.setVendor("aaaaaaaaaa");

		extension2198.add(extension2199);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2197, "extension", extension2198);

		ipeGeometricRef2194.add(iD4Referencing2197);
		eu.sarunas.junit.TestsHelper.set(operand2192, "ipeGeometricRef", ipeGeometricRef2194);

		operand2169.add(operand2192);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement2163, "operand", operand2169);
		calculationNominalElement2163.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects2201 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2201.setUuid("aaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2202 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2203 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2204 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2203, "any", any2204);
		extension2203.setVendor("");

		extension2202.add(extension2203);
		org.inspection_plusplus.Extension extension2205 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2206 = new java.util.ArrayList<Object>();
		any2206.add(new Object());
		any2206.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2205, "any", any2206);
		extension2205.setVendor("aaaaaa");

		extension2202.add(extension2205);
		org.inspection_plusplus.Extension extension2207 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2208 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2207, "any", any2208);
		extension2207.setVendor("aa");

		extension2202.add(extension2207);
		eu.sarunas.junit.TestsHelper.set(iD4Objects2201, "extension", extension2202);

		calculationNominalElement2163.setSystemID(iD4Objects2201);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2209 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing2210 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2210.setClassID("");
		iD4Referencing2210.setUuid("aaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2211 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2210, "extension", extension2211);

		commentRef2209.add(iD4Referencing2210);
		org.inspection_plusplus.ID4Referencing iD4Referencing2212 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2212.setClassID("aaaaaaa");
		iD4Referencing2212.setUuid("aaaaaaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2213 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2214 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2215 = new java.util.ArrayList<Object>();
		any2215.add(new Object());
		any2215.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2214, "any", any2215);
		extension2214.setVendor("");

		extension2213.add(extension2214);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2212, "extension", extension2213);

		commentRef2209.add(iD4Referencing2212);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement2163, "commentRef", commentRef2209);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2216 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2217 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2218 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2217, "any", any2218);
		extension2217.setVendor("");

		extension2216.add(extension2217);
		org.inspection_plusplus.Extension extension2219 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2220 = new java.util.ArrayList<Object>();
		any2220.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2219, "any", any2220);
		extension2219.setVendor("aaa");

		extension2216.add(extension2219);
		org.inspection_plusplus.Extension extension2221 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2222 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2221, "any", any2222);
		extension2221.setVendor("");

		extension2216.add(extension2221);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement2163, "extension", extension2216);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2223 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement2163, "displayID", displayID2223);
		java.util.ArrayList<org.inspection_plusplus.History> history2224 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history2225 = new org.inspection_plusplus.History();
		history2225.setAction("aaaaaaaaaaaaaaaaaaaa");
		history2225.setAuthor("");
		history2225.setComment("");
		history2225.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 49, 8)));

		history2224.add(history2225);
		org.inspection_plusplus.History history2226 = new org.inspection_plusplus.History();
		history2226.setAction("");
		history2226.setAuthor("aaa");
		history2226.setComment("");
		history2226.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 2, 44)));

		history2224.add(history2226);
		org.inspection_plusplus.History history2227 = new org.inspection_plusplus.History();
		history2227.setAction("");
		history2227.setAuthor("aaaaaaaaaa");
		history2227.setComment("");
		history2227.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 28, 43)));

		history2224.add(history2227);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement2163, "history", history2224);

		calculationNominalElement2162.add(calculationNominalElement2163);
		org.inspection_plusplus.CalculationNominalElement calculationNominalElement2228 = new org.inspection_plusplus.CalculationNominalElement();
		calculationNominalElement2228.setOperation("");
		java.util.ArrayList<org.inspection_plusplus.OperationParameter> operationParameter2229 = new java.util.ArrayList<org.inspection_plusplus.OperationParameter>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement2228, "operationParameter", operationParameter2229);
		java.util.ArrayList<org.inspection_plusplus.Operand> operand2230 = new java.util.ArrayList<org.inspection_plusplus.Operand>();
		org.inspection_plusplus.Operand operand2231 = new org.inspection_plusplus.Operand();
		operand2231.setIndex(new java.math.BigInteger("-46"));
		operand2231.setRole("aaaa");
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> referenceSystemRef2232 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing2233 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2233.setClassID("");
		iD4Referencing2233.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2234 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2235 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2236 = new java.util.ArrayList<Object>();
		any2236.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2235, "any", any2236);
		extension2235.setVendor("a");

		extension2234.add(extension2235);
		org.inspection_plusplus.Extension extension2237 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2238 = new java.util.ArrayList<Object>();
		any2238.add(new Object());
		any2238.add(new Object());
		any2238.add(new Object());
		any2238.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2237, "any", any2238);
		extension2237.setVendor("");

		extension2234.add(extension2237);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2233, "extension", extension2234);

		referenceSystemRef2232.add(iD4Referencing2233);
		eu.sarunas.junit.TestsHelper.set(operand2231, "referenceSystemRef", referenceSystemRef2232);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeGeometricRef2239 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(operand2231, "ipeGeometricRef", ipeGeometricRef2239);

		operand2230.add(operand2231);
		org.inspection_plusplus.Operand operand2240 = new org.inspection_plusplus.Operand();
		operand2240.setIndex(new java.math.BigInteger("26"));
		operand2240.setRole("a");
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> referenceSystemRef2241 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(operand2240, "referenceSystemRef", referenceSystemRef2241);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeGeometricRef2242 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing2243 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2243.setClassID("aa");
		iD4Referencing2243.setUuid("aaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2244 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2243, "extension", extension2244);

		ipeGeometricRef2242.add(iD4Referencing2243);
		eu.sarunas.junit.TestsHelper.set(operand2240, "ipeGeometricRef", ipeGeometricRef2242);

		operand2230.add(operand2240);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement2228, "operand", operand2230);
		calculationNominalElement2228.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects2245 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2245.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2246 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects2245, "extension", extension2246);

		calculationNominalElement2228.setSystemID(iD4Objects2245);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2247 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing2248 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2248.setClassID("aa");
		iD4Referencing2248.setUuid("aaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2249 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2248, "extension", extension2249);

		commentRef2247.add(iD4Referencing2248);
		org.inspection_plusplus.ID4Referencing iD4Referencing2250 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2250.setClassID("aaaaaaaaaa");
		iD4Referencing2250.setUuid("aaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2251 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2252 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2253 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2252, "any", any2253);
		extension2252.setVendor("aaaa");

		extension2251.add(extension2252);
		org.inspection_plusplus.Extension extension2254 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2255 = new java.util.ArrayList<Object>();
		any2255.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2254, "any", any2255);
		extension2254.setVendor("");

		extension2251.add(extension2254);
		org.inspection_plusplus.Extension extension2256 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2257 = new java.util.ArrayList<Object>();
		any2257.add(new Object());
		any2257.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2256, "any", any2257);
		extension2256.setVendor("aaaaaaaaaaaa");

		extension2251.add(extension2256);
		org.inspection_plusplus.Extension extension2258 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2259 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2258, "any", any2259);
		extension2258.setVendor("aa");

		extension2251.add(extension2258);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2250, "extension", extension2251);

		commentRef2247.add(iD4Referencing2250);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement2228, "commentRef", commentRef2247);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2260 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement2228, "extension", extension2260);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2261 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID2262 = new org.inspection_plusplus.DisplayID();
		displayID2262.setLanguage("aaaaaaaaaaaaaaaaaa");
		displayID2262.setText("aaaaaaaaaaaa");

		displayID2261.add(displayID2262);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement2228, "displayID", displayID2261);
		java.util.ArrayList<org.inspection_plusplus.History> history2263 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history2264 = new org.inspection_plusplus.History();
		history2264.setAction("");
		history2264.setAuthor("aaaaa");
		history2264.setComment("");
		history2264.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 59, 31)));

		history2263.add(history2264);
		org.inspection_plusplus.History history2265 = new org.inspection_plusplus.History();
		history2265.setAction("");
		history2265.setAuthor("");
		history2265.setComment("aaaaaaaa");
		history2265.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 30, 42)));

		history2263.add(history2265);
		org.inspection_plusplus.History history2266 = new org.inspection_plusplus.History();
		history2266.setAction("aaaaa");
		history2266.setAuthor("");
		history2266.setComment("");
		history2266.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 10, 5)));

		history2263.add(history2266);
		org.inspection_plusplus.History history2267 = new org.inspection_plusplus.History();
		history2267.setAction("");
		history2267.setAuthor("");
		history2267.setComment("aaaaaaaa");
		history2267.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 47, 40)));

		history2263.add(history2267);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement2228, "history", history2263);

		calculationNominalElement2162.add(calculationNominalElement2228);
		eu.sarunas.junit.TestsHelper.set(iPEGeometric2158, "calculationNominalElement", calculationNominalElement2162);
		iPEGeometric2158.setDescription("");
		iPEGeometric2158.setFunction("");
		iPEGeometric2158.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects2268 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2268.setUuid("aaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2269 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects2268, "extension", extension2269);

		iPEGeometric2158.setSystemID(iD4Objects2268);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2270 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing2271 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2271.setClassID("");
		iD4Referencing2271.setUuid("aaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2272 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2271, "extension", extension2272);

		commentRef2270.add(iD4Referencing2271);
		eu.sarunas.junit.TestsHelper.set(iPEGeometric2158, "commentRef", commentRef2270);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2273 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2274 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2275 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2274, "any", any2275);
		extension2274.setVendor("aaaaaaaaaaaa");

		extension2273.add(extension2274);
		org.inspection_plusplus.Extension extension2276 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2277 = new java.util.ArrayList<Object>();
		any2277.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2276, "any", any2277);
		extension2276.setVendor("");

		extension2273.add(extension2276);
		eu.sarunas.junit.TestsHelper.set(iPEGeometric2158, "extension", extension2273);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2278 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(iPEGeometric2158, "displayID", displayID2278);
		java.util.ArrayList<org.inspection_plusplus.History> history2279 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(iPEGeometric2158, "history", history2279);

		ipe2157.add(iPEGeometric2158);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan2099, "ipe", ipe2157);
		java.util.ArrayList<org.inspection_plusplus.ReferenceSystem> referenceSystem2280 = new java.util.ArrayList<org.inspection_plusplus.ReferenceSystem>();
		org.inspection_plusplus.ReferenceSystem referenceSystem2281 = new org.inspection_plusplus.ReferenceSystem();
		java.util.ArrayList<org.inspection_plusplus.Datum> datum2282 = new java.util.ArrayList<org.inspection_plusplus.Datum>();
		eu.sarunas.junit.TestsHelper.set(referenceSystem2281, "datum", datum2282);
		java.util.ArrayList<org.inspection_plusplus.AlignmentStrategy> alignmentStrategy2283 = new java.util.ArrayList<org.inspection_plusplus.AlignmentStrategy>();
		org.inspection_plusplus.AlignmentFSS alignmentFSS2284 = new org.inspection_plusplus.AlignmentFSS();
		alignmentFSS2284.setTerminatingCondition(-376.52);
		alignmentFSS2284.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects2285 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2285.setUuid("aaaaaaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2286 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2287 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2288 = new java.util.ArrayList<Object>();
		any2288.add(new Object());
		any2288.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2287, "any", any2288);
		extension2287.setVendor("aaaaaaaaaaaaaa");

		extension2286.add(extension2287);
		eu.sarunas.junit.TestsHelper.set(iD4Objects2285, "extension", extension2286);

		alignmentFSS2284.setSystemID(iD4Objects2285);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2289 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing2290 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2290.setClassID("");
		iD4Referencing2290.setUuid("aaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2291 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2290, "extension", extension2291);

		commentRef2289.add(iD4Referencing2290);
		eu.sarunas.junit.TestsHelper.set(alignmentFSS2284, "commentRef", commentRef2289);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2292 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(alignmentFSS2284, "extension", extension2292);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2293 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(alignmentFSS2284, "displayID", displayID2293);
		java.util.ArrayList<org.inspection_plusplus.History> history2294 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history2295 = new org.inspection_plusplus.History();
		history2295.setAction("");
		history2295.setAuthor("aaaaaaaaaa");
		history2295.setComment("aaa");
		history2295.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 42, 39)));

		history2294.add(history2295);
		org.inspection_plusplus.History history2296 = new org.inspection_plusplus.History();
		history2296.setAction("");
		history2296.setAuthor("aaaaaaaaaaaa");
		history2296.setComment("aaaa");
		history2296.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 36, 25)));

		history2294.add(history2296);
		eu.sarunas.junit.TestsHelper.set(alignmentFSS2284, "history", history2294);

		alignmentStrategy2283.add(alignmentFSS2284);
		org.inspection_plusplus.Alignment321 alignment3212297 = new org.inspection_plusplus.Alignment321();
		alignment3212297.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects2298 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2298.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2299 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2300 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2301 = new java.util.ArrayList<Object>();
		any2301.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2300, "any", any2301);
		extension2300.setVendor("aaaaaaaaaaaaaaaaaaaaaaaa");

		extension2299.add(extension2300);
		org.inspection_plusplus.Extension extension2302 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2303 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2302, "any", any2303);
		extension2302.setVendor("");

		extension2299.add(extension2302);
		eu.sarunas.junit.TestsHelper.set(iD4Objects2298, "extension", extension2299);

		alignment3212297.setSystemID(iD4Objects2298);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2304 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(alignment3212297, "commentRef", commentRef2304);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2305 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2306 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2307 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2306, "any", any2307);
		extension2306.setVendor("");

		extension2305.add(extension2306);
		org.inspection_plusplus.Extension extension2308 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2309 = new java.util.ArrayList<Object>();
		any2309.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2308, "any", any2309);
		extension2308.setVendor("");

		extension2305.add(extension2308);
		org.inspection_plusplus.Extension extension2310 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2311 = new java.util.ArrayList<Object>();
		any2311.add(new Object());
		any2311.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2310, "any", any2311);
		extension2310.setVendor("");

		extension2305.add(extension2310);
		org.inspection_plusplus.Extension extension2312 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2313 = new java.util.ArrayList<Object>();
		any2313.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2312, "any", any2313);
		extension2312.setVendor("");

		extension2305.add(extension2312);
		eu.sarunas.junit.TestsHelper.set(alignment3212297, "extension", extension2305);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2314 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID2315 = new org.inspection_plusplus.DisplayID();
		displayID2315.setLanguage("aaa");
		displayID2315.setText("");

		displayID2314.add(displayID2315);
		org.inspection_plusplus.DisplayID displayID2316 = new org.inspection_plusplus.DisplayID();
		displayID2316.setLanguage("");
		displayID2316.setText("aaaaaaaaaaa");

		displayID2314.add(displayID2316);
		eu.sarunas.junit.TestsHelper.set(alignment3212297, "displayID", displayID2314);
		java.util.ArrayList<org.inspection_plusplus.History> history2317 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(alignment3212297, "history", history2317);

		alignmentStrategy2283.add(alignment3212297);
		eu.sarunas.junit.TestsHelper.set(referenceSystem2281, "alignmentStrategy", alignmentStrategy2283);
		referenceSystem2281.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects2318 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2318.setUuid("aaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2319 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2320 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2321 = new java.util.ArrayList<Object>();
		any2321.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2320, "any", any2321);
		extension2320.setVendor("");

		extension2319.add(extension2320);
		eu.sarunas.junit.TestsHelper.set(iD4Objects2318, "extension", extension2319);

		referenceSystem2281.setSystemID(iD4Objects2318);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2322 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing2323 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2323.setClassID("aaaaaaaaaaaaaaa");
		iD4Referencing2323.setUuid("aaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2324 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2325 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2326 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2325, "any", any2326);
		extension2325.setVendor("");

		extension2324.add(extension2325);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2323, "extension", extension2324);

		commentRef2322.add(iD4Referencing2323);
		org.inspection_plusplus.ID4Referencing iD4Referencing2327 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2327.setClassID("");
		iD4Referencing2327.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2328 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2329 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2330 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2329, "any", any2330);
		extension2329.setVendor("aaaaaa");

		extension2328.add(extension2329);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2327, "extension", extension2328);

		commentRef2322.add(iD4Referencing2327);
		eu.sarunas.junit.TestsHelper.set(referenceSystem2281, "commentRef", commentRef2322);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2331 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2332 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2333 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2332, "any", any2333);
		extension2332.setVendor("aaaaaaaaaaa");

		extension2331.add(extension2332);
		eu.sarunas.junit.TestsHelper.set(referenceSystem2281, "extension", extension2331);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2334 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(referenceSystem2281, "displayID", displayID2334);
		java.util.ArrayList<org.inspection_plusplus.History> history2335 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(referenceSystem2281, "history", history2335);

		referenceSystem2280.add(referenceSystem2281);
		org.inspection_plusplus.ReferenceSystem referenceSystem2336 = new org.inspection_plusplus.ReferenceSystem();
		java.util.ArrayList<org.inspection_plusplus.Datum> datum2337 = new java.util.ArrayList<org.inspection_plusplus.Datum>();
		eu.sarunas.junit.TestsHelper.set(referenceSystem2336, "datum", datum2337);
		java.util.ArrayList<org.inspection_plusplus.AlignmentStrategy> alignmentStrategy2338 = new java.util.ArrayList<org.inspection_plusplus.AlignmentStrategy>();
		org.inspection_plusplus.AlignmentRPS alignmentRPS2339 = new org.inspection_plusplus.AlignmentRPS();
		alignmentRPS2339.setMaxNrOfIterations(new java.math.BigInteger("-162"));
		alignmentRPS2339.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects2340 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2340.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2341 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2342 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2343 = new java.util.ArrayList<Object>();
		any2343.add(new Object());
		any2343.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2342, "any", any2343);
		extension2342.setVendor("");

		extension2341.add(extension2342);
		org.inspection_plusplus.Extension extension2344 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2345 = new java.util.ArrayList<Object>();
		any2345.add(new Object());
		any2345.add(new Object());
		any2345.add(new Object());
		any2345.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2344, "any", any2345);
		extension2344.setVendor("aaaaaaa");

		extension2341.add(extension2344);
		org.inspection_plusplus.Extension extension2346 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2347 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2346, "any", any2347);
		extension2346.setVendor("");

		extension2341.add(extension2346);
		eu.sarunas.junit.TestsHelper.set(iD4Objects2340, "extension", extension2341);

		alignmentRPS2339.setSystemID(iD4Objects2340);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2348 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(alignmentRPS2339, "commentRef", commentRef2348);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2349 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(alignmentRPS2339, "extension", extension2349);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2350 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID2351 = new org.inspection_plusplus.DisplayID();
		displayID2351.setLanguage("aaaaaaaaaaaa");
		displayID2351.setText("aaaaaaaa");

		displayID2350.add(displayID2351);
		org.inspection_plusplus.DisplayID displayID2352 = new org.inspection_plusplus.DisplayID();
		displayID2352.setLanguage("");
		displayID2352.setText("aaaaa");

		displayID2350.add(displayID2352);
		eu.sarunas.junit.TestsHelper.set(alignmentRPS2339, "displayID", displayID2350);
		java.util.ArrayList<org.inspection_plusplus.History> history2353 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(alignmentRPS2339, "history", history2353);

		alignmentStrategy2338.add(alignmentRPS2339);
		eu.sarunas.junit.TestsHelper.set(referenceSystem2336, "alignmentStrategy", alignmentStrategy2338);
		referenceSystem2336.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects2354 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2354.setUuid("aaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2355 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2356 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2357 = new java.util.ArrayList<Object>();
		any2357.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2356, "any", any2357);
		extension2356.setVendor("aaaaaaaa");

		extension2355.add(extension2356);
		eu.sarunas.junit.TestsHelper.set(iD4Objects2354, "extension", extension2355);

		referenceSystem2336.setSystemID(iD4Objects2354);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2358 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(referenceSystem2336, "commentRef", commentRef2358);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2359 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2360 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2361 = new java.util.ArrayList<Object>();
		any2361.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2360, "any", any2361);
		extension2360.setVendor("aaaa");

		extension2359.add(extension2360);
		org.inspection_plusplus.Extension extension2362 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2363 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2362, "any", any2363);
		extension2362.setVendor("aaaaa");

		extension2359.add(extension2362);
		eu.sarunas.junit.TestsHelper.set(referenceSystem2336, "extension", extension2359);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2364 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID2365 = new org.inspection_plusplus.DisplayID();
		displayID2365.setLanguage("");
		displayID2365.setText("aaaaaaaaaaaa");

		displayID2364.add(displayID2365);
		org.inspection_plusplus.DisplayID displayID2366 = new org.inspection_plusplus.DisplayID();
		displayID2366.setLanguage("");
		displayID2366.setText("");

		displayID2364.add(displayID2366);
		eu.sarunas.junit.TestsHelper.set(referenceSystem2336, "displayID", displayID2364);
		java.util.ArrayList<org.inspection_plusplus.History> history2367 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history2368 = new org.inspection_plusplus.History();
		history2368.setAction("aaaa");
		history2368.setAuthor("aaaa");
		history2368.setComment("");
		history2368.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 24, 7)));

		history2367.add(history2368);
		eu.sarunas.junit.TestsHelper.set(referenceSystem2336, "history", history2367);

		referenceSystem2280.add(referenceSystem2336);
		org.inspection_plusplus.ReferenceSystem referenceSystem2369 = new org.inspection_plusplus.ReferenceSystem();
		java.util.ArrayList<org.inspection_plusplus.Datum> datum2370 = new java.util.ArrayList<org.inspection_plusplus.Datum>();
		eu.sarunas.junit.TestsHelper.set(referenceSystem2369, "datum", datum2370);
		java.util.ArrayList<org.inspection_plusplus.AlignmentStrategy> alignmentStrategy2371 = new java.util.ArrayList<org.inspection_plusplus.AlignmentStrategy>();
		eu.sarunas.junit.TestsHelper.set(referenceSystem2369, "alignmentStrategy", alignmentStrategy2371);
		referenceSystem2369.setName("aaaaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects2372 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2372.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2373 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects2372, "extension", extension2373);

		referenceSystem2369.setSystemID(iD4Objects2372);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2374 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing2375 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2375.setClassID("");
		iD4Referencing2375.setUuid("aaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2376 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2377 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2378 = new java.util.ArrayList<Object>();
		any2378.add(new Object());
		any2378.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2377, "any", any2378);
		extension2377.setVendor("aaa");

		extension2376.add(extension2377);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2375, "extension", extension2376);

		commentRef2374.add(iD4Referencing2375);
		eu.sarunas.junit.TestsHelper.set(referenceSystem2369, "commentRef", commentRef2374);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2379 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(referenceSystem2369, "extension", extension2379);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2380 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID2381 = new org.inspection_plusplus.DisplayID();
		displayID2381.setLanguage("aa");
		displayID2381.setText("aaaaaaaaaaaaaaaaaaaaa");

		displayID2380.add(displayID2381);
		org.inspection_plusplus.DisplayID displayID2382 = new org.inspection_plusplus.DisplayID();
		displayID2382.setLanguage("aaaaa");
		displayID2382.setText("");

		displayID2380.add(displayID2382);
		org.inspection_plusplus.DisplayID displayID2383 = new org.inspection_plusplus.DisplayID();
		displayID2383.setLanguage("aaaaaaaaaaaaa");
		displayID2383.setText("aaaaaaaaaaa");

		displayID2380.add(displayID2383);
		org.inspection_plusplus.DisplayID displayID2384 = new org.inspection_plusplus.DisplayID();
		displayID2384.setLanguage("");
		displayID2384.setText("aaaaaaaaaaaaaaaaaaaaaaaaaa");

		displayID2380.add(displayID2384);
		org.inspection_plusplus.DisplayID displayID2385 = new org.inspection_plusplus.DisplayID();
		displayID2385.setLanguage("aaaaaaaaaaaaa");
		displayID2385.setText("aaaaaaaaaaaaa");

		displayID2380.add(displayID2385);
		eu.sarunas.junit.TestsHelper.set(referenceSystem2369, "displayID", displayID2380);
		java.util.ArrayList<org.inspection_plusplus.History> history2386 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history2387 = new org.inspection_plusplus.History();
		history2387.setAction("aaaaa");
		history2387.setAuthor("aaaaaaaa");
		history2387.setComment("");
		history2387.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 10, 27)));

		history2386.add(history2387);
		org.inspection_plusplus.History history2388 = new org.inspection_plusplus.History();
		history2388.setAction("");
		history2388.setAuthor("");
		history2388.setComment("");
		history2388.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 0, 58, 43)));

		history2386.add(history2388);
		org.inspection_plusplus.History history2389 = new org.inspection_plusplus.History();
		history2389.setAction("aaaaaaaaaaaaaaaaaaa");
		history2389.setAuthor("");
		history2389.setComment("");
		history2389.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 31, 53)));

		history2386.add(history2389);
		eu.sarunas.junit.TestsHelper.set(referenceSystem2369, "history", history2386);

		referenceSystem2280.add(referenceSystem2369);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan2099, "referenceSystem", referenceSystem2280);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> inspectionTaskRef2390 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan2099, "inspectionTaskRef", inspectionTaskRef2390);
		java.util.ArrayList<org.inspection_plusplus.QC> qc2391 = new java.util.ArrayList<org.inspection_plusplus.QC>();
		org.inspection_plusplus.QCSingle qCSingle2392 = new org.inspection_plusplus.QCSingle();
		qCSingle2392.setGeoObjectDetail("aaaa");
		org.inspection_plusplus.ID4Referencing iD4Referencing2393 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2393.setClassID("");
		iD4Referencing2393.setUuid("aaaaaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2394 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2393, "extension", extension2394);

		qCSingle2392.setToleranceRef(iD4Referencing2393);
		qCSingle2392.setAllAssembliesReferenced(false);
		qCSingle2392.setDescription("");
		qCSingle2392.setFunction("aaaaaaaaa");
		qCSingle2392.setPartQAversion("aaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> partRef2395 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing2396 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2396.setClassID("aaaaaaa");
		iD4Referencing2396.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2397 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2398 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2399 = new java.util.ArrayList<Object>();
		any2399.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2398, "any", any2399);
		extension2398.setVendor("aaa");

		extension2397.add(extension2398);
		org.inspection_plusplus.Extension extension2400 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2401 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2400, "any", any2401);
		extension2400.setVendor("");

		extension2397.add(extension2400);
		org.inspection_plusplus.Extension extension2402 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2403 = new java.util.ArrayList<Object>();
		any2403.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2402, "any", any2403);
		extension2402.setVendor("aaaaaaa");

		extension2397.add(extension2402);
		org.inspection_plusplus.Extension extension2404 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2405 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2404, "any", any2405);
		extension2404.setVendor("");

		extension2397.add(extension2404);
		org.inspection_plusplus.Extension extension2406 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2407 = new java.util.ArrayList<Object>();
		any2407.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2406, "any", any2407);
		extension2406.setVendor("aaaaaaaaaaa");

		extension2397.add(extension2406);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2396, "extension", extension2397);

		partRef2395.add(iD4Referencing2396);
		org.inspection_plusplus.ID4Referencing iD4Referencing2408 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2408.setClassID("aaaaaaaaa");
		iD4Referencing2408.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2409 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2408, "extension", extension2409);

		partRef2395.add(iD4Referencing2408);
		org.inspection_plusplus.ID4Referencing iD4Referencing2410 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2410.setClassID("aaaaaaaaa");
		iD4Referencing2410.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2411 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2410, "extension", extension2411);

		partRef2395.add(iD4Referencing2410);
		eu.sarunas.junit.TestsHelper.set(qCSingle2392, "partRef", partRef2395);
		java.util.ArrayList<Object> calculationActualElementDetectionRef2412 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(qCSingle2392, "calculationActualElementDetectionRef", calculationActualElementDetectionRef2412);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeRef2413 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing2414 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2414.setClassID("aaaaaaaaa");
		iD4Referencing2414.setUuid("aaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2415 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2416 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2417 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2416, "any", any2417);
		extension2416.setVendor("aaaaaaaaaaa");

		extension2415.add(extension2416);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2414, "extension", extension2415);

		ipeRef2413.add(iD4Referencing2414);
		eu.sarunas.junit.TestsHelper.set(qCSingle2392, "ipeRef", ipeRef2413);
		qCSingle2392.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects2418 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2418.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2419 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects2418, "extension", extension2419);

		qCSingle2392.setSystemID(iD4Objects2418);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2420 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(qCSingle2392, "commentRef", commentRef2420);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2421 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(qCSingle2392, "extension", extension2421);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2422 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(qCSingle2392, "displayID", displayID2422);
		java.util.ArrayList<org.inspection_plusplus.History> history2423 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history2424 = new org.inspection_plusplus.History();
		history2424.setAction("");
		history2424.setAuthor("");
		history2424.setComment("");
		history2424.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 30, 23)));

		history2423.add(history2424);
		org.inspection_plusplus.History history2425 = new org.inspection_plusplus.History();
		history2425.setAction("");
		history2425.setAuthor("a");
		history2425.setComment("");
		history2425.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 37, 39)));

		history2423.add(history2425);
		eu.sarunas.junit.TestsHelper.set(qCSingle2392, "history", history2423);

		qc2391.add(qCSingle2392);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan2099, "qc", qc2391);
		inspectionPlan2099.setName("a");
		org.inspection_plusplus.ID4Objects iD4Objects2426 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2426.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2427 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects2426, "extension", extension2427);

		inspectionPlan2099.setSystemID(iD4Objects2426);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2428 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing2429 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2429.setClassID("");
		iD4Referencing2429.setUuid("aaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2430 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2431 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2432 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2431, "any", any2432);
		extension2431.setVendor("a");

		extension2430.add(extension2431);
		org.inspection_plusplus.Extension extension2433 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2434 = new java.util.ArrayList<Object>();
		any2434.add(new Object());
		any2434.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2433, "any", any2434);
		extension2433.setVendor("");

		extension2430.add(extension2433);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2429, "extension", extension2430);

		commentRef2428.add(iD4Referencing2429);
		org.inspection_plusplus.ID4Referencing iD4Referencing2435 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2435.setClassID("aaaaa");
		iD4Referencing2435.setUuid("aaaaaaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2436 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2435, "extension", extension2436);

		commentRef2428.add(iD4Referencing2435);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan2099, "commentRef", commentRef2428);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2437 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2438 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2439 = new java.util.ArrayList<Object>();
		any2439.add(new Object());
		any2439.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2438, "any", any2439);
		extension2438.setVendor("aaaaaaa");

		extension2437.add(extension2438);
		org.inspection_plusplus.Extension extension2440 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2441 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2440, "any", any2441);
		extension2440.setVendor("");

		extension2437.add(extension2440);
		org.inspection_plusplus.Extension extension2442 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2443 = new java.util.ArrayList<Object>();
		any2443.add(new Object());
		any2443.add(new Object());
		any2443.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2442, "any", any2443);
		extension2442.setVendor("aaaaaaaaaa");

		extension2437.add(extension2442);
		org.inspection_plusplus.Extension extension2444 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2445 = new java.util.ArrayList<Object>();
		any2445.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2444, "any", any2445);
		extension2444.setVendor("");

		extension2437.add(extension2444);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan2099, "extension", extension2437);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2446 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan2099, "displayID", displayID2446);
		java.util.ArrayList<org.inspection_plusplus.History> history2447 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history2448 = new org.inspection_plusplus.History();
		history2448.setAction("");
		history2448.setAuthor("");
		history2448.setComment("");
		history2448.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 4, 59)));

		history2447.add(history2448);
		org.inspection_plusplus.History history2449 = new org.inspection_plusplus.History();
		history2449.setAction("aaa");
		history2449.setAuthor("a");
		history2449.setComment("");
		history2449.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 54, 34)));

		history2447.add(history2449);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan2099, "history", history2447);
		org.inspection_plusplus.Sender sender2450 = new org.inspection_plusplus.Sender();
		sender2450.setAppID("");
		sender2450.setDomain("aaaaa");
		org.inspection_plusplus.Extension extension2451 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2452 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2451, "any", any2452);
		extension2451.setVendor("");

		org.inspection_plusplus.ReturnStatus res = testObject.storeInspectionPlan(inspectionPlan2099, "", "", sender2450, extension2451);
		junit.framework.Assert.fail();
	};

	@org.junit.Test
	public void testtestStoreInspectionPlan8() throws Throwable
	{
		org.inspection_plusplus.service.v2.WebServiceImpl testObject = new org.inspection_plusplus.service.v2.WebServiceImpl();
		org.inspection_plusplus.InspectionPlan inspectionPlan2453 = new org.inspection_plusplus.InspectionPlan();
		inspectionPlan2453.setInspectionCategories("aaaaa");
		inspectionPlan2453.setIPsymmetry("a");
		inspectionPlan2453.setPartQAversion("aaa");
		eu.sarunas.junit.TestsHelper.set(inspectionPlan2453, "pdMmaturity", "aaa");
		inspectionPlan2453.setQAmaturity("");
		inspectionPlan2453.setQAversion("");
		java.util.ArrayList<org.inspection_plusplus.Comment> comment2454 = new java.util.ArrayList<org.inspection_plusplus.Comment>();
		org.inspection_plusplus.Comment comment2455 = new org.inspection_plusplus.Comment();
		comment2455.setAuthor("");
		comment2455.setReceiver("");
		comment2455.setSubject("aaaaaaaaaaaaaa");
		comment2455.setText("");
		comment2455.setName("aaaaaaaaaaaaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects2456 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2456.setUuid("aaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2457 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2458 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2459 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2458, "any", any2459);
		extension2458.setVendor("aaaaaaaaaaaa");

		extension2457.add(extension2458);
		org.inspection_plusplus.Extension extension2460 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2461 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2460, "any", any2461);
		extension2460.setVendor("aaaaa");

		extension2457.add(extension2460);
		eu.sarunas.junit.TestsHelper.set(iD4Objects2456, "extension", extension2457);

		comment2455.setSystemID(iD4Objects2456);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2462 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing2463 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2463.setClassID("");
		iD4Referencing2463.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2464 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2463, "extension", extension2464);

		commentRef2462.add(iD4Referencing2463);
		eu.sarunas.junit.TestsHelper.set(comment2455, "commentRef", commentRef2462);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2465 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(comment2455, "extension", extension2465);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2466 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(comment2455, "displayID", displayID2466);
		java.util.ArrayList<org.inspection_plusplus.History> history2467 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history2468 = new org.inspection_plusplus.History();
		history2468.setAction("aaaaaaaaaaaa");
		history2468.setAuthor("aaaaaaa");
		history2468.setComment("aaaa");
		history2468.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 44, 28)));

		history2467.add(history2468);
		org.inspection_plusplus.History history2469 = new org.inspection_plusplus.History();
		history2469.setAction("aaaaaaaaaaa");
		history2469.setAuthor("");
		history2469.setComment("aaaaaa");
		history2469.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 33, 7)));

		history2467.add(history2469);
		eu.sarunas.junit.TestsHelper.set(comment2455, "history", history2467);

		comment2454.add(comment2455);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan2453, "comment", comment2454);
		java.util.ArrayList<org.inspection_plusplus.Tolerance> tolerance2470 = new java.util.ArrayList<org.inspection_plusplus.Tolerance>();
		org.inspection_plusplus.TolAxisAngleX tolAxisAngleX2471 = new org.inspection_plusplus.TolAxisAngleX();
		org.inspection_plusplus.IntervalToleranceCriterion intervalToleranceCriterion2472 = new org.inspection_plusplus.IntervalToleranceCriterion();
		intervalToleranceCriterion2472.setInOut("");
		intervalToleranceCriterion2472.setLowerToleranceValue(new java.math.BigDecimal(75.75));
		intervalToleranceCriterion2472.setUpperToleranceValue(new java.math.BigDecimal(80.77));

		tolAxisAngleX2471.setCriterion(intervalToleranceCriterion2472);
		org.inspection_plusplus.ID4Referencing iD4Referencing2473 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2473.setClassID("aaaaaaaaaaaa");
		iD4Referencing2473.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2474 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2475 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2476 = new java.util.ArrayList<Object>();
		any2476.add(new Object());
		any2476.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2475, "any", any2476);
		extension2475.setVendor("aaaaaa");

		extension2474.add(extension2475);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2473, "extension", extension2474);

		tolAxisAngleX2471.setReferenceSystemRef(iD4Referencing2473);
		tolAxisAngleX2471.setStandardConformity("aa");
		org.inspection_plusplus.ToleranceSource toleranceSource2477 = new org.inspection_plusplus.ToleranceSource();
		toleranceSource2477.setTyp("");
		toleranceSource2477.setValidFrom(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 39, 35)));
		toleranceSource2477.setValidTo(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 23, 1)));
		toleranceSource2477.setName("aa");
		org.inspection_plusplus.ID4Objects iD4Objects2478 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2478.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2479 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects2478, "extension", extension2479);

		toleranceSource2477.setSystemID(iD4Objects2478);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2480 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(toleranceSource2477, "commentRef", commentRef2480);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2481 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2482 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2483 = new java.util.ArrayList<Object>();
		any2483.add(new Object());
		any2483.add(new Object());
		any2483.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2482, "any", any2483);
		extension2482.setVendor("aaaaaaaaaaaaaaaaaaaaaaaaaaaa");

		extension2481.add(extension2482);
		org.inspection_plusplus.Extension extension2484 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2485 = new java.util.ArrayList<Object>();
		any2485.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2484, "any", any2485);
		extension2484.setVendor("aaaaaaaaaaaaaaaaaaa");

		extension2481.add(extension2484);
		eu.sarunas.junit.TestsHelper.set(toleranceSource2477, "extension", extension2481);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2486 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID2487 = new org.inspection_plusplus.DisplayID();
		displayID2487.setLanguage("");
		displayID2487.setText("");

		displayID2486.add(displayID2487);
		org.inspection_plusplus.DisplayID displayID2488 = new org.inspection_plusplus.DisplayID();
		displayID2488.setLanguage("aaaaaaaa");
		displayID2488.setText("");

		displayID2486.add(displayID2488);
		eu.sarunas.junit.TestsHelper.set(toleranceSource2477, "displayID", displayID2486);
		java.util.ArrayList<org.inspection_plusplus.History> history2489 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history2490 = new org.inspection_plusplus.History();
		history2490.setAction("");
		history2490.setAuthor("");
		history2490.setComment("");
		history2490.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 57, 18)));

		history2489.add(history2490);
		org.inspection_plusplus.History history2491 = new org.inspection_plusplus.History();
		history2491.setAction("");
		history2491.setAuthor("");
		history2491.setComment("");
		history2491.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 10, 28)));

		history2489.add(history2491);
		org.inspection_plusplus.History history2492 = new org.inspection_plusplus.History();
		history2492.setAction("a");
		history2492.setAuthor("aaaaaaaaaaaaa");
		history2492.setComment("aaaa");
		history2492.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 43, 9)));

		history2489.add(history2492);
		eu.sarunas.junit.TestsHelper.set(toleranceSource2477, "history", history2489);

		tolAxisAngleX2471.setToleranceSource(toleranceSource2477);
		tolAxisAngleX2471.setName("aa");
		org.inspection_plusplus.ID4Objects iD4Objects2493 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2493.setUuid("aaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2494 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects2493, "extension", extension2494);

		tolAxisAngleX2471.setSystemID(iD4Objects2493);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2495 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(tolAxisAngleX2471, "commentRef", commentRef2495);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2496 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2497 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2498 = new java.util.ArrayList<Object>();
		any2498.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2497, "any", any2498);
		extension2497.setVendor("");

		extension2496.add(extension2497);
		org.inspection_plusplus.Extension extension2499 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2500 = new java.util.ArrayList<Object>();
		any2500.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2499, "any", any2500);
		extension2499.setVendor("");

		extension2496.add(extension2499);
		eu.sarunas.junit.TestsHelper.set(tolAxisAngleX2471, "extension", extension2496);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2501 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(tolAxisAngleX2471, "displayID", displayID2501);
		java.util.ArrayList<org.inspection_plusplus.History> history2502 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history2503 = new org.inspection_plusplus.History();
		history2503.setAction("aaaaaaaa");
		history2503.setAuthor("");
		history2503.setComment("");
		history2503.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 33, 1)));

		history2502.add(history2503);
		org.inspection_plusplus.History history2504 = new org.inspection_plusplus.History();
		history2504.setAction("");
		history2504.setAuthor("aaaaaaaaaaa");
		history2504.setComment("");
		history2504.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 48, 35)));

		history2502.add(history2504);
		eu.sarunas.junit.TestsHelper.set(tolAxisAngleX2471, "history", history2502);

		tolerance2470.add(tolAxisAngleX2471);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan2453, "tolerance", tolerance2470);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> partRef2505 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing2506 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2506.setClassID("aaaa");
		iD4Referencing2506.setUuid("aaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2507 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2508 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2509 = new java.util.ArrayList<Object>();
		any2509.add(new Object());
		any2509.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2508, "any", any2509);
		extension2508.setVendor("aaaaaaaaa");

		extension2507.add(extension2508);
		org.inspection_plusplus.Extension extension2510 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2511 = new java.util.ArrayList<Object>();
		any2511.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2510, "any", any2511);
		extension2510.setVendor("");

		extension2507.add(extension2510);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2506, "extension", extension2507);

		partRef2505.add(iD4Referencing2506);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan2453, "partRef", partRef2505);
		java.util.ArrayList<org.inspection_plusplus.IPE> ipe2512 = new java.util.ArrayList<org.inspection_plusplus.IPE>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan2453, "ipe", ipe2512);
		java.util.ArrayList<org.inspection_plusplus.ReferenceSystem> referenceSystem2513 = new java.util.ArrayList<org.inspection_plusplus.ReferenceSystem>();
		org.inspection_plusplus.ReferenceSystem referenceSystem2514 = new org.inspection_plusplus.ReferenceSystem();
		java.util.ArrayList<org.inspection_plusplus.Datum> datum2515 = new java.util.ArrayList<org.inspection_plusplus.Datum>();
		org.inspection_plusplus.Datum datum2516 = new org.inspection_plusplus.Datum();
		org.inspection_plusplus.Vector3D vector3D2517 = new org.inspection_plusplus.Vector3D();
		vector3D2517.setX(371.23);
		vector3D2517.setY(319.20);
		vector3D2517.setZ(-95.44);

		datum2516.setDisplacement(vector3D2517);
		datum2516.setRole("aaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.DatumTarget> datumTarget2518 = new java.util.ArrayList<org.inspection_plusplus.DatumTarget>();
		org.inspection_plusplus.DatumTarget datumTarget2519 = new org.inspection_plusplus.DatumTarget();
		datumTarget2519.setRole("");
		org.inspection_plusplus.ID4Referencing iD4Referencing2520 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2520.setClassID("a");
		iD4Referencing2520.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2521 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2520, "extension", extension2521);

		eu.sarunas.junit.TestsHelper.set(datumTarget2519, "ipeGeometricRef", iD4Referencing2520);
		datumTarget2519.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects2522 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2522.setUuid("aaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2523 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2524 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2525 = new java.util.ArrayList<Object>();
		any2525.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2524, "any", any2525);
		extension2524.setVendor("");

		extension2523.add(extension2524);
		eu.sarunas.junit.TestsHelper.set(iD4Objects2522, "extension", extension2523);

		datumTarget2519.setSystemID(iD4Objects2522);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2526 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing2527 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2527.setClassID("aaaaaaaa");
		iD4Referencing2527.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2528 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2529 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2530 = new java.util.ArrayList<Object>();
		any2530.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2529, "any", any2530);
		extension2529.setVendor("aaaaaaaa");

		extension2528.add(extension2529);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2527, "extension", extension2528);

		commentRef2526.add(iD4Referencing2527);
		eu.sarunas.junit.TestsHelper.set(datumTarget2519, "commentRef", commentRef2526);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2531 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(datumTarget2519, "extension", extension2531);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2532 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID2533 = new org.inspection_plusplus.DisplayID();
		displayID2533.setLanguage("aaaaaaa");
		displayID2533.setText("");

		displayID2532.add(displayID2533);
		eu.sarunas.junit.TestsHelper.set(datumTarget2519, "displayID", displayID2532);
		java.util.ArrayList<org.inspection_plusplus.History> history2534 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history2535 = new org.inspection_plusplus.History();
		history2535.setAction("");
		history2535.setAuthor("aaaaaaaaaaaaaa");
		history2535.setComment("");
		history2535.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 33, 28)));

		history2534.add(history2535);
		eu.sarunas.junit.TestsHelper.set(datumTarget2519, "history", history2534);

		datumTarget2518.add(datumTarget2519);
		eu.sarunas.junit.TestsHelper.set(datum2516, "datumTarget", datumTarget2518);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeGeometricRef2536 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(datum2516, "ipeGeometricRef", ipeGeometricRef2536);
		datum2516.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects2537 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2537.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2538 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects2537, "extension", extension2538);

		datum2516.setSystemID(iD4Objects2537);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2539 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(datum2516, "commentRef", commentRef2539);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2540 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2541 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2542 = new java.util.ArrayList<Object>();
		any2542.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2541, "any", any2542);
		extension2541.setVendor("aaaaa");

		extension2540.add(extension2541);
		eu.sarunas.junit.TestsHelper.set(datum2516, "extension", extension2540);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2543 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(datum2516, "displayID", displayID2543);
		java.util.ArrayList<org.inspection_plusplus.History> history2544 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(datum2516, "history", history2544);

		datum2515.add(datum2516);
		org.inspection_plusplus.Datum datum2545 = new org.inspection_plusplus.Datum();
		org.inspection_plusplus.Vector3D vector3D2546 = new org.inspection_plusplus.Vector3D();
		vector3D2546.setX(-225.25);
		vector3D2546.setY(-27.74);
		vector3D2546.setZ(-180.35);

		datum2545.setDisplacement(vector3D2546);
		datum2545.setRole("");
		java.util.ArrayList<org.inspection_plusplus.DatumTarget> datumTarget2547 = new java.util.ArrayList<org.inspection_plusplus.DatumTarget>();
		eu.sarunas.junit.TestsHelper.set(datum2545, "datumTarget", datumTarget2547);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeGeometricRef2548 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(datum2545, "ipeGeometricRef", ipeGeometricRef2548);
		datum2545.setName("aaaaaaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects2549 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2549.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2550 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2551 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2552 = new java.util.ArrayList<Object>();
		any2552.add(new Object());
		any2552.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2551, "any", any2552);
		extension2551.setVendor("");

		extension2550.add(extension2551);
		eu.sarunas.junit.TestsHelper.set(iD4Objects2549, "extension", extension2550);

		datum2545.setSystemID(iD4Objects2549);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2553 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing2554 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2554.setClassID("");
		iD4Referencing2554.setUuid("aaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2555 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2554, "extension", extension2555);

		commentRef2553.add(iD4Referencing2554);
		eu.sarunas.junit.TestsHelper.set(datum2545, "commentRef", commentRef2553);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2556 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2557 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2558 = new java.util.ArrayList<Object>();
		any2558.add(new Object());
		any2558.add(new Object());
		any2558.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2557, "any", any2558);
		extension2557.setVendor("");

		extension2556.add(extension2557);
		eu.sarunas.junit.TestsHelper.set(datum2545, "extension", extension2556);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2559 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID2560 = new org.inspection_plusplus.DisplayID();
		displayID2560.setLanguage("");
		displayID2560.setText("");

		displayID2559.add(displayID2560);
		eu.sarunas.junit.TestsHelper.set(datum2545, "displayID", displayID2559);
		java.util.ArrayList<org.inspection_plusplus.History> history2561 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history2562 = new org.inspection_plusplus.History();
		history2562.setAction("aaaa");
		history2562.setAuthor("");
		history2562.setComment("");
		history2562.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 31, 46)));

		history2561.add(history2562);
		org.inspection_plusplus.History history2563 = new org.inspection_plusplus.History();
		history2563.setAction("aaa");
		history2563.setAuthor("aaaaaaaa");
		history2563.setComment("aaaaaaa");
		history2563.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 34, 50)));

		history2561.add(history2563);
		org.inspection_plusplus.History history2564 = new org.inspection_plusplus.History();
		history2564.setAction("");
		history2564.setAuthor("aaaaaaaaaaaaaaaaaaaaaaaaa");
		history2564.setComment("");
		history2564.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 15, 12)));

		history2561.add(history2564);
		eu.sarunas.junit.TestsHelper.set(datum2545, "history", history2561);

		datum2515.add(datum2545);
		org.inspection_plusplus.Datum datum2565 = new org.inspection_plusplus.Datum();
		org.inspection_plusplus.Vector3D vector3D2566 = new org.inspection_plusplus.Vector3D();
		vector3D2566.setX(-131.67);
		vector3D2566.setY(-72.46);
		vector3D2566.setZ(177.34);

		datum2565.setDisplacement(vector3D2566);
		datum2565.setRole("");
		java.util.ArrayList<org.inspection_plusplus.DatumTarget> datumTarget2567 = new java.util.ArrayList<org.inspection_plusplus.DatumTarget>();
		org.inspection_plusplus.DatumTarget datumTarget2568 = new org.inspection_plusplus.DatumTarget();
		datumTarget2568.setRole("");
		org.inspection_plusplus.ID4Referencing iD4Referencing2569 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2569.setClassID("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
		iD4Referencing2569.setUuid("aaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2570 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2571 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2572 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2571, "any", any2572);
		extension2571.setVendor("");

		extension2570.add(extension2571);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2569, "extension", extension2570);

		eu.sarunas.junit.TestsHelper.set(datumTarget2568, "ipeGeometricRef", iD4Referencing2569);
		datumTarget2568.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects2573 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2573.setUuid("aaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2574 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2575 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2576 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2575, "any", any2576);
		extension2575.setVendor("aaa");

		extension2574.add(extension2575);
		org.inspection_plusplus.Extension extension2577 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2578 = new java.util.ArrayList<Object>();
		any2578.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2577, "any", any2578);
		extension2577.setVendor("aaaaaaaaaaaaaaaaaa");

		extension2574.add(extension2577);
		org.inspection_plusplus.Extension extension2579 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2580 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2579, "any", any2580);
		extension2579.setVendor("aaaaaaaaaaaaa");

		extension2574.add(extension2579);
		org.inspection_plusplus.Extension extension2581 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2582 = new java.util.ArrayList<Object>();
		any2582.add(new Object());
		any2582.add(new Object());
		any2582.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2581, "any", any2582);
		extension2581.setVendor("aaa");

		extension2574.add(extension2581);
		eu.sarunas.junit.TestsHelper.set(iD4Objects2573, "extension", extension2574);

		datumTarget2568.setSystemID(iD4Objects2573);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2583 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(datumTarget2568, "commentRef", commentRef2583);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2584 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(datumTarget2568, "extension", extension2584);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2585 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID2586 = new org.inspection_plusplus.DisplayID();
		displayID2586.setLanguage("aaaa");
		displayID2586.setText("aaa");

		displayID2585.add(displayID2586);
		org.inspection_plusplus.DisplayID displayID2587 = new org.inspection_plusplus.DisplayID();
		displayID2587.setLanguage("a");
		displayID2587.setText("");

		displayID2585.add(displayID2587);
		org.inspection_plusplus.DisplayID displayID2588 = new org.inspection_plusplus.DisplayID();
		displayID2588.setLanguage("");
		displayID2588.setText("aa");

		displayID2585.add(displayID2588);
		eu.sarunas.junit.TestsHelper.set(datumTarget2568, "displayID", displayID2585);
		java.util.ArrayList<org.inspection_plusplus.History> history2589 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history2590 = new org.inspection_plusplus.History();
		history2590.setAction("");
		history2590.setAuthor("aaaaaaaaaa");
		history2590.setComment("");
		history2590.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 42, 2)));

		history2589.add(history2590);
		org.inspection_plusplus.History history2591 = new org.inspection_plusplus.History();
		history2591.setAction("aaaaaa");
		history2591.setAuthor("aaaaaaaaaaaaa");
		history2591.setComment("aaaaaaaaaa");
		history2591.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 32, 54)));

		history2589.add(history2591);
		org.inspection_plusplus.History history2592 = new org.inspection_plusplus.History();
		history2592.setAction("aaa");
		history2592.setAuthor("");
		history2592.setComment("");
		history2592.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 8, 42)));

		history2589.add(history2592);
		eu.sarunas.junit.TestsHelper.set(datumTarget2568, "history", history2589);

		datumTarget2567.add(datumTarget2568);
		org.inspection_plusplus.DatumTarget datumTarget2593 = new org.inspection_plusplus.DatumTarget();
		datumTarget2593.setRole("aaaaaaaaaaaaaaa");
		org.inspection_plusplus.ID4Referencing iD4Referencing2594 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2594.setClassID("aaaaa");
		iD4Referencing2594.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2595 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2594, "extension", extension2595);

		eu.sarunas.junit.TestsHelper.set(datumTarget2593, "ipeGeometricRef", iD4Referencing2594);
		datumTarget2593.setName("aaaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects2596 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2596.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2597 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects2596, "extension", extension2597);

		datumTarget2593.setSystemID(iD4Objects2596);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2598 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(datumTarget2593, "commentRef", commentRef2598);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2599 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(datumTarget2593, "extension", extension2599);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2600 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID2601 = new org.inspection_plusplus.DisplayID();
		displayID2601.setLanguage("");
		displayID2601.setText("aaaaaaaaaa");

		displayID2600.add(displayID2601);
		eu.sarunas.junit.TestsHelper.set(datumTarget2593, "displayID", displayID2600);
		java.util.ArrayList<org.inspection_plusplus.History> history2602 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history2603 = new org.inspection_plusplus.History();
		history2603.setAction("");
		history2603.setAuthor("aaaaaaaa");
		history2603.setComment("aaaaaaaaa");
		history2603.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 37, 31)));

		history2602.add(history2603);
		eu.sarunas.junit.TestsHelper.set(datumTarget2593, "history", history2602);

		datumTarget2567.add(datumTarget2593);
		eu.sarunas.junit.TestsHelper.set(datum2565, "datumTarget", datumTarget2567);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeGeometricRef2604 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing2605 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2605.setClassID("");
		iD4Referencing2605.setUuid("aaaaaaaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2606 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2605, "extension", extension2606);

		ipeGeometricRef2604.add(iD4Referencing2605);
		org.inspection_plusplus.ID4Referencing iD4Referencing2607 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2607.setClassID("");
		iD4Referencing2607.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2608 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2607, "extension", extension2608);

		ipeGeometricRef2604.add(iD4Referencing2607);
		eu.sarunas.junit.TestsHelper.set(datum2565, "ipeGeometricRef", ipeGeometricRef2604);
		datum2565.setName("aaaaaaaaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects2609 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2609.setUuid("aaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2610 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2611 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2612 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2611, "any", any2612);
		extension2611.setVendor("");

		extension2610.add(extension2611);
		eu.sarunas.junit.TestsHelper.set(iD4Objects2609, "extension", extension2610);

		datum2565.setSystemID(iD4Objects2609);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2613 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing2614 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2614.setClassID("aa");
		iD4Referencing2614.setUuid("aaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2615 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2614, "extension", extension2615);

		commentRef2613.add(iD4Referencing2614);
		org.inspection_plusplus.ID4Referencing iD4Referencing2616 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2616.setClassID("");
		iD4Referencing2616.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2617 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2618 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2619 = new java.util.ArrayList<Object>();
		any2619.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2618, "any", any2619);
		extension2618.setVendor("");

		extension2617.add(extension2618);
		org.inspection_plusplus.Extension extension2620 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2621 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2620, "any", any2621);
		extension2620.setVendor("");

		extension2617.add(extension2620);
		org.inspection_plusplus.Extension extension2622 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2623 = new java.util.ArrayList<Object>();
		any2623.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2622, "any", any2623);
		extension2622.setVendor("");

		extension2617.add(extension2622);
		org.inspection_plusplus.Extension extension2624 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2625 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2624, "any", any2625);
		extension2624.setVendor("");

		extension2617.add(extension2624);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2616, "extension", extension2617);

		commentRef2613.add(iD4Referencing2616);
		org.inspection_plusplus.ID4Referencing iD4Referencing2626 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2626.setClassID("aaaaaaaaaaaaaa");
		iD4Referencing2626.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2627 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2626, "extension", extension2627);

		commentRef2613.add(iD4Referencing2626);
		eu.sarunas.junit.TestsHelper.set(datum2565, "commentRef", commentRef2613);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2628 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2629 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2630 = new java.util.ArrayList<Object>();
		any2630.add(new Object());
		any2630.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2629, "any", any2630);
		extension2629.setVendor("");

		extension2628.add(extension2629);
		org.inspection_plusplus.Extension extension2631 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2632 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2631, "any", any2632);
		extension2631.setVendor("");

		extension2628.add(extension2631);
		eu.sarunas.junit.TestsHelper.set(datum2565, "extension", extension2628);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2633 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(datum2565, "displayID", displayID2633);
		java.util.ArrayList<org.inspection_plusplus.History> history2634 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history2635 = new org.inspection_plusplus.History();
		history2635.setAction("aaaaaaaaaa");
		history2635.setAuthor("");
		history2635.setComment("aaaaaaaa");
		history2635.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 45, 6)));

		history2634.add(history2635);
		org.inspection_plusplus.History history2636 = new org.inspection_plusplus.History();
		history2636.setAction("a");
		history2636.setAuthor("");
		history2636.setComment("");
		history2636.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 27, 27)));

		history2634.add(history2636);
		eu.sarunas.junit.TestsHelper.set(datum2565, "history", history2634);

		datum2515.add(datum2565);
		eu.sarunas.junit.TestsHelper.set(referenceSystem2514, "datum", datum2515);
		java.util.ArrayList<org.inspection_plusplus.AlignmentStrategy> alignmentStrategy2637 = new java.util.ArrayList<org.inspection_plusplus.AlignmentStrategy>();
		org.inspection_plusplus.AlignmentRPS alignmentRPS2638 = new org.inspection_plusplus.AlignmentRPS();
		alignmentRPS2638.setMaxNrOfIterations(new java.math.BigInteger("485"));
		alignmentRPS2638.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects2639 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2639.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2640 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2641 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2642 = new java.util.ArrayList<Object>();
		any2642.add(new Object());
		any2642.add(new Object());
		any2642.add(new Object());
		any2642.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2641, "any", any2642);
		extension2641.setVendor("");

		extension2640.add(extension2641);
		org.inspection_plusplus.Extension extension2643 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2644 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2643, "any", any2644);
		extension2643.setVendor("");

		extension2640.add(extension2643);
		eu.sarunas.junit.TestsHelper.set(iD4Objects2639, "extension", extension2640);

		alignmentRPS2638.setSystemID(iD4Objects2639);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2645 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing2646 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2646.setClassID("");
		iD4Referencing2646.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2647 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2648 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2649 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2648, "any", any2649);
		extension2648.setVendor("");

		extension2647.add(extension2648);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2646, "extension", extension2647);

		commentRef2645.add(iD4Referencing2646);
		org.inspection_plusplus.ID4Referencing iD4Referencing2650 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2650.setClassID("");
		iD4Referencing2650.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2651 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2652 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2653 = new java.util.ArrayList<Object>();
		any2653.add(new Object());
		any2653.add(new Object());
		any2653.add(new Object());
		any2653.add(new Object());
		any2653.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2652, "any", any2653);
		extension2652.setVendor("");

		extension2651.add(extension2652);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2650, "extension", extension2651);

		commentRef2645.add(iD4Referencing2650);
		org.inspection_plusplus.ID4Referencing iD4Referencing2654 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2654.setClassID("");
		iD4Referencing2654.setUuid("aa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2655 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2656 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2657 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2656, "any", any2657);
		extension2656.setVendor("");

		extension2655.add(extension2656);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2654, "extension", extension2655);

		commentRef2645.add(iD4Referencing2654);
		eu.sarunas.junit.TestsHelper.set(alignmentRPS2638, "commentRef", commentRef2645);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2658 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2659 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2660 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2659, "any", any2660);
		extension2659.setVendor("aaaaaaaaaaaa");

		extension2658.add(extension2659);
		eu.sarunas.junit.TestsHelper.set(alignmentRPS2638, "extension", extension2658);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2661 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(alignmentRPS2638, "displayID", displayID2661);
		java.util.ArrayList<org.inspection_plusplus.History> history2662 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history2663 = new org.inspection_plusplus.History();
		history2663.setAction("");
		history2663.setAuthor("");
		history2663.setComment("aaa");
		history2663.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 48, 24)));

		history2662.add(history2663);
		org.inspection_plusplus.History history2664 = new org.inspection_plusplus.History();
		history2664.setAction("");
		history2664.setAuthor("aaaaaaaaaaaaaaaaaaaaaaa");
		history2664.setComment("");
		history2664.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 12, 58)));

		history2662.add(history2664);
		eu.sarunas.junit.TestsHelper.set(alignmentRPS2638, "history", history2662);

		alignmentStrategy2637.add(alignmentRPS2638);
		org.inspection_plusplus.AlignmentFSS alignmentFSS2665 = new org.inspection_plusplus.AlignmentFSS();
		alignmentFSS2665.setTerminatingCondition(-65.85);
		alignmentFSS2665.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects2666 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2666.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2667 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects2666, "extension", extension2667);

		alignmentFSS2665.setSystemID(iD4Objects2666);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2668 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(alignmentFSS2665, "commentRef", commentRef2668);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2669 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2670 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2671 = new java.util.ArrayList<Object>();
		any2671.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2670, "any", any2671);
		extension2670.setVendor("");

		extension2669.add(extension2670);
		eu.sarunas.junit.TestsHelper.set(alignmentFSS2665, "extension", extension2669);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2672 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID2673 = new org.inspection_plusplus.DisplayID();
		displayID2673.setLanguage("");
		displayID2673.setText("");

		displayID2672.add(displayID2673);
		org.inspection_plusplus.DisplayID displayID2674 = new org.inspection_plusplus.DisplayID();
		displayID2674.setLanguage("");
		displayID2674.setText("aaaaaaaaaaaaaa");

		displayID2672.add(displayID2674);
		org.inspection_plusplus.DisplayID displayID2675 = new org.inspection_plusplus.DisplayID();
		displayID2675.setLanguage("aaaaaaaaa");
		displayID2675.setText("");

		displayID2672.add(displayID2675);
		eu.sarunas.junit.TestsHelper.set(alignmentFSS2665, "displayID", displayID2672);
		java.util.ArrayList<org.inspection_plusplus.History> history2676 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history2677 = new org.inspection_plusplus.History();
		history2677.setAction("aaaaaaaaaaaaaa");
		history2677.setAuthor("aaaaaaaaaaa");
		history2677.setComment("");
		history2677.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 30, 52)));

		history2676.add(history2677);
		eu.sarunas.junit.TestsHelper.set(alignmentFSS2665, "history", history2676);

		alignmentStrategy2637.add(alignmentFSS2665);
		eu.sarunas.junit.TestsHelper.set(referenceSystem2514, "alignmentStrategy", alignmentStrategy2637);
		referenceSystem2514.setName("aaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects2678 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2678.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2679 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2680 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2681 = new java.util.ArrayList<Object>();
		any2681.add(new Object());
		any2681.add(new Object());
		any2681.add(new Object());
		any2681.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2680, "any", any2681);
		extension2680.setVendor("aaaaa");

		extension2679.add(extension2680);
		org.inspection_plusplus.Extension extension2682 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2683 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2682, "any", any2683);
		extension2682.setVendor("");

		extension2679.add(extension2682);
		org.inspection_plusplus.Extension extension2684 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2685 = new java.util.ArrayList<Object>();
		any2685.add(new Object());
		any2685.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2684, "any", any2685);
		extension2684.setVendor("");

		extension2679.add(extension2684);
		eu.sarunas.junit.TestsHelper.set(iD4Objects2678, "extension", extension2679);

		referenceSystem2514.setSystemID(iD4Objects2678);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2686 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing2687 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2687.setClassID("aaaaaaaaaaaa");
		iD4Referencing2687.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2688 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2687, "extension", extension2688);

		commentRef2686.add(iD4Referencing2687);
		org.inspection_plusplus.ID4Referencing iD4Referencing2689 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2689.setClassID("");
		iD4Referencing2689.setUuid("aaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2690 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2691 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2692 = new java.util.ArrayList<Object>();
		any2692.add(new Object());
		any2692.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2691, "any", any2692);
		extension2691.setVendor("");

		extension2690.add(extension2691);
		org.inspection_plusplus.Extension extension2693 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2694 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2693, "any", any2694);
		extension2693.setVendor("");

		extension2690.add(extension2693);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2689, "extension", extension2690);

		commentRef2686.add(iD4Referencing2689);
		eu.sarunas.junit.TestsHelper.set(referenceSystem2514, "commentRef", commentRef2686);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2695 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2696 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2697 = new java.util.ArrayList<Object>();
		any2697.add(new Object());
		any2697.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2696, "any", any2697);
		extension2696.setVendor("");

		extension2695.add(extension2696);
		org.inspection_plusplus.Extension extension2698 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2699 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2698, "any", any2699);
		extension2698.setVendor("");

		extension2695.add(extension2698);
		org.inspection_plusplus.Extension extension2700 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2701 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2700, "any", any2701);
		extension2700.setVendor("aaa");

		extension2695.add(extension2700);
		org.inspection_plusplus.Extension extension2702 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2703 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2702, "any", any2703);
		extension2702.setVendor("");

		extension2695.add(extension2702);
		eu.sarunas.junit.TestsHelper.set(referenceSystem2514, "extension", extension2695);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2704 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID2705 = new org.inspection_plusplus.DisplayID();
		displayID2705.setLanguage("");
		displayID2705.setText("aaaaaaa");

		displayID2704.add(displayID2705);
		eu.sarunas.junit.TestsHelper.set(referenceSystem2514, "displayID", displayID2704);
		java.util.ArrayList<org.inspection_plusplus.History> history2706 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(referenceSystem2514, "history", history2706);

		referenceSystem2513.add(referenceSystem2514);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan2453, "referenceSystem", referenceSystem2513);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> inspectionTaskRef2707 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan2453, "inspectionTaskRef", inspectionTaskRef2707);
		java.util.ArrayList<org.inspection_plusplus.QC> qc2708 = new java.util.ArrayList<org.inspection_plusplus.QC>();
		org.inspection_plusplus.QCSingle qCSingle2709 = new org.inspection_plusplus.QCSingle();
		qCSingle2709.setGeoObjectDetail("");
		org.inspection_plusplus.ID4Referencing iD4Referencing2710 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2710.setClassID("");
		iD4Referencing2710.setUuid("aaaaaaaaaaaaaaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2711 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2712 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2713 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2712, "any", any2713);
		extension2712.setVendor("aaaaaaaa");

		extension2711.add(extension2712);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2710, "extension", extension2711);

		qCSingle2709.setToleranceRef(iD4Referencing2710);
		qCSingle2709.setAllAssembliesReferenced(true);
		qCSingle2709.setDescription("");
		qCSingle2709.setFunction("");
		qCSingle2709.setPartQAversion("aaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> partRef2714 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing2715 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2715.setClassID("");
		iD4Referencing2715.setUuid("aaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2716 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2715, "extension", extension2716);

		partRef2714.add(iD4Referencing2715);
		org.inspection_plusplus.ID4Referencing iD4Referencing2717 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2717.setClassID("");
		iD4Referencing2717.setUuid("aaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2718 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2719 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2720 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2719, "any", any2720);
		extension2719.setVendor("");

		extension2718.add(extension2719);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2717, "extension", extension2718);

		partRef2714.add(iD4Referencing2717);
		org.inspection_plusplus.ID4Referencing iD4Referencing2721 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2721.setClassID("aaaaaaaaaaaaa");
		iD4Referencing2721.setUuid("a");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2722 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2723 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2724 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2723, "any", any2724);
		extension2723.setVendor("aaaa");

		extension2722.add(extension2723);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2721, "extension", extension2722);

		partRef2714.add(iD4Referencing2721);
		eu.sarunas.junit.TestsHelper.set(qCSingle2709, "partRef", partRef2714);
		java.util.ArrayList<Object> calculationActualElementDetectionRef2725 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(qCSingle2709, "calculationActualElementDetectionRef", calculationActualElementDetectionRef2725);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeRef2726 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing2727 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2727.setClassID("aaaaaaaaaaaaaaaaaa");
		iD4Referencing2727.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2728 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2729 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2730 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2729, "any", any2730);
		extension2729.setVendor("aaaaa");

		extension2728.add(extension2729);
		org.inspection_plusplus.Extension extension2731 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2732 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2731, "any", any2732);
		extension2731.setVendor("");

		extension2728.add(extension2731);
		org.inspection_plusplus.Extension extension2733 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2734 = new java.util.ArrayList<Object>();
		any2734.add(new Object());
		any2734.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2733, "any", any2734);
		extension2733.setVendor("");

		extension2728.add(extension2733);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2727, "extension", extension2728);

		ipeRef2726.add(iD4Referencing2727);
		eu.sarunas.junit.TestsHelper.set(qCSingle2709, "ipeRef", ipeRef2726);
		qCSingle2709.setName("aaaaaaaaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects2735 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2735.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2736 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2737 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2738 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2737, "any", any2738);
		extension2737.setVendor("aaaaaaaaaaaaaa");

		extension2736.add(extension2737);
		org.inspection_plusplus.Extension extension2739 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2740 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2739, "any", any2740);
		extension2739.setVendor("aaaaaaaaaaaaaa");

		extension2736.add(extension2739);
		org.inspection_plusplus.Extension extension2741 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2742 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2741, "any", any2742);
		extension2741.setVendor("");

		extension2736.add(extension2741);
		org.inspection_plusplus.Extension extension2743 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2744 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2743, "any", any2744);
		extension2743.setVendor("");

		extension2736.add(extension2743);
		eu.sarunas.junit.TestsHelper.set(iD4Objects2735, "extension", extension2736);

		qCSingle2709.setSystemID(iD4Objects2735);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2745 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing2746 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2746.setClassID("aaaaaaaaaaaaaaaaaaaaaaaaaa");
		iD4Referencing2746.setUuid("aa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2747 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2748 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2749 = new java.util.ArrayList<Object>();
		any2749.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2748, "any", any2749);
		extension2748.setVendor("");

		extension2747.add(extension2748);
		org.inspection_plusplus.Extension extension2750 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2751 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2750, "any", any2751);
		extension2750.setVendor("");

		extension2747.add(extension2750);
		org.inspection_plusplus.Extension extension2752 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2753 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2752, "any", any2753);
		extension2752.setVendor("");

		extension2747.add(extension2752);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2746, "extension", extension2747);

		commentRef2745.add(iD4Referencing2746);
		org.inspection_plusplus.ID4Referencing iD4Referencing2754 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2754.setClassID("aa");
		iD4Referencing2754.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2755 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2756 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2757 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2756, "any", any2757);
		extension2756.setVendor("aaaaa");

		extension2755.add(extension2756);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2754, "extension", extension2755);

		commentRef2745.add(iD4Referencing2754);
		org.inspection_plusplus.ID4Referencing iD4Referencing2758 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2758.setClassID("");
		iD4Referencing2758.setUuid("aaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2759 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2760 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2761 = new java.util.ArrayList<Object>();
		any2761.add(new Object());
		any2761.add(new Object());
		any2761.add(new Object());
		any2761.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2760, "any", any2761);
		extension2760.setVendor("aaa");

		extension2759.add(extension2760);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2758, "extension", extension2759);

		commentRef2745.add(iD4Referencing2758);
		eu.sarunas.junit.TestsHelper.set(qCSingle2709, "commentRef", commentRef2745);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2762 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2763 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2764 = new java.util.ArrayList<Object>();
		any2764.add(new Object());
		any2764.add(new Object());
		any2764.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2763, "any", any2764);
		extension2763.setVendor("aaaaaaaaaaaaaaaaaaa");

		extension2762.add(extension2763);
		eu.sarunas.junit.TestsHelper.set(qCSingle2709, "extension", extension2762);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2765 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(qCSingle2709, "displayID", displayID2765);
		java.util.ArrayList<org.inspection_plusplus.History> history2766 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(qCSingle2709, "history", history2766);

		qc2708.add(qCSingle2709);
		org.inspection_plusplus.QCSingle qCSingle2767 = new org.inspection_plusplus.QCSingle();
		qCSingle2767.setGeoObjectDetail("aaaaaaaaaaaaaaaaaaaaa");
		org.inspection_plusplus.ID4Referencing iD4Referencing2768 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2768.setClassID("");
		iD4Referencing2768.setUuid("aaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2769 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2770 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2771 = new java.util.ArrayList<Object>();
		any2771.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2770, "any", any2771);
		extension2770.setVendor("aaaaaaaaaa");

		extension2769.add(extension2770);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2768, "extension", extension2769);

		qCSingle2767.setToleranceRef(iD4Referencing2768);
		qCSingle2767.setAllAssembliesReferenced(true);
		qCSingle2767.setDescription("aaaaaaa");
		qCSingle2767.setFunction("aaaaaa");
		qCSingle2767.setPartQAversion("");
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> partRef2772 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing2773 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2773.setClassID("aaaa");
		iD4Referencing2773.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2774 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2775 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2776 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2775, "any", any2776);
		extension2775.setVendor("aaaaaaaaaaaaaa");

		extension2774.add(extension2775);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2773, "extension", extension2774);

		partRef2772.add(iD4Referencing2773);
		eu.sarunas.junit.TestsHelper.set(qCSingle2767, "partRef", partRef2772);
		java.util.ArrayList<Object> calculationActualElementDetectionRef2777 = new java.util.ArrayList<Object>();
		calculationActualElementDetectionRef2777.add(new Object());
		calculationActualElementDetectionRef2777.add(new Object());
		calculationActualElementDetectionRef2777.add(new Object());
		eu.sarunas.junit.TestsHelper.set(qCSingle2767, "calculationActualElementDetectionRef", calculationActualElementDetectionRef2777);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeRef2778 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing2779 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2779.setClassID("a");
		iD4Referencing2779.setUuid("aaaaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2780 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2779, "extension", extension2780);

		ipeRef2778.add(iD4Referencing2779);
		eu.sarunas.junit.TestsHelper.set(qCSingle2767, "ipeRef", ipeRef2778);
		qCSingle2767.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects2781 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2781.setUuid("aaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2782 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2783 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2784 = new java.util.ArrayList<Object>();
		any2784.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2783, "any", any2784);
		extension2783.setVendor("");

		extension2782.add(extension2783);
		eu.sarunas.junit.TestsHelper.set(iD4Objects2781, "extension", extension2782);

		qCSingle2767.setSystemID(iD4Objects2781);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2785 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(qCSingle2767, "commentRef", commentRef2785);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2786 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2787 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2788 = new java.util.ArrayList<Object>();
		any2788.add(new Object());
		any2788.add(new Object());
		any2788.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2787, "any", any2788);
		extension2787.setVendor("");

		extension2786.add(extension2787);
		org.inspection_plusplus.Extension extension2789 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2790 = new java.util.ArrayList<Object>();
		any2790.add(new Object());
		any2790.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2789, "any", any2790);
		extension2789.setVendor("aaaaaaaaaaa");

		extension2786.add(extension2789);
		eu.sarunas.junit.TestsHelper.set(qCSingle2767, "extension", extension2786);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2791 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID2792 = new org.inspection_plusplus.DisplayID();
		displayID2792.setLanguage("");
		displayID2792.setText("");

		displayID2791.add(displayID2792);
		org.inspection_plusplus.DisplayID displayID2793 = new org.inspection_plusplus.DisplayID();
		displayID2793.setLanguage("aaaa");
		displayID2793.setText("aaaa");

		displayID2791.add(displayID2793);
		eu.sarunas.junit.TestsHelper.set(qCSingle2767, "displayID", displayID2791);
		java.util.ArrayList<org.inspection_plusplus.History> history2794 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history2795 = new org.inspection_plusplus.History();
		history2795.setAction("aaaaaaaaaaaa");
		history2795.setAuthor("");
		history2795.setComment("");
		history2795.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 35, 20)));

		history2794.add(history2795);
		eu.sarunas.junit.TestsHelper.set(qCSingle2767, "history", history2794);

		qc2708.add(qCSingle2767);
		org.inspection_plusplus.QCSingle qCSingle2796 = new org.inspection_plusplus.QCSingle();
		qCSingle2796.setGeoObjectDetail("aaaaaaaaaaaaaa");
		org.inspection_plusplus.ID4Referencing iD4Referencing2797 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2797.setClassID("aaaaaaa");
		iD4Referencing2797.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2798 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2799 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2800 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2799, "any", any2800);
		extension2799.setVendor("a");

		extension2798.add(extension2799);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2797, "extension", extension2798);

		qCSingle2796.setToleranceRef(iD4Referencing2797);
		qCSingle2796.setAllAssembliesReferenced(false);
		qCSingle2796.setDescription("");
		qCSingle2796.setFunction("");
		qCSingle2796.setPartQAversion("");
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> partRef2801 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(qCSingle2796, "partRef", partRef2801);
		java.util.ArrayList<Object> calculationActualElementDetectionRef2802 = new java.util.ArrayList<Object>();
		calculationActualElementDetectionRef2802.add(new Object());
		calculationActualElementDetectionRef2802.add(new Object());
		eu.sarunas.junit.TestsHelper.set(qCSingle2796, "calculationActualElementDetectionRef", calculationActualElementDetectionRef2802);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeRef2803 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing2804 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2804.setClassID("");
		iD4Referencing2804.setUuid("aaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2805 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2804, "extension", extension2805);

		ipeRef2803.add(iD4Referencing2804);
		eu.sarunas.junit.TestsHelper.set(qCSingle2796, "ipeRef", ipeRef2803);
		qCSingle2796.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects2806 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2806.setUuid("aaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2807 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2808 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2809 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2808, "any", any2809);
		extension2808.setVendor("a");

		extension2807.add(extension2808);
		org.inspection_plusplus.Extension extension2810 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2811 = new java.util.ArrayList<Object>();
		any2811.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2810, "any", any2811);
		extension2810.setVendor("");

		extension2807.add(extension2810);
		org.inspection_plusplus.Extension extension2812 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2813 = new java.util.ArrayList<Object>();
		any2813.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2812, "any", any2813);
		extension2812.setVendor("");

		extension2807.add(extension2812);
		org.inspection_plusplus.Extension extension2814 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2815 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2814, "any", any2815);
		extension2814.setVendor("");

		extension2807.add(extension2814);
		eu.sarunas.junit.TestsHelper.set(iD4Objects2806, "extension", extension2807);

		qCSingle2796.setSystemID(iD4Objects2806);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2816 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(qCSingle2796, "commentRef", commentRef2816);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2817 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(qCSingle2796, "extension", extension2817);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2818 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(qCSingle2796, "displayID", displayID2818);
		java.util.ArrayList<org.inspection_plusplus.History> history2819 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history2820 = new org.inspection_plusplus.History();
		history2820.setAction("");
		history2820.setAuthor("");
		history2820.setComment("aaaaaaaa");
		history2820.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 27, 43)));

		history2819.add(history2820);
		org.inspection_plusplus.History history2821 = new org.inspection_plusplus.History();
		history2821.setAction("aaaaaaaaaa");
		history2821.setAuthor("aaaaaaaaaaaa");
		history2821.setComment("aaaaaaaaaaaa");
		history2821.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 23, 4)));

		history2819.add(history2821);
		org.inspection_plusplus.History history2822 = new org.inspection_plusplus.History();
		history2822.setAction("");
		history2822.setAuthor("aaaaa");
		history2822.setComment("");
		history2822.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 33, 36)));

		history2819.add(history2822);
		org.inspection_plusplus.History history2823 = new org.inspection_plusplus.History();
		history2823.setAction("aaaaaaaa");
		history2823.setAuthor("aaaa");
		history2823.setComment("aaaaaaaaaaaaaaaaaaaaaaaa");
		history2823.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 45, 29)));

		history2819.add(history2823);
		org.inspection_plusplus.History history2824 = new org.inspection_plusplus.History();
		history2824.setAction("aaaa");
		history2824.setAuthor("aaaaaaaaa");
		history2824.setComment("aaa");
		history2824.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 19, 31)));

		history2819.add(history2824);
		eu.sarunas.junit.TestsHelper.set(qCSingle2796, "history", history2819);

		qc2708.add(qCSingle2796);
		org.inspection_plusplus.QCSingle qCSingle2825 = new org.inspection_plusplus.QCSingle();
		qCSingle2825.setGeoObjectDetail("aaaaaaaaaaaaaaaaaaaaaa");
		org.inspection_plusplus.ID4Referencing iD4Referencing2826 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2826.setClassID("a");
		iD4Referencing2826.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2827 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2828 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2829 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2828, "any", any2829);
		extension2828.setVendor("a");

		extension2827.add(extension2828);
		org.inspection_plusplus.Extension extension2830 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2831 = new java.util.ArrayList<Object>();
		any2831.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2830, "any", any2831);
		extension2830.setVendor("");

		extension2827.add(extension2830);
		org.inspection_plusplus.Extension extension2832 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2833 = new java.util.ArrayList<Object>();
		any2833.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2832, "any", any2833);
		extension2832.setVendor("");

		extension2827.add(extension2832);
		org.inspection_plusplus.Extension extension2834 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2835 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2834, "any", any2835);
		extension2834.setVendor("");

		extension2827.add(extension2834);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2826, "extension", extension2827);

		qCSingle2825.setToleranceRef(iD4Referencing2826);
		qCSingle2825.setAllAssembliesReferenced(true);
		qCSingle2825.setDescription("aaaaaaa");
		qCSingle2825.setFunction("");
		qCSingle2825.setPartQAversion("");
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> partRef2836 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing2837 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2837.setClassID("");
		iD4Referencing2837.setUuid("aaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2838 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2839 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2840 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2839, "any", any2840);
		extension2839.setVendor("aaaaa");

		extension2838.add(extension2839);
		org.inspection_plusplus.Extension extension2841 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2842 = new java.util.ArrayList<Object>();
		any2842.add(new Object());
		any2842.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2841, "any", any2842);
		extension2841.setVendor("aaa");

		extension2838.add(extension2841);
		org.inspection_plusplus.Extension extension2843 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2844 = new java.util.ArrayList<Object>();
		any2844.add(new Object());
		any2844.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2843, "any", any2844);
		extension2843.setVendor("aaaaaa");

		extension2838.add(extension2843);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2837, "extension", extension2838);

		partRef2836.add(iD4Referencing2837);
		eu.sarunas.junit.TestsHelper.set(qCSingle2825, "partRef", partRef2836);
		java.util.ArrayList<Object> calculationActualElementDetectionRef2845 = new java.util.ArrayList<Object>();
		calculationActualElementDetectionRef2845.add(new Object());
		calculationActualElementDetectionRef2845.add(new Object());
		calculationActualElementDetectionRef2845.add(new Object());
		eu.sarunas.junit.TestsHelper.set(qCSingle2825, "calculationActualElementDetectionRef", calculationActualElementDetectionRef2845);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeRef2846 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing2847 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2847.setClassID("aaa");
		iD4Referencing2847.setUuid("aaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2848 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2849 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2850 = new java.util.ArrayList<Object>();
		any2850.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2849, "any", any2850);
		extension2849.setVendor("");

		extension2848.add(extension2849);
		org.inspection_plusplus.Extension extension2851 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2852 = new java.util.ArrayList<Object>();
		any2852.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2851, "any", any2852);
		extension2851.setVendor("aaaaaaaaaaa");

		extension2848.add(extension2851);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2847, "extension", extension2848);

		ipeRef2846.add(iD4Referencing2847);
		eu.sarunas.junit.TestsHelper.set(qCSingle2825, "ipeRef", ipeRef2846);
		qCSingle2825.setName("aaaaaaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects2853 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2853.setUuid("aaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2854 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects2853, "extension", extension2854);

		qCSingle2825.setSystemID(iD4Objects2853);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2855 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(qCSingle2825, "commentRef", commentRef2855);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2856 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(qCSingle2825, "extension", extension2856);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2857 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(qCSingle2825, "displayID", displayID2857);
		java.util.ArrayList<org.inspection_plusplus.History> history2858 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history2859 = new org.inspection_plusplus.History();
		history2859.setAction("");
		history2859.setAuthor("aaa");
		history2859.setComment("");
		history2859.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 48, 10)));

		history2858.add(history2859);
		eu.sarunas.junit.TestsHelper.set(qCSingle2825, "history", history2858);

		qc2708.add(qCSingle2825);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan2453, "qc", qc2708);
		inspectionPlan2453.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects2860 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2860.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2861 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2862 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2863 = new java.util.ArrayList<Object>();
		any2863.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2862, "any", any2863);
		extension2862.setVendor("aa");

		extension2861.add(extension2862);
		eu.sarunas.junit.TestsHelper.set(iD4Objects2860, "extension", extension2861);

		inspectionPlan2453.setSystemID(iD4Objects2860);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2864 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing2865 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2865.setClassID("aaa");
		iD4Referencing2865.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2866 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2867 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2868 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2867, "any", any2868);
		extension2867.setVendor("");

		extension2866.add(extension2867);
		org.inspection_plusplus.Extension extension2869 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2870 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2869, "any", any2870);
		extension2869.setVendor("");

		extension2866.add(extension2869);
		org.inspection_plusplus.Extension extension2871 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2872 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2871, "any", any2872);
		extension2871.setVendor("");

		extension2866.add(extension2871);
		org.inspection_plusplus.Extension extension2873 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2874 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2873, "any", any2874);
		extension2873.setVendor("");

		extension2866.add(extension2873);
		org.inspection_plusplus.Extension extension2875 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2876 = new java.util.ArrayList<Object>();
		any2876.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2875, "any", any2876);
		extension2875.setVendor("aaaaaaaaa");

		extension2866.add(extension2875);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2865, "extension", extension2866);

		commentRef2864.add(iD4Referencing2865);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan2453, "commentRef", commentRef2864);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2877 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2878 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2879 = new java.util.ArrayList<Object>();
		any2879.add(new Object());
		any2879.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2878, "any", any2879);
		extension2878.setVendor("aaaaa");

		extension2877.add(extension2878);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan2453, "extension", extension2877);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2880 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan2453, "displayID", displayID2880);
		java.util.ArrayList<org.inspection_plusplus.History> history2881 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan2453, "history", history2881);
		org.inspection_plusplus.Sender sender2882 = new org.inspection_plusplus.Sender();
		sender2882.setAppID("aaa");
		sender2882.setDomain("aaaaaaaa");
		org.inspection_plusplus.Extension extension2883 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2884 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2883, "any", any2884);
		extension2883.setVendor("");

		org.inspection_plusplus.ReturnStatus res = testObject.storeInspectionPlan(inspectionPlan2453, "", "", sender2882, extension2883);
		junit.framework.Assert.fail();
	};

	@org.junit.Test
	public void testtestStoreInspectionPlan9() throws Throwable
	{
		org.inspection_plusplus.service.v2.WebServiceImpl testObject = new org.inspection_plusplus.service.v2.WebServiceImpl();
		org.inspection_plusplus.InspectionPlan inspectionPlan2885 = new org.inspection_plusplus.InspectionPlan();
		inspectionPlan2885.setInspectionCategories("aaa");
		inspectionPlan2885.setIPsymmetry("");
		inspectionPlan2885.setPartQAversion("aaaaaaaaaaa");
		eu.sarunas.junit.TestsHelper.set(inspectionPlan2885, "pdMmaturity", "");
		inspectionPlan2885.setQAmaturity("");
		inspectionPlan2885.setQAversion("aaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Comment> comment2886 = new java.util.ArrayList<org.inspection_plusplus.Comment>();
		org.inspection_plusplus.Comment comment2887 = new org.inspection_plusplus.Comment();
		comment2887.setAuthor("");
		comment2887.setReceiver("");
		comment2887.setSubject("");
		comment2887.setText("");
		comment2887.setName("aaaaaaaaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects2888 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2888.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2889 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2890 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2891 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2890, "any", any2891);
		extension2890.setVendor("");

		extension2889.add(extension2890);
		org.inspection_plusplus.Extension extension2892 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2893 = new java.util.ArrayList<Object>();
		any2893.add(new Object());
		any2893.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2892, "any", any2893);
		extension2892.setVendor("");

		extension2889.add(extension2892);
		eu.sarunas.junit.TestsHelper.set(iD4Objects2888, "extension", extension2889);

		comment2887.setSystemID(iD4Objects2888);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2894 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(comment2887, "commentRef", commentRef2894);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2895 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2896 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2897 = new java.util.ArrayList<Object>();
		any2897.add(new Object());
		any2897.add(new Object());
		any2897.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2896, "any", any2897);
		extension2896.setVendor("");

		extension2895.add(extension2896);
		eu.sarunas.junit.TestsHelper.set(comment2887, "extension", extension2895);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2898 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(comment2887, "displayID", displayID2898);
		java.util.ArrayList<org.inspection_plusplus.History> history2899 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history2900 = new org.inspection_plusplus.History();
		history2900.setAction("");
		history2900.setAuthor("");
		history2900.setComment("aaaaaaaaaaaaaaa");
		history2900.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 51, 26)));

		history2899.add(history2900);
		org.inspection_plusplus.History history2901 = new org.inspection_plusplus.History();
		history2901.setAction("");
		history2901.setAuthor("");
		history2901.setComment("aaaaaa");
		history2901.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 49, 55)));

		history2899.add(history2901);
		eu.sarunas.junit.TestsHelper.set(comment2887, "history", history2899);

		comment2886.add(comment2887);
		org.inspection_plusplus.Comment comment2902 = new org.inspection_plusplus.Comment();
		comment2902.setAuthor("");
		comment2902.setReceiver("aaaaaaaaaaaaaaa");
		comment2902.setSubject("");
		comment2902.setText("");
		comment2902.setName("aaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects2903 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2903.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2904 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects2903, "extension", extension2904);

		comment2902.setSystemID(iD4Objects2903);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2905 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(comment2902, "commentRef", commentRef2905);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2906 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(comment2902, "extension", extension2906);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2907 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID2908 = new org.inspection_plusplus.DisplayID();
		displayID2908.setLanguage("aaaaaaa");
		displayID2908.setText("");

		displayID2907.add(displayID2908);
		eu.sarunas.junit.TestsHelper.set(comment2902, "displayID", displayID2907);
		java.util.ArrayList<org.inspection_plusplus.History> history2909 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(comment2902, "history", history2909);

		comment2886.add(comment2902);
		org.inspection_plusplus.Comment comment2910 = new org.inspection_plusplus.Comment();
		comment2910.setAuthor("aaaaaaaaaaa");
		comment2910.setReceiver("aaaa");
		comment2910.setSubject("aaaaaaa");
		comment2910.setText("");
		comment2910.setName("aaaaaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects2911 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2911.setUuid("aaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2912 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects2911, "extension", extension2912);

		comment2910.setSystemID(iD4Objects2911);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2913 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing2914 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2914.setClassID("");
		iD4Referencing2914.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2915 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2916 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2917 = new java.util.ArrayList<Object>();
		any2917.add(new Object());
		any2917.add(new Object());
		any2917.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2916, "any", any2917);
		extension2916.setVendor("aa");

		extension2915.add(extension2916);
		org.inspection_plusplus.Extension extension2918 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2919 = new java.util.ArrayList<Object>();
		any2919.add(new Object());
		any2919.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2918, "any", any2919);
		extension2918.setVendor("");

		extension2915.add(extension2918);
		org.inspection_plusplus.Extension extension2920 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2921 = new java.util.ArrayList<Object>();
		any2921.add(new Object());
		any2921.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2920, "any", any2921);
		extension2920.setVendor("");

		extension2915.add(extension2920);
		org.inspection_plusplus.Extension extension2922 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2923 = new java.util.ArrayList<Object>();
		any2923.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2922, "any", any2923);
		extension2922.setVendor("");

		extension2915.add(extension2922);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2914, "extension", extension2915);

		commentRef2913.add(iD4Referencing2914);
		eu.sarunas.junit.TestsHelper.set(comment2910, "commentRef", commentRef2913);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2924 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2925 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2926 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2925, "any", any2926);
		extension2925.setVendor("");

		extension2924.add(extension2925);
		org.inspection_plusplus.Extension extension2927 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2928 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2927, "any", any2928);
		extension2927.setVendor("");

		extension2924.add(extension2927);
		eu.sarunas.junit.TestsHelper.set(comment2910, "extension", extension2924);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2929 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(comment2910, "displayID", displayID2929);
		java.util.ArrayList<org.inspection_plusplus.History> history2930 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(comment2910, "history", history2930);

		comment2886.add(comment2910);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan2885, "comment", comment2886);
		java.util.ArrayList<org.inspection_plusplus.Tolerance> tolerance2931 = new java.util.ArrayList<org.inspection_plusplus.Tolerance>();
		org.inspection_plusplus.TolDistance tolDistance2932 = new org.inspection_plusplus.TolDistance();
		org.inspection_plusplus.IntervalToleranceCriterion intervalToleranceCriterion2933 = new org.inspection_plusplus.IntervalToleranceCriterion();
		intervalToleranceCriterion2933.setInOut("aaa");
		intervalToleranceCriterion2933.setLowerToleranceValue(new java.math.BigDecimal(-143.61));
		intervalToleranceCriterion2933.setUpperToleranceValue(new java.math.BigDecimal(-19.45));

		tolDistance2932.setCriterion(intervalToleranceCriterion2933);
		tolDistance2932.setStandardConformity("");
		org.inspection_plusplus.ToleranceSource toleranceSource2934 = new org.inspection_plusplus.ToleranceSource();
		toleranceSource2934.setTyp("aaaaaaaaaaaa");
		toleranceSource2934.setValidFrom(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 4, 18)));
		toleranceSource2934.setValidTo(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 21, 50)));
		toleranceSource2934.setName("aaaaaaaaaaaaaaaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects2935 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2935.setUuid("aaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2936 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2937 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2938 = new java.util.ArrayList<Object>();
		any2938.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2937, "any", any2938);
		extension2937.setVendor("");

		extension2936.add(extension2937);
		org.inspection_plusplus.Extension extension2939 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2940 = new java.util.ArrayList<Object>();
		any2940.add(new Object());
		any2940.add(new Object());
		any2940.add(new Object());
		any2940.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2939, "any", any2940);
		extension2939.setVendor("aaaaaaaa");

		extension2936.add(extension2939);
		eu.sarunas.junit.TestsHelper.set(iD4Objects2935, "extension", extension2936);

		toleranceSource2934.setSystemID(iD4Objects2935);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2941 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing2942 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2942.setClassID("aaa");
		iD4Referencing2942.setUuid("aaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2943 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2944 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2945 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2944, "any", any2945);
		extension2944.setVendor("");

		extension2943.add(extension2944);
		org.inspection_plusplus.Extension extension2946 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2947 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2946, "any", any2947);
		extension2946.setVendor("");

		extension2943.add(extension2946);
		org.inspection_plusplus.Extension extension2948 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2949 = new java.util.ArrayList<Object>();
		any2949.add(new Object());
		any2949.add(new Object());
		any2949.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2948, "any", any2949);
		extension2948.setVendor("aaaa");

		extension2943.add(extension2948);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2942, "extension", extension2943);

		commentRef2941.add(iD4Referencing2942);
		org.inspection_plusplus.ID4Referencing iD4Referencing2950 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2950.setClassID("aaaa");
		iD4Referencing2950.setUuid("aaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2951 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2952 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2953 = new java.util.ArrayList<Object>();
		any2953.add(new Object());
		any2953.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2952, "any", any2953);
		extension2952.setVendor("");

		extension2951.add(extension2952);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2950, "extension", extension2951);

		commentRef2941.add(iD4Referencing2950);
		org.inspection_plusplus.ID4Referencing iD4Referencing2954 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2954.setClassID("");
		iD4Referencing2954.setUuid("aaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2955 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2956 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2957 = new java.util.ArrayList<Object>();
		any2957.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2956, "any", any2957);
		extension2956.setVendor("");

		extension2955.add(extension2956);
		org.inspection_plusplus.Extension extension2958 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2959 = new java.util.ArrayList<Object>();
		any2959.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension2958, "any", any2959);
		extension2958.setVendor("aaaaaa");

		extension2955.add(extension2958);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2954, "extension", extension2955);

		commentRef2941.add(iD4Referencing2954);
		eu.sarunas.junit.TestsHelper.set(toleranceSource2934, "commentRef", commentRef2941);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2960 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2961 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2962 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2961, "any", any2962);
		extension2961.setVendor("aaaaa");

		extension2960.add(extension2961);
		org.inspection_plusplus.Extension extension2963 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2964 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2963, "any", any2964);
		extension2963.setVendor("");

		extension2960.add(extension2963);
		org.inspection_plusplus.Extension extension2965 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2966 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2965, "any", any2966);
		extension2965.setVendor("");

		extension2960.add(extension2965);
		eu.sarunas.junit.TestsHelper.set(toleranceSource2934, "extension", extension2960);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2967 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(toleranceSource2934, "displayID", displayID2967);
		java.util.ArrayList<org.inspection_plusplus.History> history2968 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(toleranceSource2934, "history", history2968);

		tolDistance2932.setToleranceSource(toleranceSource2934);
		tolDistance2932.setName("aaaaaaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects2969 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2969.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2970 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects2969, "extension", extension2970);

		tolDistance2932.setSystemID(iD4Objects2969);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2971 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing2972 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2972.setClassID("");
		iD4Referencing2972.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2973 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2972, "extension", extension2973);

		commentRef2971.add(iD4Referencing2972);
		eu.sarunas.junit.TestsHelper.set(tolDistance2932, "commentRef", commentRef2971);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2974 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(tolDistance2932, "extension", extension2974);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2975 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID2976 = new org.inspection_plusplus.DisplayID();
		displayID2976.setLanguage("aaaaaa");
		displayID2976.setText("aaa");

		displayID2975.add(displayID2976);
		org.inspection_plusplus.DisplayID displayID2977 = new org.inspection_plusplus.DisplayID();
		displayID2977.setLanguage("");
		displayID2977.setText("");

		displayID2975.add(displayID2977);
		eu.sarunas.junit.TestsHelper.set(tolDistance2932, "displayID", displayID2975);
		java.util.ArrayList<org.inspection_plusplus.History> history2978 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history2979 = new org.inspection_plusplus.History();
		history2979.setAction("");
		history2979.setAuthor("");
		history2979.setComment("aaaaaaaaaaa");
		history2979.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 51, 19)));

		history2978.add(history2979);
		eu.sarunas.junit.TestsHelper.set(tolDistance2932, "history", history2978);

		tolerance2931.add(tolDistance2932);
		org.inspection_plusplus.TolAxisAngleX tolAxisAngleX2980 = new org.inspection_plusplus.TolAxisAngleX();
		org.inspection_plusplus.IntervalToleranceCriterion intervalToleranceCriterion2981 = new org.inspection_plusplus.IntervalToleranceCriterion();
		intervalToleranceCriterion2981.setInOut("");
		intervalToleranceCriterion2981.setLowerToleranceValue(new java.math.BigDecimal(-41.79));
		intervalToleranceCriterion2981.setUpperToleranceValue(new java.math.BigDecimal(1.33));

		tolAxisAngleX2980.setCriterion(intervalToleranceCriterion2981);
		org.inspection_plusplus.ID4Referencing iD4Referencing2982 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing2982.setClassID("aaaaaaaaaaaa");
		iD4Referencing2982.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2983 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2984 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2985 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2984, "any", any2985);
		extension2984.setVendor("aaaaaaaaaaaaaaaaaaaaaaa");

		extension2983.add(extension2984);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing2982, "extension", extension2983);

		tolAxisAngleX2980.setReferenceSystemRef(iD4Referencing2982);
		tolAxisAngleX2980.setStandardConformity("");
		org.inspection_plusplus.ToleranceSource toleranceSource2986 = new org.inspection_plusplus.ToleranceSource();
		toleranceSource2986.setTyp("aaa");
		toleranceSource2986.setValidFrom(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 1, 1)));
		toleranceSource2986.setValidTo(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 59, 14)));
		toleranceSource2986.setName("aaaaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects2987 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2987.setUuid("aaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2988 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects2987, "extension", extension2988);

		toleranceSource2986.setSystemID(iD4Objects2987);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef2989 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(toleranceSource2986, "commentRef", commentRef2989);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2990 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(toleranceSource2986, "extension", extension2990);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID2991 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID2992 = new org.inspection_plusplus.DisplayID();
		displayID2992.setLanguage("aa");
		displayID2992.setText("aaaaaaaaa");

		displayID2991.add(displayID2992);
		eu.sarunas.junit.TestsHelper.set(toleranceSource2986, "displayID", displayID2991);
		java.util.ArrayList<org.inspection_plusplus.History> history2993 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history2994 = new org.inspection_plusplus.History();
		history2994.setAction("");
		history2994.setAuthor("aa");
		history2994.setComment("");
		history2994.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 1, 56)));

		history2993.add(history2994);
		org.inspection_plusplus.History history2995 = new org.inspection_plusplus.History();
		history2995.setAction("aa");
		history2995.setAuthor("aaaaaa");
		history2995.setComment("");
		history2995.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 0, 44, 31)));

		history2993.add(history2995);
		eu.sarunas.junit.TestsHelper.set(toleranceSource2986, "history", history2993);

		tolAxisAngleX2980.setToleranceSource(toleranceSource2986);
		tolAxisAngleX2980.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects2996 = new org.inspection_plusplus.ID4Objects();
		iD4Objects2996.setUuid("aaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension2997 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension2998 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any2999 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension2998, "any", any2999);
		extension2998.setVendor("aaaa");

		extension2997.add(extension2998);
		eu.sarunas.junit.TestsHelper.set(iD4Objects2996, "extension", extension2997);

		tolAxisAngleX2980.setSystemID(iD4Objects2996);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef3000 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing3001 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3001.setClassID("");
		iD4Referencing3001.setUuid("aaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3002 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3003 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3004 = new java.util.ArrayList<Object>();
		any3004.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3003, "any", any3004);
		extension3003.setVendor("");

		extension3002.add(extension3003);
		org.inspection_plusplus.Extension extension3005 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3006 = new java.util.ArrayList<Object>();
		any3006.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3005, "any", any3006);
		extension3005.setVendor("");

		extension3002.add(extension3005);
		org.inspection_plusplus.Extension extension3007 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3008 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3007, "any", any3008);
		extension3007.setVendor("a");

		extension3002.add(extension3007);
		org.inspection_plusplus.Extension extension3009 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3010 = new java.util.ArrayList<Object>();
		any3010.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3009, "any", any3010);
		extension3009.setVendor("a");

		extension3002.add(extension3009);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3001, "extension", extension3002);

		commentRef3000.add(iD4Referencing3001);
		eu.sarunas.junit.TestsHelper.set(tolAxisAngleX2980, "commentRef", commentRef3000);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3011 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3012 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3013 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3012, "any", any3013);
		extension3012.setVendor("");

		extension3011.add(extension3012);
		org.inspection_plusplus.Extension extension3014 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3015 = new java.util.ArrayList<Object>();
		any3015.add(new Object());
		any3015.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3014, "any", any3015);
		extension3014.setVendor("aaaa");

		extension3011.add(extension3014);
		org.inspection_plusplus.Extension extension3016 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3017 = new java.util.ArrayList<Object>();
		any3017.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3016, "any", any3017);
		extension3016.setVendor("aaaaaaaaa");

		extension3011.add(extension3016);
		org.inspection_plusplus.Extension extension3018 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3019 = new java.util.ArrayList<Object>();
		any3019.add(new Object());
		any3019.add(new Object());
		any3019.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3018, "any", any3019);
		extension3018.setVendor("aaaa");

		extension3011.add(extension3018);
		eu.sarunas.junit.TestsHelper.set(tolAxisAngleX2980, "extension", extension3011);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID3020 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID3021 = new org.inspection_plusplus.DisplayID();
		displayID3021.setLanguage("");
		displayID3021.setText("aa");

		displayID3020.add(displayID3021);
		eu.sarunas.junit.TestsHelper.set(tolAxisAngleX2980, "displayID", displayID3020);
		java.util.ArrayList<org.inspection_plusplus.History> history3022 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(tolAxisAngleX2980, "history", history3022);

		tolerance2931.add(tolAxisAngleX2980);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan2885, "tolerance", tolerance2931);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> partRef3023 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan2885, "partRef", partRef3023);
		java.util.ArrayList<org.inspection_plusplus.IPE> ipe3024 = new java.util.ArrayList<org.inspection_plusplus.IPE>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan2885, "ipe", ipe3024);
		java.util.ArrayList<org.inspection_plusplus.ReferenceSystem> referenceSystem3025 = new java.util.ArrayList<org.inspection_plusplus.ReferenceSystem>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan2885, "referenceSystem", referenceSystem3025);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> inspectionTaskRef3026 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing3027 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3027.setClassID("");
		iD4Referencing3027.setUuid("aaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3028 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3029 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3030 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3029, "any", any3030);
		extension3029.setVendor("");

		extension3028.add(extension3029);
		org.inspection_plusplus.Extension extension3031 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3032 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3031, "any", any3032);
		extension3031.setVendor("aaaaaaaaaaa");

		extension3028.add(extension3031);
		org.inspection_plusplus.Extension extension3033 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3034 = new java.util.ArrayList<Object>();
		any3034.add(new Object());
		any3034.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3033, "any", any3034);
		extension3033.setVendor("");

		extension3028.add(extension3033);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3027, "extension", extension3028);

		inspectionTaskRef3026.add(iD4Referencing3027);
		org.inspection_plusplus.ID4Referencing iD4Referencing3035 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3035.setClassID("");
		iD4Referencing3035.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3036 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3037 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3038 = new java.util.ArrayList<Object>();
		any3038.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3037, "any", any3038);
		extension3037.setVendor("a");

		extension3036.add(extension3037);
		org.inspection_plusplus.Extension extension3039 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3040 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3039, "any", any3040);
		extension3039.setVendor("");

		extension3036.add(extension3039);
		org.inspection_plusplus.Extension extension3041 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3042 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3041, "any", any3042);
		extension3041.setVendor("");

		extension3036.add(extension3041);
		org.inspection_plusplus.Extension extension3043 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3044 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3043, "any", any3044);
		extension3043.setVendor("");

		extension3036.add(extension3043);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3035, "extension", extension3036);

		inspectionTaskRef3026.add(iD4Referencing3035);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan2885, "inspectionTaskRef", inspectionTaskRef3026);
		java.util.ArrayList<org.inspection_plusplus.QC> qc3045 = new java.util.ArrayList<org.inspection_plusplus.QC>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan2885, "qc", qc3045);
		inspectionPlan2885.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects3046 = new org.inspection_plusplus.ID4Objects();
		iD4Objects3046.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3047 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3048 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3049 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3048, "any", any3049);
		extension3048.setVendor("");

		extension3047.add(extension3048);
		org.inspection_plusplus.Extension extension3050 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3051 = new java.util.ArrayList<Object>();
		any3051.add(new Object());
		any3051.add(new Object());
		any3051.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3050, "any", any3051);
		extension3050.setVendor("aaaaaaaaaaaaaaa");

		extension3047.add(extension3050);
		eu.sarunas.junit.TestsHelper.set(iD4Objects3046, "extension", extension3047);

		inspectionPlan2885.setSystemID(iD4Objects3046);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef3052 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing3053 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3053.setClassID("aaaaaaaaa");
		iD4Referencing3053.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3054 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3053, "extension", extension3054);

		commentRef3052.add(iD4Referencing3053);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan2885, "commentRef", commentRef3052);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3055 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan2885, "extension", extension3055);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID3056 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID3057 = new org.inspection_plusplus.DisplayID();
		displayID3057.setLanguage("aaaaaaaaaaa");
		displayID3057.setText("aa");

		displayID3056.add(displayID3057);
		org.inspection_plusplus.DisplayID displayID3058 = new org.inspection_plusplus.DisplayID();
		displayID3058.setLanguage("aaaa");
		displayID3058.setText("");

		displayID3056.add(displayID3058);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan2885, "displayID", displayID3056);
		java.util.ArrayList<org.inspection_plusplus.History> history3059 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan2885, "history", history3059);
		org.inspection_plusplus.Sender sender3060 = new org.inspection_plusplus.Sender();
		sender3060.setAppID("aaaaaaaaaaaaaaaaaaaaaa");
		sender3060.setDomain("aaaaaaaaaa");
		org.inspection_plusplus.Extension extension3061 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3062 = new java.util.ArrayList<Object>();
		any3062.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3061, "any", any3062);
		extension3061.setVendor("");

		org.inspection_plusplus.ReturnStatus res = testObject.storeInspectionPlan(inspectionPlan2885, "aaa", "a", sender3060, extension3061);
		junit.framework.Assert.fail();
	};

	@org.junit.Test
	public void testtestStoreInspectionPlan10() throws Throwable
	{
		org.inspection_plusplus.service.v2.WebServiceImpl testObject = new org.inspection_plusplus.service.v2.WebServiceImpl();
		org.inspection_plusplus.InspectionPlan inspectionPlan3063 = new org.inspection_plusplus.InspectionPlan();
		inspectionPlan3063.setInspectionCategories("");
		inspectionPlan3063.setIPsymmetry("aa");
		inspectionPlan3063.setPartQAversion("");
		eu.sarunas.junit.TestsHelper.set(inspectionPlan3063, "pdMmaturity", "aa");
		inspectionPlan3063.setQAmaturity("aaaa");
		inspectionPlan3063.setQAversion("");
		java.util.ArrayList<org.inspection_plusplus.Comment> comment3064 = new java.util.ArrayList<org.inspection_plusplus.Comment>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan3063, "comment", comment3064);
		java.util.ArrayList<org.inspection_plusplus.Tolerance> tolerance3065 = new java.util.ArrayList<org.inspection_plusplus.Tolerance>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan3063, "tolerance", tolerance3065);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> partRef3066 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing3067 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3067.setClassID("");
		iD4Referencing3067.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3068 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3067, "extension", extension3068);

		partRef3066.add(iD4Referencing3067);
		org.inspection_plusplus.ID4Referencing iD4Referencing3069 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3069.setClassID("");
		iD4Referencing3069.setUuid("aaaaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3070 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3071 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3072 = new java.util.ArrayList<Object>();
		any3072.add(new Object());
		any3072.add(new Object());
		any3072.add(new Object());
		any3072.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3071, "any", any3072);
		extension3071.setVendor("aaaaa");

		extension3070.add(extension3071);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3069, "extension", extension3070);

		partRef3066.add(iD4Referencing3069);
		org.inspection_plusplus.ID4Referencing iD4Referencing3073 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3073.setClassID("");
		iD4Referencing3073.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3074 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3075 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3076 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3075, "any", any3076);
		extension3075.setVendor("");

		extension3074.add(extension3075);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3073, "extension", extension3074);

		partRef3066.add(iD4Referencing3073);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan3063, "partRef", partRef3066);
		java.util.ArrayList<org.inspection_plusplus.IPE> ipe3077 = new java.util.ArrayList<org.inspection_plusplus.IPE>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan3063, "ipe", ipe3077);
		java.util.ArrayList<org.inspection_plusplus.ReferenceSystem> referenceSystem3078 = new java.util.ArrayList<org.inspection_plusplus.ReferenceSystem>();
		org.inspection_plusplus.ReferenceSystem referenceSystem3079 = new org.inspection_plusplus.ReferenceSystem();
		java.util.ArrayList<org.inspection_plusplus.Datum> datum3080 = new java.util.ArrayList<org.inspection_plusplus.Datum>();
		org.inspection_plusplus.Datum datum3081 = new org.inspection_plusplus.Datum();
		org.inspection_plusplus.Vector3D vector3D3082 = new org.inspection_plusplus.Vector3D();
		vector3D3082.setX(-79.92);
		vector3D3082.setY(-199.32);
		vector3D3082.setZ(120.57);

		datum3081.setDisplacement(vector3D3082);
		datum3081.setRole("aaaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.DatumTarget> datumTarget3083 = new java.util.ArrayList<org.inspection_plusplus.DatumTarget>();
		eu.sarunas.junit.TestsHelper.set(datum3081, "datumTarget", datumTarget3083);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeGeometricRef3084 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing3085 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3085.setClassID("");
		iD4Referencing3085.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3086 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3087 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3088 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3087, "any", any3088);
		extension3087.setVendor("a");

		extension3086.add(extension3087);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3085, "extension", extension3086);

		ipeGeometricRef3084.add(iD4Referencing3085);
		eu.sarunas.junit.TestsHelper.set(datum3081, "ipeGeometricRef", ipeGeometricRef3084);
		datum3081.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects3089 = new org.inspection_plusplus.ID4Objects();
		iD4Objects3089.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3090 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3091 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3092 = new java.util.ArrayList<Object>();
		any3092.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3091, "any", any3092);
		extension3091.setVendor("a");

		extension3090.add(extension3091);
		eu.sarunas.junit.TestsHelper.set(iD4Objects3089, "extension", extension3090);

		datum3081.setSystemID(iD4Objects3089);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef3093 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing3094 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3094.setClassID("aaaaaaaaa");
		iD4Referencing3094.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3095 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3096 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3097 = new java.util.ArrayList<Object>();
		any3097.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3096, "any", any3097);
		extension3096.setVendor("aaaaaaaaaaaaaaaaaaaaa");

		extension3095.add(extension3096);
		org.inspection_plusplus.Extension extension3098 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3099 = new java.util.ArrayList<Object>();
		any3099.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3098, "any", any3099);
		extension3098.setVendor("");

		extension3095.add(extension3098);
		org.inspection_plusplus.Extension extension3100 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3101 = new java.util.ArrayList<Object>();
		any3101.add(new Object());
		any3101.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3100, "any", any3101);
		extension3100.setVendor("");

		extension3095.add(extension3100);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3094, "extension", extension3095);

		commentRef3093.add(iD4Referencing3094);
		eu.sarunas.junit.TestsHelper.set(datum3081, "commentRef", commentRef3093);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3102 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3103 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3104 = new java.util.ArrayList<Object>();
		any3104.add(new Object());
		any3104.add(new Object());
		any3104.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3103, "any", any3104);
		extension3103.setVendor("");

		extension3102.add(extension3103);
		org.inspection_plusplus.Extension extension3105 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3106 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3105, "any", any3106);
		extension3105.setVendor("aa");

		extension3102.add(extension3105);
		eu.sarunas.junit.TestsHelper.set(datum3081, "extension", extension3102);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID3107 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(datum3081, "displayID", displayID3107);
		java.util.ArrayList<org.inspection_plusplus.History> history3108 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(datum3081, "history", history3108);

		datum3080.add(datum3081);
		eu.sarunas.junit.TestsHelper.set(referenceSystem3079, "datum", datum3080);
		java.util.ArrayList<org.inspection_plusplus.AlignmentStrategy> alignmentStrategy3109 = new java.util.ArrayList<org.inspection_plusplus.AlignmentStrategy>();
		org.inspection_plusplus.AlignmentBestfit alignmentBestfit3110 = new org.inspection_plusplus.AlignmentBestfit();
		alignmentBestfit3110.setTerminatingCondition(-190.54);
		alignmentBestfit3110.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects3111 = new org.inspection_plusplus.ID4Objects();
		iD4Objects3111.setUuid("aaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3112 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3113 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3114 = new java.util.ArrayList<Object>();
		any3114.add(new Object());
		any3114.add(new Object());
		any3114.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3113, "any", any3114);
		extension3113.setVendor("aaaaaaaaaa");

		extension3112.add(extension3113);
		eu.sarunas.junit.TestsHelper.set(iD4Objects3111, "extension", extension3112);

		alignmentBestfit3110.setSystemID(iD4Objects3111);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef3115 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing3116 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3116.setClassID("aaaaaaaaaaaaaaaaaaaaaa");
		iD4Referencing3116.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3117 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3118 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3119 = new java.util.ArrayList<Object>();
		any3119.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3118, "any", any3119);
		extension3118.setVendor("aaaa");

		extension3117.add(extension3118);
		org.inspection_plusplus.Extension extension3120 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3121 = new java.util.ArrayList<Object>();
		any3121.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3120, "any", any3121);
		extension3120.setVendor("");

		extension3117.add(extension3120);
		org.inspection_plusplus.Extension extension3122 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3123 = new java.util.ArrayList<Object>();
		any3123.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3122, "any", any3123);
		extension3122.setVendor("");

		extension3117.add(extension3122);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3116, "extension", extension3117);

		commentRef3115.add(iD4Referencing3116);
		eu.sarunas.junit.TestsHelper.set(alignmentBestfit3110, "commentRef", commentRef3115);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3124 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3125 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3126 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3125, "any", any3126);
		extension3125.setVendor("");

		extension3124.add(extension3125);
		org.inspection_plusplus.Extension extension3127 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3128 = new java.util.ArrayList<Object>();
		any3128.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3127, "any", any3128);
		extension3127.setVendor("");

		extension3124.add(extension3127);
		org.inspection_plusplus.Extension extension3129 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3130 = new java.util.ArrayList<Object>();
		any3130.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3129, "any", any3130);
		extension3129.setVendor("aaaaaaaaaaaaaaa");

		extension3124.add(extension3129);
		eu.sarunas.junit.TestsHelper.set(alignmentBestfit3110, "extension", extension3124);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID3131 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID3132 = new org.inspection_plusplus.DisplayID();
		displayID3132.setLanguage("a");
		displayID3132.setText("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");

		displayID3131.add(displayID3132);
		eu.sarunas.junit.TestsHelper.set(alignmentBestfit3110, "displayID", displayID3131);
		java.util.ArrayList<org.inspection_plusplus.History> history3133 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history3134 = new org.inspection_plusplus.History();
		history3134.setAction("aaaaaaaaa");
		history3134.setAuthor("aaaa");
		history3134.setComment("");
		history3134.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 42, 28)));

		history3133.add(history3134);
		org.inspection_plusplus.History history3135 = new org.inspection_plusplus.History();
		history3135.setAction("");
		history3135.setAuthor("aaaaaaaaaaaaaaaaa");
		history3135.setComment("aaa");
		history3135.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 38, 51)));

		history3133.add(history3135);
		eu.sarunas.junit.TestsHelper.set(alignmentBestfit3110, "history", history3133);

		alignmentStrategy3109.add(alignmentBestfit3110);
		eu.sarunas.junit.TestsHelper.set(referenceSystem3079, "alignmentStrategy", alignmentStrategy3109);
		referenceSystem3079.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects3136 = new org.inspection_plusplus.ID4Objects();
		iD4Objects3136.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3137 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3138 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3139 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3138, "any", any3139);
		extension3138.setVendor("aaaaaaaaa");

		extension3137.add(extension3138);
		eu.sarunas.junit.TestsHelper.set(iD4Objects3136, "extension", extension3137);

		referenceSystem3079.setSystemID(iD4Objects3136);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef3140 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing3141 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3141.setClassID("aaa");
		iD4Referencing3141.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3142 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3141, "extension", extension3142);

		commentRef3140.add(iD4Referencing3141);
		org.inspection_plusplus.ID4Referencing iD4Referencing3143 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3143.setClassID("aaaa");
		iD4Referencing3143.setUuid("aa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3144 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3143, "extension", extension3144);

		commentRef3140.add(iD4Referencing3143);
		org.inspection_plusplus.ID4Referencing iD4Referencing3145 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3145.setClassID("aaaaa");
		iD4Referencing3145.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3146 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3147 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3148 = new java.util.ArrayList<Object>();
		any3148.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3147, "any", any3148);
		extension3147.setVendor("aaaaaaaaaaaaaaaaaaa");

		extension3146.add(extension3147);
		org.inspection_plusplus.Extension extension3149 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3150 = new java.util.ArrayList<Object>();
		any3150.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3149, "any", any3150);
		extension3149.setVendor("");

		extension3146.add(extension3149);
		org.inspection_plusplus.Extension extension3151 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3152 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3151, "any", any3152);
		extension3151.setVendor("");

		extension3146.add(extension3151);
		org.inspection_plusplus.Extension extension3153 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3154 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3153, "any", any3154);
		extension3153.setVendor("a");

		extension3146.add(extension3153);
		org.inspection_plusplus.Extension extension3155 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3156 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3155, "any", any3156);
		extension3155.setVendor("");

		extension3146.add(extension3155);
		org.inspection_plusplus.Extension extension3157 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3158 = new java.util.ArrayList<Object>();
		any3158.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3157, "any", any3158);
		extension3157.setVendor("aaaaaaaaaa");

		extension3146.add(extension3157);
		org.inspection_plusplus.Extension extension3159 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3160 = new java.util.ArrayList<Object>();
		any3160.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3159, "any", any3160);
		extension3159.setVendor("aaa");

		extension3146.add(extension3159);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3145, "extension", extension3146);

		commentRef3140.add(iD4Referencing3145);
		eu.sarunas.junit.TestsHelper.set(referenceSystem3079, "commentRef", commentRef3140);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3161 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(referenceSystem3079, "extension", extension3161);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID3162 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID3163 = new org.inspection_plusplus.DisplayID();
		displayID3163.setLanguage("");
		displayID3163.setText("aaaaaaaaaaaaaaaa");

		displayID3162.add(displayID3163);
		org.inspection_plusplus.DisplayID displayID3164 = new org.inspection_plusplus.DisplayID();
		displayID3164.setLanguage("");
		displayID3164.setText("aaaaaaaaaaaaa");

		displayID3162.add(displayID3164);
		eu.sarunas.junit.TestsHelper.set(referenceSystem3079, "displayID", displayID3162);
		java.util.ArrayList<org.inspection_plusplus.History> history3165 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(referenceSystem3079, "history", history3165);

		referenceSystem3078.add(referenceSystem3079);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan3063, "referenceSystem", referenceSystem3078);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> inspectionTaskRef3166 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan3063, "inspectionTaskRef", inspectionTaskRef3166);
		java.util.ArrayList<org.inspection_plusplus.QC> qc3167 = new java.util.ArrayList<org.inspection_plusplus.QC>();
		org.inspection_plusplus.QCSingle qCSingle3168 = new org.inspection_plusplus.QCSingle();
		qCSingle3168.setGeoObjectDetail("aaaaaaaaa");
		org.inspection_plusplus.ID4Referencing iD4Referencing3169 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3169.setClassID("aaa");
		iD4Referencing3169.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3170 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3171 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3172 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3171, "any", any3172);
		extension3171.setVendor("");

		extension3170.add(extension3171);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3169, "extension", extension3170);

		qCSingle3168.setToleranceRef(iD4Referencing3169);
		qCSingle3168.setAllAssembliesReferenced(false);
		qCSingle3168.setDescription("aaaaaaa");
		qCSingle3168.setFunction("");
		qCSingle3168.setPartQAversion("aaaaa");
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> partRef3173 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing3174 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3174.setClassID("aaaaa");
		iD4Referencing3174.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3175 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3176 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3177 = new java.util.ArrayList<Object>();
		any3177.add(new Object());
		any3177.add(new Object());
		any3177.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3176, "any", any3177);
		extension3176.setVendor("aaaaaa");

		extension3175.add(extension3176);
		org.inspection_plusplus.Extension extension3178 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3179 = new java.util.ArrayList<Object>();
		any3179.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3178, "any", any3179);
		extension3178.setVendor("aaaaaaa");

		extension3175.add(extension3178);
		org.inspection_plusplus.Extension extension3180 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3181 = new java.util.ArrayList<Object>();
		any3181.add(new Object());
		any3181.add(new Object());
		any3181.add(new Object());
		any3181.add(new Object());
		any3181.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3180, "any", any3181);
		extension3180.setVendor("aa");

		extension3175.add(extension3180);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3174, "extension", extension3175);

		partRef3173.add(iD4Referencing3174);
		org.inspection_plusplus.ID4Referencing iD4Referencing3182 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3182.setClassID("");
		iD4Referencing3182.setUuid("aaaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3183 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3182, "extension", extension3183);

		partRef3173.add(iD4Referencing3182);
		eu.sarunas.junit.TestsHelper.set(qCSingle3168, "partRef", partRef3173);
		java.util.ArrayList<Object> calculationActualElementDetectionRef3184 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(qCSingle3168, "calculationActualElementDetectionRef", calculationActualElementDetectionRef3184);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeRef3185 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing3186 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3186.setClassID("aaaaaaaaaaa");
		iD4Referencing3186.setUuid("aa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3187 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3188 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3189 = new java.util.ArrayList<Object>();
		any3189.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3188, "any", any3189);
		extension3188.setVendor("");

		extension3187.add(extension3188);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3186, "extension", extension3187);

		ipeRef3185.add(iD4Referencing3186);
		eu.sarunas.junit.TestsHelper.set(qCSingle3168, "ipeRef", ipeRef3185);
		qCSingle3168.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects3190 = new org.inspection_plusplus.ID4Objects();
		iD4Objects3190.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3191 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects3190, "extension", extension3191);

		qCSingle3168.setSystemID(iD4Objects3190);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef3192 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(qCSingle3168, "commentRef", commentRef3192);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3193 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3194 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3195 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3194, "any", any3195);
		extension3194.setVendor("");

		extension3193.add(extension3194);
		eu.sarunas.junit.TestsHelper.set(qCSingle3168, "extension", extension3193);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID3196 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID3197 = new org.inspection_plusplus.DisplayID();
		displayID3197.setLanguage("aa");
		displayID3197.setText("");

		displayID3196.add(displayID3197);
		org.inspection_plusplus.DisplayID displayID3198 = new org.inspection_plusplus.DisplayID();
		displayID3198.setLanguage("aa");
		displayID3198.setText("");

		displayID3196.add(displayID3198);
		eu.sarunas.junit.TestsHelper.set(qCSingle3168, "displayID", displayID3196);
		java.util.ArrayList<org.inspection_plusplus.History> history3199 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history3200 = new org.inspection_plusplus.History();
		history3200.setAction("aaaaaa");
		history3200.setAuthor("aaaaaaaaaa");
		history3200.setComment("");
		history3200.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 43, 8)));

		history3199.add(history3200);
		eu.sarunas.junit.TestsHelper.set(qCSingle3168, "history", history3199);

		qc3167.add(qCSingle3168);
		org.inspection_plusplus.QCSingle qCSingle3201 = new org.inspection_plusplus.QCSingle();
		qCSingle3201.setGeoObjectDetail("aaaaa");
		org.inspection_plusplus.ID4Referencing iD4Referencing3202 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3202.setClassID("aaa");
		iD4Referencing3202.setUuid("aaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3203 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3202, "extension", extension3203);

		qCSingle3201.setToleranceRef(iD4Referencing3202);
		qCSingle3201.setAllAssembliesReferenced(true);
		qCSingle3201.setDescription("");
		qCSingle3201.setFunction("");
		qCSingle3201.setPartQAversion("");
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> partRef3204 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing3205 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3205.setClassID("");
		iD4Referencing3205.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3206 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3207 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3208 = new java.util.ArrayList<Object>();
		any3208.add(new Object());
		any3208.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3207, "any", any3208);
		extension3207.setVendor("");

		extension3206.add(extension3207);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3205, "extension", extension3206);

		partRef3204.add(iD4Referencing3205);
		org.inspection_plusplus.ID4Referencing iD4Referencing3209 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3209.setClassID("");
		iD4Referencing3209.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3210 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3211 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3212 = new java.util.ArrayList<Object>();
		any3212.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3211, "any", any3212);
		extension3211.setVendor("aaaa");

		extension3210.add(extension3211);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3209, "extension", extension3210);

		partRef3204.add(iD4Referencing3209);
		eu.sarunas.junit.TestsHelper.set(qCSingle3201, "partRef", partRef3204);
		java.util.ArrayList<Object> calculationActualElementDetectionRef3213 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(qCSingle3201, "calculationActualElementDetectionRef", calculationActualElementDetectionRef3213);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeRef3214 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing3215 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3215.setClassID("");
		iD4Referencing3215.setUuid("aaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3216 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3217 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3218 = new java.util.ArrayList<Object>();
		any3218.add(new Object());
		any3218.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3217, "any", any3218);
		extension3217.setVendor("aaaaaaaaaaaaa");

		extension3216.add(extension3217);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3215, "extension", extension3216);

		ipeRef3214.add(iD4Referencing3215);
		eu.sarunas.junit.TestsHelper.set(qCSingle3201, "ipeRef", ipeRef3214);
		qCSingle3201.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects3219 = new org.inspection_plusplus.ID4Objects();
		iD4Objects3219.setUuid("aaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3220 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects3219, "extension", extension3220);

		qCSingle3201.setSystemID(iD4Objects3219);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef3221 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing3222 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3222.setClassID("aaa");
		iD4Referencing3222.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3223 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3224 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3225 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3224, "any", any3225);
		extension3224.setVendor("aaaaaa");

		extension3223.add(extension3224);
		org.inspection_plusplus.Extension extension3226 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3227 = new java.util.ArrayList<Object>();
		any3227.add(new Object());
		any3227.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3226, "any", any3227);
		extension3226.setVendor("");

		extension3223.add(extension3226);
		org.inspection_plusplus.Extension extension3228 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3229 = new java.util.ArrayList<Object>();
		any3229.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3228, "any", any3229);
		extension3228.setVendor("aaaaaaaaaaaaaaaa");

		extension3223.add(extension3228);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3222, "extension", extension3223);

		commentRef3221.add(iD4Referencing3222);
		org.inspection_plusplus.ID4Referencing iD4Referencing3230 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3230.setClassID("");
		iD4Referencing3230.setUuid("aaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3231 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3230, "extension", extension3231);

		commentRef3221.add(iD4Referencing3230);
		org.inspection_plusplus.ID4Referencing iD4Referencing3232 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3232.setClassID("");
		iD4Referencing3232.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3233 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3232, "extension", extension3233);

		commentRef3221.add(iD4Referencing3232);
		eu.sarunas.junit.TestsHelper.set(qCSingle3201, "commentRef", commentRef3221);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3234 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3235 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3236 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3235, "any", any3236);
		extension3235.setVendor("");

		extension3234.add(extension3235);
		org.inspection_plusplus.Extension extension3237 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3238 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3237, "any", any3238);
		extension3237.setVendor("");

		extension3234.add(extension3237);
		eu.sarunas.junit.TestsHelper.set(qCSingle3201, "extension", extension3234);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID3239 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID3240 = new org.inspection_plusplus.DisplayID();
		displayID3240.setLanguage("aa");
		displayID3240.setText("");

		displayID3239.add(displayID3240);
		org.inspection_plusplus.DisplayID displayID3241 = new org.inspection_plusplus.DisplayID();
		displayID3241.setLanguage("");
		displayID3241.setText("aaaaaa");

		displayID3239.add(displayID3241);
		eu.sarunas.junit.TestsHelper.set(qCSingle3201, "displayID", displayID3239);
		java.util.ArrayList<org.inspection_plusplus.History> history3242 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history3243 = new org.inspection_plusplus.History();
		history3243.setAction("");
		history3243.setAuthor("");
		history3243.setComment("aaaaaaaa");
		history3243.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 18, 38)));

		history3242.add(history3243);
		eu.sarunas.junit.TestsHelper.set(qCSingle3201, "history", history3242);

		qc3167.add(qCSingle3201);
		org.inspection_plusplus.QCSingle qCSingle3244 = new org.inspection_plusplus.QCSingle();
		qCSingle3244.setGeoObjectDetail("aaaaaa");
		org.inspection_plusplus.ID4Referencing iD4Referencing3245 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3245.setClassID("a");
		iD4Referencing3245.setUuid("aaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3246 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3247 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3248 = new java.util.ArrayList<Object>();
		any3248.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3247, "any", any3248);
		extension3247.setVendor("");

		extension3246.add(extension3247);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3245, "extension", extension3246);

		qCSingle3244.setToleranceRef(iD4Referencing3245);
		qCSingle3244.setAllAssembliesReferenced(false);
		qCSingle3244.setDescription("aaaa");
		qCSingle3244.setFunction("aaaaaaa");
		qCSingle3244.setPartQAversion("");
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> partRef3249 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing3250 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3250.setClassID("");
		iD4Referencing3250.setUuid("aaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3251 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3252 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3253 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3252, "any", any3253);
		extension3252.setVendor("");

		extension3251.add(extension3252);
		org.inspection_plusplus.Extension extension3254 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3255 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3254, "any", any3255);
		extension3254.setVendor("aaaaaaaaaaaaaaa");

		extension3251.add(extension3254);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3250, "extension", extension3251);

		partRef3249.add(iD4Referencing3250);
		eu.sarunas.junit.TestsHelper.set(qCSingle3244, "partRef", partRef3249);
		java.util.ArrayList<Object> calculationActualElementDetectionRef3256 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(qCSingle3244, "calculationActualElementDetectionRef", calculationActualElementDetectionRef3256);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeRef3257 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing3258 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3258.setClassID("");
		iD4Referencing3258.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3259 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3260 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3261 = new java.util.ArrayList<Object>();
		any3261.add(new Object());
		any3261.add(new Object());
		any3261.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3260, "any", any3261);
		extension3260.setVendor("");

		extension3259.add(extension3260);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3258, "extension", extension3259);

		ipeRef3257.add(iD4Referencing3258);
		eu.sarunas.junit.TestsHelper.set(qCSingle3244, "ipeRef", ipeRef3257);
		qCSingle3244.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects3262 = new org.inspection_plusplus.ID4Objects();
		iD4Objects3262.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3263 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3264 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3265 = new java.util.ArrayList<Object>();
		any3265.add(new Object());
		any3265.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3264, "any", any3265);
		extension3264.setVendor("");

		extension3263.add(extension3264);
		org.inspection_plusplus.Extension extension3266 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3267 = new java.util.ArrayList<Object>();
		any3267.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3266, "any", any3267);
		extension3266.setVendor("aaaaaaa");

		extension3263.add(extension3266);
		eu.sarunas.junit.TestsHelper.set(iD4Objects3262, "extension", extension3263);

		qCSingle3244.setSystemID(iD4Objects3262);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef3268 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing3269 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3269.setClassID("");
		iD4Referencing3269.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3270 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3271 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3272 = new java.util.ArrayList<Object>();
		any3272.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3271, "any", any3272);
		extension3271.setVendor("aaaaaaaa");

		extension3270.add(extension3271);
		org.inspection_plusplus.Extension extension3273 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3274 = new java.util.ArrayList<Object>();
		any3274.add(new Object());
		any3274.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3273, "any", any3274);
		extension3273.setVendor("");

		extension3270.add(extension3273);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3269, "extension", extension3270);

		commentRef3268.add(iD4Referencing3269);
		org.inspection_plusplus.ID4Referencing iD4Referencing3275 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3275.setClassID("aaa");
		iD4Referencing3275.setUuid("aa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3276 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3275, "extension", extension3276);

		commentRef3268.add(iD4Referencing3275);
		eu.sarunas.junit.TestsHelper.set(qCSingle3244, "commentRef", commentRef3268);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3277 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3278 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3279 = new java.util.ArrayList<Object>();
		any3279.add(new Object());
		any3279.add(new Object());
		any3279.add(new Object());
		any3279.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3278, "any", any3279);
		extension3278.setVendor("");

		extension3277.add(extension3278);
		org.inspection_plusplus.Extension extension3280 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3281 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3280, "any", any3281);
		extension3280.setVendor("");

		extension3277.add(extension3280);
		org.inspection_plusplus.Extension extension3282 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3283 = new java.util.ArrayList<Object>();
		any3283.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3282, "any", any3283);
		extension3282.setVendor("");

		extension3277.add(extension3282);
		eu.sarunas.junit.TestsHelper.set(qCSingle3244, "extension", extension3277);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID3284 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(qCSingle3244, "displayID", displayID3284);
		java.util.ArrayList<org.inspection_plusplus.History> history3285 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history3286 = new org.inspection_plusplus.History();
		history3286.setAction("aaaaaaaaaa");
		history3286.setAuthor("");
		history3286.setComment("");
		history3286.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 26, 50)));

		history3285.add(history3286);
		eu.sarunas.junit.TestsHelper.set(qCSingle3244, "history", history3285);

		qc3167.add(qCSingle3244);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan3063, "qc", qc3167);
		inspectionPlan3063.setName("aa");
		org.inspection_plusplus.ID4Objects iD4Objects3287 = new org.inspection_plusplus.ID4Objects();
		iD4Objects3287.setUuid("a");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3288 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3289 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3290 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3289, "any", any3290);
		extension3289.setVendor("");

		extension3288.add(extension3289);
		org.inspection_plusplus.Extension extension3291 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3292 = new java.util.ArrayList<Object>();
		any3292.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3291, "any", any3292);
		extension3291.setVendor("aaaa");

		extension3288.add(extension3291);
		org.inspection_plusplus.Extension extension3293 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3294 = new java.util.ArrayList<Object>();
		any3294.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3293, "any", any3294);
		extension3293.setVendor("aaaa");

		extension3288.add(extension3293);
		eu.sarunas.junit.TestsHelper.set(iD4Objects3287, "extension", extension3288);

		inspectionPlan3063.setSystemID(iD4Objects3287);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef3295 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing3296 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3296.setClassID("aaaaa");
		iD4Referencing3296.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3297 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3298 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3299 = new java.util.ArrayList<Object>();
		any3299.add(new Object());
		any3299.add(new Object());
		any3299.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3298, "any", any3299);
		extension3298.setVendor("");

		extension3297.add(extension3298);
		org.inspection_plusplus.Extension extension3300 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3301 = new java.util.ArrayList<Object>();
		any3301.add(new Object());
		any3301.add(new Object());
		any3301.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3300, "any", any3301);
		extension3300.setVendor("");

		extension3297.add(extension3300);
		org.inspection_plusplus.Extension extension3302 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3303 = new java.util.ArrayList<Object>();
		any3303.add(new Object());
		any3303.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3302, "any", any3303);
		extension3302.setVendor("aaaaaaaaa");

		extension3297.add(extension3302);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3296, "extension", extension3297);

		commentRef3295.add(iD4Referencing3296);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan3063, "commentRef", commentRef3295);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3304 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3305 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3306 = new java.util.ArrayList<Object>();
		any3306.add(new Object());
		any3306.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3305, "any", any3306);
		extension3305.setVendor("a");

		extension3304.add(extension3305);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan3063, "extension", extension3304);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID3307 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan3063, "displayID", displayID3307);
		java.util.ArrayList<org.inspection_plusplus.History> history3308 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history3309 = new org.inspection_plusplus.History();
		history3309.setAction("aaaaaaaaaaaaaaaa");
		history3309.setAuthor("");
		history3309.setComment("");
		history3309.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 20, 50)));

		history3308.add(history3309);
		org.inspection_plusplus.History history3310 = new org.inspection_plusplus.History();
		history3310.setAction("");
		history3310.setAuthor("aaaaaa");
		history3310.setComment("");
		history3310.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 9, 41)));

		history3308.add(history3310);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan3063, "history", history3308);
		org.inspection_plusplus.Sender sender3311 = new org.inspection_plusplus.Sender();
		sender3311.setAppID("");
		sender3311.setDomain("aaa");
		org.inspection_plusplus.Extension extension3312 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3313 = new java.util.ArrayList<Object>();
		any3313.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3312, "any", any3313);
		extension3312.setVendor("aaaaaaa");

		org.inspection_plusplus.ReturnStatus res = testObject.storeInspectionPlan(inspectionPlan3063, "aaaaaaaaaaaaaaaaaa", "", sender3311, extension3312);
		junit.framework.Assert.fail();
	};

	@org.junit.Test
	public void testtestStoreInspectionPlan11() throws Throwable
	{
		org.inspection_plusplus.service.v2.WebServiceImpl testObject = new org.inspection_plusplus.service.v2.WebServiceImpl();
		org.inspection_plusplus.InspectionPlan inspectionPlan3314 = new org.inspection_plusplus.InspectionPlan();
		inspectionPlan3314.setInspectionCategories("aaaaaaa");
		inspectionPlan3314.setIPsymmetry("");
		inspectionPlan3314.setPartQAversion("");
		eu.sarunas.junit.TestsHelper.set(inspectionPlan3314, "pdMmaturity", "");
		inspectionPlan3314.setQAmaturity("aaaaa");
		inspectionPlan3314.setQAversion("");
		java.util.ArrayList<org.inspection_plusplus.Comment> comment3315 = new java.util.ArrayList<org.inspection_plusplus.Comment>();
		org.inspection_plusplus.Comment comment3316 = new org.inspection_plusplus.Comment();
		comment3316.setAuthor("");
		comment3316.setReceiver("aaa");
		comment3316.setSubject("");
		comment3316.setText("");
		comment3316.setName("aaaaaaaaaaaaaaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects3317 = new org.inspection_plusplus.ID4Objects();
		iD4Objects3317.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3318 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects3317, "extension", extension3318);

		comment3316.setSystemID(iD4Objects3317);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef3319 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing3320 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3320.setClassID("");
		iD4Referencing3320.setUuid("aaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3321 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3322 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3323 = new java.util.ArrayList<Object>();
		any3323.add(new Object());
		any3323.add(new Object());
		any3323.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3322, "any", any3323);
		extension3322.setVendor("aaaaaaaaaaaaa");

		extension3321.add(extension3322);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3320, "extension", extension3321);

		commentRef3319.add(iD4Referencing3320);
		org.inspection_plusplus.ID4Referencing iD4Referencing3324 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3324.setClassID("aaaaaaaaaaaaaaaaaa");
		iD4Referencing3324.setUuid("aaaaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3325 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3324, "extension", extension3325);

		commentRef3319.add(iD4Referencing3324);
		eu.sarunas.junit.TestsHelper.set(comment3316, "commentRef", commentRef3319);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3326 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3327 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3328 = new java.util.ArrayList<Object>();
		any3328.add(new Object());
		any3328.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3327, "any", any3328);
		extension3327.setVendor("");

		extension3326.add(extension3327);
		org.inspection_plusplus.Extension extension3329 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3330 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3329, "any", any3330);
		extension3329.setVendor("");

		extension3326.add(extension3329);
		eu.sarunas.junit.TestsHelper.set(comment3316, "extension", extension3326);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID3331 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(comment3316, "displayID", displayID3331);
		java.util.ArrayList<org.inspection_plusplus.History> history3332 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(comment3316, "history", history3332);

		comment3315.add(comment3316);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan3314, "comment", comment3315);
		java.util.ArrayList<org.inspection_plusplus.Tolerance> tolerance3333 = new java.util.ArrayList<org.inspection_plusplus.Tolerance>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan3314, "tolerance", tolerance3333);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> partRef3334 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan3314, "partRef", partRef3334);
		java.util.ArrayList<org.inspection_plusplus.IPE> ipe3335 = new java.util.ArrayList<org.inspection_plusplus.IPE>();
		org.inspection_plusplus.IPEPoint iPEPoint3336 = new org.inspection_plusplus.IPEPoint();
		org.inspection_plusplus.Vector3D vector3D3337 = new org.inspection_plusplus.Vector3D();
		vector3D3337.setX(-100.79);
		vector3D3337.setY(3.54);
		vector3D3337.setZ(70.28);

		iPEPoint3336.setMainAxis(vector3D3337);
		iPEPoint3336.setMaterialThickness(55.04);
		iPEPoint3336.setMoveByMaterialThickness(true);
		iPEPoint3336.setOrientation(null);
		iPEPoint3336.setOrigin(null);
		iPEPoint3336.setTouchDirection(null);
		iPEPoint3336.setAuxiliaryElement(true);
		iPEPoint3336.setCalculatedElement(true);
		iPEPoint3336.setUseLeft(false);
		iPEPoint3336.setUseRight(true);
		org.inspection_plusplus.ID4Referencing iD4Referencing3338 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3338.setClassID("aaaaaaa");
		iD4Referencing3338.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3339 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3340 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3341 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3340, "any", any3341);
		extension3340.setVendor("aaaaaaaaaaaaaa");

		extension3339.add(extension3340);
		org.inspection_plusplus.Extension extension3342 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3343 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3342, "any", any3343);
		extension3342.setVendor("a");

		extension3339.add(extension3342);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3338, "extension", extension3339);

		iPEPoint3336.setGeometricObjectRef(iD4Referencing3338);
		java.util.ArrayList<org.inspection_plusplus.CalculationNominalElement> calculationNominalElement3344 = new java.util.ArrayList<org.inspection_plusplus.CalculationNominalElement>();
		org.inspection_plusplus.CalculationNominalElement calculationNominalElement3345 = new org.inspection_plusplus.CalculationNominalElement();
		calculationNominalElement3345.setOperation("aaaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.OperationParameter> operationParameter3346 = new java.util.ArrayList<org.inspection_plusplus.OperationParameter>();
		org.inspection_plusplus.OperationParameter operationParameter3347 = new org.inspection_plusplus.OperationParameter();
		org.inspection_plusplus.KeyValueOperatorList keyValueOperatorList3348 = new org.inspection_plusplus.KeyValueOperatorList();
		java.util.ArrayList<org.inspection_plusplus.KeyValueOperator> keyValueOperator3349 = new java.util.ArrayList<org.inspection_plusplus.KeyValueOperator>();
		eu.sarunas.junit.TestsHelper.set(keyValueOperatorList3348, "keyValueOperator", keyValueOperator3349);

		operationParameter3347.setKeyValuePairs(keyValueOperatorList3348);

		operationParameter3346.add(operationParameter3347);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement3345, "operationParameter", operationParameter3346);
		java.util.ArrayList<org.inspection_plusplus.Operand> operand3350 = new java.util.ArrayList<org.inspection_plusplus.Operand>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement3345, "operand", operand3350);
		calculationNominalElement3345.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects3351 = new org.inspection_plusplus.ID4Objects();
		iD4Objects3351.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3352 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3353 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3354 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3353, "any", any3354);
		extension3353.setVendor("aaaaaaaaaaaaaaaaaaaaaaaaaaa");

		extension3352.add(extension3353);
		org.inspection_plusplus.Extension extension3355 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3356 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3355, "any", any3356);
		extension3355.setVendor("");

		extension3352.add(extension3355);
		org.inspection_plusplus.Extension extension3357 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3358 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3357, "any", any3358);
		extension3357.setVendor("aa");

		extension3352.add(extension3357);
		eu.sarunas.junit.TestsHelper.set(iD4Objects3351, "extension", extension3352);

		calculationNominalElement3345.setSystemID(iD4Objects3351);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef3359 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement3345, "commentRef", commentRef3359);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3360 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3361 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3362 = new java.util.ArrayList<Object>();
		any3362.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3361, "any", any3362);
		extension3361.setVendor("");

		extension3360.add(extension3361);
		org.inspection_plusplus.Extension extension3363 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3364 = new java.util.ArrayList<Object>();
		any3364.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3363, "any", any3364);
		extension3363.setVendor("");

		extension3360.add(extension3363);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement3345, "extension", extension3360);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID3365 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement3345, "displayID", displayID3365);
		java.util.ArrayList<org.inspection_plusplus.History> history3366 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement3345, "history", history3366);

		calculationNominalElement3344.add(calculationNominalElement3345);
		org.inspection_plusplus.CalculationNominalElement calculationNominalElement3367 = new org.inspection_plusplus.CalculationNominalElement();
		calculationNominalElement3367.setOperation("");
		java.util.ArrayList<org.inspection_plusplus.OperationParameter> operationParameter3368 = new java.util.ArrayList<org.inspection_plusplus.OperationParameter>();
		org.inspection_plusplus.OperationParameter operationParameter3369 = new org.inspection_plusplus.OperationParameter();
		org.inspection_plusplus.KeyValueOperatorList keyValueOperatorList3370 = new org.inspection_plusplus.KeyValueOperatorList();
		java.util.ArrayList<org.inspection_plusplus.KeyValueOperator> keyValueOperator3371 = new java.util.ArrayList<org.inspection_plusplus.KeyValueOperator>();
		org.inspection_plusplus.KeyValueOperator keyValueOperator3372 = new org.inspection_plusplus.KeyValueOperator();
		keyValueOperator3372.setKey("aaaaaaaaaaaaaaaaaa");
		keyValueOperator3372.setOperator("aaaaaaaaaaaaaa");
		keyValueOperator3372.setValue("aaaa");

		keyValueOperator3371.add(keyValueOperator3372);
		eu.sarunas.junit.TestsHelper.set(keyValueOperatorList3370, "keyValueOperator", keyValueOperator3371);

		operationParameter3369.setKeyValuePairs(keyValueOperatorList3370);

		operationParameter3368.add(operationParameter3369);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement3367, "operationParameter", operationParameter3368);
		java.util.ArrayList<org.inspection_plusplus.Operand> operand3373 = new java.util.ArrayList<org.inspection_plusplus.Operand>();
		org.inspection_plusplus.Operand operand3374 = new org.inspection_plusplus.Operand();
		operand3374.setIndex(new java.math.BigInteger("-239"));
		operand3374.setRole("a");
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> referenceSystemRef3375 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing3376 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3376.setClassID("");
		iD4Referencing3376.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3377 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3376, "extension", extension3377);

		referenceSystemRef3375.add(iD4Referencing3376);
		org.inspection_plusplus.ID4Referencing iD4Referencing3378 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3378.setClassID("");
		iD4Referencing3378.setUuid("aaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3379 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3378, "extension", extension3379);

		referenceSystemRef3375.add(iD4Referencing3378);
		org.inspection_plusplus.ID4Referencing iD4Referencing3380 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3380.setClassID("aa");
		iD4Referencing3380.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3381 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3382 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3383 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3382, "any", any3383);
		extension3382.setVendor("");

		extension3381.add(extension3382);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3380, "extension", extension3381);

		referenceSystemRef3375.add(iD4Referencing3380);
		org.inspection_plusplus.ID4Referencing iD4Referencing3384 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3384.setClassID("");
		iD4Referencing3384.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3385 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3386 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3387 = new java.util.ArrayList<Object>();
		any3387.add(new Object());
		any3387.add(new Object());
		any3387.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3386, "any", any3387);
		extension3386.setVendor("");

		extension3385.add(extension3386);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3384, "extension", extension3385);

		referenceSystemRef3375.add(iD4Referencing3384);
		eu.sarunas.junit.TestsHelper.set(operand3374, "referenceSystemRef", referenceSystemRef3375);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeGeometricRef3388 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing3389 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3389.setClassID("");
		iD4Referencing3389.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3390 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3391 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3392 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3391, "any", any3392);
		extension3391.setVendor("aaaaaa");

		extension3390.add(extension3391);
		org.inspection_plusplus.Extension extension3393 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3394 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3393, "any", any3394);
		extension3393.setVendor("");

		extension3390.add(extension3393);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3389, "extension", extension3390);

		ipeGeometricRef3388.add(iD4Referencing3389);
		eu.sarunas.junit.TestsHelper.set(operand3374, "ipeGeometricRef", ipeGeometricRef3388);

		operand3373.add(operand3374);
		org.inspection_plusplus.Operand operand3395 = new org.inspection_plusplus.Operand();
		operand3395.setIndex(new java.math.BigInteger("-198"));
		operand3395.setRole("");
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> referenceSystemRef3396 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(operand3395, "referenceSystemRef", referenceSystemRef3396);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeGeometricRef3397 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing3398 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3398.setClassID("aaa");
		iD4Referencing3398.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3399 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3400 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3401 = new java.util.ArrayList<Object>();
		any3401.add(new Object());
		any3401.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3400, "any", any3401);
		extension3400.setVendor("aaaaaaaa");

		extension3399.add(extension3400);
		org.inspection_plusplus.Extension extension3402 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3403 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3402, "any", any3403);
		extension3402.setVendor("");

		extension3399.add(extension3402);
		org.inspection_plusplus.Extension extension3404 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3405 = new java.util.ArrayList<Object>();
		any3405.add(new Object());
		any3405.add(new Object());
		any3405.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3404, "any", any3405);
		extension3404.setVendor("");

		extension3399.add(extension3404);
		org.inspection_plusplus.Extension extension3406 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3407 = new java.util.ArrayList<Object>();
		any3407.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3406, "any", any3407);
		extension3406.setVendor("a");

		extension3399.add(extension3406);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3398, "extension", extension3399);

		ipeGeometricRef3397.add(iD4Referencing3398);
		eu.sarunas.junit.TestsHelper.set(operand3395, "ipeGeometricRef", ipeGeometricRef3397);

		operand3373.add(operand3395);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement3367, "operand", operand3373);
		calculationNominalElement3367.setName("aaaaaaaaaaaaaaaaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects3408 = new org.inspection_plusplus.ID4Objects();
		iD4Objects3408.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3409 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects3408, "extension", extension3409);

		calculationNominalElement3367.setSystemID(iD4Objects3408);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef3410 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement3367, "commentRef", commentRef3410);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3411 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3412 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3413 = new java.util.ArrayList<Object>();
		any3413.add(new Object());
		any3413.add(new Object());
		any3413.add(new Object());
		any3413.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3412, "any", any3413);
		extension3412.setVendor("");

		extension3411.add(extension3412);
		org.inspection_plusplus.Extension extension3414 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3415 = new java.util.ArrayList<Object>();
		any3415.add(new Object());
		any3415.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3414, "any", any3415);
		extension3414.setVendor("");

		extension3411.add(extension3414);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement3367, "extension", extension3411);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID3416 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID3417 = new org.inspection_plusplus.DisplayID();
		displayID3417.setLanguage("aaaaa");
		displayID3417.setText("");

		displayID3416.add(displayID3417);
		org.inspection_plusplus.DisplayID displayID3418 = new org.inspection_plusplus.DisplayID();
		displayID3418.setLanguage("aaaaaaa");
		displayID3418.setText("");

		displayID3416.add(displayID3418);
		org.inspection_plusplus.DisplayID displayID3419 = new org.inspection_plusplus.DisplayID();
		displayID3419.setLanguage("aaaaaa");
		displayID3419.setText("");

		displayID3416.add(displayID3419);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement3367, "displayID", displayID3416);
		java.util.ArrayList<org.inspection_plusplus.History> history3420 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history3421 = new org.inspection_plusplus.History();
		history3421.setAction("");
		history3421.setAuthor("");
		history3421.setComment("");
		history3421.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 42, 41)));

		history3420.add(history3421);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement3367, "history", history3420);

		calculationNominalElement3344.add(calculationNominalElement3367);
		eu.sarunas.junit.TestsHelper.set(iPEPoint3336, "calculationNominalElement", calculationNominalElement3344);
		iPEPoint3336.setDescription("");
		iPEPoint3336.setFunction("aaaaaaaaa");
		iPEPoint3336.setName("aaaa");
		org.inspection_plusplus.ID4Objects iD4Objects3422 = new org.inspection_plusplus.ID4Objects();
		iD4Objects3422.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3423 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3424 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3425 = new java.util.ArrayList<Object>();
		any3425.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3424, "any", any3425);
		extension3424.setVendor("aaaaaaa");

		extension3423.add(extension3424);
		eu.sarunas.junit.TestsHelper.set(iD4Objects3422, "extension", extension3423);

		iPEPoint3336.setSystemID(iD4Objects3422);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef3426 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(iPEPoint3336, "commentRef", commentRef3426);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3427 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3428 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3429 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3428, "any", any3429);
		extension3428.setVendor("");

		extension3427.add(extension3428);
		org.inspection_plusplus.Extension extension3430 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3431 = new java.util.ArrayList<Object>();
		any3431.add(new Object());
		any3431.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3430, "any", any3431);
		extension3430.setVendor("aaaaaaaaaaaa");

		extension3427.add(extension3430);
		eu.sarunas.junit.TestsHelper.set(iPEPoint3336, "extension", extension3427);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID3432 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(iPEPoint3336, "displayID", displayID3432);
		java.util.ArrayList<org.inspection_plusplus.History> history3433 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history3434 = new org.inspection_plusplus.History();
		history3434.setAction("");
		history3434.setAuthor("");
		history3434.setComment("");
		history3434.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 9, 32)));

		history3433.add(history3434);
		eu.sarunas.junit.TestsHelper.set(iPEPoint3336, "history", history3433);

		ipe3335.add(iPEPoint3336);
		org.inspection_plusplus.IPEGeometric iPEGeometric3435 = new org.inspection_plusplus.IPEGeometric();
		iPEGeometric3435.setMaterialThickness(-51.34);
		iPEGeometric3435.setMoveByMaterialThickness(false);
		org.inspection_plusplus.Vector3D vector3D3436 = new org.inspection_plusplus.Vector3D();
		vector3D3436.setX(263.28);
		vector3D3436.setY(-300.86);
		vector3D3436.setZ(-170.57);

		iPEGeometric3435.setOrientation(vector3D3436);
		iPEGeometric3435.setOrigin(null);
		iPEGeometric3435.setTouchDirection(null);
		iPEGeometric3435.setAuxiliaryElement(false);
		iPEGeometric3435.setCalculatedElement(true);
		iPEGeometric3435.setUseLeft(false);
		iPEGeometric3435.setUseRight(true);
		org.inspection_plusplus.ID4Referencing iD4Referencing3437 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3437.setClassID("");
		iD4Referencing3437.setUuid("aaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3438 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3437, "extension", extension3438);

		iPEGeometric3435.setGeometricObjectRef(iD4Referencing3437);
		java.util.ArrayList<org.inspection_plusplus.CalculationNominalElement> calculationNominalElement3439 = new java.util.ArrayList<org.inspection_plusplus.CalculationNominalElement>();
		org.inspection_plusplus.CalculationNominalElement calculationNominalElement3440 = new org.inspection_plusplus.CalculationNominalElement();
		calculationNominalElement3440.setOperation("");
		java.util.ArrayList<org.inspection_plusplus.OperationParameter> operationParameter3441 = new java.util.ArrayList<org.inspection_plusplus.OperationParameter>();
		org.inspection_plusplus.OperationParameter operationParameter3442 = new org.inspection_plusplus.OperationParameter();
		org.inspection_plusplus.KeyValueOperatorList keyValueOperatorList3443 = new org.inspection_plusplus.KeyValueOperatorList();
		java.util.ArrayList<org.inspection_plusplus.KeyValueOperator> keyValueOperator3444 = new java.util.ArrayList<org.inspection_plusplus.KeyValueOperator>();
		eu.sarunas.junit.TestsHelper.set(keyValueOperatorList3443, "keyValueOperator", keyValueOperator3444);

		operationParameter3442.setKeyValuePairs(keyValueOperatorList3443);

		operationParameter3441.add(operationParameter3442);
		org.inspection_plusplus.OperationParameter operationParameter3445 = new org.inspection_plusplus.OperationParameter();
		org.inspection_plusplus.KeyValueOperatorList keyValueOperatorList3446 = new org.inspection_plusplus.KeyValueOperatorList();
		java.util.ArrayList<org.inspection_plusplus.KeyValueOperator> keyValueOperator3447 = new java.util.ArrayList<org.inspection_plusplus.KeyValueOperator>();
		org.inspection_plusplus.KeyValueOperator keyValueOperator3448 = new org.inspection_plusplus.KeyValueOperator();
		keyValueOperator3448.setKey("aaaaaaaaaaaaaaaaaa");
		keyValueOperator3448.setOperator("aaaaaaaaa");
		keyValueOperator3448.setValue("aa");

		keyValueOperator3447.add(keyValueOperator3448);
		org.inspection_plusplus.KeyValueOperator keyValueOperator3449 = new org.inspection_plusplus.KeyValueOperator();
		keyValueOperator3449.setKey("");
		keyValueOperator3449.setOperator("");
		keyValueOperator3449.setValue("");

		keyValueOperator3447.add(keyValueOperator3449);
		org.inspection_plusplus.KeyValueOperator keyValueOperator3450 = new org.inspection_plusplus.KeyValueOperator();
		keyValueOperator3450.setKey("");
		keyValueOperator3450.setOperator("aa");
		keyValueOperator3450.setValue("");

		keyValueOperator3447.add(keyValueOperator3450);
		eu.sarunas.junit.TestsHelper.set(keyValueOperatorList3446, "keyValueOperator", keyValueOperator3447);

		operationParameter3445.setKeyValuePairs(keyValueOperatorList3446);

		operationParameter3441.add(operationParameter3445);
		org.inspection_plusplus.OperationParameter operationParameter3451 = new org.inspection_plusplus.OperationParameter();
		org.inspection_plusplus.KeyValueOperatorList keyValueOperatorList3452 = new org.inspection_plusplus.KeyValueOperatorList();
		java.util.ArrayList<org.inspection_plusplus.KeyValueOperator> keyValueOperator3453 = new java.util.ArrayList<org.inspection_plusplus.KeyValueOperator>();
		org.inspection_plusplus.KeyValueOperator keyValueOperator3454 = new org.inspection_plusplus.KeyValueOperator();
		keyValueOperator3454.setKey("aaa");
		keyValueOperator3454.setOperator("aaaaaaaaaaaaaaaaaaaaaaa");
		keyValueOperator3454.setValue("");

		keyValueOperator3453.add(keyValueOperator3454);
		eu.sarunas.junit.TestsHelper.set(keyValueOperatorList3452, "keyValueOperator", keyValueOperator3453);

		operationParameter3451.setKeyValuePairs(keyValueOperatorList3452);

		operationParameter3441.add(operationParameter3451);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement3440, "operationParameter", operationParameter3441);
		java.util.ArrayList<org.inspection_plusplus.Operand> operand3455 = new java.util.ArrayList<org.inspection_plusplus.Operand>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement3440, "operand", operand3455);
		calculationNominalElement3440.setName("aaaaaaaa");
		org.inspection_plusplus.ID4Objects iD4Objects3456 = new org.inspection_plusplus.ID4Objects();
		iD4Objects3456.setUuid("aaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3457 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects3456, "extension", extension3457);

		calculationNominalElement3440.setSystemID(iD4Objects3456);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef3458 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement3440, "commentRef", commentRef3458);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3459 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3460 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3461 = new java.util.ArrayList<Object>();
		any3461.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3460, "any", any3461);
		extension3460.setVendor("");

		extension3459.add(extension3460);
		org.inspection_plusplus.Extension extension3462 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3463 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3462, "any", any3463);
		extension3462.setVendor("aaa");

		extension3459.add(extension3462);
		org.inspection_plusplus.Extension extension3464 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3465 = new java.util.ArrayList<Object>();
		any3465.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3464, "any", any3465);
		extension3464.setVendor("aaaaaaaaaaaaa");

		extension3459.add(extension3464);
		org.inspection_plusplus.Extension extension3466 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3467 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3466, "any", any3467);
		extension3466.setVendor("aaaaaaaaaaa");

		extension3459.add(extension3466);
		org.inspection_plusplus.Extension extension3468 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3469 = new java.util.ArrayList<Object>();
		any3469.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3468, "any", any3469);
		extension3468.setVendor("");

		extension3459.add(extension3468);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement3440, "extension", extension3459);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID3470 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement3440, "displayID", displayID3470);
		java.util.ArrayList<org.inspection_plusplus.History> history3471 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history3472 = new org.inspection_plusplus.History();
		history3472.setAction("");
		history3472.setAuthor("");
		history3472.setComment("");
		history3472.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 59, 43)));

		history3471.add(history3472);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement3440, "history", history3471);

		calculationNominalElement3439.add(calculationNominalElement3440);
		eu.sarunas.junit.TestsHelper.set(iPEGeometric3435, "calculationNominalElement", calculationNominalElement3439);
		iPEGeometric3435.setDescription("");
		iPEGeometric3435.setFunction("aaaaaaaaaaaaaaa");
		iPEGeometric3435.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects3473 = new org.inspection_plusplus.ID4Objects();
		iD4Objects3473.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3474 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3475 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3476 = new java.util.ArrayList<Object>();
		any3476.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3475, "any", any3476);
		extension3475.setVendor("");

		extension3474.add(extension3475);
		eu.sarunas.junit.TestsHelper.set(iD4Objects3473, "extension", extension3474);

		iPEGeometric3435.setSystemID(iD4Objects3473);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef3477 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing3478 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3478.setClassID("aaaaaaaaa");
		iD4Referencing3478.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3479 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3480 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3481 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3480, "any", any3481);
		extension3480.setVendor("");

		extension3479.add(extension3480);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3478, "extension", extension3479);

		commentRef3477.add(iD4Referencing3478);
		org.inspection_plusplus.ID4Referencing iD4Referencing3482 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3482.setClassID("aa");
		iD4Referencing3482.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3483 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3482, "extension", extension3483);

		commentRef3477.add(iD4Referencing3482);
		eu.sarunas.junit.TestsHelper.set(iPEGeometric3435, "commentRef", commentRef3477);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3484 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iPEGeometric3435, "extension", extension3484);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID3485 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID3486 = new org.inspection_plusplus.DisplayID();
		displayID3486.setLanguage("");
		displayID3486.setText("aaaaaaaaaa");

		displayID3485.add(displayID3486);
		eu.sarunas.junit.TestsHelper.set(iPEGeometric3435, "displayID", displayID3485);
		java.util.ArrayList<org.inspection_plusplus.History> history3487 = new java.util.ArrayList<org.inspection_plusplus.History>();
		eu.sarunas.junit.TestsHelper.set(iPEGeometric3435, "history", history3487);

		ipe3335.add(iPEGeometric3435);
		org.inspection_plusplus.IPEGeometric iPEGeometric3488 = new org.inspection_plusplus.IPEGeometric();
		iPEGeometric3488.setMaterialThickness(-121.74);
		iPEGeometric3488.setMoveByMaterialThickness(false);
		org.inspection_plusplus.Vector3D vector3D3489 = new org.inspection_plusplus.Vector3D();
		vector3D3489.setX(-262.07);
		vector3D3489.setY(82.24);
		vector3D3489.setZ(-257.88);

		iPEGeometric3488.setOrientation(vector3D3489);
		iPEGeometric3488.setOrigin(null);
		iPEGeometric3488.setTouchDirection(null);
		iPEGeometric3488.setAuxiliaryElement(false);
		iPEGeometric3488.setCalculatedElement(false);
		iPEGeometric3488.setUseLeft(false);
		iPEGeometric3488.setUseRight(true);
		org.inspection_plusplus.ID4Referencing iD4Referencing3490 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3490.setClassID("aa");
		iD4Referencing3490.setUuid("aa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3491 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3490, "extension", extension3491);

		iPEGeometric3488.setGeometricObjectRef(iD4Referencing3490);
		java.util.ArrayList<org.inspection_plusplus.CalculationNominalElement> calculationNominalElement3492 = new java.util.ArrayList<org.inspection_plusplus.CalculationNominalElement>();
		eu.sarunas.junit.TestsHelper.set(iPEGeometric3488, "calculationNominalElement", calculationNominalElement3492);
		iPEGeometric3488.setDescription("aaaaaaaaaa");
		iPEGeometric3488.setFunction("");
		iPEGeometric3488.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects3493 = new org.inspection_plusplus.ID4Objects();
		iD4Objects3493.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3494 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3495 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3496 = new java.util.ArrayList<Object>();
		any3496.add(new Object());
		any3496.add(new Object());
		any3496.add(new Object());
		any3496.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3495, "any", any3496);
		extension3495.setVendor("aaaaaaaaaaaaaaaa");

		extension3494.add(extension3495);
		eu.sarunas.junit.TestsHelper.set(iD4Objects3493, "extension", extension3494);

		iPEGeometric3488.setSystemID(iD4Objects3493);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef3497 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(iPEGeometric3488, "commentRef", commentRef3497);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3498 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3499 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3500 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3499, "any", any3500);
		extension3499.setVendor("");

		extension3498.add(extension3499);
		org.inspection_plusplus.Extension extension3501 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3502 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3501, "any", any3502);
		extension3501.setVendor("");

		extension3498.add(extension3501);
		org.inspection_plusplus.Extension extension3503 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3504 = new java.util.ArrayList<Object>();
		any3504.add(new Object());
		any3504.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3503, "any", any3504);
		extension3503.setVendor("");

		extension3498.add(extension3503);
		eu.sarunas.junit.TestsHelper.set(iPEGeometric3488, "extension", extension3498);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID3505 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID3506 = new org.inspection_plusplus.DisplayID();
		displayID3506.setLanguage("");
		displayID3506.setText("aaaaaaaaaaaaaa");

		displayID3505.add(displayID3506);
		org.inspection_plusplus.DisplayID displayID3507 = new org.inspection_plusplus.DisplayID();
		displayID3507.setLanguage("aaaaaa");
		displayID3507.setText("aaaaaaaaaaaaaaaaaaaaaaaaaaa");

		displayID3505.add(displayID3507);
		eu.sarunas.junit.TestsHelper.set(iPEGeometric3488, "displayID", displayID3505);
		java.util.ArrayList<org.inspection_plusplus.History> history3508 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history3509 = new org.inspection_plusplus.History();
		history3509.setAction("aaaaaaaaaaaaaaaaaaaa");
		history3509.setAuthor("");
		history3509.setComment("");
		history3509.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 36, 36)));

		history3508.add(history3509);
		eu.sarunas.junit.TestsHelper.set(iPEGeometric3488, "history", history3508);

		ipe3335.add(iPEGeometric3488);
		org.inspection_plusplus.IPEGeometric iPEGeometric3510 = new org.inspection_plusplus.IPEGeometric();
		iPEGeometric3510.setMaterialThickness(-358.77);
		iPEGeometric3510.setMoveByMaterialThickness(false);
		org.inspection_plusplus.Vector3D vector3D3511 = new org.inspection_plusplus.Vector3D();
		vector3D3511.setX(-197.85);
		vector3D3511.setY(-157.07);
		vector3D3511.setZ(-236.07);

		iPEGeometric3510.setOrientation(vector3D3511);
		iPEGeometric3510.setOrigin(null);
		iPEGeometric3510.setTouchDirection(null);
		iPEGeometric3510.setAuxiliaryElement(false);
		iPEGeometric3510.setCalculatedElement(false);
		iPEGeometric3510.setUseLeft(true);
		iPEGeometric3510.setUseRight(true);
		org.inspection_plusplus.ID4Referencing iD4Referencing3512 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3512.setClassID("");
		iD4Referencing3512.setUuid("aaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3513 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3512, "extension", extension3513);

		iPEGeometric3510.setGeometricObjectRef(iD4Referencing3512);
		java.util.ArrayList<org.inspection_plusplus.CalculationNominalElement> calculationNominalElement3514 = new java.util.ArrayList<org.inspection_plusplus.CalculationNominalElement>();
		org.inspection_plusplus.CalculationNominalElement calculationNominalElement3515 = new org.inspection_plusplus.CalculationNominalElement();
		calculationNominalElement3515.setOperation("aaa");
		java.util.ArrayList<org.inspection_plusplus.OperationParameter> operationParameter3516 = new java.util.ArrayList<org.inspection_plusplus.OperationParameter>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement3515, "operationParameter", operationParameter3516);
		java.util.ArrayList<org.inspection_plusplus.Operand> operand3517 = new java.util.ArrayList<org.inspection_plusplus.Operand>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement3515, "operand", operand3517);
		calculationNominalElement3515.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects3518 = new org.inspection_plusplus.ID4Objects();
		iD4Objects3518.setUuid("a");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3519 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects3518, "extension", extension3519);

		calculationNominalElement3515.setSystemID(iD4Objects3518);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef3520 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement3515, "commentRef", commentRef3520);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3521 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement3515, "extension", extension3521);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID3522 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement3515, "displayID", displayID3522);
		java.util.ArrayList<org.inspection_plusplus.History> history3523 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history3524 = new org.inspection_plusplus.History();
		history3524.setAction("");
		history3524.setAuthor("aaaaa");
		history3524.setComment("aaa");
		history3524.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 7, 39)));

		history3523.add(history3524);
		org.inspection_plusplus.History history3525 = new org.inspection_plusplus.History();
		history3525.setAction("");
		history3525.setAuthor("");
		history3525.setComment("aaaaaa");
		history3525.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 19, 43)));

		history3523.add(history3525);
		org.inspection_plusplus.History history3526 = new org.inspection_plusplus.History();
		history3526.setAction("");
		history3526.setAuthor("");
		history3526.setComment("aaaaaa");
		history3526.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 58, 59)));

		history3523.add(history3526);
		eu.sarunas.junit.TestsHelper.set(calculationNominalElement3515, "history", history3523);

		calculationNominalElement3514.add(calculationNominalElement3515);
		eu.sarunas.junit.TestsHelper.set(iPEGeometric3510, "calculationNominalElement", calculationNominalElement3514);
		iPEGeometric3510.setDescription("");
		iPEGeometric3510.setFunction("");
		iPEGeometric3510.setName("");
		org.inspection_plusplus.ID4Objects iD4Objects3527 = new org.inspection_plusplus.ID4Objects();
		iD4Objects3527.setUuid("aaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3528 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3529 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3530 = new java.util.ArrayList<Object>();
		any3530.add(new Object());
		any3530.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3529, "any", any3530);
		extension3529.setVendor("aa");

		extension3528.add(extension3529);
		org.inspection_plusplus.Extension extension3531 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3532 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3531, "any", any3532);
		extension3531.setVendor("");

		extension3528.add(extension3531);
		org.inspection_plusplus.Extension extension3533 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3534 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3533, "any", any3534);
		extension3533.setVendor("");

		extension3528.add(extension3533);
		eu.sarunas.junit.TestsHelper.set(iD4Objects3527, "extension", extension3528);

		iPEGeometric3510.setSystemID(iD4Objects3527);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef3535 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing3536 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3536.setClassID("a");
		iD4Referencing3536.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3537 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3536, "extension", extension3537);

		commentRef3535.add(iD4Referencing3536);
		eu.sarunas.junit.TestsHelper.set(iPEGeometric3510, "commentRef", commentRef3535);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3538 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3539 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3540 = new java.util.ArrayList<Object>();
		any3540.add(new Object());
		any3540.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3539, "any", any3540);
		extension3539.setVendor("aa");

		extension3538.add(extension3539);
		org.inspection_plusplus.Extension extension3541 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3542 = new java.util.ArrayList<Object>();
		any3542.add(new Object());
		any3542.add(new Object());
		any3542.add(new Object());
		any3542.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3541, "any", any3542);
		extension3541.setVendor("");

		extension3538.add(extension3541);
		eu.sarunas.junit.TestsHelper.set(iPEGeometric3510, "extension", extension3538);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID3543 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID3544 = new org.inspection_plusplus.DisplayID();
		displayID3544.setLanguage("");
		displayID3544.setText("");

		displayID3543.add(displayID3544);
		org.inspection_plusplus.DisplayID displayID3545 = new org.inspection_plusplus.DisplayID();
		displayID3545.setLanguage("aaaaaaaaaa");
		displayID3545.setText("aaa");

		displayID3543.add(displayID3545);
		org.inspection_plusplus.DisplayID displayID3546 = new org.inspection_plusplus.DisplayID();
		displayID3546.setLanguage("");
		displayID3546.setText("aaa");

		displayID3543.add(displayID3546);
		eu.sarunas.junit.TestsHelper.set(iPEGeometric3510, "displayID", displayID3543);
		java.util.ArrayList<org.inspection_plusplus.History> history3547 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history3548 = new org.inspection_plusplus.History();
		history3548.setAction("aaaaaaaa");
		history3548.setAuthor("");
		history3548.setComment("aa");
		history3548.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 51, 57)));

		history3547.add(history3548);
		eu.sarunas.junit.TestsHelper.set(iPEGeometric3510, "history", history3547);

		ipe3335.add(iPEGeometric3510);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan3314, "ipe", ipe3335);
		java.util.ArrayList<org.inspection_plusplus.ReferenceSystem> referenceSystem3549 = new java.util.ArrayList<org.inspection_plusplus.ReferenceSystem>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan3314, "referenceSystem", referenceSystem3549);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> inspectionTaskRef3550 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(inspectionPlan3314, "inspectionTaskRef", inspectionTaskRef3550);
		java.util.ArrayList<org.inspection_plusplus.QC> qc3551 = new java.util.ArrayList<org.inspection_plusplus.QC>();
		org.inspection_plusplus.QCSingle qCSingle3552 = new org.inspection_plusplus.QCSingle();
		qCSingle3552.setGeoObjectDetail("");
		org.inspection_plusplus.ID4Referencing iD4Referencing3553 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3553.setClassID("");
		iD4Referencing3553.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3554 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3555 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3556 = new java.util.ArrayList<Object>();
		any3556.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3555, "any", any3556);
		extension3555.setVendor("aaaaaaaaaaaaaaaaaaaaa");

		extension3554.add(extension3555);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3553, "extension", extension3554);

		qCSingle3552.setToleranceRef(iD4Referencing3553);
		qCSingle3552.setAllAssembliesReferenced(false);
		qCSingle3552.setDescription("");
		qCSingle3552.setFunction("aaaaaaa");
		qCSingle3552.setPartQAversion("aaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> partRef3557 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing3558 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3558.setClassID("aaaaaaaa");
		iD4Referencing3558.setUuid("aaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3559 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3560 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3561 = new java.util.ArrayList<Object>();
		any3561.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3560, "any", any3561);
		extension3560.setVendor("");

		extension3559.add(extension3560);
		org.inspection_plusplus.Extension extension3562 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3563 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3562, "any", any3563);
		extension3562.setVendor("");

		extension3559.add(extension3562);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3558, "extension", extension3559);

		partRef3557.add(iD4Referencing3558);
		org.inspection_plusplus.ID4Referencing iD4Referencing3564 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3564.setClassID("aaaaaa");
		iD4Referencing3564.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3565 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3566 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3567 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3566, "any", any3567);
		extension3566.setVendor("");

		extension3565.add(extension3566);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3564, "extension", extension3565);

		partRef3557.add(iD4Referencing3564);
		eu.sarunas.junit.TestsHelper.set(qCSingle3552, "partRef", partRef3557);
		java.util.ArrayList<Object> calculationActualElementDetectionRef3568 = new java.util.ArrayList<Object>();
		calculationActualElementDetectionRef3568.add(new Object());
		calculationActualElementDetectionRef3568.add(new Object());
		calculationActualElementDetectionRef3568.add(new Object());
		eu.sarunas.junit.TestsHelper.set(qCSingle3552, "calculationActualElementDetectionRef", calculationActualElementDetectionRef3568);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> ipeRef3569 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing3570 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3570.setClassID("");
		iD4Referencing3570.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3571 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3570, "extension", extension3571);

		ipeRef3569.add(iD4Referencing3570);
		eu.sarunas.junit.TestsHelper.set(qCSingle3552, "ipeRef", ipeRef3569);
		qCSingle3552.setName("a");
		org.inspection_plusplus.ID4Objects iD4Objects3572 = new org.inspection_plusplus.ID4Objects();
		iD4Objects3572.setUuid("");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3573 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Objects3572, "extension", extension3573);

		qCSingle3552.setSystemID(iD4Objects3572);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef3574 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		eu.sarunas.junit.TestsHelper.set(qCSingle3552, "commentRef", commentRef3574);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3575 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(qCSingle3552, "extension", extension3575);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID3576 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID3577 = new org.inspection_plusplus.DisplayID();
		displayID3577.setLanguage("");
		displayID3577.setText("");

		displayID3576.add(displayID3577);
		eu.sarunas.junit.TestsHelper.set(qCSingle3552, "displayID", displayID3576);
		java.util.ArrayList<org.inspection_plusplus.History> history3578 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history3579 = new org.inspection_plusplus.History();
		history3579.setAction("aaaaaaaaaaaa");
		history3579.setAuthor("");
		history3579.setComment("aa");
		history3579.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 15, 34)));

		history3578.add(history3579);
		org.inspection_plusplus.History history3580 = new org.inspection_plusplus.History();
		history3580.setAction("aaaaaaaaaaaaa");
		history3580.setAuthor("");
		history3580.setComment("");
		history3580.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 1, 34, 54)));

		history3578.add(history3580);
		eu.sarunas.junit.TestsHelper.set(qCSingle3552, "history", history3578);

		qc3551.add(qCSingle3552);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan3314, "qc", qc3551);
		inspectionPlan3314.setName("aaaa");
		org.inspection_plusplus.ID4Objects iD4Objects3581 = new org.inspection_plusplus.ID4Objects();
		iD4Objects3581.setUuid("aaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3582 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3583 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3584 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3583, "any", any3584);
		extension3583.setVendor("");

		extension3582.add(extension3583);
		eu.sarunas.junit.TestsHelper.set(iD4Objects3581, "extension", extension3582);

		inspectionPlan3314.setSystemID(iD4Objects3581);
		java.util.ArrayList<org.inspection_plusplus.ID4Referencing> commentRef3585 = new java.util.ArrayList<org.inspection_plusplus.ID4Referencing>();
		org.inspection_plusplus.ID4Referencing iD4Referencing3586 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3586.setClassID("aaaaaaaaaaaaaaaa");
		iD4Referencing3586.setUuid("aaaaaaaaaaaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3587 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3588 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3589 = new java.util.ArrayList<Object>();
		any3589.add(new Object());
		any3589.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3588, "any", any3589);
		extension3588.setVendor("aaaaaaaaa");

		extension3587.add(extension3588);
		org.inspection_plusplus.Extension extension3590 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3591 = new java.util.ArrayList<Object>();
		any3591.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3590, "any", any3591);
		extension3590.setVendor("");

		extension3587.add(extension3590);
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3586, "extension", extension3587);

		commentRef3585.add(iD4Referencing3586);
		org.inspection_plusplus.ID4Referencing iD4Referencing3592 = new org.inspection_plusplus.ID4Referencing();
		iD4Referencing3592.setClassID("");
		iD4Referencing3592.setUuid("aaaaaa");
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3593 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		eu.sarunas.junit.TestsHelper.set(iD4Referencing3592, "extension", extension3593);

		commentRef3585.add(iD4Referencing3592);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan3314, "commentRef", commentRef3585);
		java.util.ArrayList<org.inspection_plusplus.Extension> extension3594 = new java.util.ArrayList<org.inspection_plusplus.Extension>();
		org.inspection_plusplus.Extension extension3595 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3596 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3595, "any", any3596);
		extension3595.setVendor("a");

		extension3594.add(extension3595);
		org.inspection_plusplus.Extension extension3597 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3598 = new java.util.ArrayList<Object>();
		eu.sarunas.junit.TestsHelper.set(extension3597, "any", any3598);
		extension3597.setVendor("");

		extension3594.add(extension3597);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan3314, "extension", extension3594);
		java.util.ArrayList<org.inspection_plusplus.DisplayID> displayID3599 = new java.util.ArrayList<org.inspection_plusplus.DisplayID>();
		org.inspection_plusplus.DisplayID displayID3600 = new org.inspection_plusplus.DisplayID();
		displayID3600.setLanguage("aaaaaaaaaaa");
		displayID3600.setText("aaaaaaaaaaaa");

		displayID3599.add(displayID3600);
		org.inspection_plusplus.DisplayID displayID3601 = new org.inspection_plusplus.DisplayID();
		displayID3601.setLanguage("aaaaaa");
		displayID3601.setText("aaaaaaaaaaa");

		displayID3599.add(displayID3601);
		org.inspection_plusplus.DisplayID displayID3602 = new org.inspection_plusplus.DisplayID();
		displayID3602.setLanguage("aa");
		displayID3602.setText("");

		displayID3599.add(displayID3602);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan3314, "displayID", displayID3599);
		java.util.ArrayList<org.inspection_plusplus.History> history3603 = new java.util.ArrayList<org.inspection_plusplus.History>();
		org.inspection_plusplus.History history3604 = new org.inspection_plusplus.History();
		history3604.setAction("");
		history3604.setAuthor("aaaaaa");
		history3604.setComment("aaaaaaaaaaaaaaaa");
		history3604.setDateTime(javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(new java.util.GregorianCalendar(1970, 1, 1, 2, 3, 43)));

		history3603.add(history3604);
		eu.sarunas.junit.TestsHelper.set(inspectionPlan3314, "history", history3603);
		org.inspection_plusplus.Sender sender3605 = new org.inspection_plusplus.Sender();
		sender3605.setAppID("aaaaaaaaaaaaaaaaaa");
		sender3605.setDomain("");
		org.inspection_plusplus.Extension extension3606 = new org.inspection_plusplus.Extension();
		java.util.ArrayList<Object> any3607 = new java.util.ArrayList<Object>();
		any3607.add(new Object());
		any3607.add(new Object());
		eu.sarunas.junit.TestsHelper.set(extension3606, "any", any3607);
		extension3606.setVendor("");

		org.inspection_plusplus.ReturnStatus res = testObject.storeInspectionPlan(inspectionPlan3314, "", "", sender3605, extension3606);
		junit.framework.Assert.fail();
	};

};
