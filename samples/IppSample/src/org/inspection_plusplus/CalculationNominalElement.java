
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Beschreibt, wie ein IPE aus der Konstruktionsgeometrie abgeleitet wird.
 * Das Ergebnis der Operation ist ein IPE_Single (siehe dort). Die Komposition zwischen IPE_Single und CalculationNominalElement besagt, dass die betreffende IPE_Single-Instanz aus anderen IPEs hergeleitet ist. Das Attribut IPE_Single.CalculatedElement ist hier auf �True� zu setzen. Gleichzeitig kann das IPE_Single �ber die geerbte Aggregation zwischen IPE und QC mit 1..n QCs verbunden sein.
 * 
 * Das geerbte Attribut Operation wird folgenderma�en definiert:
 * Standardisierte Operationen:
 * 
 * 	- IPP_DistanceBetween 	IPE_Distance zwischen den Origins zweier Input-IPE
 * 	- IPP_AngleBetween 	IPE_Angle zwischen zwei Input-IPE vom Typ IPE_Line; die Operation besitzt den Parameter �AngleMode� mit den beiden fixen Werten �interior� und �exterior�, die festlegen, ob der von den gerichteten Linien eingeschlossene Winkel oder dessen Erg�nzung zu 180 Grad gemeint ist.
 * 	- IPP_Symmetry		SymmetricPoint zwischen den Origins zweier Input-IPE
 * 	- IPP_LiesOn			alle Input-Elemente liegen auf dem Output-Element (z. B. Surface, Curve, SectionPlane)
 * 	- IPP_IsEdgeCurveOf	eine Kurve als Input-Element repr�sentiert die Kante eines Lochs als Output-Element
 * 	- IPP_Projection		das Output-Element ist das Ergebnis einer Projektion des Input-Elements mit Rolle IPP_IPE_From auf das Input-Element mit Rolle IPP_IPE_To
 * 	- IPP_Beam			das Output-Element (IPE_Surface_Point) liegt auf dem Schnitt eines Leitstrahls mit der Geometrie. Die Richtung des Leitstrahls wird �ber die OperationParameter IPP_BeamAxisX, IPP_BeamAxisY und IPP_BeamAxisZ beschrieben.
 * 	- IPP_Intersection		das Output-Element ist das Ergebnis eines Schnitts von zwei oder drei Input-Elementen. 
 * Folgende Schnittbildungen werden unterst�tzt � die Anwendung muss �ber die geeignete Vorgehensweise im Fehlerfall entscheiden:
 * 	- IPE_Line (3D) + IPE_Line (3D) -> IPE_Point.
 * 	- IPE_Line + IPE_Plane -> IPE_Point
 * 	- IPE_Line + IPE_Face -> IPE_Point
 * 	- IPE_Plane + IPE_Plane -> IPE_Line
 * 	- IPE_Plane + IPE_Face -> IPE_Curve
 * 	- IPE_LimitedPlane + IPE_Face -> IPE_Curve
 * 	- IPE_Plane + IPE_Plane + IPE_Plane -> IPE_Point
 * 
 * 
 * 
 * Der Typ der verwendeten Operanden ergibt sich aus den verwendeten IPE-Instanzen bzw. durch die in Operand.Role angegebenen Casting-Operationen. 
 * 
 * 	- IPP_Transformation	das Output-Element entsteht aus dem Input-Element �ber eine Transformation mit Werten in OperationParameter
 * 	- IPP_Mirror			das Output-Element entsteht durch Spiegelung des Input-Elements an der y-Achse.
 * 	- IPP_Construct_IPE_Line: Aus 2 IPE-Instanzen, die als Punkt interpretiert werden, wird ein IPE_Line erzeugt. Die Reihenfolge der Input-Elemente bestimmt die Ausrichtung der IPE_Line. Die normierte MainAxis der IPE_Line zeigt vom ersten zum zweiten Punkt. Die Reihenfolge der Punkte wird aus Operand.Index ermittelt. 
 * 
 * IPP_Construct_IPE_Plane: Aus 3 IPE-Instanzen, die als Punkte interpretiert werden, wird ein IPE_Plane erzeugt. Die Reihenfolge der Input-Elemente bestimmt die Ausrichtung der IPE_Plane. Die MainAxis der IPE_Plane wird �ber das Kreuzprodukt der Verbindungslinie von Punkt1 zu Punkt2 und Punkt1 zu Punkt3 definiert und anschlie�end normiert. Die Reihenfolge der Punkte wird aus Operand.Index ermittelt.
 * 
 * <p>Java class for Calculation_NominalElement complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Calculation_NominalElement">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Calculation">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Calculation_NominalElement")
public class CalculationNominalElement
    extends Calculation
{


}
