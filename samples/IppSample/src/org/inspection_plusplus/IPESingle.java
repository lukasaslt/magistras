
package org.inspection_plusplus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Abstrakte Klasse. IPE_Singles sind die Hauptbestandteile von Pr�fmerkmalspl�nen. Fr�her unscharf als Mess-Features oder Soll-Features bezeichnet. 
 * 
 * <p>Java class for IPE_Single complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IPE_Single">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IPE">
 *       &lt;sequence>
 *         &lt;element name="AuxiliaryElement" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="CalculatedElement" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Use_Left" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Use_Right" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element ref="{http://www.inspection-plusplus.org}GeometricObject_ref" minOccurs="0"/>
 *         &lt;element name="Calculation_NominalElement" type="{http://www.inspection-plusplus.org}Calculation_NominalElement" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IPE_Single", propOrder = {
    "auxiliaryElement",
    "calculatedElement",
    "useLeft",
    "useRight",
    "geometricObjectRef",
    "calculationNominalElement"
})
@XmlSeeAlso({
    IPEScalar.class,
    IPEGeometric.class
})
public abstract class IPESingle
    extends IPE
{

    @XmlElement(name = "AuxiliaryElement")
    protected boolean auxiliaryElement;
    @XmlElement(name = "CalculatedElement")
    protected boolean calculatedElement;
    @XmlElement(name = "Use_Left")
    protected boolean useLeft;
    @XmlElement(name = "Use_Right")
    protected boolean useRight;
    @XmlElement(name = "GeometricObject_ref", namespace = "http://www.inspection-plusplus.org")
    protected ID4Referencing geometricObjectRef;
    @XmlElement(name = "Calculation_NominalElement")
    protected List<CalculationNominalElement> calculationNominalElement;

    /**
     * Gets the value of the auxiliaryElement property.
     * 
     */
    public boolean isAuxiliaryElement() {
        return auxiliaryElement;
    }

    /**
     * Sets the value of the auxiliaryElement property.
     * 
     */
    public void setAuxiliaryElement(boolean value) {
        this.auxiliaryElement = value;
    }

    /**
     * Gets the value of the calculatedElement property.
     * 
     */
    public boolean isCalculatedElement() {
        return calculatedElement;
    }

    /**
     * Sets the value of the calculatedElement property.
     * 
     */
    public void setCalculatedElement(boolean value) {
        this.calculatedElement = value;
    }

    /**
     * Gets the value of the useLeft property.
     * 
     */
    public boolean isUseLeft() {
        return useLeft;
    }

    /**
     * Sets the value of the useLeft property.
     * 
     */
    public void setUseLeft(boolean value) {
        this.useLeft = value;
    }

    /**
     * Gets the value of the useRight property.
     * 
     */
    public boolean isUseRight() {
        return useRight;
    }

    /**
     * Sets the value of the useRight property.
     * 
     */
    public void setUseRight(boolean value) {
        this.useRight = value;
    }

    /**
     * Gets the value of the geometricObjectRef property.
     * 
     * @return
     *     possible object is
     *     {@link ID4Referencing }
     *     
     */
    public ID4Referencing getGeometricObjectRef() {
        return geometricObjectRef;
    }

    /**
     * Sets the value of the geometricObjectRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link ID4Referencing }
     *     
     */
    public void setGeometricObjectRef(ID4Referencing value) {
        this.geometricObjectRef = value;
    }

    /**
     * Gets the value of the calculationNominalElement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the calculationNominalElement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCalculationNominalElement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CalculationNominalElement }
     * 
     * 
     */
    public List<CalculationNominalElement> getCalculationNominalElement() {
        if (calculationNominalElement == null) {
            calculationNominalElement = new ArrayList<CalculationNominalElement>();
        }
        return this.calculationNominalElement;
    }

}
