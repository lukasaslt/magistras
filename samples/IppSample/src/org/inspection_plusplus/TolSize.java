
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Abstrakte Klasse. Gr��entoleranzen. Ihre Subklassen beschreiben Gr��enabweichungen verschiedener Art.
 * 
 * <p>Java class for Tol_Size complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Tol_Size">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Tolerance">
 *       &lt;sequence>
 *         &lt;element name="Criterion" type="{http://www.inspection-plusplus.org}IntervalToleranceCriterion"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tol_Size", propOrder = {
    "criterion"
})
@XmlSeeAlso({
    TolLinearSize.class,
})
public abstract class TolSize
    extends Tolerance
{

    @XmlElement(name = "Criterion", required = true)
    protected IntervalToleranceCriterion criterion;

    /**
     * Gets the value of the criterion property.
     * 
     * @return
     *     possible object is
     *     {@link IntervalToleranceCriterion }
     *     
     */
    public IntervalToleranceCriterion getCriterion() {
        return criterion;
    }

    /**
     * Sets the value of the criterion property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntervalToleranceCriterion }
     *     
     */
    public void setCriterion(IntervalToleranceCriterion value) {
        this.criterion = value;
    }

}
