
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Datentyp zur eindeutigen Identifikation von einzelnen �ber I++DMS �bermittelten Instanzen. Jede IppDMSentity tr�gt dazu das Attribut SystemID dieses Typs.
 * 
 * <p>Java class for ID4Objects complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ID4Objects">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}SuperID">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ID4Objects")
public class ID4Objects
    extends SuperID
{


}
