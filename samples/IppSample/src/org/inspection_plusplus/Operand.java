
package org.inspection_plusplus;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Operanden-Objekt eine Calculation
 * 
 * <p>Java class for Operand complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Operand">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Index" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="Role" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element ref="{http://www.inspection-plusplus.org}ReferenceSystem_ref" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://www.inspection-plusplus.org}IPE_Geometric_ref" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Operand", propOrder = {
    "index",
    "role",
    "referenceSystemRef",
    "ipeGeometricRef"
})
public class Operand {

    @XmlElement(name = "Index", required = true)
    protected BigInteger index;
    @XmlElement(name = "Role", required = true)
    protected String role;
    @XmlElement(name = "ReferenceSystem_ref", namespace = "http://www.inspection-plusplus.org")
    protected List<ID4Referencing> referenceSystemRef;
    @XmlElement(name = "IPE_Geometric_ref", namespace = "http://www.inspection-plusplus.org", required = true)
    protected List<ID4Referencing> ipeGeometricRef;

    /**
     * Gets the value of the index property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getIndex() {
        return index;
    }

    /**
     * Sets the value of the index property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setIndex(BigInteger value) {
        this.index = value;
    }

    /**
     * Gets the value of the role property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRole() {
        return role;
    }

    /**
     * Sets the value of the role property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRole(String value) {
        this.role = value;
    }

    /**
     * Gets the value of the referenceSystemRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the referenceSystemRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReferenceSystemRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ID4Referencing }
     * 
     * 
     */
    public List<ID4Referencing> getReferenceSystemRef() {
        if (referenceSystemRef == null) {
            referenceSystemRef = new ArrayList<ID4Referencing>();
        }
        return this.referenceSystemRef;
    }

    /**
     * Gets the value of the ipeGeometricRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ipeGeometricRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIPEGeometricRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ID4Referencing }
     * 
     * 
     */
    public List<ID4Referencing> getIPEGeometricRef() {
        if (ipeGeometricRef == null) {
            ipeGeometricRef = new ArrayList<ID4Referencing>();
        }
        return this.ipeGeometricRef;
    }

}
