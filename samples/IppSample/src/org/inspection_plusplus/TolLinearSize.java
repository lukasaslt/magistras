
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Abstrakte Klasse. Subklasse von Tol_Size. Beschreibt eine erlaubte Abweichung in der Gr��e/Ausma�. F�r die Tolerierung der Gr��e von Abst�nden, Durchmessern und Winkeln werden die Subklassen Tol_Distance, Tol_Diameter und Tol_Angle verwendet. Abstandsabweichungen, die sich auf eine Raumachse beziehen, werden �ber Subklassen von Tol_AxisDistance toleriert.
 * 
 * <p>Java class for Tol_LinearSize complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Tol_LinearSize">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Tol_Size">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tol_LinearSize")
@XmlSeeAlso({
    TolAngle.class,
    TolDistance.class
})
public abstract class TolLinearSize
    extends TolSize
{


}
