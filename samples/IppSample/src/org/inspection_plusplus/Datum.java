
package org.inspection_plusplus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Ein Bestandteil einer Referenzsystemdefinition zur Festlegung von einer oder mehr Achsen. Diese Festlegung kann entweder durch die Referenz auf 1..n IPE_Geometric-Instanzen erfolgen oder durch Angabe von 1..n DatumTargets.
 * Ein Datum beschränkt mindestens einen Freiheitsgrad. Ein Datum muss mindestens mit einem geometrischen IPE oder einem Datum Target verbunden sein.
 * Bei RPS-Systemen wird 1 Datum pro Achse verwendet.
 * 
 * <p>Java class for Datum complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Datum">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IppDMSentity">
 *       &lt;sequence>
 *         &lt;element name="Displacement" type="{http://www.inspection-plusplus.org}Vector3D"/>
 *         &lt;element name="Role" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DatumTarget" type="{http://www.inspection-plusplus.org}DatumTarget" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://www.inspection-plusplus.org}IPE_Geometric_ref" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Datum", propOrder = {
    "displacement",
    "role",
    "datumTarget",
    "ipeGeometricRef"
})
public class Datum
    extends IppDMSentity
{

    @XmlElement(name = "Displacement", required = true)
    protected Vector3D displacement;
    @XmlElement(name = "Role", required = true)
    protected String role;
    @XmlElement(name = "DatumTarget")
    protected List<DatumTarget> datumTarget;
    @XmlElement(name = "IPE_Geometric_ref", namespace = "http://www.inspection-plusplus.org")
    protected List<ID4Referencing> ipeGeometricRef;

    /**
     * Gets the value of the displacement property.
     * 
     * @return
     *     possible object is
     *     {@link Vector3D }
     *     
     */
    public Vector3D getDisplacement() {
        return displacement;
    }

    /**
     * Sets the value of the displacement property.
     * 
     * @param value
     *     allowed object is
     *     {@link Vector3D }
     *     
     */
    public void setDisplacement(Vector3D value) {
        this.displacement = value;
    }

    /**
     * Gets the value of the role property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRole() {
        return role;
    }

    /**
     * Sets the value of the role property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRole(String value) {
        this.role = value;
    }

    /**
     * Gets the value of the datumTarget property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the datumTarget property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDatumTarget().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DatumTarget }
     * 
     * 
     */
    public List<DatumTarget> getDatumTarget() {
        if (datumTarget == null) {
            datumTarget = new ArrayList<DatumTarget>();
        }
        return this.datumTarget;
    }

    /**
     * Gets the value of the ipeGeometricRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ipeGeometricRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIPEGeometricRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ID4Referencing }
     * 
     * 
     */
    public List<ID4Referencing> getIPEGeometricRef() {
        if (ipeGeometricRef == null) {
            ipeGeometricRef = new ArrayList<ID4Referencing>();
        }
        return this.ipeGeometricRef;
    }

}
