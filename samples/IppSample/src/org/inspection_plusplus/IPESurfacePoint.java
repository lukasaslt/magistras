
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Ein Punkt auf einer Oberfl�che. Diese kann durch Calculation_NominalElement festgelegt werden.
 * 
 * Das geerbte, erforderliche Attribut MainAxis definiert die Normalenrichtung der Fl�che an der Position des Punktes, vom Material wegzeigend.
 * Das geerbte Attribut Orientation definiert eine Richtung entlang der Fl�che und senkrecht auf der MainAxis.
 * 
 * <p>Java class for IPE_SurfacePoint complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IPE_SurfacePoint">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IPE_Point">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IPE_SurfacePoint")
@XmlSeeAlso({
})
public class IPESurfacePoint
    extends IPEPoint
{


}
