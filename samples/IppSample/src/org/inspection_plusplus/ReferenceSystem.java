
package org.inspection_plusplus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Referenzsystem als Bestandteil einer Toleranzangabe. Wird als solche von gewissen Toleranztypen referenziert. Es wird definiert �ber 1 bis n Datum und einer optionale AlignmentStrategy.
 * 
 * <p>Java class for ReferenceSystem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReferenceSystem">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IppDMSentity">
 *       &lt;sequence>
 *         &lt;element name="Datum" type="{http://www.inspection-plusplus.org}Datum" maxOccurs="unbounded"/>
 *         &lt;element name="AlignmentStrategy" type="{http://www.inspection-plusplus.org}AlignmentStrategy" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReferenceSystem", propOrder = {
    "datum",
    "alignmentStrategy"
})
public class ReferenceSystem
    extends IppDMSentity
{

    @XmlElement(name = "Datum", required = true)
    protected List<Datum> datum;
    @XmlElement(name = "AlignmentStrategy")
    protected List<AlignmentStrategy> alignmentStrategy;

    /**
     * Gets the value of the datum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the datum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDatum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Datum }
     * 
     * 
     */
    public List<Datum> getDatum() {
        if (datum == null) {
            datum = new ArrayList<Datum>();
        }
        return this.datum;
    }

    /**
     * Gets the value of the alignmentStrategy property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the alignmentStrategy property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAlignmentStrategy().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AlignmentStrategy }
     * 
     * 
     */
    public List<AlignmentStrategy> getAlignmentStrategy() {
        if (alignmentStrategy == null) {
            alignmentStrategy = new ArrayList<AlignmentStrategy>();
        }
        return this.alignmentStrategy;
    }

}
