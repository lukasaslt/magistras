
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Subklasse von Tol_Size und Tol_LinearSize. Winkeltoleranz. Beschreibt eine erlaubte Winkelabweichung. Wird zur Tolerierung abgeleiteter Winkel, z. B. zwischen zwei Lochachsen, eingesetzt. Der Winkel wird durch eine einzelne IPE-Instanz repr�sentiert (IPE_Angle), welche optional durch Calculation_NominalElement wiederum Design-Objekten (GeometricObjects) zugeordnet werden kann.
 * F�r die Tolerierung einer Winkelgr��e am Bauteil wird die Klasse Tol_AngularSize verwendet. Winkel bez�glich einer Raumachse werden �ber Subklassen von Tol_AxisAngle toleriert. In eine Ebene projizierte Winkel werden �ber Subklassen von Tol_PlaneAngle toleriert.
 * 
 * <p>Java class for Tol_Angle complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Tol_Angle">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Tol_LinearSize">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tol_Angle")
public class TolAngle
    extends TolLinearSize
{


}
