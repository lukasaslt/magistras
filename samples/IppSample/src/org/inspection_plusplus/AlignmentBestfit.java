
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Alignment_Bestfit complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Alignment_Bestfit">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}AlignmentStrategy">
 *       &lt;sequence>
 *         &lt;element name="TerminatingCondition" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Alignment_Bestfit", propOrder = {
    "terminatingCondition"
})
public class AlignmentBestfit
    extends AlignmentStrategy
{

    @XmlElement(name = "TerminatingCondition")
    protected double terminatingCondition;

    /**
     * Gets the value of the terminatingCondition property.
     * 
     */
    public double getTerminatingCondition() {
        return terminatingCondition;
    }

    /**
     * Sets the value of the terminatingCondition property.
     * 
     */
    public void setTerminatingCondition(double value) {
        this.terminatingCondition = value;
    }

}
