
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Abstrakte Klasse. Qualit�tskriterium. Repr�sentiert eine Anforderung an die Produktgeometrie in Form einer Toleranz. Gilt f�r bestimmte geometrische Entit�ten auf bestimmten Zusammenbaustufen (siehe Klasse InspektionTask). Die geometrische Entit�t wird �ber QC.Aggregation 1 referenziert, die Toleranz �ber Aggregation 1, und die ZSBs �ber QC.Aggregation 2.
 * Hinweis 1: Spalt und B�ndigkeit werden �ber QCs gel�st, die ggf. in einer QC_Group zusammengefasst sind.
 * Hinweis 2: um ohne Beachtung von Calculation_NominalElement-Instanzen pr�fen zu k�nnen, f�r welche Zusammenbaustufen ein QC gilt, muss das QC mit allen relevanten Part_Assembly-Instanzen verbunden werden. Siehe auch Geometriebezug von Qualit�tskriterien.
 * 
 * <p>Java class for QC_Single complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QC_Single">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}QC">
 *       &lt;sequence>
 *         &lt;element name="GeoObjectDetail" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element ref="{http://www.inspection-plusplus.org}Tolerance_ref"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QC_Single", propOrder = {
    "geoObjectDetail",
    "toleranceRef"
})
public class QCSingle
    extends QC
{

    @XmlElement(name = "GeoObjectDetail", required = true)
    protected String geoObjectDetail;
    @XmlElement(name = "Tolerance_ref", namespace = "http://www.inspection-plusplus.org", required = true)
    protected ID4Referencing toleranceRef;

    /**
     * Gets the value of the geoObjectDetail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGeoObjectDetail() {
        return geoObjectDetail;
    }

    /**
     * Sets the value of the geoObjectDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGeoObjectDetail(String value) {
        this.geoObjectDetail = value;
    }

    /**
     * Gets the value of the toleranceRef property.
     * 
     * @return
     *     possible object is
     *     {@link ID4Referencing }
     *     
     */
    public ID4Referencing getToleranceRef() {
        return toleranceRef;
    }

    /**
     * Sets the value of the toleranceRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link ID4Referencing }
     *     
     */
    public void setToleranceRef(ID4Referencing value) {
        this.toleranceRef = value;
    }

}
