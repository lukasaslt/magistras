
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Abstrakte Klasse. Ausrichtstrategie. Beschreibt, wie ein Bezugssystem (ReferenceSystem) auf einem Bauteil ermittelt wird.
 * 
 * <p>Java class for AlignmentStrategy complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AlignmentStrategy">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IppDMSentity">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AlignmentStrategy")
@XmlSeeAlso({
    AlignmentBestfit.class,
    Alignment321 .class,
    AlignmentFSS.class,
    AlignmentRPS.class
})
public abstract class AlignmentStrategy
    extends IppDMSentity
{


}
