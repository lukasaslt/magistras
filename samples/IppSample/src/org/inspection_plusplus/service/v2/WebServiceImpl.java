package org.inspection_plusplus.service.v2;

import javax.jws.WebService;
import javax.xml.ws.Holder;
import org.inspection_plusplus.Extension;
import org.inspection_plusplus.ID4Referencing;
import org.inspection_plusplus.InspectionPlan;
import org.inspection_plusplus.ReturnStatus;
import org.inspection_plusplus.Sender;

@WebService(name = "IppDmsServicePortType", targetNamespace = "http://www.inspection-plusplus.org/Service/V2.0")
public class WebServiceImpl implements IppDmsServicePortType
{
	@Override
	public ReturnStatus deleteInspectionPlan(ID4Referencing inspectionPlanID, Sender sender, Extension universal) throws Fault
	{
		return null;
	}

	@Override
	public void loadInspectionPlan(ID4Referencing inspectionPlanID, String mode, Sender sender, Extension universal, Holder<ReturnStatus> returnStatus, Holder<InspectionPlan> inspectionPlan) throws Fault
	{
	}

	@Override
	public ReturnStatus storeInspectionPlan(InspectionPlan inspectionPlan, String partVersion, String mode, Sender sender, Extension universal) throws Fault
	{
		return null;
	}
}
