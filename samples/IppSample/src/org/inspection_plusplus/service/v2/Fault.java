
package org.inspection_plusplus.service.v2;

import javax.xml.ws.WebFault;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2-b05-
 * Generated source version: 2.2
 * 
 */
@WebFault(name = "FaultInfo", targetNamespace = "http://www.inspection-plusplus.org/Service/V2.0")
public class Fault
    extends Exception
{

    /**
     * Java type that goes as soapenv:Fault detail element.
     * 
     */
    private FaultInfo faultInfo;

    /**
     * 
     * @param faultInfo
     * @param message
     */
    public Fault(String message, FaultInfo faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @param faultInfo
     * @param message
     * @param cause
     */
    public Fault(String message, FaultInfo faultInfo, Throwable cause) {
        super(message, cause);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @return
     *     returns fault bean: org.inspection_plusplus.service.v2.FaultInfo
     */
    public FaultInfo getFaultInfo() {
        return faultInfo;
    }

}
