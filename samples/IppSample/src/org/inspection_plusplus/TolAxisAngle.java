
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Abstrakte Klasse. Subklasse von Tol_AxisWithReference. Ihre Subklassen beschreiben Winkelabweichungen, die in Bezug auf jeweils eine der Raumachsen definiert werden.
 * 
 * <p>Java class for Tol_AxisAngle complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Tol_AxisAngle">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Tol_AxisWithReference">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tol_AxisAngle")
@XmlSeeAlso({
    TolAxisAngleZ.class,
    TolAxisAngleY.class,
    TolAxisAngleX.class
})
public abstract class TolAxisAngle
    extends TolAxisWithReference
{


}
