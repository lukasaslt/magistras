
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Datentyp zur Angabe eines Toleranzkriteriums in Form eines einzelnen textuellen Wertes aus einer Aufz�hlung, die durch Gen_Enumeration_Type beschrieben ist.
 * 
 *  
 * 
 *  
 * 
 * <p>Java class for DiscreteToleranceCriterion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DiscreteToleranceCriterion">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}ToleranceCriterion">
 *       &lt;sequence>
 *         &lt;element name="ToleranceValues" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DiscreteToleranceCriterion", propOrder = {
    "toleranceValues"
})
public class DiscreteToleranceCriterion
    extends ToleranceCriterion
{

    @XmlElement(name = "ToleranceValues", required = true)
    protected String toleranceValues;

    /**
     * Gets the value of the toleranceValues property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToleranceValues() {
        return toleranceValues;
    }

    /**
     * Sets the value of the toleranceValues property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToleranceValues(String value) {
        this.toleranceValues = value;
    }

}
