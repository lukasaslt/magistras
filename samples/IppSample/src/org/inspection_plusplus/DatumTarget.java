
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Repräsentiert stellvertretend eine komplexe Geometrie (Freiformfläche), die über einzelne geometrische Objekte (IPE_Geometric) nicht sinnvoll beschrieben werden kann. Wird in Datum referenziert.
 * 
 * <p>Java class for DatumTarget complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DatumTarget">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IppDMSentity">
 *       &lt;sequence>
 *         &lt;element name="Role" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element ref="{http://www.inspection-plusplus.org}IPE_Geometric_ref"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DatumTarget", propOrder = {
    "role",
    "ipeGeometricRef"
})
public class DatumTarget
    extends IppDMSentity
{

    @XmlElement(name = "Role", required = true)
    protected String role;
    @XmlElement(name = "IPE_Geometric_ref", namespace = "http://www.inspection-plusplus.org", required = true)
    protected ID4Referencing ipeGeometricRef;

    /**
     * Gets the value of the role property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRole() {
        return role;
    }

    /**
     * Sets the value of the role property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRole(String value) {
        this.role = value;
    }

    /**
     * Gets the value of the ipeGeometricRef property.
     * 
     * @return
     *     possible object is
     *     {@link ID4Referencing }
     *     
     */
    public ID4Referencing getIPEGeometricRef() {
        return ipeGeometricRef;
    }

    /**
     * Sets the value of the ipeGeometricRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link ID4Referencing }
     *     
     */
    public void setIPEGeometricRef(ID4Referencing value) {
        this.ipeGeometricRef = value;
    }

}
