
package org.inspection_plusplus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Abstrakte Klasse. Repr�sentiert eine Form der Berechnung oder Vorgehensstrategie (�Operation�).
 * Verf�gt �ber Parameter (OperationParameter) und Operanden (Operand). Das Ergebnis unterscheidet sich in den Subklassen. Siehe dort.
 * 
 * <p>Java class for Calculation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Calculation">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IppDMSentity">
 *       &lt;sequence>
 *         &lt;element name="Operation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OperationParameter" type="{http://www.inspection-plusplus.org}OperationParameter" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Operand" type="{http://www.inspection-plusplus.org}Operand" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Calculation", propOrder = {
    "operation",
    "operationParameter",
    "operand"
})
@XmlSeeAlso({
    CalculationNominalElement.class,
    CalculationActualElementDetection.class
})
public abstract class Calculation
    extends IppDMSentity
{

    @XmlElement(name = "Operation", required = true)
    protected String operation;
    @XmlElement(name = "OperationParameter")
    protected List<OperationParameter> operationParameter;
    @XmlElement(name = "Operand", required = true)
    protected List<Operand> operand;

    /**
     * Gets the value of the operation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperation() {
        return operation;
    }

    /**
     * Sets the value of the operation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperation(String value) {
        this.operation = value;
    }

    /**
     * Gets the value of the operationParameter property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the operationParameter property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOperationParameter().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OperationParameter }
     * 
     * 
     */
    public List<OperationParameter> getOperationParameter() {
        if (operationParameter == null) {
            operationParameter = new ArrayList<OperationParameter>();
        }
        return this.operationParameter;
    }

    /**
     * Gets the value of the operand property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the operand property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOperand().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Operand }
     * 
     * 
     */
    public List<Operand> getOperand() {
        if (operand == null) {
            operand = new ArrayList<Operand>();
        }
        return this.operand;
    }

}
