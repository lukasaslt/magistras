
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Beschreibt die Lage eines Objekts im Raum, also in Bezug auf alle Achsen eines Bezugssystems. Bezieht sich immer auf das Attribut Origin eines geometrischen Objekts / IPEs.
 * 
 * <p>Java class for Tol_Position complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Tol_Position">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Tolerance">
 *       &lt;sequence>
 *         &lt;element name="Criterion" type="{http://www.inspection-plusplus.org}IntervalToleranceCriterion"/>
 *         &lt;element name="ReferenceSystem" type="{http://www.inspection-plusplus.org}ReferenceSystem"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tol_Position", propOrder = {
    "criterion",
    "referenceSystem"
})
public class TolPosition
    extends Tolerance
{

    @XmlElement(name = "Criterion", required = true)
    protected IntervalToleranceCriterion criterion;
    @XmlElement(name = "ReferenceSystem", required = true)
    protected ReferenceSystem referenceSystem;

    /**
     * Gets the value of the criterion property.
     * 
     * @return
     *     possible object is
     *     {@link IntervalToleranceCriterion }
     *     
     */
    public IntervalToleranceCriterion getCriterion() {
        return criterion;
    }

    /**
     * Sets the value of the criterion property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntervalToleranceCriterion }
     *     
     */
    public void setCriterion(IntervalToleranceCriterion value) {
        this.criterion = value;
    }

    /**
     * Gets the value of the referenceSystem property.
     * 
     * @return
     *     possible object is
     *     {@link ReferenceSystem }
     *     
     */
    public ReferenceSystem getReferenceSystem() {
        return referenceSystem;
    }

    /**
     * Sets the value of the referenceSystem property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferenceSystem }
     *     
     */
    public void setReferenceSystem(ReferenceSystem value) {
        this.referenceSystem = value;
    }

}
