
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Parameter einer Calculation in Form eines Schl�ssel-Wert-Paares.
 * 
 * <p>Java class for OperationParameter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OperationParameter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="KeyValuePairs" type="{http://www.inspection-plusplus.org}KeyValueOperatorList"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OperationParameter", propOrder = {
    "keyValuePairs"
})
public class OperationParameter {

    @XmlElement(name = "KeyValuePairs", required = true)
    protected KeyValueOperatorList keyValuePairs;

    /**
     * Gets the value of the keyValuePairs property.
     * 
     * @return
     *     possible object is
     *     {@link KeyValueOperatorList }
     *     
     */
    public KeyValueOperatorList getKeyValuePairs() {
        return keyValuePairs;
    }

    /**
     * Sets the value of the keyValuePairs property.
     * 
     * @param value
     *     allowed object is
     *     {@link KeyValueOperatorList }
     *     
     */
    public void setKeyValuePairs(KeyValueOperatorList value) {
        this.keyValuePairs = value;
    }

}
