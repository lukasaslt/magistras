/*
package eu.sarunas.apps.android.mtconverter.core;

public class IncomeData
{
	public IncomeData()
	{
		this.setRate(IncomeRate.Month);
	}

	public IncomeData(double income, IncomeRate rate, float workDaysPerWeek, float workHoursPerDay) throws Exception
	{
		this.setIncome(income);
		this.setRate(rate);
		this.setWorkDaysPerWeek(workDaysPerWeek);
		this.setWorkHoursPerDay(workHoursPerDay);
	}

	public double getIncome()
	{

		return this.income;
	}

	public void setIncome(double value) throws Exception
	{
		if (value < 0.0f)
		{
			throw new Exception("income has to be positive");
		}

		this.income = value;
	}

	public IncomeRate getRate()
	{
		return this.rate;
	}

	public void setRate(IncomeRate value)
	{
		this.rate = value;
	}

	public float getWorkDaysPerWeek()
	{
		return this.workDaysPerWeek;
	}

	public void setWorkDaysPerWeek(float value) throws Exception
	{
		if ((value < 0.0f) || (value > 7.0f))
		{
			throw new Exception("work days per week has to be in range between 0 and 7");
		}

		this.workDaysPerWeek = value;
	}

	public float getWorkHoursPerDay()
	{
		return this.workHoursPerDay;
	}

	public void setWorkHoursPerDay(float value) throws Exception
	{
		if ((value < 0.0f) || (value > 24.0f))
		{
			throw new Exception("work hours per day has to be in range between 0 and 24");
		}

		this.workHoursPerDay = value;
	}

	public double getHourRate()
	{
		switch (this.rate)
		{
			case Hour:
				return this.getIncome();
			case Month:
				return this.getIncome() / this.getWorkDaysPerWeek() / this.getWorkHoursPerDay() / 4;
			case Week:
				return this.getIncome() / this.getWorkDaysPerWeek() / this.getWorkHoursPerDay();
			case Year:
				return this.getIncome() / this.getWorkDaysPerWeek() / this.getWorkHoursPerDay() / 4 / 12;
			default:
				throw new RuntimeException("NotSupportedException");
		}
	}

	private float workDaysPerWeek = 5.0f;
	private float workHoursPerDay = 8.0f;
	private double income = 1000.0;
	private IncomeRate rate = IncomeRate.Month;
}
*/