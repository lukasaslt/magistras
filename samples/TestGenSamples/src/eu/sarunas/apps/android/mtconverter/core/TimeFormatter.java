//package eu.sarunas.apps.android.mtconverter.core;
//
//public class TimeFormatter
//{
//	/*
//	public String formatTime(double time, IncomeData incomeData)
//	{
//		if (true == Double.isInfinite(time))
//		{
//			return "Forever";
//		}
//
//		if (time <= 0.00000000000001)
//		{
//			return "It's free";
//		}
//
//		String result = "";
//
//		long years = (long) (time / incomeData.getWorkDaysPerWeek() / incomeData.getWorkHoursPerDay() / 4.0 / 12.0);
//		time -= (years * incomeData.getWorkDaysPerWeek() * incomeData.getWorkHoursPerDay() * 4.0 * 12.0);
//
//		long months = (long) (time / incomeData.getWorkDaysPerWeek() / incomeData.getWorkHoursPerDay() / 4.0);
//		time -= (months * incomeData.getWorkDaysPerWeek() * incomeData.getWorkHoursPerDay() * 4.0);
//
//		long weeks = (long) (time / incomeData.getWorkDaysPerWeek() / incomeData.getWorkHoursPerDay());
//		time -= (weeks * incomeData.getWorkDaysPerWeek() * incomeData.getWorkHoursPerDay());
//
//		long days = (long) (time / incomeData.getWorkHoursPerDay());
//		time -= (days * incomeData.getWorkHoursPerDay());
//
//		long hours = (long) time;
//
//		long minutes = (long) Math.ceil((time - (double) hours) * 60.0);
//
//		if (minutes > 50)
//		{
//			minutes = 0;
//			hours++;
//		}
//
//		if (hours >= incomeData.getWorkHoursPerDay())
//		{
//			hours = 0;
//			days++;
//		}
//
//		if (days >= incomeData.getWorkDaysPerWeek())
//		{
//			days = 0;
//			weeks++;
//		}
//
//		if (weeks >= 4)
//		{
//			weeks = 0;
//			months++;
//		}
//
//		if (months >= 12)
//		{
//			months = 0;
//			years++;
//		}
//
//		if (years > 0)
//		{
//			minutes = 0;
//			hours = 0;
//			days = 0;
//
//			if (weeks > 2)
//			{
//				months++;
//			}
//
//			if (months >= 12)
//			{
//				years++;
//				months = 0;
//			}
//
//			weeks = 0;
//		}
//		else if (months > 0)
//		{
//			minutes = 0;
//			hours = 0;
//
//			if (days >= incomeData.getWorkDaysPerWeek() / 2)
//			{
//				weeks++;
//			}
//
//			if (weeks >= 4)
//			{
//				months++;
//				weeks = 0;
//
//				if (months >= 12)
//				{
//					years++;
//					months = 0;
//				}
//			}
//
//			days = 0;
//		}
//		else if (weeks > 0)
//		{
//			minutes = 0;
//
//			if (hours >= incomeData.getWorkHoursPerDay() / 2)
//			{
//				days++;
//			}
//
//			if (days >= incomeData.getWorkDaysPerWeek())
//			{
//				days = 0;
//				weeks++;
//			}
//
//			if (weeks >= 4)
//			{
//				months++;
//				weeks = 0;
//
//				/*
//				 * if (months >= 12) { years++; months = 0; }
//				 *-/
//			}
//
//			hours = 0;
//		}
//		else if (days > 0)
//		{
//			minutes = 0;
//		}
//
//		result += formatYears(years, months, weeks, days, hours, minutes);
//		result += formatMonths(years, months, weeks, days, hours, minutes);
//		result += formatWeeks(years, months, weeks, days, hours, minutes);
//		result += formatDays(years, months, weeks, days, hours, minutes);
//		result += formatHours(years, months, weeks, days, hours, minutes);
//		result += formatMinutes(years, months, weeks, days, hours, minutes);
//
//		return result.trim();
//	}
//*/
//	private String formatYears(long years, long months, long weeks, long days, long hours, long minutes)
//	{
//		if (years > 0)
//		{
//			return " " + years + " year" + getS(years);
//		}
//
//		return "";
//	}
//
//	private String formatMonths(long years, long months, long weeks, long days, long hours, long minutes)
//	{
//		if (months > 0)
//		{
//			return " " + months + " month" + getS(months);
//		}
//
//		return "";
//	}
//
//	private String formatWeeks(long years, long months, long weeks, long days, long hours, long minutes)
//	{
//		if (weeks > 0)
//		{
//			return " " + weeks + " week" + getS(weeks);
//		}
//
//		return "";
//	}
//
//	private String formatDays(long years, long months, long weeks, long days, long hours, long minutes)
//	{
//		if (days > 0)
//		{
//			return " " + days + " day" + getS(days);
//		}
//
//		return "";
//	}
//
//	private String formatHours(long years, long months, long weeks, long days, long hours, long minutes)
//	{
//		if (hours > 0)
//		{
//			return " " + hours + " hour" + getS(hours);
//		}
//
//		return "";
//	}
//
//	private String formatMinutes(long years, long months, long weeks, long days, long hours, long minutes)
//	{
//		if (minutes == 0)
//		{
//			return "";
//		}
//
//		if (hours > 0)
//		{
//			if (minutes <= 7)
//			{
//				return " and 5 minutes";
//			}
//			else if (minutes <= 12)
//			{
//				return " and 10 minutes";
//			}
//			else if (minutes <= 17)
//			{
//				return " and 15 minutes";
//			}
//			else if (minutes <= 37)
//			{
//				return " and a half";
//			}
//			else
//			// if (minutes <= 53)
//			{
//				return " and 45 minutes";
//			}
//			// else
//			// {
//			// return " and " + minutes + " minute" + GetS(minutes);
//			// }
//		}
//		else
//		{
//			return " " + minutes + " minute" + getS(minutes);
//		}
//	}
//
//	private String getS(long number)
//	{
//		if ((number % 10 == 1) && (11 != number))
//		{
//			return "";
//		}
//		else
//		{
//			return "s";
//		}
//	}
//}
