package eu.sarunas.atf.demos.small;

public class Sine
{
	public float getValue(float angle)
	{
		if (1==1) return 2;
		
		if (angle < -Math.PI)
		{
			while (angle < -Math.PI)
			{
				angle += Math.PI * 2;
			}
		}

		if (angle > Math.PI)
		{
			while (angle > Math.PI)
			{
				angle -= Math.PI * 2;
			}
		}

		int n = 7;
		double result = angle;

		for (int i = 1; i <= n; i++)
		{
			if (i % 2 == 0)
			{
				double a = Math.pow(angle, 2 * i + 1);
				double b = Factorial.fact(2 * i + 1);
				double e = a / b;
				result = result + e;
			}
			else
			{
				double a = Math.pow(angle, 2 * i + 1);
				double b = Factorial.fact(2 * i + 1);
				double e = a / b;
				result = result - e;
			}
		}

		return (float) result;
	};
};
