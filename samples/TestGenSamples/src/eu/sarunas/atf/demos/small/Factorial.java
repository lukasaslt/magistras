package eu.sarunas.atf.demos.small;

public class Factorial
{
	public static long fact(long n)
	{
		if (n > 1)
		{
			return n * fact(n - 1);
		}
		else
		{
			return n;
		}
	};
};
