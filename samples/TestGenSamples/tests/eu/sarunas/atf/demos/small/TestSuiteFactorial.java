package eu.sarunas.atf.demos.small;
public class TestSuiteFactorial
{
	@org.junit.Test
	public void testFact1() throws Throwable
	{
		eu.sarunas.atf.demos.small.Factorial testObject = new eu.sarunas.atf.demos.small.Factorial();

		long res = testObject.fact((long)0);
		java.util.ArrayList<Object> preValues0 = new java.util.ArrayList<Object>();
		preValues0.add((long)0);

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("fact", long.class), preValues0, res, "/eu/sarunas/atf/demos/small/model.ocl");
	};

	@org.junit.Test
	public void testFact2() throws Throwable
	{
		eu.sarunas.atf.demos.small.Factorial testObject = new eu.sarunas.atf.demos.small.Factorial();

		long res = testObject.fact((long)-15);
		java.util.ArrayList<Object> preValues1 = new java.util.ArrayList<Object>();
		preValues1.add((long)-15);

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("fact", long.class), preValues1, res, "/eu/sarunas/atf/demos/small/model.ocl");
	};

	@org.junit.Test
	public void testFact3() throws Throwable
	{
		eu.sarunas.atf.demos.small.Factorial testObject = new eu.sarunas.atf.demos.small.Factorial();

		long res = testObject.fact((long)1);
		java.util.ArrayList<Object> preValues2 = new java.util.ArrayList<Object>();
		preValues2.add((long)1);

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("fact", long.class), preValues2, res, "/eu/sarunas/atf/demos/small/model.ocl");
	};

	@org.junit.Test
	public void testFact4() throws Throwable
	{
		eu.sarunas.atf.demos.small.Factorial testObject = new eu.sarunas.atf.demos.small.Factorial();

		long res = testObject.fact((long)9);
		java.util.ArrayList<Object> preValues3 = new java.util.ArrayList<Object>();
		preValues3.add((long)9);

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("fact", long.class), preValues3, res, "/eu/sarunas/atf/demos/small/model.ocl");
	};

	@org.junit.Test
	public void testFact5() throws Throwable
	{
		eu.sarunas.atf.demos.small.Factorial testObject = new eu.sarunas.atf.demos.small.Factorial();

		long res = testObject.fact((long)-2);
		java.util.ArrayList<Object> preValues4 = new java.util.ArrayList<Object>();
		preValues4.add((long)-2);

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("fact", long.class), preValues4, res, "/eu/sarunas/atf/demos/small/model.ocl");
	};

	@org.junit.Test
	public void testFact6() throws Throwable
	{
		eu.sarunas.atf.demos.small.Factorial testObject = new eu.sarunas.atf.demos.small.Factorial();

		long res = testObject.fact((long)-26);
		java.util.ArrayList<Object> preValues5 = new java.util.ArrayList<Object>();
		preValues5.add((long)-26);

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("fact", long.class), preValues5, res, "/eu/sarunas/atf/demos/small/model.ocl");
	};

	@org.junit.Test
	public void testFact7() throws Throwable
	{
		eu.sarunas.atf.demos.small.Factorial testObject = new eu.sarunas.atf.demos.small.Factorial();

		long res = testObject.fact((long)-5);
		java.util.ArrayList<Object> preValues6 = new java.util.ArrayList<Object>();
		preValues6.add((long)-5);

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("fact", long.class), preValues6, res, "/eu/sarunas/atf/demos/small/model.ocl");
	};

	@org.junit.Test
	public void testFact8() throws Throwable
	{
		eu.sarunas.atf.demos.small.Factorial testObject = new eu.sarunas.atf.demos.small.Factorial();

		long res = testObject.fact((long)-53);
		java.util.ArrayList<Object> preValues7 = new java.util.ArrayList<Object>();
		preValues7.add((long)-53);

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("fact", long.class), preValues7, res, "/eu/sarunas/atf/demos/small/model.ocl");
	};

	@org.junit.Test
	public void testFact9() throws Throwable
	{
		eu.sarunas.atf.demos.small.Factorial testObject = new eu.sarunas.atf.demos.small.Factorial();

		long res = testObject.fact((long)-12);
		java.util.ArrayList<Object> preValues8 = new java.util.ArrayList<Object>();
		preValues8.add((long)-12);

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("fact", long.class), preValues8, res, "/eu/sarunas/atf/demos/small/model.ocl");
	};

	@org.junit.Test
	public void testFact10() throws Throwable
	{
		eu.sarunas.atf.demos.small.Factorial testObject = new eu.sarunas.atf.demos.small.Factorial();

		long res = testObject.fact((long)-9);
		java.util.ArrayList<Object> preValues9 = new java.util.ArrayList<Object>();
		preValues9.add((long)-9);

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("fact", long.class), preValues9, res, "/eu/sarunas/atf/demos/small/model.ocl");
	};

	@org.junit.Test
	public void testFact11() throws Throwable
	{
		eu.sarunas.atf.demos.small.Factorial testObject = new eu.sarunas.atf.demos.small.Factorial();

		long res = testObject.fact((long)16);
		java.util.ArrayList<Object> preValues10 = new java.util.ArrayList<Object>();
		preValues10.add((long)16);

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("fact", long.class), preValues10, res, "/eu/sarunas/atf/demos/small/model.ocl");
	};

};
