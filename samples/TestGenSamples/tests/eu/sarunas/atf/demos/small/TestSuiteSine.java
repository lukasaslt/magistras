package eu.sarunas.atf.demos.small;
public class TestSuiteSine
{
	@org.junit.Test
	public void testGetValue1() throws Throwable
	{
		eu.sarunas.atf.demos.small.Sine testObject = new eu.sarunas.atf.demos.small.Sine();

		float res = testObject.getValue(8.91f);
		java.util.ArrayList<Object> preValues0 = new java.util.ArrayList<Object>();
		preValues0.add(8.91f);

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("getValue", float.class), preValues0, res, "/eu/sarunas/atf/demos/small/model.ocl");
	};

	@org.junit.Test
	public void testGetValue2() throws Throwable
	{
		eu.sarunas.atf.demos.small.Sine testObject = new eu.sarunas.atf.demos.small.Sine();

		float res = testObject.getValue(19.28f);
		java.util.ArrayList<Object> preValues1 = new java.util.ArrayList<Object>();
		preValues1.add(19.28f);

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("getValue", float.class), preValues1, res, "/eu/sarunas/atf/demos/small/model.ocl");
	};

	@org.junit.Test
	public void testGetValue3() throws Throwable
	{
		eu.sarunas.atf.demos.small.Sine testObject = new eu.sarunas.atf.demos.small.Sine();

		float res = testObject.getValue(2.20f);
		java.util.ArrayList<Object> preValues2 = new java.util.ArrayList<Object>();
		preValues2.add(2.20f);

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("getValue", float.class), preValues2, res, "/eu/sarunas/atf/demos/small/model.ocl");
	};

	@org.junit.Test
	public void testGetValue4() throws Throwable
	{
		eu.sarunas.atf.demos.small.Sine testObject = new eu.sarunas.atf.demos.small.Sine();

		float res = testObject.getValue(9.30f);
		java.util.ArrayList<Object> preValues3 = new java.util.ArrayList<Object>();
		preValues3.add(9.30f);

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("getValue", float.class), preValues3, res, "/eu/sarunas/atf/demos/small/model.ocl");
	};

	@org.junit.Test
	public void testGetValue5() throws Throwable
	{
		eu.sarunas.atf.demos.small.Sine testObject = new eu.sarunas.atf.demos.small.Sine();

		float res = testObject.getValue(5.80f);
		java.util.ArrayList<Object> preValues4 = new java.util.ArrayList<Object>();
		preValues4.add(5.80f);

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("getValue", float.class), preValues4, res, "/eu/sarunas/atf/demos/small/model.ocl");
	};

	@org.junit.Test
	public void testGetValue6() throws Throwable
	{
		eu.sarunas.atf.demos.small.Sine testObject = new eu.sarunas.atf.demos.small.Sine();

		float res = testObject.getValue(1.83f);
		java.util.ArrayList<Object> preValues5 = new java.util.ArrayList<Object>();
		preValues5.add(1.83f);

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("getValue", float.class), preValues5, res, "/eu/sarunas/atf/demos/small/model.ocl");
	};

	@org.junit.Test
	public void testGetValue7() throws Throwable
	{
		eu.sarunas.atf.demos.small.Sine testObject = new eu.sarunas.atf.demos.small.Sine();

		float res = testObject.getValue(7.02f);
		java.util.ArrayList<Object> preValues6 = new java.util.ArrayList<Object>();
		preValues6.add(7.02f);

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("getValue", float.class), preValues6, res, "/eu/sarunas/atf/demos/small/model.ocl");
	};

	@org.junit.Test
	public void testGetValue8() throws Throwable
	{
		eu.sarunas.atf.demos.small.Sine testObject = new eu.sarunas.atf.demos.small.Sine();

		float res = testObject.getValue(1.09f);
		java.util.ArrayList<Object> preValues7 = new java.util.ArrayList<Object>();
		preValues7.add(1.09f);

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("getValue", float.class), preValues7, res, "/eu/sarunas/atf/demos/small/model.ocl");
	};

	@org.junit.Test
	public void testGetValue9() throws Throwable
	{
		eu.sarunas.atf.demos.small.Sine testObject = new eu.sarunas.atf.demos.small.Sine();

		float res = testObject.getValue(31.08f);
		java.util.ArrayList<Object> preValues8 = new java.util.ArrayList<Object>();
		preValues8.add(31.08f);

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("getValue", float.class), preValues8, res, "/eu/sarunas/atf/demos/small/model.ocl");
	};

	@org.junit.Test
	public void testGetValue10() throws Throwable
	{
		eu.sarunas.atf.demos.small.Sine testObject = new eu.sarunas.atf.demos.small.Sine();

		float res = testObject.getValue(3.95f);
		java.util.ArrayList<Object> preValues9 = new java.util.ArrayList<Object>();
		preValues9.add(3.95f);

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("getValue", float.class), preValues9, res, "/eu/sarunas/atf/demos/small/model.ocl");
	};

	@org.junit.Test
	public void testGetValue11() throws Throwable
	{
		eu.sarunas.atf.demos.small.Sine testObject = new eu.sarunas.atf.demos.small.Sine();

		float res = testObject.getValue(22.65f);
		java.util.ArrayList<Object> preValues10 = new java.util.ArrayList<Object>();
		preValues10.add(22.65f);

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("getValue", float.class), preValues10, res, "/eu/sarunas/atf/demos/small/model.ocl");
	};

};
