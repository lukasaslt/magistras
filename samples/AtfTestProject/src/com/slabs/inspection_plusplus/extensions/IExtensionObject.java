package com.slabs.inspection_plusplus.extensions;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;

public interface IExtensionObject
{
	Document getDocument() throws JAXBException, ParserConfigurationException;
}
