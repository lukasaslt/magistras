package com.slabs.inspection_plusplus.extensions;

import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.inspection_plusplus.SuperID;
import org.inspection_plusplus.Vector3D;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import com.slabs.carmen.ipp.utils.IppDataConverter;

@XmlRootElement
@XmlType(name = "", propOrder = { "method", "vec1", "vec2", "vec3", "ref1", "ref2" })
public class BMWFeatureParams implements IExtensionObject
{
	private Integer		method;
	private Vector3D	vec1;
	private Vector3D	vec2;
	private Vector3D	vec3;
	private SuperID		ref1;
	private SuperID		ref2;

	public BMWFeatureParams()
	{
	}

	public Integer getMethod()
	{
		return method;
	}

	public void setMethod(Integer method)
	{
		this.method = method;
	}

	public Vector3D getVec1()
	{
		return vec1;
	}

	public void setVec1(Vector3D vec1)
	{
		this.vec1 = vec1;
	}

	public Vector3D getVec2()
	{
		return vec2;
	}

	public void setVec2(Vector3D vec2)
	{
		this.vec2 = vec2;
	}

	public Vector3D getVec3()
	{
		return vec3;
	}

	public void setVec3(Vector3D vec3)
	{
		this.vec3 = vec3;
	}

	public SuperID getRef1()
	{
		return ref1;
	}

	public void setRef1(SuperID ref1)
	{
		this.ref1 = ref1;
	}

	public SuperID getRef2()
	{
		return ref2;
	}

	public void setRef2(SuperID ref2)
	{
		this.ref2 = ref2;
	}

	public Document getDocument() throws JAXBException, ParserConfigurationException
	{
		DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
		Document document = docBuilder.newDocument();

		IppDataConverter.getBMWFeatureParamsMarshaller().marshal(this, document);

		return document;
	}

	public static BMWFeatureParams loadFromElement(Element element) throws JAXBException, ParserConfigurationException
	{
		return (BMWFeatureParams) IppDataConverter.getBMWFeatureParamsUnMarshaller().unmarshal(element.getOwnerDocument());
	}
}
