package com.slabs.inspection_plusplus.extensions;

import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.inspection_plusplus.Extension;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

@XmlRootElement(namespace = "")
@XmlType(name = "", propOrder = { "cpRefNumber", "referenceNumber", "buildPhase", "ipSymmetry", "singlePart", "ipVersionComment", "comment", "project" })
public class IPmetaData implements IExtensionObject
{
	private String	cpRefNumber;
	private String	referenceNumber;
	private String	buildPhase;
	private String	ipSymmetry;
	private Boolean	singlePart;
	private String	ipVersionComment;
	private String	comment;
	private String	project;

	public String getProject()
	{
		return project;
	}

	public void setProject(String project)
	{
		this.project = project;
	}

	public Boolean getSinglePart()
	{
		return singlePart;
	}

	public void setSinglePart(Boolean singlePart)
	{
		this.singlePart = singlePart;
	}

	public String getComment()
	{
		return comment;
	}

	public void setComment(String comment)
	{
		this.comment = comment;
	}

	public String getCpRefNumber()
	{
		return cpRefNumber;
	}

	public void setCpRefNumber(String cpRefNumber)
	{
		this.cpRefNumber = cpRefNumber;
	}

	public String getReferenceNumber()
	{
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber)
	{
		this.referenceNumber = referenceNumber;
	}

	public String getBuildPhase()
	{
		return buildPhase;
	}

	public void setBuildPhase(String buildPhase)
	{
		this.buildPhase = buildPhase;
	}

	public String getIpSymmetry()
	{
		return ipSymmetry;
	}

	public void setIpSymmetry(String psymmetry)
	{
		ipSymmetry = psymmetry;
	}

	public Document getDocument() throws JAXBException, ParserConfigurationException
	{
		DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
		Document document = docBuilder.newDocument();

		JAXBContext jaxbContext = JAXBContext.newInstance(IPmetaData.class);
		Marshaller marshaller = jaxbContext.createMarshaller();
		marshaller.setProperty("jaxb.formatted.output", Boolean.valueOf(true));

		marshaller.marshal(this, document);

		return document;
	}

	public static IPmetaData loadFromElement(Element element) throws JAXBException, ParserConfigurationException
	{
		/*
		 * IPmetaData result = new IPmetaData();
		 * result.setCpRefNumber(element.getElementsByTagName("cpRefNumber").item(0).getTextContent().trim());
		 * result.setReferenceNumber(element.getElementsByTagName("referenceNumber").item(0).getTextContent().trim());
		 * result.setBuildPhase(element.getElementsByTagName("buildPhase").item(0).getTextContent().trim());
		 * result.setIpSymmetry(element.getElementsByTagName("ipSymmetry").item(0).getTextContent().trim());
		 * result.setIpVersionComment(element.getElementsByTagName("ipVersionComment").item(0).getTextContent().trim());
		 * result.setComment(element.getElementsByTagName("comment").item(0).getTextContent().trim());
		 * result.setProject(element.getElementsByTagName("project").item(0).getTextContent().trim());
		 * result.setSinglePart(null == element.getElementsByTagName("singlePart").item(0).getTextContent().trim() ? null :
		 * Boolean.parseBoolean(element.getAttribute("singlePart")));
		 * return result;
		 */
		JAXBContext jaxbContext = JAXBContext.newInstance(IPmetaData.class);
		Unmarshaller unMarshaller = jaxbContext.createUnmarshaller();

		return (IPmetaData) unMarshaller.unmarshal(element.getOwnerDocument());
	}

	public static IPmetaData loadFromExtensionList(List<Extension> list) throws JAXBException, ParserConfigurationException
	{
		if (list != null)
		{
			for (Extension ext : list)
			{
				for (Object o : ext.getAny())
				{
					if (o instanceof Element)
					{
						Element e = (Element) o;
						IPmetaData params = IPmetaData.loadFromElement(e);
						return params;
					}
				}
			}
		}
		return null;
	}

	public String getIpVersionComment()
	{
		return ipVersionComment;
	}

	public void setIpVersionComment(String versionComment)
	{
		ipVersionComment = versionComment;
	}

	public static boolean isThisElement(Element element)
	{
		if (element != null)
		{
			return IPmetaData.class.getSimpleName().equalsIgnoreCase(element.getLocalName());
		}

		return false;
	}

}
