package com.slabs.carmen.ipp;

public class Constants
{
	// Side
	public static final String	left															= "L";
	public static final String	right															= "R";
	public static final String	IPP_IPE														= "IPP_IPE";

	// Client types
	public static final String	SERVICE_CLIENT_TYPE_BMW						= "bmw";
	public static final String	SERVICE_CLIENT_TYPE_DAIMLER				= "daimler";

	// Default comment for new object
	public static final String	DEFAULT_COMMENT										= "I++ Import";
	public static final String	RELEASED													= "released";
	public static final String	PRE_RELEASED											= "pre-released";
	public static final String	DRAFT															= "draft";

	// language
	public static final String	L_ENGLISH													= "en";
	public static final String	L_GERMAN													= "de";

	public static final String	PartPrefix												= "Part";
	public static final String	AssemblyPrefix										= "Asm";

	// related to Quality Agreements
	// QC group type
	public static final String	QCGROUP_TYPE_CLASSIFICATION				= "Classification";
	public static final String	QCGROUP_TYPE_HIERARCHY						= "Hierarchy";

	public static final String	QCGROUP_FUNCTION_QA								= "QualityAgreement";
	public static final String	QCGROUP_FUNCTION_INSPECTIONPLAN		= "InspectionPlan";
	public static final String	QCGROUP_FUNCTION_EVAL_DEF					= "Eval_Def";
	public static final String	QCGROUP_FUNCTION_EVAL_FUNCTION		= "Eval_Function";
	
	public static final String	QCGROUP_DESCRIPTION_COMMON_ANALYSIS	= "Common Analysis";
	public static final String	QCGROUP_DESCRIPTION_FMK_FUNCTION		= "fmk";

	public static final String	QC_DESCRIPTION_MAIN_AXIS					= "MainAxis";
	public static final String	QC_DESCRIPTION_MAIN_AXIS2					= "MainAxis2";
	public static final String	QC_DESCRIPTION_LENGTH							= "Length";
	public static final String	QC_DESCRIPTION_WIDTH							= "Width";

	public static final String	QCGROUP_NAME_FMK									= "fmk";
	public static final String	QCGROUP_NAME_CPK									= "CPK";
	public static final String	QCGROUP_NAME_INLINE								= "Inline";
	public static final String	QCGROUP_NAME_PRODUCTION_RUN				= "ProductionRun";
	public static final String	QCGROUP_NAME_ALIGNMENT						= "Alignment";
	public static final String	QCGROUP_NAME_BEREICH_NUMMER				= "bereich.nummer";
	public static final String	QCGROUP_NAME_BEREICH_BEZEICHNUNG	= "bereich.bezeichnung";
	public static final String	QCGROUP_NAME_BEREICH_SACHNUMMER		= "bereich.sachnummer";
	public static final String	QCGROUP_NAME_ROHBAU								= "BodyInWhite";

	public static final String	FORM_MIN_SUFFIX										= "_min";
	public static final String	FORM_MAX_SUFFIX										= "_max";
	public static final String	FORM_DISP_SUFFIX									= "_dispersion";
	
	public static final String EMMA_ANALYST_KEY				= "EMMA_3D_EVALUATION";
	public static final String EMMA_PLANNER_KEY				= "EMMA_3D_PLANNER";
	public static final String EMMA_ILLUSTRATOR_KEY			= "EMMA_ILLUSTRATOR";
	public static final String EMMA_CORRELATION_KEY			= "EMMA_CORRELATION"; 
	public static final String EMMA_DX_KEY					= "EMMA_DX";
	public static final String EMMA_EVALUATIONREPORTS_KEY	= "EVALUATIONREPORTS";
}
