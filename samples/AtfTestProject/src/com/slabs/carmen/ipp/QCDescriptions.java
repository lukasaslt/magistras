package com.slabs.carmen.ipp;

public enum QCDescriptions
{
	MDM_TOL_A, MDM_TOL_B,

	MDM_TOL_LENGTH, MDM_TOL_WIDTH;
}
