package com.slabs.carmen.ipp;

public enum OperationParameters
{
	distance_x, distance_y, distance_z,

	point_x, point_y, point_z,

	angle_x, angle_y, angle_z,

	ref_axis_x, ref_axis_y, ref_axis_z,

	IPP_BeamAxisX, IPP_BeamAxisY, IPP_BeamAxisZ;
}
