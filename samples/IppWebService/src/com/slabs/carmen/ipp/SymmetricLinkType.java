package com.slabs.carmen.ipp;

public enum SymmetricLinkType
{
	NONE, MDM_FULL_SYMMETRY, MDM_PARTIAL_SYMMETRY;
}
