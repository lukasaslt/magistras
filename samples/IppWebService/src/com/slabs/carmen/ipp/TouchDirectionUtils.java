package com.slabs.carmen.ipp;

import org.inspection_plusplus.Vector3D;

public class TouchDirectionUtils
{
	public static Vector3D getTouchDirectionVector(String directionString)
	{
		double value = 1.0;

		char axisSymbol = ' ';

		if (directionString.length() == 2)
		{
			value = (directionString.charAt(0) == '-') ? -1.0 : 1.0;
			axisSymbol = directionString.charAt(1);
		}

		Vector3D vector = new Vector3D();

		vector.setX(Double.valueOf((axisSymbol == 'X') ? value : 0.0));
		vector.setY(Double.valueOf((axisSymbol == 'Y') ? value : 0.0));
		vector.setZ(Double.valueOf((axisSymbol == 'Z') ? value : 0.0));

		return vector;
	}

	public static String getTouchDirectionName(Vector3D vector)
	{
		if (vector == null)
		{
			return null;
		}

		double x = vector.getX();
		double y = vector.getY();
		double z = vector.getZ();

		if ((Math.abs(x) == 1.0) && (y == 0.0) && (z == 0.0))
		{
			return (x > 0.0) ? "+X" : "-X";
		}
		else if ((x == 0.0) && (Math.abs(y) == 1.0) && (z == 0.0))
		{
			return (y > 0.0) ? "+Y" : "-Y";
		}
		else if ((x == 0.0) && (y == 0.0) && (Math.abs(z) == 1.0))
		{
			return (z > 0.0) ? "+Z" : "-Z";
		}
		else
		{
			return null;
		}
	}
}
