package com.slabs.carmen.ipp;

public enum Operations
{
	IPP_DistanceBetween, IPP_Intersection, IPP_AngleBetween,

	MDM_AngleBetween_2QFE, MDM_AngleBetween_2QFX, MDM_AngleBetween_4QF,

	IPP_Symmetry, IPP_LiesOn, IPP_Beam,

	MDM_GenericLink, MDM_FormLink;
}
