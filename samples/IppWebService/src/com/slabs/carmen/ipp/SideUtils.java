package com.slabs.carmen.ipp;

import org.inspection_plusplus.Vector3D;

public class SideUtils
{
	public static double	DELTA	= 0.005;

	/**
	 * IMPORTANT: use it for all features except ABSTAND
	 * NOTE: how we calculate side for WINKEL?
	 */
	public static boolean isLeftSideByCoordinates(Vector3D v)
	{
		if (v != null)
		{
			if (v.getY() < DELTA)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return true;
		}
	}
}
