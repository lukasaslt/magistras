
package org.inspection_plusplus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Definition eines generischen Merkmaltyps, der nicht durch eine eigene IPE-Subklasse spezifiziert ist.
 * 
 * Dies erlaubt die allgemeine Unterst�tzung weiterer Merkmalstypen neben den klassischen geometrischen Messplan-Elementen, wie z.B. Spalt-, Versatz- und Dichtma�e oder Schlie�kr�fte.
 * 
 * Die Merkmalsdefinition setzt sich zusammen aus Typbezeichner (Code) und einer beliebigen Anzahl "generischer" Attribute von elementarem Datentyp (Generic_Attribute_Type und Subklassen).
 * 
 * Die Instanziierung eines IPE von generischem Merkmaltyp erfolgt �ber die Klasse IPE_Generic, die dann auf diesen Typ verweist.
 * 
 * <p>Java class for Generic_IPE_Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Generic_IPE_Type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IppDMSentity">
 *       &lt;sequence>
 *         &lt;element name="Code" type="{http://www.inspection-plusplus.org}MultiLanguageString"/>
 *         &lt;element name="Generic_Attribute_Type" type="{http://www.inspection-plusplus.org}Generic_Attribute_Type" maxOccurs="unbounded"/>
 *         &lt;element name="IPE_Generic" type="{http://www.inspection-plusplus.org}IPE_Generic"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Generic_IPE_Type", propOrder = {
    "code",
    "genericAttributeType",
    "ipeGeneric"
})
public class GenericIPEType
    extends IppDMSentity
{

    @XmlElement(name = "Code", required = true)
    protected MultiLanguageString code;
    @XmlElement(name = "Generic_Attribute_Type", required = true)
    protected List<GenericAttributeType> genericAttributeType;
    @XmlElement(name = "IPE_Generic", required = true)
    protected IPEGeneric ipeGeneric;

    /**
     * Gets the value of the code property.
     * 
     * @return
     *     possible object is
     *     {@link MultiLanguageString }
     *     
     */
    public MultiLanguageString getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * 
     * @param value
     *     allowed object is
     *     {@link MultiLanguageString }
     *     
     */
    public void setCode(MultiLanguageString value) {
        this.code = value;
    }

    /**
     * Gets the value of the genericAttributeType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the genericAttributeType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGenericAttributeType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GenericAttributeType }
     * 
     * 
     */
    public List<GenericAttributeType> getGenericAttributeType() {
        if (genericAttributeType == null) {
            genericAttributeType = new ArrayList<GenericAttributeType>();
        }
        return this.genericAttributeType;
    }

    /**
     * Gets the value of the ipeGeneric property.
     * 
     * @return
     *     possible object is
     *     {@link IPEGeneric }
     *     
     */
    public IPEGeneric getIPEGeneric() {
        return ipeGeneric;
    }

    /**
     * Sets the value of the ipeGeneric property.
     * 
     * @param value
     *     allowed object is
     *     {@link IPEGeneric }
     *     
     */
    public void setIPEGeneric(IPEGeneric value) {
        this.ipeGeneric = value;
    }

}
