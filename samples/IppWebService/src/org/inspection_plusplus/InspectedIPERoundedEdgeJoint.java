
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Siehe IPE_RoundedEdgeJoint
 * 
 * <p>Java class for Inspected_IPE_RoundedEdgeJoint complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Inspected_IPE_RoundedEdgeJoint">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Inspected_IPE_Geometric">
 *       &lt;sequence>
 *         &lt;element name="Flash" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="Gap" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="MainAxis" type="{http://www.inspection-plusplus.org}Vector3D" minOccurs="0"/>
 *         &lt;element name="Radius1" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="Radius2" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Inspected_IPE_RoundedEdgeJoint", propOrder = {
    "flash",
    "gap",
    "mainAxis",
    "radius1",
    "radius2"
})
public class InspectedIPERoundedEdgeJoint
    extends InspectedIPEGeometric
{

    @XmlElement(name = "Flash")
    protected Double flash;
    @XmlElement(name = "Gap")
    protected Double gap;
    @XmlElement(name = "MainAxis")
    protected Vector3D mainAxis;
    @XmlElement(name = "Radius1")
    protected Double radius1;
    @XmlElement(name = "Radius2")
    protected Double radius2;

    /**
     * Gets the value of the flash property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getFlash() {
        return flash;
    }

    /**
     * Sets the value of the flash property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setFlash(Double value) {
        this.flash = value;
    }

    /**
     * Gets the value of the gap property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getGap() {
        return gap;
    }

    /**
     * Sets the value of the gap property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setGap(Double value) {
        this.gap = value;
    }

    /**
     * Gets the value of the mainAxis property.
     * 
     * @return
     *     possible object is
     *     {@link Vector3D }
     *     
     */
    public Vector3D getMainAxis() {
        return mainAxis;
    }

    /**
     * Sets the value of the mainAxis property.
     * 
     * @param value
     *     allowed object is
     *     {@link Vector3D }
     *     
     */
    public void setMainAxis(Vector3D value) {
        this.mainAxis = value;
    }

    /**
     * Gets the value of the radius1 property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getRadius1() {
        return radius1;
    }

    /**
     * Sets the value of the radius1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setRadius1(Double value) {
        this.radius1 = value;
    }

    /**
     * Gets the value of the radius2 property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getRadius2() {
        return radius2;
    }

    /**
     * Sets the value of the radius2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setRadius2(Double value) {
        this.radius2 = value;
    }

}
