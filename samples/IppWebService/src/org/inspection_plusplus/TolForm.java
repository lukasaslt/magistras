
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Abstrakte Klasse. Formtoleranz. Beschreibt Abweichungen von der jeweiligen Sollform eines geometrischen Objekts.
 * 
 * <p>Java class for Tol_Form complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Tol_Form">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Tolerance">
 *       &lt;sequence>
 *         &lt;element name="Criterion" type="{http://www.inspection-plusplus.org}ScalarToleranceCriterion"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tol_Form", propOrder = {
    "criterion"
})
@XmlSeeAlso({
    TolProfileOfSurface.class,
    TolCylindricity.class,
    TolStraightness.class,
    TolProfileOfLine.class,
    TolFlatness.class,
    TolPattern.class,
    TolCircularity.class
})
public abstract class TolForm
    extends Tolerance
{

    @XmlElement(name = "Criterion", required = true)
    protected ScalarToleranceCriterion criterion;

    /**
     * Gets the value of the criterion property.
     * 
     * @return
     *     possible object is
     *     {@link ScalarToleranceCriterion }
     *     
     */
    public ScalarToleranceCriterion getCriterion() {
        return criterion;
    }

    /**
     * Sets the value of the criterion property.
     * 
     * @param value
     *     allowed object is
     *     {@link ScalarToleranceCriterion }
     *     
     */
    public void setCriterion(ScalarToleranceCriterion value) {
        this.criterion = value;
    }

}
