
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Subklasse von Tol_Size. Beschreibt eine erlaubte Winkelabweichung. Wird f�r die Tolerierung direkt am Bauteil vorliegender Winkelgr��en eingesetzt.
 * F�r die Tolerierung abgeleiteter Winkel wird die Klasse Tol_Angle verwendet. Winkel bez�glich einer Raumachse werden �ber Subklassen von Tol_AxisAngle toleriert. In eine Ebene projizierte Winkel werden �ber Subklassen von Tol_PlaneAngle toleriert.
 * 
 * <p>Java class for Tol_AngularSize complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Tol_AngularSize">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Tol_Size">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tol_AngularSize")
public class TolAngularSize
    extends TolSize
{


}
