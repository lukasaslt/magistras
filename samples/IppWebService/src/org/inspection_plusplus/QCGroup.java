
package org.inspection_plusplus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Gruppierung von QC_Singles und QC_Groups nach wählbaren Gesichtspunkten, um die Übersicht in den Datenbeständen zu erhöhen und zusammengehörige Elemente gemeinsam behandeln zu können.
 * 
 * <p>Java class for QC_Group complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QC_Group">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}QC">
 *       &lt;sequence>
 *         &lt;element name="GroupType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element ref="{http://www.inspection-plusplus.org}QC_ref" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QC_Group", propOrder = {
    "groupType",
    "qcRef"
})
public class QCGroup
    extends QC
{

    @XmlElement(name = "GroupType", required = true)
    protected String groupType;
    @XmlElement(name = "QC_ref", namespace = "http://www.inspection-plusplus.org", required = true)
    protected List<ID4Referencing> qcRef;

    /**
     * Gets the value of the groupType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupType() {
        return groupType;
    }

    /**
     * Sets the value of the groupType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupType(String value) {
        this.groupType = value;
    }

    /**
     * Gets the value of the qcRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the qcRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getQCRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ID4Referencing }
     * 
     * 
     */
    public List<ID4Referencing> getQCRef() {
        if (qcRef == null) {
            qcRef = new ArrayList<ID4Referencing>();
        }
        return this.qcRef;
    }

}
