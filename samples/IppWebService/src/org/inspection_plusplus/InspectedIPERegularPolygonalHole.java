
package org.inspection_plusplus;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Inspected_IPE_RegularPolygonalHole complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Inspected_IPE_RegularPolygonalHole">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Inspected_IPE_Hole">
 *       &lt;sequence>
 *         &lt;element name="InnerDiameter" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="NumberOfCorners" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Inspected_IPE_RegularPolygonalHole", propOrder = {
    "innerDiameter",
    "numberOfCorners"
})
public class InspectedIPERegularPolygonalHole
    extends InspectedIPEHole
{

    @XmlElement(name = "InnerDiameter")
    protected Double innerDiameter;
    @XmlElement(name = "NumberOfCorners")
    protected BigInteger numberOfCorners;

    /**
     * Gets the value of the innerDiameter property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getInnerDiameter() {
        return innerDiameter;
    }

    /**
     * Sets the value of the innerDiameter property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setInnerDiameter(Double value) {
        this.innerDiameter = value;
    }

    /**
     * Gets the value of the numberOfCorners property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfCorners() {
        return numberOfCorners;
    }

    /**
     * Sets the value of the numberOfCorners property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfCorners(BigInteger value) {
        this.numberOfCorners = value;
    }

}
