
package org.inspection_plusplus.service.v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.inspection_plusplus.Extension;
import org.inspection_plusplus.ID4Referencing;
import org.inspection_plusplus.Sender;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PSnodeID" type="{http://www.inspection-plusplus.org}ID4Referencing"/>
 *         &lt;element name="FatherNodeID" type="{http://www.inspection-plusplus.org}ID4Referencing" minOccurs="0"/>
 *         &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element ref="{http://www.inspection-plusplus.org}Sender" minOccurs="0"/>
 *         &lt;element name="Universal" type="{http://www.inspection-plusplus.org}Extension" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pSnodeID",
    "fatherNodeID",
    "version",
    "sender",
    "universal"
})
@XmlRootElement(name = "DeleteProductStructure")
public class DeleteProductStructure {

    @XmlElement(name = "PSnodeID", required = true)
    protected ID4Referencing pSnodeID;
    @XmlElement(name = "FatherNodeID")
    protected ID4Referencing fatherNodeID;
    @XmlElement(name = "Version")
    protected String version;
    @XmlElement(name = "Sender", namespace = "http://www.inspection-plusplus.org")
    protected Sender sender;
    @XmlElement(name = "Universal")
    protected Extension universal;

    /**
     * Gets the value of the pSnodeID property.
     * 
     * @return
     *     possible object is
     *     {@link ID4Referencing }
     *     
     */
    public ID4Referencing getPSnodeID() {
        return pSnodeID;
    }

    /**
     * Sets the value of the pSnodeID property.
     * 
     * @param value
     *     allowed object is
     *     {@link ID4Referencing }
     *     
     */
    public void setPSnodeID(ID4Referencing value) {
        this.pSnodeID = value;
    }

    /**
     * Gets the value of the fatherNodeID property.
     * 
     * @return
     *     possible object is
     *     {@link ID4Referencing }
     *     
     */
    public ID4Referencing getFatherNodeID() {
        return fatherNodeID;
    }

    /**
     * Sets the value of the fatherNodeID property.
     * 
     * @param value
     *     allowed object is
     *     {@link ID4Referencing }
     *     
     */
    public void setFatherNodeID(ID4Referencing value) {
        this.fatherNodeID = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Gets the value of the sender property.
     * 
     * @return
     *     possible object is
     *     {@link Sender }
     *     
     */
    public Sender getSender() {
        return sender;
    }

    /**
     * Sets the value of the sender property.
     * 
     * @param value
     *     allowed object is
     *     {@link Sender }
     *     
     */
    public void setSender(Sender value) {
        this.sender = value;
    }

    /**
     * Gets the value of the universal property.
     * 
     * @return
     *     possible object is
     *     {@link Extension }
     *     
     */
    public Extension getUniversal() {
        return universal;
    }

    /**
     * Sets the value of the universal property.
     * 
     * @param value
     *     allowed object is
     *     {@link Extension }
     *     
     */
    public void setUniversal(Extension value) {
        this.universal = value;
    }

}
