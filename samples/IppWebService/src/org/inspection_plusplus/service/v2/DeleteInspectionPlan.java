
package org.inspection_plusplus.service.v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.inspection_plusplus.Extension;
import org.inspection_plusplus.ID4Referencing;
import org.inspection_plusplus.Sender;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InspectionPlanID" type="{http://www.inspection-plusplus.org}ID4Referencing"/>
 *         &lt;element ref="{http://www.inspection-plusplus.org}Sender" minOccurs="0"/>
 *         &lt;element name="Universal" type="{http://www.inspection-plusplus.org}Extension" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "inspectionPlanID",
    "sender",
    "universal"
})
@XmlRootElement(name = "DeleteInspectionPlan")
public class DeleteInspectionPlan {

    @XmlElement(name = "InspectionPlanID", required = true)
    protected ID4Referencing inspectionPlanID;
    @XmlElement(name = "Sender", namespace = "http://www.inspection-plusplus.org")
    protected Sender sender;
    @XmlElement(name = "Universal")
    protected Extension universal;

    /**
     * Gets the value of the inspectionPlanID property.
     * 
     * @return
     *     possible object is
     *     {@link ID4Referencing }
     *     
     */
    public ID4Referencing getInspectionPlanID() {
        return inspectionPlanID;
    }

    /**
     * Sets the value of the inspectionPlanID property.
     * 
     * @param value
     *     allowed object is
     *     {@link ID4Referencing }
     *     
     */
    public void setInspectionPlanID(ID4Referencing value) {
        this.inspectionPlanID = value;
    }

    /**
     * Gets the value of the sender property.
     * 
     * @return
     *     possible object is
     *     {@link Sender }
     *     
     */
    public Sender getSender() {
        return sender;
    }

    /**
     * Sets the value of the sender property.
     * 
     * @param value
     *     allowed object is
     *     {@link Sender }
     *     
     */
    public void setSender(Sender value) {
        this.sender = value;
    }

    /**
     * Gets the value of the universal property.
     * 
     * @return
     *     possible object is
     *     {@link Extension }
     *     
     */
    public Extension getUniversal() {
        return universal;
    }

    /**
     * Sets the value of the universal property.
     * 
     * @param value
     *     allowed object is
     *     {@link Extension }
     *     
     */
    public void setUniversal(Extension value) {
        this.universal = value;
    }

}
