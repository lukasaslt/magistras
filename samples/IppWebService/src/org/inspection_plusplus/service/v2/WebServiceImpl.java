package org.inspection_plusplus.service.v2;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.xml.ws.Holder;
import javax.xml.ws.WebServiceContext;

import org.inspection_plusplus.AllPScontexts;
import org.inspection_plusplus.Extension;
import org.inspection_plusplus.ID4Referencing;
import org.inspection_plusplus.InspectionPlan;
import org.inspection_plusplus.InspectionPlanList;
import org.inspection_plusplus.InspectionTaskFilter;
import org.inspection_plusplus.InspectionTaskList;
import org.inspection_plusplus.KeyValueOperatorList;
import org.inspection_plusplus.ProductStructures;
import org.inspection_plusplus.ReturnStatus;
import org.inspection_plusplus.Sender;

@WebService(name = "IppDmsServicePortType", targetNamespace = "http://www.inspection-plusplus.org/Service/V2.0")
public class WebServiceImpl implements IppDmsServicePortType
{
	@Resource(name = "wsContext")
	WebServiceContext wsCtxt;

	@Override
	public void loadProductStructures(ID4Referencing pSnodeID, ID4Referencing pScontextID, String version, String mode, Sender sender, Extension universal, Holder<ReturnStatus> returnStatus, Holder<ProductStructures> productStructures) throws Fault
	{
		// TODO Auto-generated method stub

	}

	@Override
	public ReturnStatus storeProductStructures(ProductStructures productStructures, String mode, Sender sender, Extension universal) throws Fault
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ReturnStatus deleteProductStructure(ID4Referencing pSnodeID, ID4Referencing fatherNodeID, String version, Sender sender, Extension universal) throws Fault
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void loadInspectionPlan(ID4Referencing inspectionPlanID, String mode, Sender sender, Extension universal, Holder<ReturnStatus> returnStatus, Holder<InspectionPlan> inspectionPlan) throws Fault
	{
		// TODO Auto-generated method stub

	}

	@Override
	public ReturnStatus storeInspectionPlan(InspectionPlan inspectionPlan, String partVersion, String mode, Sender sender, Extension universal) throws Fault
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ReturnStatus deleteInspectionPlan(ID4Referencing inspectionPlanID, Sender sender, Extension universal) throws Fault
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void loadInspectionTasks(InspectionTaskFilter filter, Sender sender, Extension universal, Holder<ReturnStatus> returnStatus, Holder<InspectionTaskList> inspectionTaskList) throws Fault
	{
		// TODO Auto-generated method stub

	}

	@Override
	public ReturnStatus storeInspectionTasks(InspectionTaskList inspectionTaskList, Sender sender, Extension universal) throws Fault
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ReturnStatus deleteInspectionTask(ID4Referencing inspectionTaskID, Sender sender, Extension universal) throws Fault
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void searchInspectionPlans(KeyValueOperatorList searchKeyList, Sender sender, Extension universal, Holder<ReturnStatus> returnStatus, Holder<InspectionPlanList> inspectionPlanList) throws Fault
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void readAllPScontexts(Sender sender, Extension universal, Holder<ReturnStatus> returnStatus, Holder<AllPScontexts> allPScontexts) throws Fault
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void readPSnodesForPScontext(ID4Referencing pScontextID, String version, Boolean lockedOnly, Sender sender, Extension universal, Holder<ReturnStatus> returnStatus, Holder<ProductStructures> productStructures) throws Fault
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void readIPsForPSnode(ID4Referencing pSnodeID, String partVersion, Boolean lockedOnly, Sender sender, Extension universal, Holder<ReturnStatus> returnStatus, Holder<InspectionPlanList> inspectionPlanList) throws Fault
	{
		// TODO Auto-generated method stub

	}

	@Override
	public ReturnStatus unlockProductStructure(ID4Referencing pSnodeID, String partVersion, Sender sender, Extension universal) throws Fault
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ReturnStatus unlockInspectionPlan(ID4Referencing inspectionPlanID, Sender sender, Extension universal) throws Fault
	{
		// TODO Auto-generated method stub
		return null;
	}
}
