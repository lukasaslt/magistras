
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Inspected_IPE_TruncatedCone complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Inspected_IPE_TruncatedCone">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Inspected_IPE_Cone">
 *       &lt;sequence>
 *         &lt;element name="EffectiveLength" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Inspected_IPE_TruncatedCone", propOrder = {
    "effectiveLength"
})
public class InspectedIPETruncatedCone
    extends InspectedIPECone
{

    @XmlElement(name = "EffectiveLength")
    protected Double effectiveLength;

    /**
     * Gets the value of the effectiveLength property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getEffectiveLength() {
        return effectiveLength;
    }

    /**
     * Sets the value of the effectiveLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setEffectiveLength(Double value) {
        this.effectiveLength = value;
    }

}
