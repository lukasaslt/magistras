
package org.inspection_plusplus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Transportobjekt zum Transport von mehreren Messungen.
 * 
 * <p>Java class for InspectionList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InspectionList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Inspection" type="{http://www.inspection-plusplus.org}Inspection" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InspectionList", propOrder = {
    "inspection"
})
public class InspectionList {

    @XmlElement(name = "Inspection")
    protected List<Inspection> inspection;

    /**
     * Gets the value of the inspection property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the inspection property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInspection().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Inspection }
     * 
     * 
     */
    public List<Inspection> getInspection() {
        if (inspection == null) {
            inspection = new ArrayList<Inspection>();
        }
        return this.inspection;
    }

}
