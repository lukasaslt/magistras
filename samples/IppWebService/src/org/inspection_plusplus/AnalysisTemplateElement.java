
package org.inspection_plusplus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * TODO: Beschreibung �berpr�fen: Ein Analysetemplate Element repr�sentiert ein beliebiges Element, welches bei der Darstellung einer Analyse verwendet wird. In der Regel sind dies Auswerteboxen eines bestimmten Types, die Analyseergebnisse zu einem bestimmten QC beinhalten.
 * 
 * <p>Java class for AnalysisTemplate_Element complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AnalysisTemplate_Element">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Position" type="{http://www.inspection-plusplus.org}Vector3D"/>
 *         &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="QC" type="{http://www.inspection-plusplus.org}QC" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AnalysisTemplate_Element", propOrder = {
    "position",
    "type",
    "qc"
})
public class AnalysisTemplateElement {

    @XmlElement(name = "Position", required = true)
    protected Vector3D position;
    @XmlElement(name = "Type", required = true)
    protected String type;
    @XmlElement(name = "QC", required = true)
    protected List<QC> qc;

    /**
     * Gets the value of the position property.
     * 
     * @return
     *     possible object is
     *     {@link Vector3D }
     *     
     */
    public Vector3D getPosition() {
        return position;
    }

    /**
     * Sets the value of the position property.
     * 
     * @param value
     *     allowed object is
     *     {@link Vector3D }
     *     
     */
    public void setPosition(Vector3D value) {
        this.position = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the qc property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the qc property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getQC().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QC }
     * 
     * 
     */
    public List<QC> getQC() {
        if (qc == null) {
            qc = new ArrayList<QC>();
        }
        return this.qc;
    }

}
