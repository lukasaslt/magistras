
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Kugel
 * 
 * Das geerbte Attribut Origin definiert den Mittelpunkt der Kugel.
 * Das geerbte Attribut MainAxis definiert den Vector, der senkrecht auf der Fl�che am Basispunkt der Kugel steht, also dort, wo die Kugel die Fl�che ber�hrt.
 * Das geerbte Attribut Orientation definiert den Vector senkrecht zur MainAxis.
 * 
 * <p>Java class for IPE_Sphere complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IPE_Sphere">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IPE_Prismatic">
 *       &lt;sequence>
 *         &lt;element name="Diameter" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IPE_Sphere", propOrder = {
    "diameter"
})
@XmlSeeAlso({
    IPEHemiSphere.class
})
public class IPESphere
    extends IPEPrismatic
{

    @XmlElement(name = "Diameter")
    protected double diameter;

    /**
     * Gets the value of the diameter property.
     * 
     */
    public double getDiameter() {
        return diameter;
    }

    /**
     * Sets the value of the diameter property.
     * 
     */
    public void setDiameter(double value) {
        this.diameter = value;
    }

}
