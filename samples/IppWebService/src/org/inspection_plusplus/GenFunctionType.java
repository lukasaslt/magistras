
package org.inspection_plusplus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Repr�sentiert einen Datentyp f�r ein Attribut eines generischen Merkmals, welches als Funktion aus anderen numerischen Attributen realisiert wird.
 * 
 * <p>Java class for Gen_Function_Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Gen_Function_Type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Generic_Attribute_Type">
 *       &lt;sequence>
 *         &lt;element name="Function" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element ref="{http://www.inspection-plusplus.org}Gen_Numerical_Type_ref" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Gen_Function_Type", propOrder = {
    "function",
    "genNumericalTypeRef"
})
public class GenFunctionType
    extends GenericAttributeType
{

    @XmlElement(name = "Function", required = true)
    protected String function;
    @XmlElement(name = "Gen_Numerical_Type_ref", namespace = "http://www.inspection-plusplus.org", required = true)
    protected List<ID4Referencing> genNumericalTypeRef;

    /**
     * Gets the value of the function property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFunction() {
        return function;
    }

    /**
     * Sets the value of the function property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFunction(String value) {
        this.function = value;
    }

    /**
     * Gets the value of the genNumericalTypeRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the genNumericalTypeRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGenNumericalTypeRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ID4Referencing }
     * 
     * 
     */
    public List<ID4Referencing> getGenNumericalTypeRef() {
        if (genNumericalTypeRef == null) {
            genNumericalTypeRef = new ArrayList<ID4Referencing>();
        }
        return this.genNumericalTypeRef;
    }

}
