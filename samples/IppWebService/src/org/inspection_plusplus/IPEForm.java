
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Repr�sentiert ein abgeleitetes Pr�fplanelement, welches zur �bertragung von berechneten Kenngr��en auf Basis einer Menge von referenzierten IPE-Instanzen verwendet wird. Diese k�nnen �ber eine Instanz von Calculation_NominalElement zugeordnet werden.
 * 
 * Das IPE_Form kann beispielsweise verwendet werden, um Gr��en wie Minimum, Maximum, Streuung oder Varianz der einzelnen Merkmale zu beschreiben.
 * 
 * Alle referenzierten IPEs eines IPE_Form m�ssen dabei vom gleichen (Basis-)typ sein.
 *  
 * 
 * <p>Java class for IPE_Form complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IPE_Form">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IPE_Single">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IPE_Form")
public class IPEForm
    extends IPESingle
{


}
