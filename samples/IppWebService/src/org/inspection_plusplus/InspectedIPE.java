
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Abstrake Klasse. Tats�chlich gepr�ftes Pr�fmerkmal. Die weiterne gemessenen Pr�fmerkmale sind in der Beschreibung analog zu den IPEs aus dem Pr�fmerkmalsplan, nur dass die Werte hier, die gemessenen Werte repr�sentieren und nicht die Sollwerte.
 * 
 * <p>Java class for InspectedIPE complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InspectedIPE">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IppDMSentity">
 *       &lt;sequence>
 *         &lt;element name="IPE" type="{http://www.inspection-plusplus.org}IPE" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InspectedIPE", propOrder = {
    "ipe"
})
@XmlSeeAlso({
    InspectedIPEGroup.class,
    InspectedIPESingle.class
})
public abstract class InspectedIPE
    extends IppDMSentity
{

    @XmlElement(name = "IPE")
    protected IPE ipe;

    /**
     * Gets the value of the ipe property.
     * 
     * @return
     *     possible object is
     *     {@link IPE }
     *     
     */
    public IPE getIPE() {
        return ipe;
    }

    /**
     * Sets the value of the ipe property.
     * 
     * @param value
     *     allowed object is
     *     {@link IPE }
     *     
     */
    public void setIPE(IPE value) {
        this.ipe = value;
    }

}
