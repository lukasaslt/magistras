
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Einzelteil. Siehe Beschreibung zu Part. Ein Einzelteil hat keine weiteren dokumentierten Bestandteile (Instanzen von). Ein Einzelteil darf durch die Zuordnung von -Instanzen detailliert werden.
 * 
 * <p>Java class for Part_Single complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Part_Single">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Part">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Part_Single")
public class PartSingle
    extends Part
{


}
