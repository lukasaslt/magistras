
package org.inspection_plusplus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Ein Suchfilter ist f�r alle "Such"-Operationen identisch aufgebaut. Er besteht aus zwei Ebenen. Die obere Ebene enth�lt eine Menge von KeyValueOperatorList-Elementen, die logisch miteinander "ODER"-verkn�pft werden. Jede dieser Listen enth�lt wiederum KeyValueOperator Elemente, die miteinander "UND"-verkn�pft sind.
 * 
 * <p>Java class for SearchFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="KeyValueOperatorList" type="{http://www.inspection-plusplus.org}KeyValueOperatorList" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchFilter", propOrder = {
    "keyValueOperatorList"
})
public class SearchFilter {

    @XmlElement(name = "KeyValueOperatorList", required = true)
    protected List<KeyValueOperatorList> keyValueOperatorList;

    /**
     * Gets the value of the keyValueOperatorList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the keyValueOperatorList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getKeyValueOperatorList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link KeyValueOperatorList }
     * 
     * 
     */
    public List<KeyValueOperatorList> getKeyValueOperatorList() {
        if (keyValueOperatorList == null) {
            keyValueOperatorList = new ArrayList<KeyValueOperatorList>();
        }
        return this.keyValueOperatorList;
    }

}
