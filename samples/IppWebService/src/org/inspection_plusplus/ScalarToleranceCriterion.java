
package org.inspection_plusplus;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Datentyp zur Angabe eines Toleranzkriteriums in Form eines einzelnen Zahlenwertes.
 * 
 * <p>Java class for ScalarToleranceCriterion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ScalarToleranceCriterion">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}ToleranceCriterion">
 *       &lt;sequence>
 *         &lt;element name="MinMax" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ToleranceValue" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ScalarToleranceCriterion", propOrder = {
    "minMax",
    "toleranceValue"
})
public class ScalarToleranceCriterion
    extends ToleranceCriterion
{

    @XmlElement(name = "MinMax", required = true)
    protected String minMax;
    @XmlElement(name = "ToleranceValue", required = true)
    protected BigDecimal toleranceValue;

    /**
     * Gets the value of the minMax property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMinMax() {
        return minMax;
    }

    /**
     * Sets the value of the minMax property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMinMax(String value) {
        this.minMax = value;
    }

    /**
     * Gets the value of the toleranceValue property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getToleranceValue() {
        return toleranceValue;
    }

    /**
     * Sets the value of the toleranceValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setToleranceValue(BigDecimal value) {
        this.toleranceValue = value;
    }

}
