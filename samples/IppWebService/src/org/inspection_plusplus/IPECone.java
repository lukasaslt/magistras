
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Kegel
 * 
 * Das geerbte Attribut Origin definiert den  Basispunkt der Kegelachse (Mittelpunkt der Basiskreisfläche).
 * Das geerbte Attribut MainAxis beginnt im Origin und zeigt auf die Kegelspitze.
 * Das geerbte Attribut Orientation steht senkrecht zur Main Axis.
 * 
 * <p>Java class for IPE_Cone complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IPE_Cone">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IPE_Prismatic">
 *       &lt;sequence>
 *         &lt;element name="Angle" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="Length" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IPE_Cone", propOrder = {
    "angle",
    "length"
})
@XmlSeeAlso({
    IPETruncatedCone.class
})
public class IPECone
    extends IPEPrismatic
{

    @XmlElement(name = "Angle")
    protected double angle;
    @XmlElement(name = "Length")
    protected double length;

    /**
     * Gets the value of the angle property.
     * 
     */
    public double getAngle() {
        return angle;
    }

    /**
     * Sets the value of the angle property.
     * 
     */
    public void setAngle(double value) {
        this.angle = value;
    }

    /**
     * Gets the value of the length property.
     * 
     */
    public double getLength() {
        return length;
    }

    /**
     * Sets the value of the length property.
     * 
     */
    public void setLength(double value) {
        this.length = value;
    }

}
