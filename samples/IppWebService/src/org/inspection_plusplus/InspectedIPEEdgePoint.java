
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Inspected_IPE_EdgePoint complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Inspected_IPE_EdgePoint">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Inspected_IPE_CurvePoint">
 *       &lt;sequence>
 *         &lt;element name="EdgeRadius" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Inspected_IPE_EdgePoint", propOrder = {
    "edgeRadius"
})
public class InspectedIPEEdgePoint
    extends InspectedIPECurvePoint
{

    @XmlElement(name = "EdgeRadius")
    protected Double edgeRadius;

    /**
     * Gets the value of the edgeRadius property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getEdgeRadius() {
        return edgeRadius;
    }

    /**
     * Sets the value of the edgeRadius property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setEdgeRadius(Double value) {
        this.edgeRadius = value;
    }

}
