
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Schl�ssel Wert Paare, die jeweils mit einem Operator versehen werden. Hierdurch kann ein Such-Kriterium entsprechend definiert werden.
 * Beispiel: 
 * Key: "InspectionPlan.QAversion"
 * Value: "1.0"
 * Operator: "<"
 * 
 * Als Operatoren sind folgende Werte f�r die jeweiligen Datentypen zu implementieren:
 * Numerische Datentypen und Kalenderdatum
 * 
 * 	- kleiner
 * 	- kleiner gleich
 * 	- gleich
 * 	- gr��er gleich
 * 	- gr��er
 * 
 * 
 * String-Datentypen:
 * 
 * 	- Gleich - vollst�ndiger String-Vergleich
 * 	- Enth�lt - enth�lt String-Vergleich
 * 
 * <p>Java class for KeyValueOperator complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="KeyValueOperator">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Key" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Operator" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Value" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "KeyValueOperator", propOrder = {
    "key",
    "operator",
    "value"
})
public class KeyValueOperator {

    @XmlElement(name = "Key", required = true)
    protected String key;
    @XmlElement(name = "Operator", required = true)
    protected String operator;
    @XmlElement(name = "Value", required = true)
    protected String value;

    /**
     * Gets the value of the key property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKey() {
        return key;
    }

    /**
     * Sets the value of the key property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKey(String value) {
        this.key = value;
    }

    /**
     * Gets the value of the operator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperator() {
        return operator;
    }

    /**
     * Sets the value of the operator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperator(String value) {
        this.operator = value;
    }

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

}
