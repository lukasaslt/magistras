
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Halbkugel. Siehe Abbildung.
 * 
 * Das geerbte Attribut Origin beschreibt den Mittelpunkt der Halbkugel.
 * Das geerbte Attribut Main Axis gibt die Achse senkrecht zur �quatorialebene an
 * Das geerbte Attribut Orientation gibt den Vektor senkrecht zur Main Axis ab
 * Das geerbte Attribut Diameter gibt den Durchmesser des �quatorialen Kreises an
 * 
 * <p>Java class for IPE_HemiSphere complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IPE_HemiSphere">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IPE_Sphere">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IPE_HemiSphere")
public class IPEHemiSphere
    extends IPESphere
{


}
