
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Loch
 * 
 * Das geerbte Objekt Orientation gibt den Vektor, der senkrecht auf MainAxis steht und definiert i.d.R. die Ausrichtung des Lochs.
 * 
 * <p>Java class for IPE_Hole complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IPE_Hole">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IPE_Geometric">
 *       &lt;sequence>
 *         &lt;element name="DepthRestriction" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="MainAxis" type="{http://www.inspection-plusplus.org}Vector3D"/>
 *         &lt;element name="MainAxis2" type="{http://www.inspection-plusplus.org}Vector3D"/>
 *         &lt;element name="RetractionDepth" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IPE_Hole", propOrder = {
    "depthRestriction",
    "mainAxis",
    "mainAxis2",
    "retractionDepth"
})
@XmlSeeAlso({
    IPERegularPolygonalHole.class,
    IPEOblongHole.class,
    IPERectangularHole.class,
    IPERoundHole.class
})
public class IPEHole
    extends IPEGeometric
{

    @XmlElement(name = "DepthRestriction")
    protected double depthRestriction;
    @XmlElement(name = "MainAxis", required = true)
    protected Vector3D mainAxis;
    @XmlElement(name = "MainAxis2", required = true)
    protected Vector3D mainAxis2;
    @XmlElement(name = "RetractionDepth")
    protected double retractionDepth;

    /**
     * Gets the value of the depthRestriction property.
     * 
     */
    public double getDepthRestriction() {
        return depthRestriction;
    }

    /**
     * Sets the value of the depthRestriction property.
     * 
     */
    public void setDepthRestriction(double value) {
        this.depthRestriction = value;
    }

    /**
     * Gets the value of the mainAxis property.
     * 
     * @return
     *     possible object is
     *     {@link Vector3D }
     *     
     */
    public Vector3D getMainAxis() {
        return mainAxis;
    }

    /**
     * Sets the value of the mainAxis property.
     * 
     * @param value
     *     allowed object is
     *     {@link Vector3D }
     *     
     */
    public void setMainAxis(Vector3D value) {
        this.mainAxis = value;
    }

    /**
     * Gets the value of the mainAxis2 property.
     * 
     * @return
     *     possible object is
     *     {@link Vector3D }
     *     
     */
    public Vector3D getMainAxis2() {
        return mainAxis2;
    }

    /**
     * Sets the value of the mainAxis2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Vector3D }
     *     
     */
    public void setMainAxis2(Vector3D value) {
        this.mainAxis2 = value;
    }

    /**
     * Gets the value of the retractionDepth property.
     * 
     */
    public double getRetractionDepth() {
        return retractionDepth;
    }

    /**
     * Sets the value of the retractionDepth property.
     * 
     */
    public void setRetractionDepth(double value) {
        this.retractionDepth = value;
    }

}
