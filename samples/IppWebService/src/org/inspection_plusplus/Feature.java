
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Ein geometrisches Detail einer Part-Instanz. Repräsentiert ein Konstruktions-Feature. Auf die Ausdetaillierung dieser Klasse wird in I++DMS V1.1 verzichtet. Feature-Instanzen können also als einfache Stellvertreter der Konstruktions-Features betrachtet werden.
 * 
 * Bemerkung:
 * Das geerbte Name Attribut dient als Referenz zum zugehörige Feature im CAD-Datensatz.
 * 
 * <p>Java class for Feature complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Feature">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}GeometricObject">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Feature")
public class Feature
    extends GeometricObject
{


}
