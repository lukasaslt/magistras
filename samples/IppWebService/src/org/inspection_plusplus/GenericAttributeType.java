
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Repr�sentiert einen Datentyp f�r ein Attribut eines generischen Merkmals.
 * 
 * <p>Java class for Generic_Attribute_Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Generic_Attribute_Type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IppDMSentity">
 *       &lt;sequence>
 *         &lt;element name="Code" type="{http://www.inspection-plusplus.org}MultiLanguageString"/>
 *         &lt;element name="Index" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Unit" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Generic_Attribute" type="{http://www.inspection-plusplus.org}Generic_Attribute"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Generic_Attribute_Type", propOrder = {
    "code",
    "index",
    "unit",
    "genericAttribute"
})
@XmlSeeAlso({
    GenNumericalType.class,
    GenFunctionType.class,
    GenEnumerationType.class,
    GenBooleanType.class,
    GenVector3DType.class
})
public class GenericAttributeType
    extends IppDMSentity
{

    @XmlElement(name = "Code", required = true)
    protected MultiLanguageString code;
    @XmlElement(name = "Index")
    protected int index;
    @XmlElement(name = "Unit", required = true)
    protected String unit;
    @XmlElement(name = "Generic_Attribute", required = true)
    protected GenericAttribute genericAttribute;

    /**
     * Gets the value of the code property.
     * 
     * @return
     *     possible object is
     *     {@link MultiLanguageString }
     *     
     */
    public MultiLanguageString getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * 
     * @param value
     *     allowed object is
     *     {@link MultiLanguageString }
     *     
     */
    public void setCode(MultiLanguageString value) {
        this.code = value;
    }

    /**
     * Gets the value of the index property.
     * 
     */
    public int getIndex() {
        return index;
    }

    /**
     * Sets the value of the index property.
     * 
     */
    public void setIndex(int value) {
        this.index = value;
    }

    /**
     * Gets the value of the unit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnit() {
        return unit;
    }

    /**
     * Sets the value of the unit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnit(String value) {
        this.unit = value;
    }

    /**
     * Gets the value of the genericAttribute property.
     * 
     * @return
     *     possible object is
     *     {@link GenericAttribute }
     *     
     */
    public GenericAttribute getGenericAttribute() {
        return genericAttribute;
    }

    /**
     * Sets the value of the genericAttribute property.
     * 
     * @param value
     *     allowed object is
     *     {@link GenericAttribute }
     *     
     */
    public void setGenericAttribute(GenericAttribute value) {
        this.genericAttribute = value;
    }

}
