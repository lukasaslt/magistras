
package org.inspection_plusplus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Pr�fauftrag. Wird verwendet, um den organisatorischen Hintergrund f�r die Erstellung oder �nderung von Bauteilen bzw. Produktstrukturen oder Pr�fmerkmalspl�nen (InspectionPlan) grob zu erfassen und damit mehr Transparenz und Nachvollziehbarkeit in die Datenhaltung zu bekommen. Weiteres siehe Abschnitt InspectionTasks.
 * 
 * <p>Java class for InspectionTask complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InspectionTask">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IppDMSentity">
 *       &lt;sequence>
 *         &lt;element name="Company" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IPQAversion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Motivation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PartQAversion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Project" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element ref="{http://www.inspection-plusplus.org}InspectionTask_ref" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InspectionTask", propOrder = {
    "company",
    "ipqAversion",
    "motivation",
    "partQAversion",
    "project",
    "inspectionTaskRef"
})
public class InspectionTask
    extends IppDMSentity
{

    @XmlElement(name = "Company", required = true)
    protected String company;
    @XmlElement(name = "IPQAversion", required = true)
    protected String ipqAversion;
    @XmlElement(name = "Motivation", required = true)
    protected String motivation;
    @XmlElement(name = "PartQAversion", required = true)
    protected String partQAversion;
    @XmlElement(name = "Project", required = true)
    protected String project;
    @XmlElement(name = "InspectionTask_ref", namespace = "http://www.inspection-plusplus.org")
    protected List<ID4Referencing> inspectionTaskRef;

    /**
     * Gets the value of the company property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompany() {
        return company;
    }

    /**
     * Sets the value of the company property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompany(String value) {
        this.company = value;
    }

    /**
     * Gets the value of the ipqAversion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIPQAversion() {
        return ipqAversion;
    }

    /**
     * Sets the value of the ipqAversion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIPQAversion(String value) {
        this.ipqAversion = value;
    }

    /**
     * Gets the value of the motivation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMotivation() {
        return motivation;
    }

    /**
     * Sets the value of the motivation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMotivation(String value) {
        this.motivation = value;
    }

    /**
     * Gets the value of the partQAversion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartQAversion() {
        return partQAversion;
    }

    /**
     * Sets the value of the partQAversion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartQAversion(String value) {
        this.partQAversion = value;
    }

    /**
     * Gets the value of the project property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProject() {
        return project;
    }

    /**
     * Sets the value of the project property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProject(String value) {
        this.project = value;
    }

    /**
     * Gets the value of the inspectionTaskRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the inspectionTaskRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInspectionTaskRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ID4Referencing }
     * 
     * 
     */
    public List<ID4Referencing> getInspectionTaskRef() {
        if (inspectionTaskRef == null) {
            inspectionTaskRef = new ArrayList<ID4Referencing>();
        }
        return this.inspectionTaskRef;
    }

}
