
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Inspected_IPE_Hole complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Inspected_IPE_Hole">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Inspected_IPE_Geometric">
 *       &lt;sequence>
 *         &lt;element name="DepthRestriction" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="MainAxis" type="{http://www.inspection-plusplus.org}Vector3D" minOccurs="0"/>
 *         &lt;element name="MainAxis2" type="{http://www.inspection-plusplus.org}Vector3D" minOccurs="0"/>
 *         &lt;element name="RetractionDepth" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Inspected_IPE_Hole", propOrder = {
    "depthRestriction",
    "mainAxis",
    "mainAxis2",
    "retractionDepth"
})
@XmlSeeAlso({
    InspectedIPERectangularHole.class,
    InspectedIPEOblongHole.class,
    InspectedIPERegularPolygonalHole.class,
    InspectedIPERoundHole.class
})
public class InspectedIPEHole
    extends InspectedIPEGeometric
{

    @XmlElement(name = "DepthRestriction")
    protected Double depthRestriction;
    @XmlElement(name = "MainAxis")
    protected Vector3D mainAxis;
    @XmlElement(name = "MainAxis2")
    protected Vector3D mainAxis2;
    @XmlElement(name = "RetractionDepth")
    protected Double retractionDepth;

    /**
     * Gets the value of the depthRestriction property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getDepthRestriction() {
        return depthRestriction;
    }

    /**
     * Sets the value of the depthRestriction property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setDepthRestriction(Double value) {
        this.depthRestriction = value;
    }

    /**
     * Gets the value of the mainAxis property.
     * 
     * @return
     *     possible object is
     *     {@link Vector3D }
     *     
     */
    public Vector3D getMainAxis() {
        return mainAxis;
    }

    /**
     * Sets the value of the mainAxis property.
     * 
     * @param value
     *     allowed object is
     *     {@link Vector3D }
     *     
     */
    public void setMainAxis(Vector3D value) {
        this.mainAxis = value;
    }

    /**
     * Gets the value of the mainAxis2 property.
     * 
     * @return
     *     possible object is
     *     {@link Vector3D }
     *     
     */
    public Vector3D getMainAxis2() {
        return mainAxis2;
    }

    /**
     * Sets the value of the mainAxis2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Vector3D }
     *     
     */
    public void setMainAxis2(Vector3D value) {
        this.mainAxis2 = value;
    }

    /**
     * Gets the value of the retractionDepth property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getRetractionDepth() {
        return retractionDepth;
    }

    /**
     * Sets the value of the retractionDepth property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setRetractionDepth(Double value) {
        this.retractionDepth = value;
    }

}
