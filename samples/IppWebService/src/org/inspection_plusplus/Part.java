
package org.inspection_plusplus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Abstrakte Klasse. Bauteil; siehe Beschreibung zu QS-Produktstrukturen.
 * Sie dient, zusammen mit PositionedPart, der Abbildung einer Fertigungsstruktur aus QS-Sicht, (hier auch vereinfacht Produktstruktur oder QS-Produktstruktur genannt). Pr�fmerkmalspl�ne (siehe Klasse InspectionPlan) verweisen auf Instanzen von Part, wodurch ihr Anwendungsbereich dokumentiert wird.
 * 
 * Bemerkung:
 * Der Name, der von von IppDMSentity geerbt wird, ist hier die Sachnummer, die aus dem Stammdatensatz (PDM) stammt.
 * 
 * <p>Java class for Part complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Part">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}GeometricObject">
 *       &lt;sequence>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PDMmaturity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PDMstructureIndicator" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PDMvariant" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PDMversion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="QAmaturity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="QAversion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="QAversionBaseInstance" type="{http://www.inspection-plusplus.org}ID4Referencing"/>
 *         &lt;element name="Feature" type="{http://www.inspection-plusplus.org}Feature" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://www.inspection-plusplus.org}InspectionTask_ref" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Part", propOrder = {
    "description",
    "pdMmaturity",
    "pdMstructureIndicator",
    "pdMvariant",
    "pdMversion",
    "qAmaturity",
    "qAversion",
    "qAversionBaseInstance",
    "feature",
    "inspectionTaskRef"
})
@XmlSeeAlso({
    PartAssembly.class,
    PartSingle.class
})
public abstract class Part
    extends GeometricObject
{

    @XmlElement(name = "Description", required = true)
    protected String description;
    @XmlElement(name = "PDMmaturity", required = true)
    protected String pdMmaturity;
    @XmlElement(name = "PDMstructureIndicator", required = true)
    protected String pdMstructureIndicator;
    @XmlElement(name = "PDMvariant", required = true)
    protected String pdMvariant;
    @XmlElement(name = "PDMversion", required = true)
    protected String pdMversion;
    @XmlElement(name = "QAmaturity", required = true)
    protected String qAmaturity;
    @XmlElement(name = "QAversion", required = true)
    protected String qAversion;
    @XmlElement(name = "QAversionBaseInstance", required = true)
    protected ID4Referencing qAversionBaseInstance;
    @XmlElement(name = "Feature")
    protected List<Feature> feature;
    @XmlElement(name = "InspectionTask_ref", namespace = "http://www.inspection-plusplus.org")
    protected List<ID4Referencing> inspectionTaskRef;

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the pdMmaturity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPDMmaturity() {
        return pdMmaturity;
    }

    /**
     * Sets the value of the pdMmaturity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPDMmaturity(String value) {
        this.pdMmaturity = value;
    }

    /**
     * Gets the value of the pdMstructureIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPDMstructureIndicator() {
        return pdMstructureIndicator;
    }

    /**
     * Sets the value of the pdMstructureIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPDMstructureIndicator(String value) {
        this.pdMstructureIndicator = value;
    }

    /**
     * Gets the value of the pdMvariant property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPDMvariant() {
        return pdMvariant;
    }

    /**
     * Sets the value of the pdMvariant property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPDMvariant(String value) {
        this.pdMvariant = value;
    }

    /**
     * Gets the value of the pdMversion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPDMversion() {
        return pdMversion;
    }

    /**
     * Sets the value of the pdMversion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPDMversion(String value) {
        this.pdMversion = value;
    }

    /**
     * Gets the value of the qAmaturity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQAmaturity() {
        return qAmaturity;
    }

    /**
     * Sets the value of the qAmaturity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQAmaturity(String value) {
        this.qAmaturity = value;
    }

    /**
     * Gets the value of the qAversion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQAversion() {
        return qAversion;
    }

    /**
     * Sets the value of the qAversion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQAversion(String value) {
        this.qAversion = value;
    }

    /**
     * Gets the value of the qAversionBaseInstance property.
     * 
     * @return
     *     possible object is
     *     {@link ID4Referencing }
     *     
     */
    public ID4Referencing getQAversionBaseInstance() {
        return qAversionBaseInstance;
    }

    /**
     * Sets the value of the qAversionBaseInstance property.
     * 
     * @param value
     *     allowed object is
     *     {@link ID4Referencing }
     *     
     */
    public void setQAversionBaseInstance(ID4Referencing value) {
        this.qAversionBaseInstance = value;
    }

    /**
     * Gets the value of the feature property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the feature property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFeature().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Feature }
     * 
     * 
     */
    public List<Feature> getFeature() {
        if (feature == null) {
            feature = new ArrayList<Feature>();
        }
        return this.feature;
    }

    /**
     * Gets the value of the inspectionTaskRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the inspectionTaskRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInspectionTaskRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ID4Referencing }
     * 
     * 
     */
    public List<ID4Referencing> getInspectionTaskRef() {
        if (inspectionTaskRef == null) {
            inspectionTaskRef = new ArrayList<ID4Referencing>();
        }
        return this.inspectionTaskRef;
    }

}
