
package org.inspection_plusplus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Falls eine Gruppe von verschiedenen Pr�fmerkmalen gepr�ft wurde, k�nnen diese �ber eine Gruppe von Pr�fmerkmalen abgebildet werden
 * 
 * <p>Java class for Inspected_IPE_Group complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Inspected_IPE_Group">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}InspectedIPE">
 *       &lt;sequence>
 *         &lt;element name="InspectedIPE" type="{http://www.inspection-plusplus.org}InspectedIPE" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Inspected_IPE_Group", propOrder = {
    "inspectedIPE"
})
public class InspectedIPEGroup
    extends InspectedIPE
{

    @XmlElement(name = "InspectedIPE", required = true)
    protected List<InspectedIPE> inspectedIPE;

    /**
     * Gets the value of the inspectedIPE property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the inspectedIPE property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInspectedIPE().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InspectedIPE }
     * 
     * 
     */
    public List<InspectedIPE> getInspectedIPE() {
        if (inspectedIPE == null) {
            inspectedIPE = new ArrayList<InspectedIPE>();
        }
        return this.inspectedIPE;
    }

}
