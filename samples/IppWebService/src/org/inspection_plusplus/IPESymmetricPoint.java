
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Ein ggf. theoretischer Punkt, der mittig zwischen zwei anderen IPEs liegt. Diese m�ssen durch Calculation_NominalElement definiert werden.
 * 
 * Das geerbte, optionale Attribut MainAxis definiert die Achse zwischen Punkt 1 und Punkt 2.
 * Das geerbte Attribut CalculatedElement muss f�r dieses IPE  auf "true" gesetzt werden.
 * 
 * <p>Java class for IPE_SymmetricPoint complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IPE_SymmetricPoint">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IPE_Point">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IPE_SymmetricPoint")
public class IPESymmetricPoint
    extends IPEPoint
{


}
