
package org.inspection_plusplus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Gruppierung von IPE_Singles oder IPE_Groups nach bestimmten Gesichtspunkten.
 * 
 * <p>Java class for IPE_Group complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IPE_Group">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IPE">
 *       &lt;sequence>
 *         &lt;element name="GroupType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element ref="{http://www.inspection-plusplus.org}IPE_ref" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IPE_Group", propOrder = {
    "groupType",
    "ipeRef"
})
public class IPEGroup
    extends IPE
{

    @XmlElement(name = "GroupType", required = true)
    protected String groupType;
    @XmlElement(name = "IPE_ref", namespace = "http://www.inspection-plusplus.org", required = true)
    protected List<ID4Referencing> ipeRef;

    /**
     * Gets the value of the groupType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupType() {
        return groupType;
    }

    /**
     * Sets the value of the groupType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupType(String value) {
        this.groupType = value;
    }

    /**
     * Gets the value of the ipeRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ipeRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIPERef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ID4Referencing }
     * 
     * 
     */
    public List<ID4Referencing> getIPERef() {
        if (ipeRef == null) {
            ipeRef = new ArrayList<ID4Referencing>();
        }
        return this.ipeRef;
    }

}
