
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Inspected_IPE_RoundHole complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Inspected_IPE_RoundHole">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Inspected_IPE_Hole">
 *       &lt;sequence>
 *         &lt;element name="Diameter" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Inspected_IPE_RoundHole", propOrder = {
    "diameter"
})
@XmlSeeAlso({
    InspectedIPEThreadedHole.class,
    InspectedIPEFlangedRoundHole.class,
    InspectedIPESlot.class
})
public class InspectedIPERoundHole
    extends InspectedIPEHole
{

    @XmlElement(name = "Diameter")
    protected Double diameter;

    /**
     * Gets the value of the diameter property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getDiameter() {
        return diameter;
    }

    /**
     * Sets the value of the diameter property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setDiameter(Double value) {
        this.diameter = value;
    }

}
