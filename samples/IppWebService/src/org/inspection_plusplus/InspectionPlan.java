
package org.inspection_plusplus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Leitinstanz und Transportobjekt f�r einen Pr�fmerkmalsplan (PMP). Enth�lt die Kopfdaten, die die Bez�ge zur QS-Produktstruktur und transportiert alle direkten und indirekten Inhalte. Ein PMP wird von der Pr�fmerkmalsplanung erstellt und von der Messprogrammierung gelesen, um dann die Erstellung von Messprogrammen zu steuern.
 * 
 * <p>Java class for InspectionPlan complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InspectionPlan">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IppDMSentity">
 *       &lt;sequence>
 *         &lt;element name="InspectionCategories" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IPsymmetry" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PartQAversion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PDMmaturity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="QAmaturity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="QAversion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Comment" type="{http://www.inspection-plusplus.org}Comment" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Tolerance" type="{http://www.inspection-plusplus.org}Tolerance" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://www.inspection-plusplus.org}Part_ref" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="IPE" type="{http://www.inspection-plusplus.org}IPE" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ReferenceSystem" type="{http://www.inspection-plusplus.org}ReferenceSystem" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://www.inspection-plusplus.org}InspectionTask_ref" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="QC" type="{http://www.inspection-plusplus.org}QC" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Generic_IPE_Type" type="{http://www.inspection-plusplus.org}Generic_IPE_Type" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InspectionPlan", propOrder = {
    "inspectionCategories",
    "iPsymmetry",
    "partQAversion",
    "pdMmaturity",
    "qAmaturity",
    "qAversion",
    "comment",
    "tolerance",
    "partRef",
    "ipe",
    "referenceSystem",
    "inspectionTaskRef",
    "qc",
    "genericIPEType"
})
public class InspectionPlan
    extends IppDMSentity
{

    @XmlElement(name = "InspectionCategories", required = true)
    protected String inspectionCategories;
    @XmlElement(name = "IPsymmetry", required = true)
    protected String iPsymmetry;
    @XmlElement(name = "PartQAversion", required = true)
    protected String partQAversion;
    @XmlElement(name = "PDMmaturity", required = true)
    protected String pdMmaturity;
    @XmlElement(name = "QAmaturity", required = true)
    protected String qAmaturity;
    @XmlElement(name = "QAversion", required = true)
    protected String qAversion;
    @XmlElement(name = "Comment")
    protected List<Comment> comment;
    @XmlElement(name = "Tolerance")
    protected List<Tolerance> tolerance;
    @XmlElement(name = "Part_ref", namespace = "http://www.inspection-plusplus.org")
    protected List<ID4Referencing> partRef;
    @XmlElement(name = "IPE")
    protected List<IPE> ipe;
    @XmlElement(name = "ReferenceSystem")
    protected List<ReferenceSystem> referenceSystem;
    @XmlElement(name = "InspectionTask_ref", namespace = "http://www.inspection-plusplus.org")
    protected List<ID4Referencing> inspectionTaskRef;
    @XmlElement(name = "QC")
    protected List<QC> qc;
    @XmlElement(name = "Generic_IPE_Type")
    protected List<GenericIPEType> genericIPEType;

    /**
     * Gets the value of the inspectionCategories property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInspectionCategories() {
        return inspectionCategories;
    }

    /**
     * Sets the value of the inspectionCategories property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInspectionCategories(String value) {
        this.inspectionCategories = value;
    }

    /**
     * Gets the value of the iPsymmetry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIPsymmetry() {
        return iPsymmetry;
    }

    /**
     * Sets the value of the iPsymmetry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIPsymmetry(String value) {
        this.iPsymmetry = value;
    }

    /**
     * Gets the value of the partQAversion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartQAversion() {
        return partQAversion;
    }

    /**
     * Sets the value of the partQAversion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartQAversion(String value) {
        this.partQAversion = value;
    }

    /**
     * Gets the value of the pdMmaturity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPDMmaturity() {
        return pdMmaturity;
    }

    /**
     * Sets the value of the pdMmaturity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPDMmaturity(String value) {
        this.pdMmaturity = value;
    }

    /**
     * Gets the value of the qAmaturity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQAmaturity() {
        return qAmaturity;
    }

    /**
     * Sets the value of the qAmaturity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQAmaturity(String value) {
        this.qAmaturity = value;
    }

    /**
     * Gets the value of the qAversion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQAversion() {
        return qAversion;
    }

    /**
     * Sets the value of the qAversion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQAversion(String value) {
        this.qAversion = value;
    }

    /**
     * Gets the value of the comment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the comment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Comment }
     * 
     * 
     */
    public List<Comment> getComment() {
        if (comment == null) {
            comment = new ArrayList<Comment>();
        }
        return this.comment;
    }

    /**
     * Gets the value of the tolerance property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tolerance property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTolerance().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tolerance }
     * 
     * 
     */
    public List<Tolerance> getTolerance() {
        if (tolerance == null) {
            tolerance = new ArrayList<Tolerance>();
        }
        return this.tolerance;
    }

    /**
     * Gets the value of the partRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the partRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPartRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ID4Referencing }
     * 
     * 
     */
    public List<ID4Referencing> getPartRef() {
        if (partRef == null) {
            partRef = new ArrayList<ID4Referencing>();
        }
        return this.partRef;
    }

    /**
     * Gets the value of the ipe property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ipe property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIPE().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IPE }
     * 
     * 
     */
    public List<IPE> getIPE() {
        if (ipe == null) {
            ipe = new ArrayList<IPE>();
        }
        return this.ipe;
    }

    /**
     * Gets the value of the referenceSystem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the referenceSystem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReferenceSystem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReferenceSystem }
     * 
     * 
     */
    public List<ReferenceSystem> getReferenceSystem() {
        if (referenceSystem == null) {
            referenceSystem = new ArrayList<ReferenceSystem>();
        }
        return this.referenceSystem;
    }

    /**
     * Gets the value of the inspectionTaskRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the inspectionTaskRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInspectionTaskRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ID4Referencing }
     * 
     * 
     */
    public List<ID4Referencing> getInspectionTaskRef() {
        if (inspectionTaskRef == null) {
            inspectionTaskRef = new ArrayList<ID4Referencing>();
        }
        return this.inspectionTaskRef;
    }

    /**
     * Gets the value of the qc property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the qc property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getQC().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QC }
     * 
     * 
     */
    public List<QC> getQC() {
        if (qc == null) {
            qc = new ArrayList<QC>();
        }
        return this.qc;
    }

    /**
     * Gets the value of the genericIPEType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the genericIPEType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGenericIPEType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GenericIPEType }
     * 
     * 
     */
    public List<GenericIPEType> getGenericIPEType() {
        if (genericIPEType == null) {
            genericIPEType = new ArrayList<GenericIPEType>();
        }
        return this.genericIPEType;
    }

}
