
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Kegelstumpf (Frustrum). Siehe Abbildung zu IPE_Cone.
 * 
 * <p>Java class for IPE_TruncatedCone complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IPE_TruncatedCone">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IPE_Cone">
 *       &lt;sequence>
 *         &lt;element name="EffectiveLength" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IPE_TruncatedCone", propOrder = {
    "effectiveLength"
})
public class IPETruncatedCone
    extends IPECone
{

    @XmlElement(name = "EffectiveLength")
    protected double effectiveLength;

    /**
     * Gets the value of the effectiveLength property.
     * 
     */
    public double getEffectiveLength() {
        return effectiveLength;
    }

    /**
     * Sets the value of the effectiveLength property.
     * 
     */
    public void setEffectiveLength(double value) {
        this.effectiveLength = value;
    }

}
