
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Inspected_IPE_KeyHole complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Inspected_IPE_KeyHole">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Inspected_IPE_Slot">
 *       &lt;sequence>
 *         &lt;element name="Diameter2" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="RoundingRadius" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Inspected_IPE_KeyHole", propOrder = {
    "diameter2",
    "roundingRadius"
})
public class InspectedIPEKeyHole
    extends InspectedIPESlot
{

    @XmlElement(name = "Diameter2")
    protected Double diameter2;
    @XmlElement(name = "RoundingRadius")
    protected Double roundingRadius;

    /**
     * Gets the value of the diameter2 property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getDiameter2() {
        return diameter2;
    }

    /**
     * Sets the value of the diameter2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setDiameter2(Double value) {
        this.diameter2 = value;
    }

    /**
     * Gets the value of the roundingRadius property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getRoundingRadius() {
        return roundingRadius;
    }

    /**
     * Sets the value of the roundingRadius property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setRoundingRadius(Double value) {
        this.roundingRadius = value;
    }

}
