
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Inspected_IPE_RoundedRectangularHole complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Inspected_IPE_RoundedRectangularHole">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Inspected_IPE_RectangularHole">
 *       &lt;sequence>
 *         &lt;element name="CornerRadius" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Inspected_IPE_RoundedRectangularHole", propOrder = {
    "cornerRadius"
})
public class InspectedIPERoundedRectangularHole
    extends InspectedIPERectangularHole
{

    @XmlElement(name = "CornerRadius")
    protected Double cornerRadius;

    /**
     * Gets the value of the cornerRadius property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getCornerRadius() {
        return cornerRadius;
    }

    /**
     * Sets the value of the cornerRadius property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setCornerRadius(Double value) {
        this.cornerRadius = value;
    }

}
