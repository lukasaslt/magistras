
package org.inspection_plusplus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Abstrakte Basisklasse f�r die meisten I++DMS-Klassen, mit Ausnahme von Klassen, die Transportobjekte, Datentypen oder Referenzen repr�sentieren.
 * 
 * <p>Java class for IppDMSentity complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IppDMSentity">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SystemID" type="{http://www.inspection-plusplus.org}ID4Objects"/>
 *         &lt;element ref="{http://www.inspection-plusplus.org}Comment_ref" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Extension" type="{http://www.inspection-plusplus.org}Extension" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="DisplayID" type="{http://www.inspection-plusplus.org}DisplayID" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="History" type="{http://www.inspection-plusplus.org}History" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IppDMSentity", propOrder = {
    "name",
    "systemID",
    "commentRef",
    "extension",
    "displayID",
    "history"
})
@XmlSeeAlso({
    InspectionPlan.class,
    GeometricObject.class,
    InspectionProgram.class,
    ProductStructureContext.class,
    QC.class,
    Comment.class,
    Picture.class,
    DatumTarget.class,
    ToleranceSource.class,
    Datum.class,
    PositionedPart.class,
    InspectionTask.class,
    GenericAttributeType.class,
    GenericIPEType.class,
    AnalysisTemplate.class,
    AlignmentStrategy.class,
    Inspection.class,
    Tolerance.class,
    Calculation.class,
    InspectedIPE.class,
    ReferenceSystem.class,
    IPE.class,
    GenericAttribute.class
})
public class IppDMSentity {

    @XmlElement(name = "Name", required = true)
    protected String name;
    @XmlElement(name = "SystemID", required = true)
    protected ID4Objects systemID;
    @XmlElement(name = "Comment_ref", namespace = "http://www.inspection-plusplus.org")
    protected List<ID4Referencing> commentRef;
    @XmlElement(name = "Extension")
    protected List<Extension> extension;
    @XmlElement(name = "DisplayID")
    protected List<DisplayID> displayID;
    @XmlElement(name = "History")
    protected List<History> history;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the systemID property.
     * 
     * @return
     *     possible object is
     *     {@link ID4Objects }
     *     
     */
    public ID4Objects getSystemID() {
        return systemID;
    }

    /**
     * Sets the value of the systemID property.
     * 
     * @param value
     *     allowed object is
     *     {@link ID4Objects }
     *     
     */
    public void setSystemID(ID4Objects value) {
        this.systemID = value;
    }

    /**
     * Gets the value of the commentRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the commentRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCommentRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ID4Referencing }
     * 
     * 
     */
    public List<ID4Referencing> getCommentRef() {
        if (commentRef == null) {
            commentRef = new ArrayList<ID4Referencing>();
        }
        return this.commentRef;
    }

    /**
     * Gets the value of the extension property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the extension property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExtension().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Extension }
     * 
     * 
     */
    public List<Extension> getExtension() {
        if (extension == null) {
            extension = new ArrayList<Extension>();
        }
        return this.extension;
    }

    /**
     * Gets the value of the displayID property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the displayID property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDisplayID().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DisplayID }
     * 
     * 
     */
    public List<DisplayID> getDisplayID() {
        if (displayID == null) {
            displayID = new ArrayList<DisplayID>();
        }
        return this.displayID;
    }

    /**
     * Gets the value of the history property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the history property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHistory().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link History }
     * 
     * 
     */
    public List<History> getHistory() {
        if (history == null) {
            history = new ArrayList<History>();
        }
        return this.history;
    }

}
