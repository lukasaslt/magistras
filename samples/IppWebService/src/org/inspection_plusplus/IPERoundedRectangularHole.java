
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Gerundetes Rechteckloch. 
 * 
 * <p>Java class for IPE_RoundedRectangularHole complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IPE_RoundedRectangularHole">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IPE_RectangularHole">
 *       &lt;sequence>
 *         &lt;element name="CornerRadius" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IPE_RoundedRectangularHole", propOrder = {
    "cornerRadius"
})
public class IPERoundedRectangularHole
    extends IPERectangularHole
{

    @XmlElement(name = "CornerRadius")
    protected double cornerRadius;

    /**
     * Gets the value of the cornerRadius property.
     * 
     */
    public double getCornerRadius() {
        return cornerRadius;
    }

    /**
     * Sets the value of the cornerRadius property.
     * 
     */
    public void setCornerRadius(double value) {
        this.cornerRadius = value;
    }

}
