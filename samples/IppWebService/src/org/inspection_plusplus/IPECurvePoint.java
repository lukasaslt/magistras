
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Ein Punkt auf einer oder mehreren Kurven, welche durch Calculation_NominalElement angegeben werden k�nnen. 
 * 
 * Das geerbte, erforderlich Attribut MainAxis definiert die Normalenrichtung einer Kurvenseite. Zeigt immer in die Richtung, aus der das IPE gemessen werden sollte.
 * 
 * <p>Java class for IPE_CurvePoint complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IPE_CurvePoint">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IPE_Point">
 *       &lt;sequence>
 *         &lt;element name="MainAxis2" type="{http://www.inspection-plusplus.org}Vector3D"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IPE_CurvePoint", propOrder = {
    "mainAxis2"
})
@XmlSeeAlso({
    IPEEdgePoint.class,
    IPECutPoint.class,
    IPEVertexPoint.class
})
public class IPECurvePoint
    extends IPEPoint
{

    @XmlElement(name = "MainAxis2", required = true)
    protected Vector3D mainAxis2;

    /**
     * Gets the value of the mainAxis2 property.
     * 
     * @return
     *     possible object is
     *     {@link Vector3D }
     *     
     */
    public Vector3D getMainAxis2() {
        return mainAxis2;
    }

    /**
     * Sets the value of the mainAxis2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Vector3D }
     *     
     */
    public void setMainAxis2(Vector3D value) {
        this.mainAxis2 = value;
    }

}
