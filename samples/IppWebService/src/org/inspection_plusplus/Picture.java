
package org.inspection_plusplus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * TODO Beschreibung pr�fen: Ein Analysetemplate basiert immer auf einem Bild, welches aus der CAD Zeichnung des Bauteils erstellt wird. Dieses Bild zeigt eine 2D Sicht auf das Bauteil, an dem die verschiedenen Messpunkte auf dem Analysetemplate mit Hilfe von AnalysisTemplate_Elements dargestellt werden.
 * 
 * <p>Java class for Picture complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Picture">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IppDMSentity">
 *       &lt;sequence>
 *         &lt;element name="TransformationMatrix" type="{http://www.inspection-plusplus.org}TransformationMatrix"/>
 *         &lt;element name="Part" type="{http://www.inspection-plusplus.org}Part"/>
 *         &lt;element name="Comment" type="{http://www.inspection-plusplus.org}Comment" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Picture", propOrder = {
    "transformationMatrix",
    "part",
    "comment"
})
public class Picture
    extends IppDMSentity
{

    @XmlElement(name = "TransformationMatrix", required = true)
    protected TransformationMatrix transformationMatrix;
    @XmlElement(name = "Part", required = true)
    protected Part part;
    @XmlElement(name = "Comment")
    protected List<Comment> comment;

    /**
     * Gets the value of the transformationMatrix property.
     * 
     * @return
     *     possible object is
     *     {@link TransformationMatrix }
     *     
     */
    public TransformationMatrix getTransformationMatrix() {
        return transformationMatrix;
    }

    /**
     * Sets the value of the transformationMatrix property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransformationMatrix }
     *     
     */
    public void setTransformationMatrix(TransformationMatrix value) {
        this.transformationMatrix = value;
    }

    /**
     * Gets the value of the part property.
     * 
     * @return
     *     possible object is
     *     {@link Part }
     *     
     */
    public Part getPart() {
        return part;
    }

    /**
     * Sets the value of the part property.
     * 
     * @param value
     *     allowed object is
     *     {@link Part }
     *     
     */
    public void setPart(Part value) {
        this.part = value;
    }

    /**
     * Gets the value of the comment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the comment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Comment }
     * 
     * 
     */
    public List<Comment> getComment() {
        if (comment == null) {
            comment = new ArrayList<Comment>();
        }
        return this.comment;
    }

}
