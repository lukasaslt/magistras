
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Abstrakte Klasse. Objekt, f�r das eine detaillierte CAD-Repr�sentation (au�erhalb von I++DMS) existiert. Superklasse der Klassen Part und Feature.
 * 
 * <p>Java class for GeometricObject complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GeometricObject">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IppDMSentity">
 *       &lt;sequence>
 *         &lt;element name="Location" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PartAdaptor" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Symmetry" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GeometricObject", propOrder = {
    "location",
    "partAdaptor",
    "symmetry"
})
@XmlSeeAlso({
    Feature.class,
    Part.class
})
public abstract class GeometricObject
    extends IppDMSentity
{

    @XmlElement(name = "Location", required = true)
    protected String location;
    @XmlElement(name = "PartAdaptor")
    protected boolean partAdaptor;
    @XmlElement(name = "Symmetry", required = true)
    protected String symmetry;

    /**
     * Gets the value of the location property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocation() {
        return location;
    }

    /**
     * Sets the value of the location property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocation(String value) {
        this.location = value;
    }

    /**
     * Gets the value of the partAdaptor property.
     * 
     */
    public boolean isPartAdaptor() {
        return partAdaptor;
    }

    /**
     * Sets the value of the partAdaptor property.
     * 
     */
    public void setPartAdaptor(boolean value) {
        this.partAdaptor = value;
    }

    /**
     * Gets the value of the symmetry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSymmetry() {
        return symmetry;
    }

    /**
     * Sets the value of the symmetry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSymmetry(String value) {
        this.symmetry = value;
    }

}
