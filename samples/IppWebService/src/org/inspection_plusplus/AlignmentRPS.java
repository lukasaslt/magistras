
package org.inspection_plusplus;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Alignment_RPS complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Alignment_RPS">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}AlignmentStrategy">
 *       &lt;sequence>
 *         &lt;element name="MaxNrOfIterations" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Alignment_RPS", propOrder = {
    "maxNrOfIterations"
})
public class AlignmentRPS
    extends AlignmentStrategy
{

    @XmlElement(name = "MaxNrOfIterations", required = true)
    protected BigInteger maxNrOfIterations;

    /**
     * Gets the value of the maxNrOfIterations property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaxNrOfIterations() {
        return maxNrOfIterations;
    }

    /**
     * Sets the value of the maxNrOfIterations property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaxNrOfIterations(BigInteger value) {
        this.maxNrOfIterations = value;
    }

}
