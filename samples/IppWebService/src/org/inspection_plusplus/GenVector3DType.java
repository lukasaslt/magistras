
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Repr�sentiert einen Datentyp f�r ein vektorielles Attribut eines generischen Merkmals.
 * 
 *  
 * 
 * <p>Java class for Gen_Vector3D_Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Gen_Vector3D_Type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Generic_Attribute_Type">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Gen_Vector3D_Type")
public class GenVector3DType
    extends GenericAttributeType
{


}
