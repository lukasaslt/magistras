
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Toleriert die Lage mehrerer Geometrien (in der Regel L�cher) zueinander.
 * 
 * <p>Java class for Tol_Pattern complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Tol_Pattern">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Tol_Form">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tol_Pattern")
public class TolPattern
    extends TolForm
{


}
