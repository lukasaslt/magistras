
package org.inspection_plusplus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Transportobjekt, das eine oder mehrere Produktstrukturen mit Versionsangabe enthalten kann. Siehe auch Abschnitt �ber QS-Produktstrukturen. Transportobjekte werden als Parameter oder R�ckgabewerte von Operationen verwendet.
 * 
 * <p>Java class for ProductStructures complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProductStructures">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Comment" type="{http://www.inspection-plusplus.org}Comment" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ProductStructure" type="{http://www.inspection-plusplus.org}ProductStructure" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProductStructures", propOrder = {
    "version",
    "comment",
    "productStructure"
})
public class ProductStructures {

    @XmlElement(name = "Version", required = true)
    protected String version;
    @XmlElement(name = "Comment")
    protected List<Comment> comment;
    @XmlElement(name = "ProductStructure")
    protected List<ProductStructure> productStructure;

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Gets the value of the comment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the comment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Comment }
     * 
     * 
     */
    public List<Comment> getComment() {
        if (comment == null) {
            comment = new ArrayList<Comment>();
        }
        return this.comment;
    }

    /**
     * Gets the value of the productStructure property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the productStructure property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProductStructure().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProductStructure }
     * 
     * 
     */
    public List<ProductStructure> getProductStructure() {
        if (productStructure == null) {
            productStructure = new ArrayList<ProductStructure>();
        }
        return this.productStructure;
    }

}
