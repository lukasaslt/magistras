
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Abstrakte Klasse. Ein InspectionPlanElement, dessen Form in der I++DMS-Schnittstelle nicht beschrieben wird. Stattdessen wird eine Referenz auf das zugeh�rige CAD-Objekt verwendet. Die genaue Form kann z.B. �ber eine CAD-Schnittstelle �bertragen werden. 
 * 
 * <p>Java class for IPE_Freeform complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IPE_Freeform">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IPE_Geometric">
 *       &lt;sequence>
 *         &lt;element name="GeoObjectReference" type="{http://www.inspection-plusplus.org}ID4Referencing"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IPE_Freeform", propOrder = {
    "geoObjectReference"
})
@XmlSeeAlso({
    IPECurve.class,
    IPEFace.class
})
public abstract class IPEFreeform
    extends IPEGeometric
{

    @XmlElement(name = "GeoObjectReference", required = true)
    protected ID4Referencing geoObjectReference;

    /**
     * Gets the value of the geoObjectReference property.
     * 
     * @return
     *     possible object is
     *     {@link ID4Referencing }
     *     
     */
    public ID4Referencing getGeoObjectReference() {
        return geoObjectReference;
    }

    /**
     * Sets the value of the geoObjectReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link ID4Referencing }
     *     
     */
    public void setGeoObjectReference(ID4Referencing value) {
        this.geoObjectReference = value;
    }

}
