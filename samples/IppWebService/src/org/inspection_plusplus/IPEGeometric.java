
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Abstrakte Klasse. Ein IPE mit geometrischen Eigenschaften.
 * 
 * <p>Java class for IPE_Geometric complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IPE_Geometric">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IPE_Single">
 *       &lt;sequence>
 *         &lt;element name="MaterialThickness" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="MoveByMaterialThickness" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Orientation" type="{http://www.inspection-plusplus.org}Vector3D"/>
 *         &lt;element name="Origin" type="{http://www.inspection-plusplus.org}Vector3D"/>
 *         &lt;element name="TouchDirection" type="{http://www.inspection-plusplus.org}Vector3D"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IPE_Geometric", propOrder = {
    "materialThickness",
    "moveByMaterialThickness",
    "orientation",
    "origin",
    "touchDirection"
})
@XmlSeeAlso({
    IPECircle.class,
    IPERoundedEdgeJoint.class,
    IPEPoint.class,
    IPELine.class,
    IPEPrismatic.class,
    IPEFreeform.class,
    IPEPlane.class,
    IPEHole.class
})
public class IPEGeometric
    extends IPESingle
{

    @XmlElement(name = "MaterialThickness")
    protected Double materialThickness;
    @XmlElement(name = "MoveByMaterialThickness")
    protected boolean moveByMaterialThickness;
    @XmlElement(name = "Orientation", required = true)
    protected Vector3D orientation;
    @XmlElement(name = "Origin", required = true)
    protected Vector3D origin;
    @XmlElement(name = "TouchDirection", required = true)
    protected Vector3D touchDirection;

    /**
     * Gets the value of the materialThickness property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getMaterialThickness() {
        return materialThickness;
    }

    /**
     * Sets the value of the materialThickness property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setMaterialThickness(Double value) {
        this.materialThickness = value;
    }

    /**
     * Gets the value of the moveByMaterialThickness property.
     * 
     */
    public boolean isMoveByMaterialThickness() {
        return moveByMaterialThickness;
    }

    /**
     * Sets the value of the moveByMaterialThickness property.
     * 
     */
    public void setMoveByMaterialThickness(boolean value) {
        this.moveByMaterialThickness = value;
    }

    /**
     * Gets the value of the orientation property.
     * 
     * @return
     *     possible object is
     *     {@link Vector3D }
     *     
     */
    public Vector3D getOrientation() {
        return orientation;
    }

    /**
     * Sets the value of the orientation property.
     * 
     * @param value
     *     allowed object is
     *     {@link Vector3D }
     *     
     */
    public void setOrientation(Vector3D value) {
        this.orientation = value;
    }

    /**
     * Gets the value of the origin property.
     * 
     * @return
     *     possible object is
     *     {@link Vector3D }
     *     
     */
    public Vector3D getOrigin() {
        return origin;
    }

    /**
     * Sets the value of the origin property.
     * 
     * @param value
     *     allowed object is
     *     {@link Vector3D }
     *     
     */
    public void setOrigin(Vector3D value) {
        this.origin = value;
    }

    /**
     * Gets the value of the touchDirection property.
     * 
     * @return
     *     possible object is
     *     {@link Vector3D }
     *     
     */
    public Vector3D getTouchDirection() {
        return touchDirection;
    }

    /**
     * Sets the value of the touchDirection property.
     * 
     * @param value
     *     allowed object is
     *     {@link Vector3D }
     *     
     */
    public void setTouchDirection(Vector3D value) {
        this.touchDirection = value;
    }

}
