
package org.inspection_plusplus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Repr�sentiert einen Datentyp f�r ein enumeratives Attribut eines generischen Merkmals.
 * 
 * <p>Java class for Gen_Enumeration_Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Gen_Enumeration_Type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Generic_Attribute_Type">
 *       &lt;sequence>
 *         &lt;element name="Enum_Values" type="{http://www.inspection-plusplus.org}MultiLanguageString" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Gen_Enumeration_Type", propOrder = {
    "enumValues"
})
public class GenEnumerationType
    extends GenericAttributeType
{

    @XmlElement(name = "Enum_Values", required = true)
    protected List<MultiLanguageString> enumValues;

    /**
     * Gets the value of the enumValues property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the enumValues property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEnumValues().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MultiLanguageString }
     * 
     * 
     */
    public List<MultiLanguageString> getEnumValues() {
        if (enumValues == null) {
            enumValues = new ArrayList<MultiLanguageString>();
        }
        return this.enumValues;
    }

}
