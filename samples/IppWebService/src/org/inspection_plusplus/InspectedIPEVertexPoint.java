
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Inspected_IPE_VertexPoint complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Inspected_IPE_VertexPoint">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Inspected_IPE_CurvePoint">
 *       &lt;sequence>
 *         &lt;element name="MainAxis3" type="{http://www.inspection-plusplus.org}Vector3D" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Inspected_IPE_VertexPoint", propOrder = {
    "mainAxis3"
})
public class InspectedIPEVertexPoint
    extends InspectedIPECurvePoint
{

    @XmlElement(name = "MainAxis3")
    protected Vector3D mainAxis3;

    /**
     * Gets the value of the mainAxis3 property.
     * 
     * @return
     *     possible object is
     *     {@link Vector3D }
     *     
     */
    public Vector3D getMainAxis3() {
        return mainAxis3;
    }

    /**
     * Sets the value of the mainAxis3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Vector3D }
     *     
     */
    public void setMainAxis3(Vector3D value) {
        this.mainAxis3 = value;
    }

}
