
package org.inspection_plusplus;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * R�ckgabeinformation f�r Operationen. Optional k�nnen �ber die �Komposition 1� Detailinformationen zu einzelnen Objekten in den globalen ReturnStatus eingebettet werden. Dazu werden einzelne betroffene ObjectStatus-Instanzen genannt und dokumentiert.
 * 
 * <p>Java class for ReturnStatus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReturnStatus">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Message" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="ObjectStatus" type="{http://www.inspection-plusplus.org}ObjectStatus" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReturnStatus", propOrder = {
    "message",
    "status",
    "objectStatus"
})
public class ReturnStatus {

    @XmlElement(name = "Message", required = true)
    protected String message;
    @XmlElement(name = "Status", required = true)
    protected BigInteger status;
    @XmlElement(name = "ObjectStatus")
    protected List<ObjectStatus> objectStatus;

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessage(String value) {
        this.message = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setStatus(BigInteger value) {
        this.status = value;
    }

    /**
     * Gets the value of the objectStatus property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the objectStatus property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getObjectStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjectStatus }
     * 
     * 
     */
    public List<ObjectStatus> getObjectStatus() {
        if (objectStatus == null) {
            objectStatus = new ArrayList<ObjectStatus>();
        }
        return this.objectStatus;
    }

}
