
package org.inspection_plusplus;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Datentyp zur Angabe eines Toleranzkriteriums in Form eines Intervalls.
 * 
 * <p>Java class for IntervalToleranceCriterion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IntervalToleranceCriterion">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}ToleranceCriterion">
 *       &lt;sequence>
 *         &lt;element name="InOut" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LowerToleranceValue" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="UpperToleranceValue" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IntervalToleranceCriterion", propOrder = {
    "inOut",
    "lowerToleranceValue",
    "upperToleranceValue"
})
public class IntervalToleranceCriterion
    extends ToleranceCriterion
{

    @XmlElement(name = "InOut", required = true)
    protected String inOut;
    @XmlElement(name = "LowerToleranceValue", required = true)
    protected BigDecimal lowerToleranceValue;
    @XmlElement(name = "UpperToleranceValue", required = true)
    protected BigDecimal upperToleranceValue;

    /**
     * Gets the value of the inOut property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInOut() {
        return inOut;
    }

    /**
     * Sets the value of the inOut property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInOut(String value) {
        this.inOut = value;
    }

    /**
     * Gets the value of the lowerToleranceValue property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLowerToleranceValue() {
        return lowerToleranceValue;
    }

    /**
     * Sets the value of the lowerToleranceValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLowerToleranceValue(BigDecimal value) {
        this.lowerToleranceValue = value;
    }

    /**
     * Gets the value of the upperToleranceValue property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getUpperToleranceValue() {
        return upperToleranceValue;
    }

    /**
     * Sets the value of the upperToleranceValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setUpperToleranceValue(BigDecimal value) {
        this.upperToleranceValue = value;
    }

}
