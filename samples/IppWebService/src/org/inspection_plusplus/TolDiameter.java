
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Subklasse von Tol_Size. Durchmessertoleranz. Beschreibt die erlaubte Abweichung eines Objekts von seinem Solldurchmesser. Bezieht sich immer auf das Attribut Diameter eines geometrischen Objekts / IPEs.
 * 
 * <p>Java class for Tol_Diameter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Tol_Diameter">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Tol_LinearSize">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tol_Diameter")
public class TolDiameter
    extends TolLinearSize
{


}
