
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Instanzen k�nnen in beliebige Element-Instanzen eingebettet werden. Sprachspezifische Zusatzinformation �ber Bezeichnungen f�r das betreffende Objekt. Diese Klasse ist im Zusammenhang mit IppDMSentity.Name zu sehen: Verwendungsregel: Sowohl der Inhalt des Attributs IppDMSentity.Name als auch Instanzen der Klasse DisplayID dienen dazu, einem Anwender gegen�ber als identifizierendes Merkmal einer Instanz angezeigt zu werden. Wenn eine DisplayID-Instanz f�r die vom Anwender bevorzugte Sprache existiert, dann ist diese anstelle von IppDMSentity.Name anzuzeigen. Wenn keine DisplayID existiert, ist der Inhalt des Attributs IppDMSentity.Name anzuzeigen.
 * 
 * <p>Java class for DisplayID complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DisplayID">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Language" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DisplayID", propOrder = {
    "language",
    "text"
})
public class DisplayID {

    @XmlElement(name = "Language", required = true)
    protected String language;
    @XmlElement(name = "Text", required = true)
    protected String text;

    /**
     * Gets the value of the language property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Sets the value of the language property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguage(String value) {
        this.language = value;
    }

    /**
     * Gets the value of the text property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getText() {
        return text;
    }

    /**
     * Sets the value of the text property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setText(String value) {
        this.text = value;
    }

}
