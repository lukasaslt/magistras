
package org.inspection_plusplus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Messung. Eine Messung umfasst alle Ergebnis-Informationen einer Messung mit einer Messmaschine. Es wird das gemessene Teil dokumentiert und die gesamten Messelemente, die bei der Messung gemessen wurden.
 * 
 * <p>Java class for Inspection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Inspection">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IppDMSentity">
 *       &lt;sequence>
 *         &lt;element name="DevelopmentStage" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Inspector" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IsAbsoluteMeasuring" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsLongTerm" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Location" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Maturity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MeasuringGroup" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ObjectCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OrderNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PartID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ProductionEquipment" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ProductionState" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ReceiptNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SerialNumberBodyshellWorkCharge" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Shift" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Supplier" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Time" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Variant" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="VehicleIdentificationNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="InspectedIPE" type="{http://www.inspection-plusplus.org}InspectedIPE" maxOccurs="unbounded"/>
 *         &lt;element name="InspectionProgram" type="{http://www.inspection-plusplus.org}InspectionProgram" minOccurs="0"/>
 *         &lt;element name="Comment" type="{http://www.inspection-plusplus.org}Comment" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="InspectedPart" type="{http://www.inspection-plusplus.org}InspectedPart" maxOccurs="unbounded"/>
 *         &lt;element name="InspectionResource" type="{http://www.inspection-plusplus.org}InspectionResource"/>
 *         &lt;element name="Generic_IPE_Type" type="{http://www.inspection-plusplus.org}Generic_IPE_Type" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Inspection", propOrder = {
    "developmentStage",
    "inspector",
    "isAbsoluteMeasuring",
    "isLongTerm",
    "location",
    "maturity",
    "measuringGroup",
    "objectCode",
    "orderNumber",
    "partID",
    "productionEquipment",
    "productionState",
    "receiptNumber",
    "serialNumberBodyshellWorkCharge",
    "shift",
    "supplier",
    "time",
    "variant",
    "vehicleIdentificationNumber",
    "inspectedIPE",
    "inspectionProgram",
    "comment",
    "inspectedPart",
    "inspectionResource",
    "genericIPEType"
})
public class Inspection
    extends IppDMSentity
{

    @XmlElement(name = "DevelopmentStage", required = true)
    protected String developmentStage;
    @XmlElement(name = "Inspector", required = true)
    protected String inspector;
    @XmlElement(name = "IsAbsoluteMeasuring")
    protected boolean isAbsoluteMeasuring;
    @XmlElement(name = "IsLongTerm")
    protected boolean isLongTerm;
    @XmlElement(name = "Location", required = true)
    protected String location;
    @XmlElement(name = "Maturity", required = true)
    protected String maturity;
    @XmlElement(name = "MeasuringGroup", required = true)
    protected String measuringGroup;
    @XmlElement(name = "ObjectCode", required = true)
    protected String objectCode;
    @XmlElement(name = "OrderNumber", required = true)
    protected String orderNumber;
    @XmlElement(name = "PartID", required = true)
    protected String partID;
    @XmlElement(name = "ProductionEquipment", required = true)
    protected String productionEquipment;
    @XmlElement(name = "ProductionState", required = true)
    protected String productionState;
    @XmlElement(name = "ReceiptNumber", required = true)
    protected String receiptNumber;
    @XmlElement(name = "SerialNumberBodyshellWorkCharge", required = true)
    protected String serialNumberBodyshellWorkCharge;
    @XmlElement(name = "Shift", required = true)
    protected String shift;
    @XmlElement(name = "Supplier", required = true)
    protected String supplier;
    @XmlElement(name = "Time", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar time;
    @XmlElement(name = "Variant", required = true)
    protected String variant;
    @XmlElement(name = "VehicleIdentificationNumber", required = true)
    protected String vehicleIdentificationNumber;
    @XmlElement(name = "InspectedIPE", required = true)
    protected List<InspectedIPE> inspectedIPE;
    @XmlElement(name = "InspectionProgram")
    protected InspectionProgram inspectionProgram;
    @XmlElement(name = "Comment")
    protected List<Comment> comment;
    @XmlElement(name = "InspectedPart", required = true)
    protected List<InspectedPart> inspectedPart;
    @XmlElement(name = "InspectionResource", required = true)
    protected InspectionResource inspectionResource;
    @XmlElement(name = "Generic_IPE_Type")
    protected List<GenericIPEType> genericIPEType;

    /**
     * Gets the value of the developmentStage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDevelopmentStage() {
        return developmentStage;
    }

    /**
     * Sets the value of the developmentStage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDevelopmentStage(String value) {
        this.developmentStage = value;
    }

    /**
     * Gets the value of the inspector property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInspector() {
        return inspector;
    }

    /**
     * Sets the value of the inspector property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInspector(String value) {
        this.inspector = value;
    }

    /**
     * Gets the value of the isAbsoluteMeasuring property.
     * 
     */
    public boolean isIsAbsoluteMeasuring() {
        return isAbsoluteMeasuring;
    }

    /**
     * Sets the value of the isAbsoluteMeasuring property.
     * 
     */
    public void setIsAbsoluteMeasuring(boolean value) {
        this.isAbsoluteMeasuring = value;
    }

    /**
     * Gets the value of the isLongTerm property.
     * 
     */
    public boolean isIsLongTerm() {
        return isLongTerm;
    }

    /**
     * Sets the value of the isLongTerm property.
     * 
     */
    public void setIsLongTerm(boolean value) {
        this.isLongTerm = value;
    }

    /**
     * Gets the value of the location property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocation() {
        return location;
    }

    /**
     * Sets the value of the location property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocation(String value) {
        this.location = value;
    }

    /**
     * Gets the value of the maturity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaturity() {
        return maturity;
    }

    /**
     * Sets the value of the maturity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaturity(String value) {
        this.maturity = value;
    }

    /**
     * Gets the value of the measuringGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMeasuringGroup() {
        return measuringGroup;
    }

    /**
     * Sets the value of the measuringGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeasuringGroup(String value) {
        this.measuringGroup = value;
    }

    /**
     * Gets the value of the objectCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjectCode() {
        return objectCode;
    }

    /**
     * Sets the value of the objectCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjectCode(String value) {
        this.objectCode = value;
    }

    /**
     * Gets the value of the orderNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderNumber() {
        return orderNumber;
    }

    /**
     * Sets the value of the orderNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderNumber(String value) {
        this.orderNumber = value;
    }

    /**
     * Gets the value of the partID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartID() {
        return partID;
    }

    /**
     * Sets the value of the partID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartID(String value) {
        this.partID = value;
    }

    /**
     * Gets the value of the productionEquipment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductionEquipment() {
        return productionEquipment;
    }

    /**
     * Sets the value of the productionEquipment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductionEquipment(String value) {
        this.productionEquipment = value;
    }

    /**
     * Gets the value of the productionState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductionState() {
        return productionState;
    }

    /**
     * Sets the value of the productionState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductionState(String value) {
        this.productionState = value;
    }

    /**
     * Gets the value of the receiptNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiptNumber() {
        return receiptNumber;
    }

    /**
     * Sets the value of the receiptNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiptNumber(String value) {
        this.receiptNumber = value;
    }

    /**
     * Gets the value of the serialNumberBodyshellWorkCharge property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSerialNumberBodyshellWorkCharge() {
        return serialNumberBodyshellWorkCharge;
    }

    /**
     * Sets the value of the serialNumberBodyshellWorkCharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSerialNumberBodyshellWorkCharge(String value) {
        this.serialNumberBodyshellWorkCharge = value;
    }

    /**
     * Gets the value of the shift property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShift() {
        return shift;
    }

    /**
     * Sets the value of the shift property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShift(String value) {
        this.shift = value;
    }

    /**
     * Gets the value of the supplier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSupplier() {
        return supplier;
    }

    /**
     * Sets the value of the supplier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSupplier(String value) {
        this.supplier = value;
    }

    /**
     * Gets the value of the time property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTime() {
        return time;
    }

    /**
     * Sets the value of the time property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTime(XMLGregorianCalendar value) {
        this.time = value;
    }

    /**
     * Gets the value of the variant property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVariant() {
        return variant;
    }

    /**
     * Sets the value of the variant property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVariant(String value) {
        this.variant = value;
    }

    /**
     * Gets the value of the vehicleIdentificationNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehicleIdentificationNumber() {
        return vehicleIdentificationNumber;
    }

    /**
     * Sets the value of the vehicleIdentificationNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehicleIdentificationNumber(String value) {
        this.vehicleIdentificationNumber = value;
    }

    /**
     * Gets the value of the inspectedIPE property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the inspectedIPE property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInspectedIPE().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InspectedIPE }
     * 
     * 
     */
    public List<InspectedIPE> getInspectedIPE() {
        if (inspectedIPE == null) {
            inspectedIPE = new ArrayList<InspectedIPE>();
        }
        return this.inspectedIPE;
    }

    /**
     * Gets the value of the inspectionProgram property.
     * 
     * @return
     *     possible object is
     *     {@link InspectionProgram }
     *     
     */
    public InspectionProgram getInspectionProgram() {
        return inspectionProgram;
    }

    /**
     * Sets the value of the inspectionProgram property.
     * 
     * @param value
     *     allowed object is
     *     {@link InspectionProgram }
     *     
     */
    public void setInspectionProgram(InspectionProgram value) {
        this.inspectionProgram = value;
    }

    /**
     * Gets the value of the comment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the comment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Comment }
     * 
     * 
     */
    public List<Comment> getComment() {
        if (comment == null) {
            comment = new ArrayList<Comment>();
        }
        return this.comment;
    }

    /**
     * Gets the value of the inspectedPart property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the inspectedPart property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInspectedPart().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InspectedPart }
     * 
     * 
     */
    public List<InspectedPart> getInspectedPart() {
        if (inspectedPart == null) {
            inspectedPart = new ArrayList<InspectedPart>();
        }
        return this.inspectedPart;
    }

    /**
     * Gets the value of the inspectionResource property.
     * 
     * @return
     *     possible object is
     *     {@link InspectionResource }
     *     
     */
    public InspectionResource getInspectionResource() {
        return inspectionResource;
    }

    /**
     * Sets the value of the inspectionResource property.
     * 
     * @param value
     *     allowed object is
     *     {@link InspectionResource }
     *     
     */
    public void setInspectionResource(InspectionResource value) {
        this.inspectionResource = value;
    }

    /**
     * Gets the value of the genericIPEType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the genericIPEType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGenericIPEType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GenericIPEType }
     * 
     * 
     */
    public List<GenericIPEType> getGenericIPEType() {
        if (genericIPEType == null) {
            genericIPEType = new ArrayList<GenericIPEType>();
        }
        return this.genericIPEType;
    }

}
