
package org.inspection_plusplus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Assembly, Zusammenbauteil, Zusammenbaustufe; siehe Beschreibung zu QS-Produktstrukturen. Ein Zusammenbauteil verf�gt �ber Komponenten, die �ber je 1 PositionedPart-Instanz pro Komponente inkl. Lagebeschreibung zugeordnet werden.
 * 
 * <p>Java class for Part_Assembly complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Part_Assembly">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Part">
 *       &lt;sequence>
 *         &lt;element name="AllComponents" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="PositionedPart" type="{http://www.inspection-plusplus.org}PositionedPart" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Part_Assembly", propOrder = {
    "allComponents",
    "positionedPart"
})
public class PartAssembly
    extends Part
{

    @XmlElement(name = "AllComponents")
    protected boolean allComponents;
    @XmlElement(name = "PositionedPart", required = true)
    protected List<PositionedPart> positionedPart;

    /**
     * Gets the value of the allComponents property.
     * 
     */
    public boolean isAllComponents() {
        return allComponents;
    }

    /**
     * Sets the value of the allComponents property.
     * 
     */
    public void setAllComponents(boolean value) {
        this.allComponents = value;
    }

    /**
     * Gets the value of the positionedPart property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the positionedPart property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPositionedPart().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PositionedPart }
     * 
     * 
     */
    public List<PositionedPart> getPositionedPart() {
        if (positionedPart == null) {
            positionedPart = new ArrayList<PositionedPart>();
        }
        return this.positionedPart;
    }

}
