
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Abstrakte Klasse. Subklasse von Tol_AxisWithReference. Ihre Subklassen beschreiben Abstandsabweichungen, die auf jeweils eine Raumachse projiziert werden.
 * 
 * <p>Java class for Tol_AxisDistance complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Tol_AxisDistance">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Tol_AxisWithReference">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tol_AxisDistance")
@XmlSeeAlso({
    TolAxisDistY.class,
    TolAxisDistX.class,
    TolAxisDistZ.class
})
public abstract class TolAxisDistance
    extends TolAxisWithReference
{


}
