
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Datentyp zur Angabe eines Toleranzkriteriums in Form einer natürlichsprachlichen Beschreibung.
 * 
 * <p>Java class for NaturalLanguageToleranceCriterion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NaturalLanguageToleranceCriterion">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}ToleranceCriterion">
 *       &lt;sequence>
 *         &lt;element name="ToleranceConstraint" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NaturalLanguageToleranceCriterion", propOrder = {
    "toleranceConstraint"
})
public class NaturalLanguageToleranceCriterion
    extends ToleranceCriterion
{

    @XmlElement(name = "ToleranceConstraint", required = true)
    protected String toleranceConstraint;

    /**
     * Gets the value of the toleranceConstraint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToleranceConstraint() {
        return toleranceConstraint;
    }

    /**
     * Sets the value of the toleranceConstraint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToleranceConstraint(String value) {
        this.toleranceConstraint = value;
    }

}
