
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Datentyp zur Angabe eines Toleranzkriteriums in Form eines einzelnen booleschen Wertes.
 * 
 *  
 * 
 * <p>Java class for BooleanToleranceCriterion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BooleanToleranceCriterion">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}ToleranceCriterion">
 *       &lt;sequence>
 *         &lt;element name="ToleranceValue" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BooleanToleranceCriterion", propOrder = {
    "toleranceValue"
})
public class BooleanToleranceCriterion
    extends ToleranceCriterion
{

    @XmlElement(name = "ToleranceValue")
    protected boolean toleranceValue;

    /**
     * Gets the value of the toleranceValue property.
     * 
     */
    public boolean isToleranceValue() {
        return toleranceValue;
    }

    /**
     * Sets the value of the toleranceValue property.
     * 
     */
    public void setToleranceValue(boolean value) {
        this.toleranceValue = value;
    }

}
