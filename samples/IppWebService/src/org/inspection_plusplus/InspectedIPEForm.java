
package org.inspection_plusplus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Inspected_IPE_Form complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Inspected_IPE_Form">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Inspected_IPE_Single">
 *       &lt;sequence>
 *         &lt;element name="InspectedIPE" type="{http://www.inspection-plusplus.org}InspectedIPE" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Inspected_IPE_Form", propOrder = {
    "inspectedIPE"
})
public class InspectedIPEForm
    extends InspectedIPESingle
{

    @XmlElement(name = "InspectedIPE")
    protected List<InspectedIPE> inspectedIPE;

    /**
     * Gets the value of the inspectedIPE property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the inspectedIPE property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInspectedIPE().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InspectedIPE }
     * 
     * 
     */
    public List<InspectedIPE> getInspectedIPE() {
        if (inspectedIPE == null) {
            inspectedIPE = new ArrayList<InspectedIPE>();
        }
        return this.inspectedIPE;
    }

}
