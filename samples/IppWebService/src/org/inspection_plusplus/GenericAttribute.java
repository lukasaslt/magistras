
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Repräsentiert ein Attribut eines generischen Merkmals.
 * 
 * Der Datentyp dieses Merkmals wird durch den Generic_Attribute_Type festgelegt.
 * 
 * Der Wert dieses Merkmals wird durch die jeweilige Subklasse dieser Basisklasse übertragen.
 * 
 * <p>Java class for Generic_Attribute complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Generic_Attribute">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IppDMSentity">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.inspection-plusplus.org}Generic_Attribute_Type_ref"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Generic_Attribute", propOrder = {
    "genericAttributeTypeRef"
})
@XmlSeeAlso({
    BooleanAttribute.class,
    NumericalAttribute.class,
    Vector3DAttribute.class,
    TextualAttribute.class
})
public class GenericAttribute
    extends IppDMSentity
{

    @XmlElement(name = "Generic_Attribute_Type_ref", namespace = "http://www.inspection-plusplus.org", required = true)
    protected ID4Referencing genericAttributeTypeRef;

    /**
     * Gets the value of the genericAttributeTypeRef property.
     * 
     * @return
     *     possible object is
     *     {@link ID4Referencing }
     *     
     */
    public ID4Referencing getGenericAttributeTypeRef() {
        return genericAttributeTypeRef;
    }

    /**
     * Sets the value of the genericAttributeTypeRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link ID4Referencing }
     *     
     */
    public void setGenericAttributeTypeRef(ID4Referencing value) {
        this.genericAttributeTypeRef = value;
    }

}
