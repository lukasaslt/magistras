
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Repr�sentiert einen Abstand zwischen zwei Entit�ten. Diese k�nnen �ber eine Instanz von Calculation_NominalElement zugeordnet werden. Die Attribute enthalten die Sollwerte f�r einen r�umlichen Abstand (entspricht Tol_Distance) bzw. den Abstand, der auf eine Raumachse projiziert wird (Tol_AxisDist_X&hellip;). Hinweis: Abst�nde werden also nicht mehr repr�sentiert �ber 1 QC mit Bez�gen zu 2 GeoObjects (oder alternativ zu 2 IPEs) und 1 Abstandstoleranz.
 * 
 * <p>Java class for IPE_Distance complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IPE_Distance">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IPE_Scalar">
 *       &lt;sequence>
 *         &lt;element name="AxisDist_X" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="AxisDist_Y" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="AxisDist_Z" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="Distance" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IPE_Distance", propOrder = {
    "axisDistX",
    "axisDistY",
    "axisDistZ",
    "distance"
})
public class IPEDistance
    extends IPEScalar
{

    @XmlElement(name = "AxisDist_X")
    protected double axisDistX;
    @XmlElement(name = "AxisDist_Y")
    protected double axisDistY;
    @XmlElement(name = "AxisDist_Z")
    protected double axisDistZ;
    @XmlElement(name = "Distance")
    protected double distance;

    /**
     * Gets the value of the axisDistX property.
     * 
     */
    public double getAxisDistX() {
        return axisDistX;
    }

    /**
     * Sets the value of the axisDistX property.
     * 
     */
    public void setAxisDistX(double value) {
        this.axisDistX = value;
    }

    /**
     * Gets the value of the axisDistY property.
     * 
     */
    public double getAxisDistY() {
        return axisDistY;
    }

    /**
     * Sets the value of the axisDistY property.
     * 
     */
    public void setAxisDistY(double value) {
        this.axisDistY = value;
    }

    /**
     * Gets the value of the axisDistZ property.
     * 
     */
    public double getAxisDistZ() {
        return axisDistZ;
    }

    /**
     * Sets the value of the axisDistZ property.
     * 
     */
    public void setAxisDistZ(double value) {
        this.axisDistZ = value;
    }

    /**
     * Gets the value of the distance property.
     * 
     */
    public double getDistance() {
        return distance;
    }

    /**
     * Sets the value of the distance property.
     * 
     */
    public void setDistance(double value) {
        this.distance = value;
    }

}
