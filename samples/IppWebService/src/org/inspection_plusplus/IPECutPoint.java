
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Beschnittpunkt. Punkt auf einer flachen Kante einer Beschnittfläche. Zu einem schrägen Beschnitt kann die Normale der Beschnittfläche angegeben werden. Zur Definition von Offsets siehe den Abschnitt zu IPE_Geometric.
 * 
 * Das geerbte Attribut MainAxis beschreibt die Richtung entlang der Oberfläche.
 * Das geerbte Attribut MainAxis2 beschreibt die Oberflächennormale.
 * Das geerbte, optionale Attribut Orientation beschreibt die Richtung entlang der Kante.
 * 
 * <p>Java class for IPE_CutPoint complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IPE_CutPoint">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IPE_CurvePoint">
 *       &lt;sequence>
 *         &lt;element name="CutPlaneNormal" type="{http://www.inspection-plusplus.org}Vector3D"/>
 *         &lt;element name="RetractionDepth" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IPE_CutPoint", propOrder = {
    "cutPlaneNormal",
    "retractionDepth"
})
public class IPECutPoint
    extends IPECurvePoint
{

    @XmlElement(name = "CutPlaneNormal", required = true)
    protected Vector3D cutPlaneNormal;
    @XmlElement(name = "RetractionDepth")
    protected double retractionDepth;

    /**
     * Gets the value of the cutPlaneNormal property.
     * 
     * @return
     *     possible object is
     *     {@link Vector3D }
     *     
     */
    public Vector3D getCutPlaneNormal() {
        return cutPlaneNormal;
    }

    /**
     * Sets the value of the cutPlaneNormal property.
     * 
     * @param value
     *     allowed object is
     *     {@link Vector3D }
     *     
     */
    public void setCutPlaneNormal(Vector3D value) {
        this.cutPlaneNormal = value;
    }

    /**
     * Gets the value of the retractionDepth property.
     * 
     */
    public double getRetractionDepth() {
        return retractionDepth;
    }

    /**
     * Sets the value of the retractionDepth property.
     * 
     */
    public void setRetractionDepth(double value) {
        this.retractionDepth = value;
    }

}
