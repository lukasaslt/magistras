
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Abstrakte Klasse. Basisklasse f�r Toleranzen, die Merkmale repr�sentieren, die sich auf eine Fl�che beziehen. Sie ben�tigen ein Achsensystem (Bezugssystem). 
 * 
 * <p>Java class for Tol_PlaneWithReference complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Tol_PlaneWithReference">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Tolerance">
 *       &lt;sequence>
 *         &lt;element name="Criterion" type="{http://www.inspection-plusplus.org}IntervalToleranceCriterion"/>
 *         &lt;element ref="{http://www.inspection-plusplus.org}ReferenceSystem_ref"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tol_PlaneWithReference", propOrder = {
    "criterion",
    "referenceSystemRef"
})
@XmlSeeAlso({
    TolPlaneAngle.class
})
public abstract class TolPlaneWithReference
    extends Tolerance
{

    @XmlElement(name = "Criterion", required = true)
    protected IntervalToleranceCriterion criterion;
    @XmlElement(name = "ReferenceSystem_ref", namespace = "http://www.inspection-plusplus.org", required = true)
    protected ID4Referencing referenceSystemRef;

    /**
     * Gets the value of the criterion property.
     * 
     * @return
     *     possible object is
     *     {@link IntervalToleranceCriterion }
     *     
     */
    public IntervalToleranceCriterion getCriterion() {
        return criterion;
    }

    /**
     * Sets the value of the criterion property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntervalToleranceCriterion }
     *     
     */
    public void setCriterion(IntervalToleranceCriterion value) {
        this.criterion = value;
    }

    /**
     * Gets the value of the referenceSystemRef property.
     * 
     * @return
     *     possible object is
     *     {@link ID4Referencing }
     *     
     */
    public ID4Referencing getReferenceSystemRef() {
        return referenceSystemRef;
    }

    /**
     * Sets the value of the referenceSystemRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link ID4Referencing }
     *     
     */
    public void setReferenceSystemRef(ID4Referencing value) {
        this.referenceSystemRef = value;
    }

}
