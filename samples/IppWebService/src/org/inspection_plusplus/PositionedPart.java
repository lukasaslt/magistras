
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Zuordnung von -Komponenten zu einer Part_Assembly-Instanz; siehe Beschreibung zu QS-Produktstrukturen und zu Klasse Part. Jede Komponente einer Part_Assembly erh�lt eine eigene PositionedPart-Instanz. PositionedPart gibt auch die Einbaulage der Komponenten innerhalb einer Assembly an.
 * 
 * <p>Java class for PositionedPart complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PositionedPart">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IppDMSentity">
 *       &lt;sequence>
 *         &lt;element name="AssemblyIndex" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IsMirrored" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="PartPDMvariant" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="QAversion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="QAversionBaseInstance" type="{http://www.inspection-plusplus.org}ID4Referencing"/>
 *         &lt;element name="Transformation" type="{http://www.inspection-plusplus.org}TransformationMatrix"/>
 *         &lt;element ref="{http://www.inspection-plusplus.org}Part_ref"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PositionedPart", propOrder = {
    "assemblyIndex",
    "isMirrored",
    "partPDMvariant",
    "qAversion",
    "qAversionBaseInstance",
    "transformation",
    "partRef"
})
public class PositionedPart
    extends IppDMSentity
{

    @XmlElement(name = "AssemblyIndex", required = true)
    protected String assemblyIndex;
    @XmlElement(name = "IsMirrored")
    protected boolean isMirrored;
    @XmlElement(name = "PartPDMvariant", required = true)
    protected String partPDMvariant;
    @XmlElement(name = "QAversion", required = true)
    protected String qAversion;
    @XmlElement(name = "QAversionBaseInstance", required = true)
    protected ID4Referencing qAversionBaseInstance;
    @XmlElement(name = "Transformation", required = true)
    protected TransformationMatrix transformation;
    @XmlElement(name = "Part_ref", namespace = "http://www.inspection-plusplus.org", required = true)
    protected ID4Referencing partRef;

    /**
     * Gets the value of the assemblyIndex property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssemblyIndex() {
        return assemblyIndex;
    }

    /**
     * Sets the value of the assemblyIndex property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssemblyIndex(String value) {
        this.assemblyIndex = value;
    }

    /**
     * Gets the value of the isMirrored property.
     * 
     */
    public boolean isIsMirrored() {
        return isMirrored;
    }

    /**
     * Sets the value of the isMirrored property.
     * 
     */
    public void setIsMirrored(boolean value) {
        this.isMirrored = value;
    }

    /**
     * Gets the value of the partPDMvariant property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartPDMvariant() {
        return partPDMvariant;
    }

    /**
     * Sets the value of the partPDMvariant property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartPDMvariant(String value) {
        this.partPDMvariant = value;
    }

    /**
     * Gets the value of the qAversion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQAversion() {
        return qAversion;
    }

    /**
     * Sets the value of the qAversion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQAversion(String value) {
        this.qAversion = value;
    }

    /**
     * Gets the value of the qAversionBaseInstance property.
     * 
     * @return
     *     possible object is
     *     {@link ID4Referencing }
     *     
     */
    public ID4Referencing getQAversionBaseInstance() {
        return qAversionBaseInstance;
    }

    /**
     * Sets the value of the qAversionBaseInstance property.
     * 
     * @param value
     *     allowed object is
     *     {@link ID4Referencing }
     *     
     */
    public void setQAversionBaseInstance(ID4Referencing value) {
        this.qAversionBaseInstance = value;
    }

    /**
     * Gets the value of the transformation property.
     * 
     * @return
     *     possible object is
     *     {@link TransformationMatrix }
     *     
     */
    public TransformationMatrix getTransformation() {
        return transformation;
    }

    /**
     * Sets the value of the transformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransformationMatrix }
     *     
     */
    public void setTransformation(TransformationMatrix value) {
        this.transformation = value;
    }

    /**
     * Gets the value of the partRef property.
     * 
     * @return
     *     possible object is
     *     {@link ID4Referencing }
     *     
     */
    public ID4Referencing getPartRef() {
        return partRef;
    }

    /**
     * Sets the value of the partRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link ID4Referencing }
     *     
     */
    public void setPartRef(ID4Referencing value) {
        this.partRef = value;
    }

}
