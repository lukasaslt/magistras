
package org.inspection_plusplus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Repräsentiert eine Instanz eines generisch modellierten Merkmalstypen (siehe dazu auch Generic_IPE_Type).
 * 
 * Enthält Attribute vom Typ Generic_Attribute bzw. dessen Subtypen.
 * 
 * <p>Java class for IPE_Generic complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IPE_Generic">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IPE_Single">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.inspection-plusplus.org}Generic_IPE_Type_ref"/>
 *         &lt;element name="Generic_Attribute" type="{http://www.inspection-plusplus.org}Generic_Attribute" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IPE_Generic", propOrder = {
    "genericIPETypeRef",
    "genericAttribute"
})
public class IPEGeneric
    extends IPESingle
{

    @XmlElement(name = "Generic_IPE_Type_ref", namespace = "http://www.inspection-plusplus.org", required = true)
    protected ID4Referencing genericIPETypeRef;
    @XmlElement(name = "Generic_Attribute", required = true)
    protected List<GenericAttribute> genericAttribute;

    /**
     * Gets the value of the genericIPETypeRef property.
     * 
     * @return
     *     possible object is
     *     {@link ID4Referencing }
     *     
     */
    public ID4Referencing getGenericIPETypeRef() {
        return genericIPETypeRef;
    }

    /**
     * Sets the value of the genericIPETypeRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link ID4Referencing }
     *     
     */
    public void setGenericIPETypeRef(ID4Referencing value) {
        this.genericIPETypeRef = value;
    }

    /**
     * Gets the value of the genericAttribute property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the genericAttribute property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGenericAttribute().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GenericAttribute }
     * 
     * 
     */
    public List<GenericAttribute> getGenericAttribute() {
        if (genericAttribute == null) {
            genericAttribute = new ArrayList<GenericAttribute>();
        }
        return this.genericAttribute;
    }

}
