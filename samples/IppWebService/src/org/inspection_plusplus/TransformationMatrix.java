
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Transformationsmatrix aus 4 Vektoren. Rotation (A-x) und Verschiebung (Delta-x).
 * 
 * <p>Java class for TransformationMatrix complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransformationMatrix">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="A11" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="A12" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="A13" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="A21" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="A22" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="A23" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="A31" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="A32" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="A33" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="DeltaX" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="DeltaY" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="DeltaZ" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransformationMatrix", propOrder = {
    "a11",
    "a12",
    "a13",
    "a21",
    "a22",
    "a23",
    "a31",
    "a32",
    "a33",
    "deltaX",
    "deltaY",
    "deltaZ"
})
public class TransformationMatrix {

    @XmlElement(name = "A11")
    protected double a11;
    @XmlElement(name = "A12")
    protected double a12;
    @XmlElement(name = "A13")
    protected double a13;
    @XmlElement(name = "A21")
    protected double a21;
    @XmlElement(name = "A22")
    protected double a22;
    @XmlElement(name = "A23")
    protected double a23;
    @XmlElement(name = "A31")
    protected double a31;
    @XmlElement(name = "A32")
    protected double a32;
    @XmlElement(name = "A33")
    protected double a33;
    @XmlElement(name = "DeltaX")
    protected double deltaX;
    @XmlElement(name = "DeltaY")
    protected double deltaY;
    @XmlElement(name = "DeltaZ")
    protected double deltaZ;

    /**
     * Gets the value of the a11 property.
     * 
     */
    public double getA11() {
        return a11;
    }

    /**
     * Sets the value of the a11 property.
     * 
     */
    public void setA11(double value) {
        this.a11 = value;
    }

    /**
     * Gets the value of the a12 property.
     * 
     */
    public double getA12() {
        return a12;
    }

    /**
     * Sets the value of the a12 property.
     * 
     */
    public void setA12(double value) {
        this.a12 = value;
    }

    /**
     * Gets the value of the a13 property.
     * 
     */
    public double getA13() {
        return a13;
    }

    /**
     * Sets the value of the a13 property.
     * 
     */
    public void setA13(double value) {
        this.a13 = value;
    }

    /**
     * Gets the value of the a21 property.
     * 
     */
    public double getA21() {
        return a21;
    }

    /**
     * Sets the value of the a21 property.
     * 
     */
    public void setA21(double value) {
        this.a21 = value;
    }

    /**
     * Gets the value of the a22 property.
     * 
     */
    public double getA22() {
        return a22;
    }

    /**
     * Sets the value of the a22 property.
     * 
     */
    public void setA22(double value) {
        this.a22 = value;
    }

    /**
     * Gets the value of the a23 property.
     * 
     */
    public double getA23() {
        return a23;
    }

    /**
     * Sets the value of the a23 property.
     * 
     */
    public void setA23(double value) {
        this.a23 = value;
    }

    /**
     * Gets the value of the a31 property.
     * 
     */
    public double getA31() {
        return a31;
    }

    /**
     * Sets the value of the a31 property.
     * 
     */
    public void setA31(double value) {
        this.a31 = value;
    }

    /**
     * Gets the value of the a32 property.
     * 
     */
    public double getA32() {
        return a32;
    }

    /**
     * Sets the value of the a32 property.
     * 
     */
    public void setA32(double value) {
        this.a32 = value;
    }

    /**
     * Gets the value of the a33 property.
     * 
     */
    public double getA33() {
        return a33;
    }

    /**
     * Sets the value of the a33 property.
     * 
     */
    public void setA33(double value) {
        this.a33 = value;
    }

    /**
     * Gets the value of the deltaX property.
     * 
     */
    public double getDeltaX() {
        return deltaX;
    }

    /**
     * Sets the value of the deltaX property.
     * 
     */
    public void setDeltaX(double value) {
        this.deltaX = value;
    }

    /**
     * Gets the value of the deltaY property.
     * 
     */
    public double getDeltaY() {
        return deltaY;
    }

    /**
     * Sets the value of the deltaY property.
     * 
     */
    public void setDeltaY(double value) {
        this.deltaY = value;
    }

    /**
     * Gets the value of the deltaZ property.
     * 
     */
    public double getDeltaZ() {
        return deltaZ;
    }

    /**
     * Sets the value of the deltaZ property.
     * 
     */
    public void setDeltaZ(double value) {
        this.deltaZ = value;
    }

}
