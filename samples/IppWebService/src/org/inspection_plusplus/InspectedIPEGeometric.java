
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Inspected_IPE_Geometric complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Inspected_IPE_Geometric">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Inspected_IPE_Single">
 *       &lt;sequence>
 *         &lt;element name="Orientation" type="{http://www.inspection-plusplus.org}Vector3D" minOccurs="0"/>
 *         &lt;element name="Origin" type="{http://www.inspection-plusplus.org}Vector3D" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Inspected_IPE_Geometric", propOrder = {
    "orientation",
    "origin"
})
@XmlSeeAlso({
    InspectedIPERoundedEdgeJoint.class,
    InspectedIPECircle.class,
    InspectedIPEFreeform.class,
    InspectedIPELine.class,
    InspectedIPEPrismatic.class,
    InspectedIPEPoint.class,
    InspectedIPEPlane.class,
    InspectedIPEHole.class
})
public abstract class InspectedIPEGeometric
    extends InspectedIPESingle
{

    @XmlElement(name = "Orientation")
    protected Vector3D orientation;
    @XmlElement(name = "Origin")
    protected Vector3D origin;

    /**
     * Gets the value of the orientation property.
     * 
     * @return
     *     possible object is
     *     {@link Vector3D }
     *     
     */
    public Vector3D getOrientation() {
        return orientation;
    }

    /**
     * Sets the value of the orientation property.
     * 
     * @param value
     *     allowed object is
     *     {@link Vector3D }
     *     
     */
    public void setOrientation(Vector3D value) {
        this.orientation = value;
    }

    /**
     * Gets the value of the origin property.
     * 
     * @return
     *     possible object is
     *     {@link Vector3D }
     *     
     */
    public Vector3D getOrigin() {
        return origin;
    }

    /**
     * Sets the value of the origin property.
     * 
     * @param value
     *     allowed object is
     *     {@link Vector3D }
     *     
     */
    public void setOrigin(Vector3D value) {
        this.origin = value;
    }

}
