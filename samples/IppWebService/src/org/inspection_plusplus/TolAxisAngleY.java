
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Subklasse von Tol_AxisAngle. Beschreibt Winkelabweichungen, die in Bezug auf die Y-Achse definiert werden.
 * 
 * <p>Java class for Tol_AxisAngle_Y complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Tol_AxisAngle_Y">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Tol_AxisAngle">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tol_AxisAngle_Y")
public class TolAxisAngleY
    extends TolAxisAngle
{


}
