
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Inspected_IPE_Prismatic complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Inspected_IPE_Prismatic">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Inspected_IPE_Geometric">
 *       &lt;sequence>
 *         &lt;element name="MainAxis" type="{http://www.inspection-plusplus.org}Vector3D" minOccurs="0"/>
 *         &lt;element name="MeasuringAid" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="StandardPartReference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Inspected_IPE_Prismatic", propOrder = {
    "mainAxis",
    "measuringAid",
    "standardPartReference"
})
@XmlSeeAlso({
    InspectedIPEBolt.class,
    InspectedIPECone.class,
    InspectedIPESphere.class
})
public abstract class InspectedIPEPrismatic
    extends InspectedIPEGeometric
{

    @XmlElement(name = "MainAxis")
    protected Vector3D mainAxis;
    @XmlElement(name = "MeasuringAid")
    protected Boolean measuringAid;
    @XmlElement(name = "StandardPartReference")
    protected String standardPartReference;

    /**
     * Gets the value of the mainAxis property.
     * 
     * @return
     *     possible object is
     *     {@link Vector3D }
     *     
     */
    public Vector3D getMainAxis() {
        return mainAxis;
    }

    /**
     * Sets the value of the mainAxis property.
     * 
     * @param value
     *     allowed object is
     *     {@link Vector3D }
     *     
     */
    public void setMainAxis(Vector3D value) {
        this.mainAxis = value;
    }

    /**
     * Gets the value of the measuringAid property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMeasuringAid() {
        return measuringAid;
    }

    /**
     * Sets the value of the measuringAid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMeasuringAid(Boolean value) {
        this.measuringAid = value;
    }

    /**
     * Gets the value of the standardPartReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStandardPartReference() {
        return standardPartReference;
    }

    /**
     * Sets the value of the standardPartReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStandardPartReference(String value) {
        this.standardPartReference = value;
    }

}
