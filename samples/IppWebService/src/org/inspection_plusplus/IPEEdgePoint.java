
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Kantenpunt. Ggf. Theoretischer Punkt
 * 
 * <p>Java class for IPE_EdgePoint complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IPE_EdgePoint">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IPE_CurvePoint">
 *       &lt;sequence>
 *         &lt;element name="EdgeRadius" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IPE_EdgePoint", propOrder = {
    "edgeRadius"
})
public class IPEEdgePoint
    extends IPECurvePoint
{

    @XmlElement(name = "EdgeRadius")
    protected double edgeRadius;

    /**
     * Gets the value of the edgeRadius property.
     * 
     */
    public double getEdgeRadius() {
        return edgeRadius;
    }

    /**
     * Sets the value of the edgeRadius property.
     * 
     */
    public void setEdgeRadius(double value) {
        this.edgeRadius = value;
    }

}
