
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Bolzen im Bauteil oder ein zylindrisches Messhilfsmittel. 
 * 
 * Das geerbte Attribut Main Axis verl�uft vom entlang der Zylinderachse durch den Bolzen.
 * Das geerbte Attribut Origin ist der Anfangspunkt (Basis) der Zylinderachse.
 * Das geerbte Attribut Orientation steht senkrecht auf Main Axis.
 * 
 * <p>Java class for IPE_Bolt complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IPE_Bolt">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IPE_Prismatic">
 *       &lt;sequence>
 *         &lt;element name="Diameter" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="Length" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IPE_Bolt", propOrder = {
    "diameter",
    "length"
})
@XmlSeeAlso({
    IPEThreadedBolt.class
})
public class IPEBolt
    extends IPEPrismatic
{

    @XmlElement(name = "Diameter")
    protected double diameter;
    @XmlElement(name = "Length")
    protected double length;

    /**
     * Gets the value of the diameter property.
     * 
     */
    public double getDiameter() {
        return diameter;
    }

    /**
     * Sets the value of the diameter property.
     * 
     */
    public void setDiameter(double value) {
        this.diameter = value;
    }

    /**
     * Gets the value of the length property.
     * 
     */
    public double getLength() {
        return length;
    }

    /**
     * Sets the value of the length property.
     * 
     */
    public void setLength(double value) {
        this.length = value;
    }

}
