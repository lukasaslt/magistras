
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Subklasse von Tol_Size und Tol_LinearSize. Abstandstoleranz. Spezialisierung der abstrakten Klasse Tol_LinearSize, die der Tolerierung von Längen oder Winkelabständen dient. Beschreibt die erlaubte Abweichung im Abstand zweier Objekte. Der Abstand der beiden Objekte wird durch eine einzelne IPE-Instanz repräsentiert (IPE_Distance), welches optional durch Calculation_NominalElement wiederum zwei Design-Objekten (GeometricObjects) zugeordnet werden kann. Tol_AxisDistance ist inhaltlich nahe verwandt, beschreibt den Abstand aber nach Projektion auf eine der Raumachsen.
 * 
 * <p>Java class for Tol_Distance complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Tol_Distance">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Tol_LinearSize">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tol_Distance")
public class TolDistance
    extends TolLinearSize
{


}
