
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * siehe IPE_CurvePoint
 * 
 * <p>Java class for Inspected_IPE_CurvePoint complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Inspected_IPE_CurvePoint">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Inspected_IPE_Point">
 *       &lt;sequence>
 *         &lt;element name="MainAxis2" type="{http://www.inspection-plusplus.org}Vector3D"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Inspected_IPE_CurvePoint", propOrder = {
    "mainAxis2"
})
@XmlSeeAlso({
    InspectedIPEEdgePoint.class,
    InspectedIPECutPoint.class,
    InspectedIPEVertexPoint.class
})
public class InspectedIPECurvePoint
    extends InspectedIPEPoint
{

    @XmlElement(name = "MainAxis2", required = true)
    protected Vector3D mainAxis2;

    /**
     * Gets the value of the mainAxis2 property.
     * 
     * @return
     *     possible object is
     *     {@link Vector3D }
     *     
     */
    public Vector3D getMainAxis2() {
        return mainAxis2;
    }

    /**
     * Sets the value of the mainAxis2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Vector3D }
     *     
     */
    public void setMainAxis2(Vector3D value) {
        this.mainAxis2 = value;
    }

}
