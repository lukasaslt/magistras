
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Ringf�rmige ebene Fl�che. Siehe Abbildung.
 * 
 * Das geerbte Attribut Origin gibt hier den Mittelpunkt der inneren und �u�eren Fl�chenr�nder an.
 * 
 * <p>Java class for IPE_Annulus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IPE_Annulus">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IPE_Plane">
 *       &lt;sequence>
 *         &lt;element name="InnerDiameter" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="OuterDiameter" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IPE_Annulus", propOrder = {
    "innerDiameter",
    "outerDiameter"
})
public class IPEAnnulus
    extends IPEPlane
{

    @XmlElement(name = "InnerDiameter")
    protected double innerDiameter;
    @XmlElement(name = "OuterDiameter")
    protected double outerDiameter;

    /**
     * Gets the value of the innerDiameter property.
     * 
     */
    public double getInnerDiameter() {
        return innerDiameter;
    }

    /**
     * Sets the value of the innerDiameter property.
     * 
     */
    public void setInnerDiameter(double value) {
        this.innerDiameter = value;
    }

    /**
     * Gets the value of the outerDiameter property.
     * 
     */
    public double getOuterDiameter() {
        return outerDiameter;
    }

    /**
     * Sets the value of the outerDiameter property.
     * 
     */
    public void setOuterDiameter(double value) {
        this.outerDiameter = value;
    }

}
