
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Langloch, definiert �ber L�nge und Durchmesser. Siehe Abbildung.
 * Unterscheidet sich semantisch vom IPE_OblongHole, weswegen Toleranzangaben nicht ineinander �berf�hrbar sind. 
 * 
 * Das geerbte Attribut Origin gibt den Mittelpunkt des Rundlochs an.
 * Das geerbte Pflichtattribut Orientation definiert die  L�ngsrichtung des Langlochs vom durch Origin definierten Mittelpunkt ausgehend.
 * Das geerbte Attribut Diameter definiert die Breite senkrecht zur Orientation.
 * 
 * <p>Java class for IPE_Slot complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IPE_Slot">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IPE_RoundHole">
 *       &lt;sequence>
 *         &lt;element name="Length" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IPE_Slot", propOrder = {
    "length"
})
@XmlSeeAlso({
    IPEKeyHole.class,
    IPEFlangedSlot.class
})
public class IPESlot
    extends IPERoundHole
{

    @XmlElement(name = "Length")
    protected double length;

    /**
     * Gets the value of the length property.
     * 
     */
    public double getLength() {
        return length;
    }

    /**
     * Sets the value of the length property.
     * 
     */
    public void setLength(double value) {
        this.length = value;
    }

}
