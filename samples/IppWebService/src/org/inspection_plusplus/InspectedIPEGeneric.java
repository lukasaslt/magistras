
package org.inspection_plusplus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Inspected_IPE_Generic complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Inspected_IPE_Generic">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Inspected_IPE_Single">
 *       &lt;sequence>
 *         &lt;element name="Generic_IPE_Type" type="{http://www.inspection-plusplus.org}Generic_IPE_Type"/>
 *         &lt;element name="Inspected_Generic_Attribute" type="{http://www.inspection-plusplus.org}Inspected_Generic_Attribute" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Inspected_IPE_Generic", propOrder = {
    "genericIPEType",
    "inspectedGenericAttribute"
})
public class InspectedIPEGeneric
    extends InspectedIPESingle
{

    @XmlElement(name = "Generic_IPE_Type", required = true)
    protected GenericIPEType genericIPEType;
    @XmlElement(name = "Inspected_Generic_Attribute")
    protected List<InspectedGenericAttribute> inspectedGenericAttribute;

    /**
     * Gets the value of the genericIPEType property.
     * 
     * @return
     *     possible object is
     *     {@link GenericIPEType }
     *     
     */
    public GenericIPEType getGenericIPEType() {
        return genericIPEType;
    }

    /**
     * Sets the value of the genericIPEType property.
     * 
     * @param value
     *     allowed object is
     *     {@link GenericIPEType }
     *     
     */
    public void setGenericIPEType(GenericIPEType value) {
        this.genericIPEType = value;
    }

    /**
     * Gets the value of the inspectedGenericAttribute property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the inspectedGenericAttribute property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInspectedGenericAttribute().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InspectedGenericAttribute }
     * 
     * 
     */
    public List<InspectedGenericAttribute> getInspectedGenericAttribute() {
        if (inspectedGenericAttribute == null) {
            inspectedGenericAttribute = new ArrayList<InspectedGenericAttribute>();
        }
        return this.inspectedGenericAttribute;
    }

}
