
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Resultierender Winkel zwischen zwei Geraden, die durch IPEs beschrieben werden. Kann das Ergebnis einer Calculation sein. Die Input-IPEs k�nnen als Operanden der Calculation definiert werden.
 * 
 * <p>Java class for IPE_Angle complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IPE_Angle">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IPE_Scalar">
 *       &lt;sequence>
 *         &lt;element name="Angle" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="AxisAngle_X" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="AxisAngle_Y" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="AxisAngle_Z" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="PlaneAngle_XY" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="PlaneAngle_XZ" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="PlaneAngle_YZ" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IPE_Angle", propOrder = {
    "angle",
    "axisAngleX",
    "axisAngleY",
    "axisAngleZ",
    "planeAngleXY",
    "planeAngleXZ",
    "planeAngleYZ"
})
public class IPEAngle
    extends IPEScalar
{

    @XmlElement(name = "Angle")
    protected double angle;
    @XmlElement(name = "AxisAngle_X")
    protected double axisAngleX;
    @XmlElement(name = "AxisAngle_Y")
    protected double axisAngleY;
    @XmlElement(name = "AxisAngle_Z")
    protected double axisAngleZ;
    @XmlElement(name = "PlaneAngle_XY")
    protected double planeAngleXY;
    @XmlElement(name = "PlaneAngle_XZ")
    protected double planeAngleXZ;
    @XmlElement(name = "PlaneAngle_YZ")
    protected double planeAngleYZ;

    /**
     * Gets the value of the angle property.
     * 
     */
    public double getAngle() {
        return angle;
    }

    /**
     * Sets the value of the angle property.
     * 
     */
    public void setAngle(double value) {
        this.angle = value;
    }

    /**
     * Gets the value of the axisAngleX property.
     * 
     */
    public double getAxisAngleX() {
        return axisAngleX;
    }

    /**
     * Sets the value of the axisAngleX property.
     * 
     */
    public void setAxisAngleX(double value) {
        this.axisAngleX = value;
    }

    /**
     * Gets the value of the axisAngleY property.
     * 
     */
    public double getAxisAngleY() {
        return axisAngleY;
    }

    /**
     * Sets the value of the axisAngleY property.
     * 
     */
    public void setAxisAngleY(double value) {
        this.axisAngleY = value;
    }

    /**
     * Gets the value of the axisAngleZ property.
     * 
     */
    public double getAxisAngleZ() {
        return axisAngleZ;
    }

    /**
     * Sets the value of the axisAngleZ property.
     * 
     */
    public void setAxisAngleZ(double value) {
        this.axisAngleZ = value;
    }

    /**
     * Gets the value of the planeAngleXY property.
     * 
     */
    public double getPlaneAngleXY() {
        return planeAngleXY;
    }

    /**
     * Sets the value of the planeAngleXY property.
     * 
     */
    public void setPlaneAngleXY(double value) {
        this.planeAngleXY = value;
    }

    /**
     * Gets the value of the planeAngleXZ property.
     * 
     */
    public double getPlaneAngleXZ() {
        return planeAngleXZ;
    }

    /**
     * Sets the value of the planeAngleXZ property.
     * 
     */
    public void setPlaneAngleXZ(double value) {
        this.planeAngleXZ = value;
    }

    /**
     * Gets the value of the planeAngleYZ property.
     * 
     */
    public double getPlaneAngleYZ() {
        return planeAngleYZ;
    }

    /**
     * Sets the value of the planeAngleYZ property.
     * 
     */
    public void setPlaneAngleYZ(double value) {
        this.planeAngleYZ = value;
    }

}
