
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Abstrakte Klasse. Anforderung an die geometrische Erscheinung eines Ausschniitts der Produktgeometrie. Zuordnung von Toleranz zu Geometrie �ber QC_Single-Instanzen. Das Attribut Criterion beschreibt jeweils den Toleranzwert.
 * 
 * <p>Java class for Tolerance complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Tolerance">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IppDMSentity">
 *       &lt;sequence>
 *         &lt;element name="StandardConformity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ToleranceSource" type="{http://www.inspection-plusplus.org}ToleranceSource"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tolerance", propOrder = {
    "standardConformity",
    "toleranceSource"
})
@XmlSeeAlso({
    TolPosition.class,
    TolTextual.class,
    TolRoughness.class,
    TolUserDefined.class,
    TolProfileOfPoint.class,
    TolAxisWithReference.class,
    TolSize.class,
    TolDiscrete.class,
    TolForm.class,
    TolPlaneWithReference.class
})
public abstract class Tolerance
    extends IppDMSentity
{

    @XmlElement(name = "StandardConformity", required = true)
    protected String standardConformity;
    @XmlElement(name = "ToleranceSource", required = true)
    protected ToleranceSource toleranceSource;

    /**
     * Gets the value of the standardConformity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStandardConformity() {
        return standardConformity;
    }

    /**
     * Sets the value of the standardConformity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStandardConformity(String value) {
        this.standardConformity = value;
    }

    /**
     * Gets the value of the toleranceSource property.
     * 
     * @return
     *     possible object is
     *     {@link ToleranceSource }
     *     
     */
    public ToleranceSource getToleranceSource() {
        return toleranceSource;
    }

    /**
     * Sets the value of the toleranceSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link ToleranceSource }
     *     
     */
    public void setToleranceSource(ToleranceSource value) {
        this.toleranceSource = value;
    }

}
