
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Subklasse von Tol_PlaneAngle. Beschreibt Winkelabweichungen, die in Bezug auf die XY-Fl�che des Bezugssystems definiert werden.
 * 
 * <p>Java class for Tol_PlaneAngle_XY complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Tol_PlaneAngle_XY">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Tol_PlaneAngle">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tol_PlaneAngle_XY")
public class TolPlaneAngleXY
    extends TolPlaneAngle
{


}
