
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Reale Ausprägung eines Teils, welches in der Messmaschine gemessen wurde.
 * 
 * <p>Java class for InspectedPart complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InspectedPart">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DevelopmentStage" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SerialNumberBodyshellWorkCharge" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Supplier" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="VehicleIdentificationNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Part" type="{http://www.inspection-plusplus.org}Part"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InspectedPart", propOrder = {
    "developmentStage",
    "serialNumberBodyshellWorkCharge",
    "supplier",
    "vehicleIdentificationNumber",
    "part"
})
public class InspectedPart {

    @XmlElement(name = "DevelopmentStage", required = true)
    protected String developmentStage;
    @XmlElement(name = "SerialNumberBodyshellWorkCharge", required = true)
    protected String serialNumberBodyshellWorkCharge;
    @XmlElement(name = "Supplier", required = true)
    protected String supplier;
    @XmlElement(name = "VehicleIdentificationNumber", required = true)
    protected String vehicleIdentificationNumber;
    @XmlElement(name = "Part", required = true)
    protected Part part;

    /**
     * Gets the value of the developmentStage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDevelopmentStage() {
        return developmentStage;
    }

    /**
     * Sets the value of the developmentStage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDevelopmentStage(String value) {
        this.developmentStage = value;
    }

    /**
     * Gets the value of the serialNumberBodyshellWorkCharge property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSerialNumberBodyshellWorkCharge() {
        return serialNumberBodyshellWorkCharge;
    }

    /**
     * Sets the value of the serialNumberBodyshellWorkCharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSerialNumberBodyshellWorkCharge(String value) {
        this.serialNumberBodyshellWorkCharge = value;
    }

    /**
     * Gets the value of the supplier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSupplier() {
        return supplier;
    }

    /**
     * Sets the value of the supplier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSupplier(String value) {
        this.supplier = value;
    }

    /**
     * Gets the value of the vehicleIdentificationNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehicleIdentificationNumber() {
        return vehicleIdentificationNumber;
    }

    /**
     * Sets the value of the vehicleIdentificationNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehicleIdentificationNumber(String value) {
        this.vehicleIdentificationNumber = value;
    }

    /**
     * Gets the value of the part property.
     * 
     * @return
     *     possible object is
     *     {@link Part }
     *     
     */
    public Part getPart() {
        return part;
    }

    /**
     * Sets the value of the part property.
     * 
     * @param value
     *     allowed object is
     *     {@link Part }
     *     
     */
    public void setPart(Part value) {
        this.part = value;
    }

}
