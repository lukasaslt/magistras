
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Inspected_IPE_ThreadedBolt complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Inspected_IPE_ThreadedBolt">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Inspected_IPE_Bolt">
 *       &lt;sequence>
 *         &lt;element name="StandardConformity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Thread" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Inspected_IPE_ThreadedBolt", propOrder = {
    "standardConformity",
    "thread"
})
public class InspectedIPEThreadedBolt
    extends InspectedIPEBolt
{

    @XmlElement(name = "StandardConformity")
    protected String standardConformity;
    @XmlElement(name = "Thread")
    protected String thread;

    /**
     * Gets the value of the standardConformity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStandardConformity() {
        return standardConformity;
    }

    /**
     * Sets the value of the standardConformity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStandardConformity(String value) {
        this.standardConformity = value;
    }

    /**
     * Gets the value of the thread property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getThread() {
        return thread;
    }

    /**
     * Sets the value of the thread property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setThread(String value) {
        this.thread = value;
    }

}
