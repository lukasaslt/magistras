
package org.inspection_plusplus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Transportobjekt. Transportiert beliebig viele Instanzen von ProductStructureContext.
 * 
 * <p>Java class for AllPScontexts complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AllPScontexts">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Comment" type="{http://www.inspection-plusplus.org}Comment" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ProductStructureContext" type="{http://www.inspection-plusplus.org}ProductStructureContext" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AllPScontexts", propOrder = {
    "comment",
    "productStructureContext"
})
public class AllPScontexts {

    @XmlElement(name = "Comment")
    protected List<Comment> comment;
    @XmlElement(name = "ProductStructureContext")
    protected List<ProductStructureContext> productStructureContext;

    /**
     * Gets the value of the comment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the comment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Comment }
     * 
     * 
     */
    public List<Comment> getComment() {
        if (comment == null) {
            comment = new ArrayList<Comment>();
        }
        return this.comment;
    }

    /**
     * Gets the value of the productStructureContext property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the productStructureContext property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProductStructureContext().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProductStructureContext }
     * 
     * 
     */
    public List<ProductStructureContext> getProductStructureContext() {
        if (productStructureContext == null) {
            productStructureContext = new ArrayList<ProductStructureContext>();
        }
        return this.productStructureContext;
    }

}
