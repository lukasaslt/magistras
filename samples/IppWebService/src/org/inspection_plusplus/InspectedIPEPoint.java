
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * siehe IPE_Point
 * 
 * <p>Java class for Inspected_IPE_Point complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Inspected_IPE_Point">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Inspected_IPE_Geometric">
 *       &lt;sequence>
 *         &lt;element name="MainAxis" type="{http://www.inspection-plusplus.org}Vector3D" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Inspected_IPE_Point", propOrder = {
    "mainAxis"
})
@XmlSeeAlso({
    InspectedIPECurvePoint.class,
    InspectedIPESurfacePoint.class,
    InspectedIPESymmetricPoint.class
})
public class InspectedIPEPoint
    extends InspectedIPEGeometric
{

    @XmlElement(name = "MainAxis")
    protected Vector3D mainAxis;

    /**
     * Gets the value of the mainAxis property.
     * 
     * @return
     *     possible object is
     *     {@link Vector3D }
     *     
     */
    public Vector3D getMainAxis() {
        return mainAxis;
    }

    /**
     * Sets the value of the mainAxis property.
     * 
     * @param value
     *     allowed object is
     *     {@link Vector3D }
     *     
     */
    public void setMainAxis(Vector3D value) {
        this.mainAxis = value;
    }

}
