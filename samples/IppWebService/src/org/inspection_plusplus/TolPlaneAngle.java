
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Abstrakte Klasse. Subklasse von Tol_PlaneWithReference. Ihre Subklassen beschreiben Winkelabweichungen, die in Bezug auf je eine Fl�che des Bezugssystems definiert werden.
 * 
 * <p>Java class for Tol_PlaneAngle complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Tol_PlaneAngle">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Tol_PlaneWithReference">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tol_PlaneAngle")
@XmlSeeAlso({
    TolPlaneAngleXY.class,
    TolPlaneAngleXZ.class,
    TolPlaneAngleYZ.class
})
public abstract class TolPlaneAngle
    extends TolPlaneWithReference
{


}
