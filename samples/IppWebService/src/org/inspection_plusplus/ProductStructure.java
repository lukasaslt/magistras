
package org.inspection_plusplus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Transportobjekt f�r 1 logische Produktstruktur im Sinne von Part_Assembly-Instanzen. Enth�lt genau 1 Version einer QS-Produktstruktur. InspectionTasks werden hier nicht transportiert.
 * 
 * <p>Java class for ProductStructure complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProductStructure">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FatherNodeID" type="{http://www.inspection-plusplus.org}ID4Referencing"/>
 *         &lt;element name="PositionedPartID" type="{http://www.inspection-plusplus.org}ID4Referencing"/>
 *         &lt;element name="PScontextID" type="{http://www.inspection-plusplus.org}ID4Referencing"/>
 *         &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Comment" type="{http://www.inspection-plusplus.org}Comment" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Part" type="{http://www.inspection-plusplus.org}Part" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProductStructure", propOrder = {
    "fatherNodeID",
    "positionedPartID",
    "pScontextID",
    "version",
    "comment",
    "part"
})
public class ProductStructure {

    @XmlElement(name = "FatherNodeID", required = true)
    protected ID4Referencing fatherNodeID;
    @XmlElement(name = "PositionedPartID", required = true)
    protected ID4Referencing positionedPartID;
    @XmlElement(name = "PScontextID", required = true)
    protected ID4Referencing pScontextID;
    @XmlElement(name = "Version", required = true)
    protected String version;
    @XmlElement(name = "Comment")
    protected List<Comment> comment;
    @XmlElement(name = "Part")
    protected List<Part> part;

    /**
     * Gets the value of the fatherNodeID property.
     * 
     * @return
     *     possible object is
     *     {@link ID4Referencing }
     *     
     */
    public ID4Referencing getFatherNodeID() {
        return fatherNodeID;
    }

    /**
     * Sets the value of the fatherNodeID property.
     * 
     * @param value
     *     allowed object is
     *     {@link ID4Referencing }
     *     
     */
    public void setFatherNodeID(ID4Referencing value) {
        this.fatherNodeID = value;
    }

    /**
     * Gets the value of the positionedPartID property.
     * 
     * @return
     *     possible object is
     *     {@link ID4Referencing }
     *     
     */
    public ID4Referencing getPositionedPartID() {
        return positionedPartID;
    }

    /**
     * Sets the value of the positionedPartID property.
     * 
     * @param value
     *     allowed object is
     *     {@link ID4Referencing }
     *     
     */
    public void setPositionedPartID(ID4Referencing value) {
        this.positionedPartID = value;
    }

    /**
     * Gets the value of the pScontextID property.
     * 
     * @return
     *     possible object is
     *     {@link ID4Referencing }
     *     
     */
    public ID4Referencing getPScontextID() {
        return pScontextID;
    }

    /**
     * Sets the value of the pScontextID property.
     * 
     * @param value
     *     allowed object is
     *     {@link ID4Referencing }
     *     
     */
    public void setPScontextID(ID4Referencing value) {
        this.pScontextID = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Gets the value of the comment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the comment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Comment }
     * 
     * 
     */
    public List<Comment> getComment() {
        if (comment == null) {
            comment = new ArrayList<Comment>();
        }
        return this.comment;
    }

    /**
     * Gets the value of the part property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the part property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPart().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Part }
     * 
     * 
     */
    public List<Part> getPart() {
        if (part == null) {
            part = new ArrayList<Part>();
        }
        return this.part;
    }

}
