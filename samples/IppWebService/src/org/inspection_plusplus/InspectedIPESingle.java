
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * siehe IPE _Single
 * 
 * <p>Java class for Inspected_IPE_Single complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Inspected_IPE_Single">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}InspectedIPE">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Inspected_IPE_Single")
@XmlSeeAlso({
    InspectedIPEForm.class,
    InspectedIPEScalar.class,
    InspectedIPEGeneric.class,
    InspectedIPEGeometric.class
})
public class InspectedIPESingle
    extends InspectedIPE
{


}
