
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Inspected_Generic_Attribute complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Inspected_Generic_Attribute">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.inspection-plusplus.org}Generic_Attribute_Type_ref"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Inspected_Generic_Attribute", propOrder = {
    "genericAttributeTypeRef"
})
@XmlSeeAlso({
    InspectedNumericalAttribute.class,
    InspectedBooleanAttribute.class,
    InspectedTextualAttribute.class,
    InspectedVector3DAttribute.class
})
public class InspectedGenericAttribute {

    @XmlElement(name = "Generic_Attribute_Type_ref", namespace = "http://www.inspection-plusplus.org", required = true)
    protected ID4Referencing genericAttributeTypeRef;

    /**
     * Gets the value of the genericAttributeTypeRef property.
     * 
     * @return
     *     possible object is
     *     {@link ID4Referencing }
     *     
     */
    public ID4Referencing getGenericAttributeTypeRef() {
        return genericAttributeTypeRef;
    }

    /**
     * Sets the value of the genericAttributeTypeRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link ID4Referencing }
     *     
     */
    public void setGenericAttributeTypeRef(ID4Referencing value) {
        this.genericAttributeTypeRef = value;
    }

}
