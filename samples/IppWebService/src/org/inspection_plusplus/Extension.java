
package org.inspection_plusplus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * Dient der freien Erweiterbarkeit von I++DMS-Klassen durch einzelne OEMs um weitere Datenstrukturen (komplexe oder einfache Datentypen). Soll nur dann angewendet werden, wenn der standardisierte Anteil des I++DMS-Modells eine angemessene Abbildung der betreffenden Information nicht erlaubt.
 * Jeder I++DMS-Client muss in der Lage sein, �ber Leseoperationen ankommende Instanzen erweiterter Klassen ohne Fehlermeldungen zu akzeptieren und ihre Standard-Anteile zu verarbeiten. Beim Zur�ckschreiben d�rfen die Extension-Inhalte verworfen werden.
 * Eine OEM-spezifische Umsetzung der I++DMS-Dienste muss so beschaffen sein, dass jeder standardkonforme I++DMS-Client ohne spezielle Anpassungen an eventuell vorhandene Extensions betrieben werden kann.
 * 
 * <p>Java class for Extension complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Extension">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;any maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Vendor" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Extension", propOrder = {
    "any"
})
public class Extension {

    @XmlAnyElement(lax = true)
    protected List<Object> any;
    @XmlAttribute(name = "Vendor")
    protected String vendor;

    /**
     * Gets the value of the any property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the any property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAny().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getAny() {
        if (any == null) {
            any = new ArrayList<Object>();
        }
        return this.any;
    }

    /**
     * Gets the value of the vendor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVendor() {
        return vendor;
    }

    /**
     * Sets the value of the vendor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVendor(String value) {
        this.vendor = value;
    }

}
