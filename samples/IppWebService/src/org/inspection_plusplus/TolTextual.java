
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Benutzerdefinierte, also nicht standardisierte Toleranzanforderung an ein Objekt. Maschinell nicht interpretierbar. Nur f�r Gebrauch durch den Anwender selbst.
 * 
 * <p>Java class for Tol_Textual complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Tol_Textual">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Tolerance">
 *       &lt;sequence>
 *         &lt;element name="Criterion" type="{http://www.inspection-plusplus.org}NaturalLanguageToleranceCriterion"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tol_Textual", propOrder = {
    "criterion"
})
public class TolTextual
    extends Tolerance
{

    @XmlElement(name = "Criterion", required = true)
    protected NaturalLanguageToleranceCriterion criterion;

    /**
     * Gets the value of the criterion property.
     * 
     * @return
     *     possible object is
     *     {@link NaturalLanguageToleranceCriterion }
     *     
     */
    public NaturalLanguageToleranceCriterion getCriterion() {
        return criterion;
    }

    /**
     * Sets the value of the criterion property.
     * 
     * @param value
     *     allowed object is
     *     {@link NaturalLanguageToleranceCriterion }
     *     
     */
    public void setCriterion(NaturalLanguageToleranceCriterion value) {
        this.criterion = value;
    }

}
