
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Tol_Boolean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Tol_Boolean">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Tol_Discrete">
 *       &lt;sequence>
 *         &lt;element name="criterion" type="{http://www.inspection-plusplus.org}BooleanToleranceCriterion"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tol_Boolean", propOrder = {
    "criterion"
})
public class TolBoolean
    extends TolDiscrete
{

    @XmlElement(required = true)
    protected BooleanToleranceCriterion criterion;

    /**
     * Gets the value of the criterion property.
     * 
     * @return
     *     possible object is
     *     {@link BooleanToleranceCriterion }
     *     
     */
    public BooleanToleranceCriterion getCriterion() {
        return criterion;
    }

    /**
     * Sets the value of the criterion property.
     * 
     * @param value
     *     allowed object is
     *     {@link BooleanToleranceCriterion }
     *     
     */
    public void setCriterion(BooleanToleranceCriterion value) {
        this.criterion = value;
    }

}
