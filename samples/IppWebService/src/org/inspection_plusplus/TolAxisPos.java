
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Abstrakte Klasse. Subklasse von Tol_AxisWithReference. Ihre Subklassen beschreiben Positionsabweichungen, die auf jeweils eine Raumachse projiziert werden. Aus dem Positionsvektor wird also nur jeweils 1 Koordinate betrachtet.
 * 
 * <p>Java class for Tol_AxisPos complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Tol_AxisPos">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Tol_AxisWithReference">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tol_AxisPos")
@XmlSeeAlso({
    TolAxisPosX.class,
    TolAxisPosZ.class,
    TolAxisPosY.class
})
public abstract class TolAxisPos
    extends TolAxisWithReference
{


}
