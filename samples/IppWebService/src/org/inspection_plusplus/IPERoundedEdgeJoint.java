
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IPE_RoundedEdgeJoint complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IPE_RoundedEdgeJoint">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IPE_Geometric">
 *       &lt;sequence>
 *         &lt;element name="Flash" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="Gap" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="MainAxis" type="{http://www.inspection-plusplus.org}Vector3D"/>
 *         &lt;element name="Radius1" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="Radius2" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IPE_RoundedEdgeJoint", propOrder = {
    "flash",
    "gap",
    "mainAxis",
    "radius1",
    "radius2"
})
public class IPERoundedEdgeJoint
    extends IPEGeometric
{

    @XmlElement(name = "Flash")
    protected double flash;
    @XmlElement(name = "Gap")
    protected double gap;
    @XmlElement(name = "MainAxis", required = true)
    protected Vector3D mainAxis;
    @XmlElement(name = "Radius1")
    protected double radius1;
    @XmlElement(name = "Radius2")
    protected double radius2;

    /**
     * Gets the value of the flash property.
     * 
     */
    public double getFlash() {
        return flash;
    }

    /**
     * Sets the value of the flash property.
     * 
     */
    public void setFlash(double value) {
        this.flash = value;
    }

    /**
     * Gets the value of the gap property.
     * 
     */
    public double getGap() {
        return gap;
    }

    /**
     * Sets the value of the gap property.
     * 
     */
    public void setGap(double value) {
        this.gap = value;
    }

    /**
     * Gets the value of the mainAxis property.
     * 
     * @return
     *     possible object is
     *     {@link Vector3D }
     *     
     */
    public Vector3D getMainAxis() {
        return mainAxis;
    }

    /**
     * Sets the value of the mainAxis property.
     * 
     * @param value
     *     allowed object is
     *     {@link Vector3D }
     *     
     */
    public void setMainAxis(Vector3D value) {
        this.mainAxis = value;
    }

    /**
     * Gets the value of the radius1 property.
     * 
     */
    public double getRadius1() {
        return radius1;
    }

    /**
     * Sets the value of the radius1 property.
     * 
     */
    public void setRadius1(double value) {
        this.radius1 = value;
    }

    /**
     * Gets the value of the radius2 property.
     * 
     */
    public double getRadius2() {
        return radius2;
    }

    /**
     * Sets the value of the radius2 property.
     * 
     */
    public void setRadius2(double value) {
        this.radius2 = value;
    }

}
