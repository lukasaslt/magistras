
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Pr�fprogramme, die nicht direkt einem Pr�fplan zugeordnet werden k�nnen, sondern die nur einen Teil eines �bergeordneten Pr�fprogramms darstellen.
 * 
 * <p>Java class for InspectionSubProgram complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InspectionSubProgram">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}InspectionProgram">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InspectionSubProgram")
public class InspectionSubProgram
    extends InspectionProgram
{


}
