
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Subklasse von Tolerance. Beschreibt die erlaubte Abweichung eines Objekts (IPE_SurfacePoint) in und entgegen der Richtung der Flächennormalen. UpperLimit gibt die Abweichung in Normalenrichtug an, LowerLimit die entgegengesetzte Abweichung.
 * 
 * <p>Java class for Tol_ProfileOfPoint complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Tol_ProfileOfPoint">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Tolerance">
 *       &lt;sequence>
 *         &lt;element name="Criterion" type="{http://www.inspection-plusplus.org}IntervalToleranceCriterion"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tol_ProfileOfPoint", propOrder = {
    "criterion"
})
public class TolProfileOfPoint
    extends Tolerance
{

    @XmlElement(name = "Criterion", required = true)
    protected IntervalToleranceCriterion criterion;

    /**
     * Gets the value of the criterion property.
     * 
     * @return
     *     possible object is
     *     {@link IntervalToleranceCriterion }
     *     
     */
    public IntervalToleranceCriterion getCriterion() {
        return criterion;
    }

    /**
     * Sets the value of the criterion property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntervalToleranceCriterion }
     *     
     */
    public void setCriterion(IntervalToleranceCriterion value) {
        this.criterion = value;
    }

}
