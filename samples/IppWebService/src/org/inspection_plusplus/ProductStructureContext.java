
package org.inspection_plusplus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Ankerobjekt f�r eine oder mehrere QS-Produktstrukturen. Siehe Beschreibung zu QS-Produktstrukturen und Abschnitt Mehrfachvererbung von Bauteilen.
 * Eine ProductStructureContext-Instanz repr�sentiert die organisatorische Einbettung von 0..n Part-Instanzen (i.d.R. Zusammenbaustrukturen), indem sie den Bezug zu einer Baureihe herstellt.. Zur Verwendung von Bauteilen in mehreren ProductStructureContexts  siehe Abschnitt zur Mehrfachvererbung von Bauteilen
 * 
 * <p>Java class for ProductStructureContext complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProductStructureContext">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IppDMSentity">
 *       &lt;sequence>
 *         &lt;element name="Company" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Location" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Project" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ProjectFamily" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Part" type="{http://www.inspection-plusplus.org}Part" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProductStructureContext", propOrder = {
    "company",
    "location",
    "project",
    "projectFamily",
    "part"
})
public class ProductStructureContext
    extends IppDMSentity
{

    @XmlElement(name = "Company", required = true)
    protected String company;
    @XmlElement(name = "Location", required = true)
    protected String location;
    @XmlElement(name = "Project", required = true)
    protected String project;
    @XmlElement(name = "ProjectFamily", required = true)
    protected String projectFamily;
    @XmlElement(name = "Part")
    protected List<Part> part;

    /**
     * Gets the value of the company property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompany() {
        return company;
    }

    /**
     * Sets the value of the company property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompany(String value) {
        this.company = value;
    }

    /**
     * Gets the value of the location property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocation() {
        return location;
    }

    /**
     * Sets the value of the location property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocation(String value) {
        this.location = value;
    }

    /**
     * Gets the value of the project property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProject() {
        return project;
    }

    /**
     * Sets the value of the project property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProject(String value) {
        this.project = value;
    }

    /**
     * Gets the value of the projectFamily property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProjectFamily() {
        return projectFamily;
    }

    /**
     * Sets the value of the projectFamily property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProjectFamily(String value) {
        this.projectFamily = value;
    }

    /**
     * Gets the value of the part property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the part property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPart().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Part }
     * 
     * 
     */
    public List<Part> getPart() {
        if (part == null) {
            part = new ArrayList<Part>();
        }
        return this.part;
    }

}
