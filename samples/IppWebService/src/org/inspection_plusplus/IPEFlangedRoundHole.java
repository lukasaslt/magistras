
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Kragenrundloch. 
 * 
 * <p>Java class for IPE_FlangedRoundHole complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IPE_FlangedRoundHole">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IPE_RoundHole">
 *       &lt;sequence>
 *         &lt;element name="FlangeHeigth" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="FlangeMatchesMainAxis" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="FlangeRadius" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IPE_FlangedRoundHole", propOrder = {
    "flangeHeigth",
    "flangeMatchesMainAxis",
    "flangeRadius"
})
public class IPEFlangedRoundHole
    extends IPERoundHole
{

    @XmlElement(name = "FlangeHeigth")
    protected double flangeHeigth;
    @XmlElement(name = "FlangeMatchesMainAxis")
    protected boolean flangeMatchesMainAxis;
    @XmlElement(name = "FlangeRadius")
    protected double flangeRadius;

    /**
     * Gets the value of the flangeHeigth property.
     * 
     */
    public double getFlangeHeigth() {
        return flangeHeigth;
    }

    /**
     * Sets the value of the flangeHeigth property.
     * 
     */
    public void setFlangeHeigth(double value) {
        this.flangeHeigth = value;
    }

    /**
     * Gets the value of the flangeMatchesMainAxis property.
     * 
     */
    public boolean isFlangeMatchesMainAxis() {
        return flangeMatchesMainAxis;
    }

    /**
     * Sets the value of the flangeMatchesMainAxis property.
     * 
     */
    public void setFlangeMatchesMainAxis(boolean value) {
        this.flangeMatchesMainAxis = value;
    }

    /**
     * Gets the value of the flangeRadius property.
     * 
     */
    public double getFlangeRadius() {
        return flangeRadius;
    }

    /**
     * Sets the value of the flangeRadius property.
     * 
     */
    public void setFlangeRadius(double value) {
        this.flangeRadius = value;
    }

}
