
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Inspected_IPE_FlangedSlot complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Inspected_IPE_FlangedSlot">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Inspected_IPE_Slot">
 *       &lt;sequence>
 *         &lt;element name="FlangeHeight" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="FlangeMatchesMainAxis" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="FlangeRadius" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Inspected_IPE_FlangedSlot", propOrder = {
    "flangeHeight",
    "flangeMatchesMainAxis",
    "flangeRadius"
})
public class InspectedIPEFlangedSlot
    extends InspectedIPESlot
{

    @XmlElement(name = "FlangeHeight")
    protected Double flangeHeight;
    @XmlElement(name = "FlangeMatchesMainAxis")
    protected Boolean flangeMatchesMainAxis;
    @XmlElement(name = "FlangeRadius")
    protected Double flangeRadius;

    /**
     * Gets the value of the flangeHeight property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getFlangeHeight() {
        return flangeHeight;
    }

    /**
     * Sets the value of the flangeHeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setFlangeHeight(Double value) {
        this.flangeHeight = value;
    }

    /**
     * Gets the value of the flangeMatchesMainAxis property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFlangeMatchesMainAxis() {
        return flangeMatchesMainAxis;
    }

    /**
     * Sets the value of the flangeMatchesMainAxis property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFlangeMatchesMainAxis(Boolean value) {
        this.flangeMatchesMainAxis = value;
    }

    /**
     * Gets the value of the flangeRadius property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getFlangeRadius() {
        return flangeRadius;
    }

    /**
     * Sets the value of the flangeRadius property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setFlangeRadius(Double value) {
        this.flangeRadius = value;
    }

}
