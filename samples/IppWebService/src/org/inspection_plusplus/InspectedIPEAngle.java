
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * siehe IPE_Angle
 * 
 * <p>Java class for Inspected_IPE_Angle complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Inspected_IPE_Angle">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Inspected_IPE_Scalar">
 *       &lt;sequence>
 *         &lt;element name="Angle" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="AxisAngle_X" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="AxisAngle_Y" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="AxisAngle_Z" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="PlaneAngle_XY" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="PlaneAngle_XZ" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="PlaneAngle_YZ" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Inspected_IPE_Angle", propOrder = {
    "angle",
    "axisAngleX",
    "axisAngleY",
    "axisAngleZ",
    "planeAngleXY",
    "planeAngleXZ",
    "planeAngleYZ"
})
public class InspectedIPEAngle
    extends InspectedIPEScalar
{

    @XmlElement(name = "Angle")
    protected Double angle;
    @XmlElement(name = "AxisAngle_X")
    protected Double axisAngleX;
    @XmlElement(name = "AxisAngle_Y")
    protected Double axisAngleY;
    @XmlElement(name = "AxisAngle_Z")
    protected Double axisAngleZ;
    @XmlElement(name = "PlaneAngle_XY")
    protected Double planeAngleXY;
    @XmlElement(name = "PlaneAngle_XZ")
    protected Double planeAngleXZ;
    @XmlElement(name = "PlaneAngle_YZ")
    protected Double planeAngleYZ;

    /**
     * Gets the value of the angle property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getAngle() {
        return angle;
    }

    /**
     * Sets the value of the angle property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setAngle(Double value) {
        this.angle = value;
    }

    /**
     * Gets the value of the axisAngleX property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getAxisAngleX() {
        return axisAngleX;
    }

    /**
     * Sets the value of the axisAngleX property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setAxisAngleX(Double value) {
        this.axisAngleX = value;
    }

    /**
     * Gets the value of the axisAngleY property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getAxisAngleY() {
        return axisAngleY;
    }

    /**
     * Sets the value of the axisAngleY property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setAxisAngleY(Double value) {
        this.axisAngleY = value;
    }

    /**
     * Gets the value of the axisAngleZ property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getAxisAngleZ() {
        return axisAngleZ;
    }

    /**
     * Sets the value of the axisAngleZ property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setAxisAngleZ(Double value) {
        this.axisAngleZ = value;
    }

    /**
     * Gets the value of the planeAngleXY property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getPlaneAngleXY() {
        return planeAngleXY;
    }

    /**
     * Sets the value of the planeAngleXY property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setPlaneAngleXY(Double value) {
        this.planeAngleXY = value;
    }

    /**
     * Gets the value of the planeAngleXZ property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getPlaneAngleXZ() {
        return planeAngleXZ;
    }

    /**
     * Sets the value of the planeAngleXZ property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setPlaneAngleXZ(Double value) {
        this.planeAngleXZ = value;
    }

    /**
     * Gets the value of the planeAngleYZ property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getPlaneAngleYZ() {
        return planeAngleYZ;
    }

    /**
     * Sets the value of the planeAngleYZ property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setPlaneAngleYZ(Double value) {
        this.planeAngleYZ = value;
    }

}
