
package org.inspection_plusplus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Transportobjekt, das als R�ckgabeergebnis f�r Operationen verwendet werden kann. Derzeit f�r searchInspectionPlans. Achtung: enth�lt keine vollst�ndigen Inspection-Pl�ne, sondern nur die Leitinstanzen der Klasse InspectionPlan. Keine Comment-Instanzen.
 * 
 * <p>Java class for InspectionPlanList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InspectionPlanList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InspectionPlan" type="{http://www.inspection-plusplus.org}InspectionPlan" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InspectionPlanList", propOrder = {
    "inspectionPlan"
})
public class InspectionPlanList {

    @XmlElement(name = "InspectionPlan")
    protected List<InspectionPlan> inspectionPlan;

    /**
     * Gets the value of the inspectionPlan property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the inspectionPlan property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInspectionPlan().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InspectionPlan }
     * 
     * 
     */
    public List<InspectionPlan> getInspectionPlan() {
        if (inspectionPlan == null) {
            inspectionPlan = new ArrayList<InspectionPlan>();
        }
        return this.inspectionPlan;
    }

}
