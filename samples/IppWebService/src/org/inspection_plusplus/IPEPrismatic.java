
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Abstrakte Klasse. Repr�sentiert einen bestimmten regul�ren K�rper.
 * 
 * <p>Java class for IPE_Prismatic complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IPE_Prismatic">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IPE_Geometric">
 *       &lt;sequence>
 *         &lt;element name="MainAxis" type="{http://www.inspection-plusplus.org}Vector3D"/>
 *         &lt;element name="MeasuringAid" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="StandardPartReference" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IPE_Prismatic", propOrder = {
    "mainAxis",
    "measuringAid",
    "standardPartReference"
})
@XmlSeeAlso({
    IPECone.class,
    IPESphere.class,
    IPEBolt.class
})
public abstract class IPEPrismatic
    extends IPEGeometric
{

    @XmlElement(name = "MainAxis", required = true)
    protected Vector3D mainAxis;
    @XmlElement(name = "MeasuringAid")
    protected boolean measuringAid;
    @XmlElement(name = "StandardPartReference", required = true)
    protected String standardPartReference;

    /**
     * Gets the value of the mainAxis property.
     * 
     * @return
     *     possible object is
     *     {@link Vector3D }
     *     
     */
    public Vector3D getMainAxis() {
        return mainAxis;
    }

    /**
     * Sets the value of the mainAxis property.
     * 
     * @param value
     *     allowed object is
     *     {@link Vector3D }
     *     
     */
    public void setMainAxis(Vector3D value) {
        this.mainAxis = value;
    }

    /**
     * Gets the value of the measuringAid property.
     * 
     */
    public boolean isMeasuringAid() {
        return measuringAid;
    }

    /**
     * Sets the value of the measuringAid property.
     * 
     */
    public void setMeasuringAid(boolean value) {
        this.measuringAid = value;
    }

    /**
     * Gets the value of the standardPartReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStandardPartReference() {
        return standardPartReference;
    }

    /**
     * Sets the value of the standardPartReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStandardPartReference(String value) {
        this.standardPartReference = value;
    }

}
