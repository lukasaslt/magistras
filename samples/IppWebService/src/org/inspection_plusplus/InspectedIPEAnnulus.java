
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Inspected_IPE_Annulus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Inspected_IPE_Annulus">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Inspected_IPE_Plane">
 *       &lt;sequence>
 *         &lt;element name="InnerDiameter" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="OuterDiameter" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Inspected_IPE_Annulus", propOrder = {
    "innerDiameter",
    "outerDiameter"
})
public class InspectedIPEAnnulus
    extends InspectedIPEPlane
{

    @XmlElement(name = "InnerDiameter")
    protected Double innerDiameter;
    @XmlElement(name = "OuterDiameter")
    protected Double outerDiameter;

    /**
     * Gets the value of the innerDiameter property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getInnerDiameter() {
        return innerDiameter;
    }

    /**
     * Sets the value of the innerDiameter property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setInnerDiameter(Double value) {
        this.innerDiameter = value;
    }

    /**
     * Gets the value of the outerDiameter property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getOuterDiameter() {
        return outerDiameter;
    }

    /**
     * Sets the value of the outerDiameter property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setOuterDiameter(Double value) {
        this.outerDiameter = value;
    }

}
