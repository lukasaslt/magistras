
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Unendliche Linie, Gerade.
 * 
 * <p>Java class for IPE_Line complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IPE_Line">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IPE_Geometric">
 *       &lt;sequence>
 *         &lt;element name="MainAxis" type="{http://www.inspection-plusplus.org}Vector3D"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IPE_Line", propOrder = {
    "mainAxis"
})
@XmlSeeAlso({
    IPELimitedLine.class
})
public class IPELine
    extends IPEGeometric
{

    @XmlElement(name = "MainAxis", required = true)
    protected Vector3D mainAxis;

    /**
     * Gets the value of the mainAxis property.
     * 
     * @return
     *     possible object is
     *     {@link Vector3D }
     *     
     */
    public Vector3D getMainAxis() {
        return mainAxis;
    }

    /**
     * Sets the value of the mainAxis property.
     * 
     * @param value
     *     allowed object is
     *     {@link Vector3D }
     *     
     */
    public void setMainAxis(Vector3D value) {
        this.mainAxis = value;
    }

}
