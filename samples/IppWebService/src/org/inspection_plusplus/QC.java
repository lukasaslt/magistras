
package org.inspection_plusplus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Abstrakte Klasse. B�ndelt gemeinsame Inhalte von QC_Group und QC_Single.
 * 
 * <p>Java class for QC complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QC">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IppDMSentity">
 *       &lt;sequence>
 *         &lt;element name="AllAssembliesReferenced" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Function" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PartQAversion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element ref="{http://www.inspection-plusplus.org}Part_ref" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://www.inspection-plusplus.org}Calculation_ActualElementDetection_ref" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://www.inspection-plusplus.org}IPE_ref" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QC", propOrder = {
    "allAssembliesReferenced",
    "description",
    "function",
    "partQAversion",
    "partRef",
    "calculationActualElementDetectionRef",
    "ipeRef"
})
@XmlSeeAlso({
    QCGroup.class,
    QCSingle.class
})
public abstract class QC
    extends IppDMSentity
{

    @XmlElement(name = "AllAssembliesReferenced")
    protected boolean allAssembliesReferenced;
    @XmlElement(name = "Description", required = true)
    protected String description;
    @XmlElement(name = "Function", required = true)
    protected String function;
    @XmlElement(name = "PartQAversion", required = true)
    protected String partQAversion;
    @XmlElement(name = "Part_ref", namespace = "http://www.inspection-plusplus.org")
    protected List<ID4Referencing> partRef;
    @XmlElement(name = "Calculation_ActualElementDetection_ref", namespace = "http://www.inspection-plusplus.org")
    protected List<Object> calculationActualElementDetectionRef;
    @XmlElement(name = "IPE_ref", namespace = "http://www.inspection-plusplus.org")
    protected List<ID4Referencing> ipeRef;

    /**
     * Gets the value of the allAssembliesReferenced property.
     * 
     */
    public boolean isAllAssembliesReferenced() {
        return allAssembliesReferenced;
    }

    /**
     * Sets the value of the allAssembliesReferenced property.
     * 
     */
    public void setAllAssembliesReferenced(boolean value) {
        this.allAssembliesReferenced = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the function property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFunction() {
        return function;
    }

    /**
     * Sets the value of the function property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFunction(String value) {
        this.function = value;
    }

    /**
     * Gets the value of the partQAversion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartQAversion() {
        return partQAversion;
    }

    /**
     * Sets the value of the partQAversion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartQAversion(String value) {
        this.partQAversion = value;
    }

    /**
     * Gets the value of the partRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the partRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPartRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ID4Referencing }
     * 
     * 
     */
    public List<ID4Referencing> getPartRef() {
        if (partRef == null) {
            partRef = new ArrayList<ID4Referencing>();
        }
        return this.partRef;
    }

    /**
     * Gets the value of the calculationActualElementDetectionRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the calculationActualElementDetectionRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCalculationActualElementDetectionRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getCalculationActualElementDetectionRef() {
        if (calculationActualElementDetectionRef == null) {
            calculationActualElementDetectionRef = new ArrayList<Object>();
        }
        return this.calculationActualElementDetectionRef;
    }

    /**
     * Gets the value of the ipeRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ipeRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIPERef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ID4Referencing }
     * 
     * 
     */
    public List<ID4Referencing> getIPERef() {
        if (ipeRef == null) {
            ipeRef = new ArrayList<ID4Referencing>();
        }
        return this.ipeRef;
    }

}
