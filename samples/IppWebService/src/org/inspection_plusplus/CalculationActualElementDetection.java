
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Repräsentiert eine Berechnung oder auszuführende Strategie, die als Input bereits existierende IPEs verwendet (diese werden als Operanden angegeben). Im Infomodell wird kein Output definiert. Calculation_ActualElementDetection erbt die Komposition zu QC von seiner Elternklasse, ist also genau einem QC (QC_Single oder QC_Group) logisch zugeordnet und wird in diese durch dieselbe Komposition auch für Transportzwecke eingebettet. 
 * Beispielanwendung: Messprinzipien, auch Messstrategien genannt.
 * 
 * <p>Java class for Calculation_ActualElementDetection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Calculation_ActualElementDetection">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Calculation">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Calculation_ActualElementDetection")
public class CalculationActualElementDetection
    extends Calculation
{


}
