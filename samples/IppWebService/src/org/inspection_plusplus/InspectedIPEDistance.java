
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * siehe IPE_Distance
 * 
 * <p>Java class for Inspected_IPE_Distance complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Inspected_IPE_Distance">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}Inspected_IPE_Scalar">
 *       &lt;sequence>
 *         &lt;element name="AxisDist_X" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="AxisDist_Y" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="AxisDist_Z" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="Distance" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Inspected_IPE_Distance", propOrder = {
    "axisDistX",
    "axisDistY",
    "axisDistZ",
    "distance"
})
public class InspectedIPEDistance
    extends InspectedIPEScalar
{

    @XmlElement(name = "AxisDist_X")
    protected Double axisDistX;
    @XmlElement(name = "AxisDist_Y")
    protected Double axisDistY;
    @XmlElement(name = "AxisDist_Z")
    protected Double axisDistZ;
    @XmlElement(name = "Distance")
    protected Double distance;

    /**
     * Gets the value of the axisDistX property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getAxisDistX() {
        return axisDistX;
    }

    /**
     * Sets the value of the axisDistX property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setAxisDistX(Double value) {
        this.axisDistX = value;
    }

    /**
     * Gets the value of the axisDistY property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getAxisDistY() {
        return axisDistY;
    }

    /**
     * Sets the value of the axisDistY property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setAxisDistY(Double value) {
        this.axisDistY = value;
    }

    /**
     * Gets the value of the axisDistZ property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getAxisDistZ() {
        return axisDistZ;
    }

    /**
     * Sets the value of the axisDistZ property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setAxisDistZ(Double value) {
        this.axisDistZ = value;
    }

    /**
     * Gets the value of the distance property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getDistance() {
        return distance;
    }

    /**
     * Sets the value of the distance property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setDistance(Double value) {
        this.distance = value;
    }

}
