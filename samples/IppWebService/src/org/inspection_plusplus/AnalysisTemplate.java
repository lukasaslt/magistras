
package org.inspection_plusplus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Eine f�r einen bestimmten Zweck (oder mehrere Zwecke) erstellte Auswertevorlage. 
 * 
 * Bei VW haben in der Praxis erstellte Auswertevorlagen immer einen Bezug zu Teilen und einem PMP.
 * 
 * <p>Java class for AnalysisTemplate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AnalysisTemplate">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IppDMSentity">
 *       &lt;sequence>
 *         &lt;element name="FileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FileType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Purpose" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AnalysisTemplate_Element" type="{http://www.inspection-plusplus.org}AnalysisTemplate_Element" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Picture" type="{http://www.inspection-plusplus.org}Picture"/>
 *         &lt;element name="InspectionPlan" type="{http://www.inspection-plusplus.org}InspectionPlan" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Part" type="{http://www.inspection-plusplus.org}Part" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AnalysisTemplate", propOrder = {
    "fileName",
    "fileType",
    "purpose",
    "analysisTemplateElement",
    "picture",
    "inspectionPlan",
    "part"
})
public class AnalysisTemplate
    extends IppDMSentity
{

    @XmlElement(name = "FileName", required = true)
    protected String fileName;
    @XmlElement(name = "FileType", required = true)
    protected String fileType;
    @XmlElement(name = "Purpose", required = true)
    protected String purpose;
    @XmlElement(name = "AnalysisTemplate_Element")
    protected List<AnalysisTemplateElement> analysisTemplateElement;
    @XmlElement(name = "Picture", required = true)
    protected Picture picture;
    @XmlElement(name = "InspectionPlan")
    protected List<InspectionPlan> inspectionPlan;
    @XmlElement(name = "Part")
    protected List<Part> part;

    /**
     * Gets the value of the fileName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Sets the value of the fileName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileName(String value) {
        this.fileName = value;
    }

    /**
     * Gets the value of the fileType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileType() {
        return fileType;
    }

    /**
     * Sets the value of the fileType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileType(String value) {
        this.fileType = value;
    }

    /**
     * Gets the value of the purpose property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurpose() {
        return purpose;
    }

    /**
     * Sets the value of the purpose property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurpose(String value) {
        this.purpose = value;
    }

    /**
     * Gets the value of the analysisTemplateElement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the analysisTemplateElement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAnalysisTemplateElement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AnalysisTemplateElement }
     * 
     * 
     */
    public List<AnalysisTemplateElement> getAnalysisTemplateElement() {
        if (analysisTemplateElement == null) {
            analysisTemplateElement = new ArrayList<AnalysisTemplateElement>();
        }
        return this.analysisTemplateElement;
    }

    /**
     * Gets the value of the picture property.
     * 
     * @return
     *     possible object is
     *     {@link Picture }
     *     
     */
    public Picture getPicture() {
        return picture;
    }

    /**
     * Sets the value of the picture property.
     * 
     * @param value
     *     allowed object is
     *     {@link Picture }
     *     
     */
    public void setPicture(Picture value) {
        this.picture = value;
    }

    /**
     * Gets the value of the inspectionPlan property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the inspectionPlan property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInspectionPlan().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InspectionPlan }
     * 
     * 
     */
    public List<InspectionPlan> getInspectionPlan() {
        if (inspectionPlan == null) {
            inspectionPlan = new ArrayList<InspectionPlan>();
        }
        return this.inspectionPlan;
    }

    /**
     * Gets the value of the part property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the part property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPart().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Part }
     * 
     * 
     */
    public List<Part> getPart() {
        if (part == null) {
            part = new ArrayList<Part>();
        }
        return this.part;
    }

}
