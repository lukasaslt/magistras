
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Kreis. Kann als Hilfselement in einer Calculation verwendet werden.
 * 
 * Das geerbte Attribut Origin definiert den Mittelpunkt des Kreises.
 * Das optionale, geerbte Attribut Orientation definiert den Startwinkel des Kreises.
 * 
 * <p>Java class for IPE_Circle complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IPE_Circle">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IPE_Geometric">
 *       &lt;sequence>
 *         &lt;element name="Diameter" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="MainAxis" type="{http://www.inspection-plusplus.org}Vector3D"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IPE_Circle", propOrder = {
    "diameter",
    "mainAxis"
})
public class IPECircle
    extends IPEGeometric
{

    @XmlElement(name = "Diameter")
    protected double diameter;
    @XmlElement(name = "MainAxis", required = true)
    protected Vector3D mainAxis;

    /**
     * Gets the value of the diameter property.
     * 
     */
    public double getDiameter() {
        return diameter;
    }

    /**
     * Sets the value of the diameter property.
     * 
     */
    public void setDiameter(double value) {
        this.diameter = value;
    }

    /**
     * Gets the value of the mainAxis property.
     * 
     * @return
     *     possible object is
     *     {@link Vector3D }
     *     
     */
    public Vector3D getMainAxis() {
        return mainAxis;
    }

    /**
     * Sets the value of the mainAxis property.
     * 
     * @param value
     *     allowed object is
     *     {@link Vector3D }
     *     
     */
    public void setMainAxis(Vector3D value) {
        this.mainAxis = value;
    }

}
