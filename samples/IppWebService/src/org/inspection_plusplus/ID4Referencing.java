
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Datentyp zur Referenzierung zwischen zwei Instanzen (Schlüssel, Fremdschlüssel). Nicht zu verwechseln mit ID4Objects.
 * 
 * <p>Java class for ID4Referencing complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ID4Referencing">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}SuperID">
 *       &lt;sequence>
 *         &lt;element name="ClassID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ID4Referencing", propOrder = {
    "classID"
})
public class ID4Referencing
    extends SuperID
{

    @XmlElement(name = "ClassID", required = true)
    protected String classID;

    /**
     * Gets the value of the classID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassID() {
        return classID;
    }

    /**
     * Sets the value of the classID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassID(String value) {
        this.classID = value;
    }

}
