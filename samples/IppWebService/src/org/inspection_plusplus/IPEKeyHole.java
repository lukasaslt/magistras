
package org.inspection_plusplus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Schlüsselloch. Spezielles Langloch mit zwei unterschiedlichen Lochdurchmessern, aber parallelen Längsseiten. 
 * 
 * <p>Java class for IPE_KeyHole complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IPE_KeyHole">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IPE_Slot">
 *       &lt;sequence>
 *         &lt;element name="Diameter2" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="RoundingRadius" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IPE_KeyHole", propOrder = {
    "diameter2",
    "roundingRadius"
})
public class IPEKeyHole
    extends IPESlot
{

    @XmlElement(name = "Diameter2")
    protected double diameter2;
    @XmlElement(name = "RoundingRadius")
    protected double roundingRadius;

    /**
     * Gets the value of the diameter2 property.
     * 
     */
    public double getDiameter2() {
        return diameter2;
    }

    /**
     * Sets the value of the diameter2 property.
     * 
     */
    public void setDiameter2(double value) {
        this.diameter2 = value;
    }

    /**
     * Gets the value of the roundingRadius property.
     * 
     */
    public double getRoundingRadius() {
        return roundingRadius;
    }

    /**
     * Sets the value of the roundingRadius property.
     * 
     */
    public void setRoundingRadius(double value) {
        this.roundingRadius = value;
    }

}
