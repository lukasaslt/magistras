
package org.inspection_plusplus;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Ein Loch mit drei oder mehr Ecken, bei dem alle Kanten und Winkel gleich lang/gro� sind.
 * 
 * Das geerbte Attribut Orientation zeigt auf eine Spitze.
 * Das geerbte Attribut Origin defineirt den Mittelpunkt des Kreises.
 * 
 * <p>Java class for IPE_RegularPolygonalHole complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IPE_RegularPolygonalHole">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IPE_Hole">
 *       &lt;sequence>
 *         &lt;element name="InnerDiameter" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="NumberOfCorners" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IPE_RegularPolygonalHole", propOrder = {
    "innerDiameter",
    "numberOfCorners"
})
public class IPERegularPolygonalHole
    extends IPEHole
{

    @XmlElement(name = "InnerDiameter")
    protected double innerDiameter;
    @XmlElement(name = "NumberOfCorners", required = true)
    protected BigInteger numberOfCorners;

    /**
     * Gets the value of the innerDiameter property.
     * 
     */
    public double getInnerDiameter() {
        return innerDiameter;
    }

    /**
     * Sets the value of the innerDiameter property.
     * 
     */
    public void setInnerDiameter(double value) {
        this.innerDiameter = value;
    }

    /**
     * Gets the value of the numberOfCorners property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfCorners() {
        return numberOfCorners;
    }

    /**
     * Sets the value of the numberOfCorners property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfCorners(BigInteger value) {
        this.numberOfCorners = value;
    }

}
