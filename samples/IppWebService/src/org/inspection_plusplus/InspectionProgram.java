
package org.inspection_plusplus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Messprogramm. Die Inhalte von Messprogrammen werden nicht vollständig repräsentiert, sondern hier wird nur ein Verweis auf das eigentliche Messprogramm verwaltet.
 * 
 * <p>Java class for InspectionProgram complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InspectionProgram">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.inspection-plusplus.org}IppDMSentity">
 *       &lt;sequence>
 *         &lt;element name="CatalogueID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="HardwareCompatibility" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IsIPconform" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Revision" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="StorageLocation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="QC" type="{http://www.inspection-plusplus.org}QC" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="InspectionTask" type="{http://www.inspection-plusplus.org}InspectionTask" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="InspectionSubProgram" type="{http://www.inspection-plusplus.org}InspectionSubProgram" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Calculation_ActualElementDetection" type="{http://www.inspection-plusplus.org}Calculation_ActualElementDetection" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="InspectionPlan" type="{http://www.inspection-plusplus.org}InspectionPlan" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InspectionProgram", propOrder = {
    "catalogueID",
    "hardwareCompatibility",
    "isIPconform",
    "revision",
    "storageLocation",
    "qc",
    "inspectionTask",
    "inspectionSubProgram",
    "calculationActualElementDetection",
    "inspectionPlan"
})
@XmlSeeAlso({
    InspectionSubProgram.class
})
public class InspectionProgram
    extends IppDMSentity
{

    @XmlElement(name = "CatalogueID", required = true)
    protected String catalogueID;
    @XmlElement(name = "HardwareCompatibility", required = true)
    protected String hardwareCompatibility;
    @XmlElement(name = "IsIPconform")
    protected boolean isIPconform;
    @XmlElement(name = "Revision", required = true)
    protected String revision;
    @XmlElement(name = "StorageLocation", required = true)
    protected String storageLocation;
    @XmlElement(name = "QC")
    protected List<QC> qc;
    @XmlElement(name = "InspectionTask")
    protected List<InspectionTask> inspectionTask;
    @XmlElement(name = "InspectionSubProgram")
    protected List<InspectionSubProgram> inspectionSubProgram;
    @XmlElement(name = "Calculation_ActualElementDetection")
    protected List<CalculationActualElementDetection> calculationActualElementDetection;
    @XmlElement(name = "InspectionPlan")
    protected List<InspectionPlan> inspectionPlan;

    /**
     * Gets the value of the catalogueID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCatalogueID() {
        return catalogueID;
    }

    /**
     * Sets the value of the catalogueID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCatalogueID(String value) {
        this.catalogueID = value;
    }

    /**
     * Gets the value of the hardwareCompatibility property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHardwareCompatibility() {
        return hardwareCompatibility;
    }

    /**
     * Sets the value of the hardwareCompatibility property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHardwareCompatibility(String value) {
        this.hardwareCompatibility = value;
    }

    /**
     * Gets the value of the isIPconform property.
     * 
     */
    public boolean isIsIPconform() {
        return isIPconform;
    }

    /**
     * Sets the value of the isIPconform property.
     * 
     */
    public void setIsIPconform(boolean value) {
        this.isIPconform = value;
    }

    /**
     * Gets the value of the revision property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRevision() {
        return revision;
    }

    /**
     * Sets the value of the revision property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRevision(String value) {
        this.revision = value;
    }

    /**
     * Gets the value of the storageLocation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStorageLocation() {
        return storageLocation;
    }

    /**
     * Sets the value of the storageLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStorageLocation(String value) {
        this.storageLocation = value;
    }

    /**
     * Gets the value of the qc property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the qc property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getQC().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QC }
     * 
     * 
     */
    public List<QC> getQC() {
        if (qc == null) {
            qc = new ArrayList<QC>();
        }
        return this.qc;
    }

    /**
     * Gets the value of the inspectionTask property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the inspectionTask property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInspectionTask().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InspectionTask }
     * 
     * 
     */
    public List<InspectionTask> getInspectionTask() {
        if (inspectionTask == null) {
            inspectionTask = new ArrayList<InspectionTask>();
        }
        return this.inspectionTask;
    }

    /**
     * Gets the value of the inspectionSubProgram property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the inspectionSubProgram property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInspectionSubProgram().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InspectionSubProgram }
     * 
     * 
     */
    public List<InspectionSubProgram> getInspectionSubProgram() {
        if (inspectionSubProgram == null) {
            inspectionSubProgram = new ArrayList<InspectionSubProgram>();
        }
        return this.inspectionSubProgram;
    }

    /**
     * Gets the value of the calculationActualElementDetection property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the calculationActualElementDetection property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCalculationActualElementDetection().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CalculationActualElementDetection }
     * 
     * 
     */
    public List<CalculationActualElementDetection> getCalculationActualElementDetection() {
        if (calculationActualElementDetection == null) {
            calculationActualElementDetection = new ArrayList<CalculationActualElementDetection>();
        }
        return this.calculationActualElementDetection;
    }

    /**
     * Gets the value of the inspectionPlan property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the inspectionPlan property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInspectionPlan().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InspectionPlan }
     * 
     * 
     */
    public List<InspectionPlan> getInspectionPlan() {
        if (inspectionPlan == null) {
            inspectionPlan = new ArrayList<InspectionPlan>();
        }
        return this.inspectionPlan;
    }

}
