package eu.sarunas.atf.demo.geometry;

import eu.sarunas.atf.demo.geometry.shapes.Triangle;

public class AreaCalculator
{
	public float calculateArea(Triangle triangle)
	{
		float s = (triangle.getA() + triangle.getB() + triangle.getC()) / 2;

		return (float)Math.sqrt(s * (s - triangle.getA()) * (s - triangle.getB()) * (s - triangle.getC()));
	};
	
	public float foo(float a)
	{
		return a + 100.0f;
	};
};
