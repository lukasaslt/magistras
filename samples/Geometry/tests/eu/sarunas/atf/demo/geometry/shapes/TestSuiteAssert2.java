package eu.sarunas.atf.demo.geometry.shapes;
public class TestSuiteAssert2
{
	@org.junit.Test
	public void testAssertPostConditions1() throws Throwable
	{
		eu.sarunas.atf.demo.geometry.shapes.Assert2 testObject = new eu.sarunas.atf.demo.geometry.shapes.Assert2();
		java.lang.reflect.Method method0 = new java.lang.reflect.Method();

		testObject.assertPostConditions(new Object(), method0, new Object[] { new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object() }, new Object(), new String[] { "", "", "aaaa", "", "aaaaaaaaaaaaaa", "", "", "", "aa", "a", "aaaaaaaaaaaaaaa", "aaaaaaaaaaaaaa", "", "", "", "aaaaaaaa" });
		java.util.ArrayList<Object> preValues1 = new java.util.ArrayList<Object>();
		preValues1.add(new Object());
		preValues1.add(method0);
		preValues1.add(new Object[] { new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object() });
		preValues1.add(new Object());
		preValues1.add(new String[] { "", "", "aaaa", "", "aaaaaaaaaaaaaa", "", "", "", "aa", "a", "aaaaaaaaaaaaaaa", "aaaaaaaaaaaaaa", "", "", "", "aaaaaaaa" });

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("assertPostConditions", Object.class, java.lang.reflect.Method.class, Object.class, Object.class, String.class), preValues1, res, "/eu/sarunas/atf/demo/geometry/model.ocl", "/eu/sarunas/atf/demo/geometry/shapes/model.ocl");
	};

	@org.junit.Test
	public void testAssertPostConditions2() throws Throwable
	{
		eu.sarunas.atf.demo.geometry.shapes.Assert2 testObject = new eu.sarunas.atf.demo.geometry.shapes.Assert2();
		java.lang.reflect.Method method2 = new java.lang.reflect.Method();

		testObject.assertPostConditions(new Object(), method2, new Object[] { new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object() }, new Object(), new String[] { "", "", "aaa", "", "", "", "", "aaaaaaa", "aaa", "aaaaaaaaaaaaa", "", "" });
		java.util.ArrayList<Object> preValues3 = new java.util.ArrayList<Object>();
		preValues3.add(new Object());
		preValues3.add(method2);
		preValues3.add(new Object[] { new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object() });
		preValues3.add(new Object());
		preValues3.add(new String[] { "", "", "aaa", "", "", "", "", "aaaaaaa", "aaa", "aaaaaaaaaaaaa", "", "" });

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("assertPostConditions", Object.class, java.lang.reflect.Method.class, Object.class, Object.class, String.class), preValues3, res, "/eu/sarunas/atf/demo/geometry/model.ocl", "/eu/sarunas/atf/demo/geometry/shapes/model.ocl");
	};

	@org.junit.Test
	public void testAssertPostConditions3() throws Throwable
	{
		eu.sarunas.atf.demo.geometry.shapes.Assert2 testObject = new eu.sarunas.atf.demo.geometry.shapes.Assert2();
		java.lang.reflect.Method method4 = new java.lang.reflect.Method();

		testObject.assertPostConditions(new Object(), method4, new Object[] { new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object() }, new Object(), new String[] { "aaaaa", "aaa", "aaaaaaaaaaaa", "", "aaaaaaaa" });
		java.util.ArrayList<Object> preValues5 = new java.util.ArrayList<Object>();
		preValues5.add(new Object());
		preValues5.add(method4);
		preValues5.add(new Object[] { new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object() });
		preValues5.add(new Object());
		preValues5.add(new String[] { "aaaaa", "aaa", "aaaaaaaaaaaa", "", "aaaaaaaa" });

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("assertPostConditions", Object.class, java.lang.reflect.Method.class, Object.class, Object.class, String.class), preValues5, res, "/eu/sarunas/atf/demo/geometry/model.ocl", "/eu/sarunas/atf/demo/geometry/shapes/model.ocl");
	};

	@org.junit.Test
	public void testAssertPostConditions4() throws Throwable
	{
		eu.sarunas.atf.demo.geometry.shapes.Assert2 testObject = new eu.sarunas.atf.demo.geometry.shapes.Assert2();
		java.lang.reflect.Method method6 = new java.lang.reflect.Method();

		testObject.assertPostConditions(new Object(), method6, new Object[] { new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object() }, new Object(), new String[] { "aaa", "aaaaaaaaaaaaaaaaaaa", "", "", "", "", "" });
		java.util.ArrayList<Object> preValues7 = new java.util.ArrayList<Object>();
		preValues7.add(new Object());
		preValues7.add(method6);
		preValues7.add(new Object[] { new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object() });
		preValues7.add(new Object());
		preValues7.add(new String[] { "aaa", "aaaaaaaaaaaaaaaaaaa", "", "", "", "", "" });

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("assertPostConditions", Object.class, java.lang.reflect.Method.class, Object.class, Object.class, String.class), preValues7, res, "/eu/sarunas/atf/demo/geometry/model.ocl", "/eu/sarunas/atf/demo/geometry/shapes/model.ocl");
	};

	@org.junit.Test
	public void testAssertPostConditions5() throws Throwable
	{
		eu.sarunas.atf.demo.geometry.shapes.Assert2 testObject = new eu.sarunas.atf.demo.geometry.shapes.Assert2();
		java.lang.reflect.Method method8 = new java.lang.reflect.Method();

		testObject.assertPostConditions(new Object(), method8, new Object[] { new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object() }, new Object(), new String[] { "aaaaaa", "", "", "aaaaaaaaaa", "", "", "", "aa", "aaaa", "aaaaa", "", "", "aaa", "aaa", "aaaaaaaaaaaaaaa", "", "a" });
		java.util.ArrayList<Object> preValues9 = new java.util.ArrayList<Object>();
		preValues9.add(new Object());
		preValues9.add(method8);
		preValues9.add(new Object[] { new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object() });
		preValues9.add(new Object());
		preValues9.add(new String[] { "aaaaaa", "", "", "aaaaaaaaaa", "", "", "", "aa", "aaaa", "aaaaa", "", "", "aaa", "aaa", "aaaaaaaaaaaaaaa", "", "a" });

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("assertPostConditions", Object.class, java.lang.reflect.Method.class, Object.class, Object.class, String.class), preValues9, res, "/eu/sarunas/atf/demo/geometry/model.ocl", "/eu/sarunas/atf/demo/geometry/shapes/model.ocl");
	};

	@org.junit.Test
	public void testAssertPostConditions6() throws Throwable
	{
		eu.sarunas.atf.demo.geometry.shapes.Assert2 testObject = new eu.sarunas.atf.demo.geometry.shapes.Assert2();
		java.lang.reflect.Method method10 = new java.lang.reflect.Method();

		testObject.assertPostConditions(new Object(), method10, new Object[] { new Object(), new Object(), new Object(), new Object(), new Object(), new Object() }, new Object(), new String[] { "" });
		java.util.ArrayList<Object> preValues11 = new java.util.ArrayList<Object>();
		preValues11.add(new Object());
		preValues11.add(method10);
		preValues11.add(new Object[] { new Object(), new Object(), new Object(), new Object(), new Object(), new Object() });
		preValues11.add(new Object());
		preValues11.add(new String[] { "" });

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("assertPostConditions", Object.class, java.lang.reflect.Method.class, Object.class, Object.class, String.class), preValues11, res, "/eu/sarunas/atf/demo/geometry/model.ocl", "/eu/sarunas/atf/demo/geometry/shapes/model.ocl");
	};

	@org.junit.Test
	public void testAssertPostConditions7() throws Throwable
	{
		eu.sarunas.atf.demo.geometry.shapes.Assert2 testObject = new eu.sarunas.atf.demo.geometry.shapes.Assert2();
		java.lang.reflect.Method method12 = new java.lang.reflect.Method();

		testObject.assertPostConditions(new Object(), method12, new Object[] { new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object() }, new Object(), new String[] { "a" });
		java.util.ArrayList<Object> preValues13 = new java.util.ArrayList<Object>();
		preValues13.add(new Object());
		preValues13.add(method12);
		preValues13.add(new Object[] { new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object() });
		preValues13.add(new Object());
		preValues13.add(new String[] { "a" });

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("assertPostConditions", Object.class, java.lang.reflect.Method.class, Object.class, Object.class, String.class), preValues13, res, "/eu/sarunas/atf/demo/geometry/model.ocl", "/eu/sarunas/atf/demo/geometry/shapes/model.ocl");
	};

	@org.junit.Test
	public void testAssertPostConditions8() throws Throwable
	{
		eu.sarunas.atf.demo.geometry.shapes.Assert2 testObject = new eu.sarunas.atf.demo.geometry.shapes.Assert2();
		java.lang.reflect.Method method14 = new java.lang.reflect.Method();

		testObject.assertPostConditions(new Object(), method14, new Object[] { new Object(), new Object(), new Object() }, new Object(), new String[] { "", "", "aaaaaaaa", "aaaaaaa", "" });
		java.util.ArrayList<Object> preValues15 = new java.util.ArrayList<Object>();
		preValues15.add(new Object());
		preValues15.add(method14);
		preValues15.add(new Object[] { new Object(), new Object(), new Object() });
		preValues15.add(new Object());
		preValues15.add(new String[] { "", "", "aaaaaaaa", "aaaaaaa", "" });

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("assertPostConditions", Object.class, java.lang.reflect.Method.class, Object.class, Object.class, String.class), preValues15, res, "/eu/sarunas/atf/demo/geometry/model.ocl", "/eu/sarunas/atf/demo/geometry/shapes/model.ocl");
	};

	@org.junit.Test
	public void testAssertPostConditions9() throws Throwable
	{
		eu.sarunas.atf.demo.geometry.shapes.Assert2 testObject = new eu.sarunas.atf.demo.geometry.shapes.Assert2();
		java.lang.reflect.Method method16 = new java.lang.reflect.Method();

		testObject.assertPostConditions(new Object(), method16, new Object[] { new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object() }, new Object(), new String[] { "aaaaaaaaaa", "", "", "", "aa", "", "aaaaaaaaaaaaaa", "", "aaaa" });
		java.util.ArrayList<Object> preValues17 = new java.util.ArrayList<Object>();
		preValues17.add(new Object());
		preValues17.add(method16);
		preValues17.add(new Object[] { new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object() });
		preValues17.add(new Object());
		preValues17.add(new String[] { "aaaaaaaaaa", "", "", "", "aa", "", "aaaaaaaaaaaaaa", "", "aaaa" });

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("assertPostConditions", Object.class, java.lang.reflect.Method.class, Object.class, Object.class, String.class), preValues17, res, "/eu/sarunas/atf/demo/geometry/model.ocl", "/eu/sarunas/atf/demo/geometry/shapes/model.ocl");
	};

	@org.junit.Test
	public void testAssertPostConditions10() throws Throwable
	{
		eu.sarunas.atf.demo.geometry.shapes.Assert2 testObject = new eu.sarunas.atf.demo.geometry.shapes.Assert2();
		java.lang.reflect.Method method18 = new java.lang.reflect.Method();

		testObject.assertPostConditions(new Object(), method18, new Object[] { new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object() }, new Object(), new String[] { "aaaaaaaaaaaaa", "aaaaaaaaaaaaaaaaaa", "", "a", "aaaaaaaaaaaa", "", "", "aaaaaaaaaaaaaaaaaaaa", "", "aaaaaaaaaaa", "", "", "aaaaaaaaa", "", "", "", "aaaaa", "", "aaaaaaa", "", "", "aaa", "", "aaa", "aaaaaaaaaaaaaaaaaa", "", "", "a", "aaaaaaaaaaa", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaa", "", "aaaaaa", "", "a", "", "aaaaa", "a", "", "", "", "aaaaaaaaa", "", "aaaaaaaaaaaaaaaaaaaa", "", "aaaaa", "aaa", "", "aaa", "" });
		java.util.ArrayList<Object> preValues19 = new java.util.ArrayList<Object>();
		preValues19.add(new Object());
		preValues19.add(method18);
		preValues19.add(new Object[] { new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object() });
		preValues19.add(new Object());
		preValues19.add(new String[] { "aaaaaaaaaaaaa", "aaaaaaaaaaaaaaaaaa", "", "a", "aaaaaaaaaaaa", "", "", "aaaaaaaaaaaaaaaaaaaa", "", "aaaaaaaaaaa", "", "", "aaaaaaaaa", "", "", "", "aaaaa", "", "aaaaaaa", "", "", "aaa", "", "aaa", "aaaaaaaaaaaaaaaaaa", "", "", "a", "aaaaaaaaaaa", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaa", "", "aaaaaa", "", "a", "", "aaaaa", "a", "", "", "", "aaaaaaaaa", "", "aaaaaaaaaaaaaaaaaaaa", "", "aaaaa", "aaa", "", "aaa", "" });

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("assertPostConditions", Object.class, java.lang.reflect.Method.class, Object.class, Object.class, String.class), preValues19, res, "/eu/sarunas/atf/demo/geometry/model.ocl", "/eu/sarunas/atf/demo/geometry/shapes/model.ocl");
	};

	@org.junit.Test
	public void testAssertPostConditions11() throws Throwable
	{
		eu.sarunas.atf.demo.geometry.shapes.Assert2 testObject = new eu.sarunas.atf.demo.geometry.shapes.Assert2();
		java.lang.reflect.Method method20 = new java.lang.reflect.Method();

		testObject.assertPostConditions(new Object(), method20, new Object[] { new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object() }, new Object(), new String[] { "", "aaaaaaaaaa", "", "aa", "aaaaaaaaaa", "", "aaa", "aaaaaaaaaaaaaaaaaaa", "aaa", "", "aaaaaaaaaaaaa", "", "", "", "", "aaaaaaa", "aaa", "aaaaa", "a", "aaaaaaaaaa", "aaaaaaaaaaaaaaaa", "aaaaaaaa", "aaaaaaaaaaaaaaaaaaaaa", "", "", "aaaaaaa", "", "", "", "aaaaaaaaaaaaaaa", "", "aaaaaaaaaaaaaaaaaaaaaaaaaaa", "", "", "", "", "a" });
		java.util.ArrayList<Object> preValues21 = new java.util.ArrayList<Object>();
		preValues21.add(new Object());
		preValues21.add(method20);
		preValues21.add(new Object[] { new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object(), new Object() });
		preValues21.add(new Object());
		preValues21.add(new String[] { "", "aaaaaaaaaa", "", "aa", "aaaaaaaaaa", "", "aaa", "aaaaaaaaaaaaaaaaaaa", "aaa", "", "aaaaaaaaaaaaa", "", "", "", "", "aaaaaaa", "aaa", "aaaaa", "a", "aaaaaaaaaa", "aaaaaaaaaaaaaaaa", "aaaaaaaa", "aaaaaaaaaaaaaaaaaaaaa", "", "", "aaaaaaa", "", "", "", "aaaaaaaaaaaaaaa", "", "aaaaaaaaaaaaaaaaaaaaaaaaaaa", "", "", "", "", "a" });

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("assertPostConditions", Object.class, java.lang.reflect.Method.class, Object.class, Object.class, String.class), preValues21, res, "/eu/sarunas/atf/demo/geometry/model.ocl", "/eu/sarunas/atf/demo/geometry/shapes/model.ocl");
	};

	@org.junit.Test
	public void testGetConstraints12() throws Throwable
	{
		eu.sarunas.atf.demo.geometry.shapes.Assert2 testObject = new eu.sarunas.atf.demo.geometry.shapes.Assert2();
		tudresden.ocl20.pivot.metamodels.java.internal.model.JavaModel javaModel22 = new tudresden.ocl20.pivot.metamodels.java.internal.model.JavaModel();

		Constraint res = testObject.getConstraints(javaModel22, new String[] { "aaaaa", "aaaaaaaaaaaaaaaaaa", "aaaaaaaa", "aaaaaaa", "", "", "", "aaa", "", "aaaaaaaaaaaaaaaa", "", "", "aaaaaaaaaaaaaa", "aaaaaaaa", "aa", "a", "", "aaaaaaa", "aaa", "", "aaaaaaaaaaa", "", "" });
		java.util.ArrayList<Object> preValues23 = new java.util.ArrayList<Object>();
		preValues23.add(javaModel22);
		preValues23.add(new String[] { "aaaaa", "aaaaaaaaaaaaaaaaaa", "aaaaaaaa", "aaaaaaa", "", "", "", "aaa", "", "aaaaaaaaaaaaaaaa", "", "", "aaaaaaaaaaaaaa", "aaaaaaaa", "aa", "a", "", "aaaaaaa", "aaa", "", "aaaaaaaaaaa", "", "" });

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("getConstraints", tudresden.ocl20.pivot.metamodels.java.internal.model.JavaModel.class, String.class), preValues23, res, "/eu/sarunas/atf/demo/geometry/model.ocl", "/eu/sarunas/atf/demo/geometry/shapes/model.ocl");
	};

	@org.junit.Test
	public void testGetConstraints13() throws Throwable
	{
		eu.sarunas.atf.demo.geometry.shapes.Assert2 testObject = new eu.sarunas.atf.demo.geometry.shapes.Assert2();
		tudresden.ocl20.pivot.metamodels.java.internal.model.JavaModel javaModel24 = new tudresden.ocl20.pivot.metamodels.java.internal.model.JavaModel();

		Constraint res = testObject.getConstraints(javaModel24, new String[] { "aaaaa", "", "", "", "", "", "", "aaaa", "aaaaaaaaa", "", "", "aa", "", "a", "aaaaaaaaaa", "aaaaaaaaaa", "aaaaaa", "", "" });
		java.util.ArrayList<Object> preValues25 = new java.util.ArrayList<Object>();
		preValues25.add(javaModel24);
		preValues25.add(new String[] { "aaaaa", "", "", "", "", "", "", "aaaa", "aaaaaaaaa", "", "", "aa", "", "a", "aaaaaaaaaa", "aaaaaaaaaa", "aaaaaa", "", "" });

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("getConstraints", tudresden.ocl20.pivot.metamodels.java.internal.model.JavaModel.class, String.class), preValues25, res, "/eu/sarunas/atf/demo/geometry/model.ocl", "/eu/sarunas/atf/demo/geometry/shapes/model.ocl");
	};

	@org.junit.Test
	public void testGetConstraints14() throws Throwable
	{
		eu.sarunas.atf.demo.geometry.shapes.Assert2 testObject = new eu.sarunas.atf.demo.geometry.shapes.Assert2();
		tudresden.ocl20.pivot.metamodels.java.internal.model.JavaModel javaModel26 = new tudresden.ocl20.pivot.metamodels.java.internal.model.JavaModel();

		Constraint res = testObject.getConstraints(javaModel26, new String[] { "", "aaaaaaa", "aaaaaaaaaaaaaaaaaaaaaa", "", "aaaaaaaaa", "aaaa", "aaaaa", "", "aaaaaaaa", "aaaaaaaaaa" });
		java.util.ArrayList<Object> preValues27 = new java.util.ArrayList<Object>();
		preValues27.add(javaModel26);
		preValues27.add(new String[] { "", "aaaaaaa", "aaaaaaaaaaaaaaaaaaaaaa", "", "aaaaaaaaa", "aaaa", "aaaaa", "", "aaaaaaaa", "aaaaaaaaaa" });

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("getConstraints", tudresden.ocl20.pivot.metamodels.java.internal.model.JavaModel.class, String.class), preValues27, res, "/eu/sarunas/atf/demo/geometry/model.ocl", "/eu/sarunas/atf/demo/geometry/shapes/model.ocl");
	};

	@org.junit.Test
	public void testGetConstraints15() throws Throwable
	{
		eu.sarunas.atf.demo.geometry.shapes.Assert2 testObject = new eu.sarunas.atf.demo.geometry.shapes.Assert2();
		tudresden.ocl20.pivot.metamodels.java.internal.model.JavaModel javaModel28 = new tudresden.ocl20.pivot.metamodels.java.internal.model.JavaModel();

		Constraint res = testObject.getConstraints(javaModel28, new String[] { "aaaa", "aaaaaaaaa", "aaaa", "", "", "", "aaa", "", "", "", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaa", "", "", "", "", "", "", "aaaaaaaaaaaaa", "aaaaaaa", "aaaaa", "aaaaaaaaaaaaa", "", "aaaaaaaaa", "aaaaaaa", "", "aaaaaaaaaa", "", "aaaaaaaaaaaaaa", "", "", "", "", "", "", "", "", "aaaaa", "aaaaaaaaaaaaaa", "", "aaaaa", "", "", "aaaaaaaa", "", "", "aaaaaaaaaa", "aaaaaaaa", "" });
		java.util.ArrayList<Object> preValues29 = new java.util.ArrayList<Object>();
		preValues29.add(javaModel28);
		preValues29.add(new String[] { "aaaa", "aaaaaaaaa", "aaaa", "", "", "", "aaa", "", "", "", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaa", "", "", "", "", "", "", "aaaaaaaaaaaaa", "aaaaaaa", "aaaaa", "aaaaaaaaaaaaa", "", "aaaaaaaaa", "aaaaaaa", "", "aaaaaaaaaa", "", "aaaaaaaaaaaaaa", "", "", "", "", "", "", "", "", "aaaaa", "aaaaaaaaaaaaaa", "", "aaaaa", "", "", "aaaaaaaa", "", "", "aaaaaaaaaa", "aaaaaaaa", "" });

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("getConstraints", tudresden.ocl20.pivot.metamodels.java.internal.model.JavaModel.class, String.class), preValues29, res, "/eu/sarunas/atf/demo/geometry/model.ocl", "/eu/sarunas/atf/demo/geometry/shapes/model.ocl");
	};

	@org.junit.Test
	public void testGetConstraints16() throws Throwable
	{
		eu.sarunas.atf.demo.geometry.shapes.Assert2 testObject = new eu.sarunas.atf.demo.geometry.shapes.Assert2();
		tudresden.ocl20.pivot.metamodels.java.internal.model.JavaModel javaModel30 = new tudresden.ocl20.pivot.metamodels.java.internal.model.JavaModel();

		Constraint res = testObject.getConstraints(javaModel30, new String[] { "aaaaaaaaaaaaaaaaaaaaaaa", "aaaa", "" });
		java.util.ArrayList<Object> preValues31 = new java.util.ArrayList<Object>();
		preValues31.add(javaModel30);
		preValues31.add(new String[] { "aaaaaaaaaaaaaaaaaaaaaaa", "aaaa", "" });

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("getConstraints", tudresden.ocl20.pivot.metamodels.java.internal.model.JavaModel.class, String.class), preValues31, res, "/eu/sarunas/atf/demo/geometry/model.ocl", "/eu/sarunas/atf/demo/geometry/shapes/model.ocl");
	};

	@org.junit.Test
	public void testGetConstraints17() throws Throwable
	{
		eu.sarunas.atf.demo.geometry.shapes.Assert2 testObject = new eu.sarunas.atf.demo.geometry.shapes.Assert2();
		tudresden.ocl20.pivot.metamodels.java.internal.model.JavaModel javaModel32 = new tudresden.ocl20.pivot.metamodels.java.internal.model.JavaModel();

		Constraint res = testObject.getConstraints(javaModel32, new String[] { "aaaaaaaaaaaa", "", "aaaaaaa", "" });
		java.util.ArrayList<Object> preValues33 = new java.util.ArrayList<Object>();
		preValues33.add(javaModel32);
		preValues33.add(new String[] { "aaaaaaaaaaaa", "", "aaaaaaa", "" });

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("getConstraints", tudresden.ocl20.pivot.metamodels.java.internal.model.JavaModel.class, String.class), preValues33, res, "/eu/sarunas/atf/demo/geometry/model.ocl", "/eu/sarunas/atf/demo/geometry/shapes/model.ocl");
	};

	@org.junit.Test
	public void testGetConstraints18() throws Throwable
	{
		eu.sarunas.atf.demo.geometry.shapes.Assert2 testObject = new eu.sarunas.atf.demo.geometry.shapes.Assert2();
		tudresden.ocl20.pivot.metamodels.java.internal.model.JavaModel javaModel34 = new tudresden.ocl20.pivot.metamodels.java.internal.model.JavaModel();

		Constraint res = testObject.getConstraints(javaModel34, new String[] { "aaaaaaaaaaaaaaa", "", "aaaaaaaaaaaaaaaaa", "aaaa", "", "a", "", "", "aaaaaaaaaaaaa", "", "", "aaaaaaaaaaaaaaaa", "" });
		java.util.ArrayList<Object> preValues35 = new java.util.ArrayList<Object>();
		preValues35.add(javaModel34);
		preValues35.add(new String[] { "aaaaaaaaaaaaaaa", "", "aaaaaaaaaaaaaaaaa", "aaaa", "", "a", "", "", "aaaaaaaaaaaaa", "", "", "aaaaaaaaaaaaaaaa", "" });

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("getConstraints", tudresden.ocl20.pivot.metamodels.java.internal.model.JavaModel.class, String.class), preValues35, res, "/eu/sarunas/atf/demo/geometry/model.ocl", "/eu/sarunas/atf/demo/geometry/shapes/model.ocl");
	};

	@org.junit.Test
	public void testGetConstraints19() throws Throwable
	{
		eu.sarunas.atf.demo.geometry.shapes.Assert2 testObject = new eu.sarunas.atf.demo.geometry.shapes.Assert2();
		tudresden.ocl20.pivot.metamodels.java.internal.model.JavaModel javaModel36 = new tudresden.ocl20.pivot.metamodels.java.internal.model.JavaModel();

		Constraint res = testObject.getConstraints(javaModel36, new String[] { "", "aaaaaa", "", "aaaa", "", "", "aaaaaaaa", "", "", "", "", "aaaaaaaaaa", "", "aaaaa", "" });
		java.util.ArrayList<Object> preValues37 = new java.util.ArrayList<Object>();
		preValues37.add(javaModel36);
		preValues37.add(new String[] { "", "aaaaaa", "", "aaaa", "", "", "aaaaaaaa", "", "", "", "", "aaaaaaaaaa", "", "aaaaa", "" });

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("getConstraints", tudresden.ocl20.pivot.metamodels.java.internal.model.JavaModel.class, String.class), preValues37, res, "/eu/sarunas/atf/demo/geometry/model.ocl", "/eu/sarunas/atf/demo/geometry/shapes/model.ocl");
	};

	@org.junit.Test
	public void testGetConstraints20() throws Throwable
	{
		eu.sarunas.atf.demo.geometry.shapes.Assert2 testObject = new eu.sarunas.atf.demo.geometry.shapes.Assert2();
		tudresden.ocl20.pivot.metamodels.java.internal.model.JavaModel javaModel38 = new tudresden.ocl20.pivot.metamodels.java.internal.model.JavaModel();

		Constraint res = testObject.getConstraints(javaModel38, new String[] { "", "aaaaaaaaaaaaaaa", "", "", "", "aaaaaaaaaa" });
		java.util.ArrayList<Object> preValues39 = new java.util.ArrayList<Object>();
		preValues39.add(javaModel38);
		preValues39.add(new String[] { "", "aaaaaaaaaaaaaaa", "", "", "", "aaaaaaaaaa" });

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("getConstraints", tudresden.ocl20.pivot.metamodels.java.internal.model.JavaModel.class, String.class), preValues39, res, "/eu/sarunas/atf/demo/geometry/model.ocl", "/eu/sarunas/atf/demo/geometry/shapes/model.ocl");
	};

	@org.junit.Test
	public void testGetConstraints21() throws Throwable
	{
		eu.sarunas.atf.demo.geometry.shapes.Assert2 testObject = new eu.sarunas.atf.demo.geometry.shapes.Assert2();
		tudresden.ocl20.pivot.metamodels.java.internal.model.JavaModel javaModel40 = new tudresden.ocl20.pivot.metamodels.java.internal.model.JavaModel();

		Constraint res = testObject.getConstraints(javaModel40, new String[] { "aaaa", "aaaaaa", "", "", "aaaaaaaaa", "aaaaaaaaa", "aaa", "", "", "", "aaaaaaa", "aaaaaaaaaaaaaa", "", "", "aaaaaaaaaaaaaaa", "aaaaaaaaaaaaaaaaaaaaaa", "" });
		java.util.ArrayList<Object> preValues41 = new java.util.ArrayList<Object>();
		preValues41.add(javaModel40);
		preValues41.add(new String[] { "aaaa", "aaaaaa", "", "", "aaaaaaaaa", "aaaaaaaaa", "aaa", "", "", "", "aaaaaaa", "aaaaaaaaaaaaaa", "", "", "aaaaaaaaaaaaaaa", "aaaaaaaaaaaaaaaaaaaaaa", "" });

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("getConstraints", tudresden.ocl20.pivot.metamodels.java.internal.model.JavaModel.class, String.class), preValues41, res, "/eu/sarunas/atf/demo/geometry/model.ocl", "/eu/sarunas/atf/demo/geometry/shapes/model.ocl");
	};

	@org.junit.Test
	public void testGetConstraints22() throws Throwable
	{
		eu.sarunas.atf.demo.geometry.shapes.Assert2 testObject = new eu.sarunas.atf.demo.geometry.shapes.Assert2();
		tudresden.ocl20.pivot.metamodels.java.internal.model.JavaModel javaModel42 = new tudresden.ocl20.pivot.metamodels.java.internal.model.JavaModel();

		Constraint res = testObject.getConstraints(javaModel42, new String[] { "", "aaaaaaaaaaaaaaa", "", "", "aaaaaaaaaa", "", "", "", "", "", "", "aaaaaaaa", "aaaaaaaaaa", "", "", "aa", "", "", "", "", "aaaaaaaa", "", "a", "", "aaaaaaaa" });
		java.util.ArrayList<Object> preValues43 = new java.util.ArrayList<Object>();
		preValues43.add(javaModel42);
		preValues43.add(new String[] { "", "aaaaaaaaaaaaaaa", "", "", "aaaaaaaaaa", "", "", "", "", "", "", "aaaaaaaa", "aaaaaaaaaa", "", "", "aa", "", "", "", "", "aaaaaaaa", "", "a", "", "aaaaaaaa" });

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("getConstraints", tudresden.ocl20.pivot.metamodels.java.internal.model.JavaModel.class, String.class), preValues43, res, "/eu/sarunas/atf/demo/geometry/model.ocl", "/eu/sarunas/atf/demo/geometry/shapes/model.ocl");
	};

	@org.junit.Test
	public void testReadToString23() throws Throwable
	{
		eu.sarunas.atf.demo.geometry.shapes.Assert2 testObject = new eu.sarunas.atf.demo.geometry.shapes.Assert2();

		String res = testObject.readToString("aaaaaaaaaaaa");
		java.util.ArrayList<Object> preValues44 = new java.util.ArrayList<Object>();
		preValues44.add("aaaaaaaaaaaa");

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("readToString", String.class), preValues44, res, "/eu/sarunas/atf/demo/geometry/model.ocl", "/eu/sarunas/atf/demo/geometry/shapes/model.ocl");
	};

	@org.junit.Test
	public void testReadToString24() throws Throwable
	{
		eu.sarunas.atf.demo.geometry.shapes.Assert2 testObject = new eu.sarunas.atf.demo.geometry.shapes.Assert2();

		String res = testObject.readToString("");
		java.util.ArrayList<Object> preValues45 = new java.util.ArrayList<Object>();
		preValues45.add("");

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("readToString", String.class), preValues45, res, "/eu/sarunas/atf/demo/geometry/model.ocl", "/eu/sarunas/atf/demo/geometry/shapes/model.ocl");
	};

	@org.junit.Test
	public void testReadToString25() throws Throwable
	{
		eu.sarunas.atf.demo.geometry.shapes.Assert2 testObject = new eu.sarunas.atf.demo.geometry.shapes.Assert2();

		String res = testObject.readToString("");
		java.util.ArrayList<Object> preValues46 = new java.util.ArrayList<Object>();
		preValues46.add("");

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("readToString", String.class), preValues46, res, "/eu/sarunas/atf/demo/geometry/model.ocl", "/eu/sarunas/atf/demo/geometry/shapes/model.ocl");
	};

	@org.junit.Test
	public void testReadToString26() throws Throwable
	{
		eu.sarunas.atf.demo.geometry.shapes.Assert2 testObject = new eu.sarunas.atf.demo.geometry.shapes.Assert2();

		String res = testObject.readToString("a");
		java.util.ArrayList<Object> preValues47 = new java.util.ArrayList<Object>();
		preValues47.add("a");

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("readToString", String.class), preValues47, res, "/eu/sarunas/atf/demo/geometry/model.ocl", "/eu/sarunas/atf/demo/geometry/shapes/model.ocl");
	};

	@org.junit.Test
	public void testReadToString27() throws Throwable
	{
		eu.sarunas.atf.demo.geometry.shapes.Assert2 testObject = new eu.sarunas.atf.demo.geometry.shapes.Assert2();

		String res = testObject.readToString("a");
		java.util.ArrayList<Object> preValues48 = new java.util.ArrayList<Object>();
		preValues48.add("a");

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("readToString", String.class), preValues48, res, "/eu/sarunas/atf/demo/geometry/model.ocl", "/eu/sarunas/atf/demo/geometry/shapes/model.ocl");
	};

	@org.junit.Test
	public void testReadToString28() throws Throwable
	{
		eu.sarunas.atf.demo.geometry.shapes.Assert2 testObject = new eu.sarunas.atf.demo.geometry.shapes.Assert2();

		String res = testObject.readToString("aaaaaaaaa");
		java.util.ArrayList<Object> preValues49 = new java.util.ArrayList<Object>();
		preValues49.add("aaaaaaaaa");

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("readToString", String.class), preValues49, res, "/eu/sarunas/atf/demo/geometry/model.ocl", "/eu/sarunas/atf/demo/geometry/shapes/model.ocl");
	};

	@org.junit.Test
	public void testReadToString29() throws Throwable
	{
		eu.sarunas.atf.demo.geometry.shapes.Assert2 testObject = new eu.sarunas.atf.demo.geometry.shapes.Assert2();

		String res = testObject.readToString("");
		java.util.ArrayList<Object> preValues50 = new java.util.ArrayList<Object>();
		preValues50.add("");

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("readToString", String.class), preValues50, res, "/eu/sarunas/atf/demo/geometry/model.ocl", "/eu/sarunas/atf/demo/geometry/shapes/model.ocl");
	};

	@org.junit.Test
	public void testReadToString30() throws Throwable
	{
		eu.sarunas.atf.demo.geometry.shapes.Assert2 testObject = new eu.sarunas.atf.demo.geometry.shapes.Assert2();

		String res = testObject.readToString("aaaaaaaaaa");
		java.util.ArrayList<Object> preValues51 = new java.util.ArrayList<Object>();
		preValues51.add("aaaaaaaaaa");

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("readToString", String.class), preValues51, res, "/eu/sarunas/atf/demo/geometry/model.ocl", "/eu/sarunas/atf/demo/geometry/shapes/model.ocl");
	};

	@org.junit.Test
	public void testReadToString31() throws Throwable
	{
		eu.sarunas.atf.demo.geometry.shapes.Assert2 testObject = new eu.sarunas.atf.demo.geometry.shapes.Assert2();

		String res = testObject.readToString("");
		java.util.ArrayList<Object> preValues52 = new java.util.ArrayList<Object>();
		preValues52.add("");

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("readToString", String.class), preValues52, res, "/eu/sarunas/atf/demo/geometry/model.ocl", "/eu/sarunas/atf/demo/geometry/shapes/model.ocl");
	};

	@org.junit.Test
	public void testReadToString32() throws Throwable
	{
		eu.sarunas.atf.demo.geometry.shapes.Assert2 testObject = new eu.sarunas.atf.demo.geometry.shapes.Assert2();

		String res = testObject.readToString("aaaaa");
		java.util.ArrayList<Object> preValues53 = new java.util.ArrayList<Object>();
		preValues53.add("aaaaa");

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("readToString", String.class), preValues53, res, "/eu/sarunas/atf/demo/geometry/model.ocl", "/eu/sarunas/atf/demo/geometry/shapes/model.ocl");
	};

	@org.junit.Test
	public void testReadToString33() throws Throwable
	{
		eu.sarunas.atf.demo.geometry.shapes.Assert2 testObject = new eu.sarunas.atf.demo.geometry.shapes.Assert2();

		String res = testObject.readToString("aaaa");
		java.util.ArrayList<Object> preValues54 = new java.util.ArrayList<Object>();
		preValues54.add("aaaa");

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("readToString", String.class), preValues54, res, "/eu/sarunas/atf/demo/geometry/model.ocl", "/eu/sarunas/atf/demo/geometry/shapes/model.ocl");
	};

};
