package eu.sarunas.atf.demo.geometry.shapes;

import static org.junit.Assert.*;

import org.junit.*;

public class TriangleTest {


 protected transient eu.sarunas.atf.demo.geometry.shapes.Triangle test = null;
@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
test = new eu.sarunas.atf.demo.geometry.shapes.Triangle()
;
	}
@After
	public void tearDown() throws Exception {

	}


@Test(timeout=60000)
public void testGetB_Float_GT_1(){
try{
assertNotNull("Value should not be null",test.getB()
);
}catch (Exception e)
{fail(e.getMessage());}
}

@Test(timeout=60000)
public void testSetA_Voidfloat_ST_1(){
try {
 test.setA(0.1f)
;
 }
 catch (Exception e) {
 fail(e.getMessage());
}
}

@Test(timeout=60000)
public void testSetC_Voidfloat_ST_1(){
try {
 test.setC(0.1f)
;
 }
 catch (Exception e) {
 fail(e.getMessage());
}
}

@Test(timeout=60000)
public void testGetC_Float_GT_1(){
try{
assertNotNull("Value should not be null",test.getC()
);
}catch (Exception e)
{fail(e.getMessage());}
}

@Test(timeout=60000)
public void testGetA_Float_GT_1(){
try{
assertNotNull("Value should not be null",test.getA()
);
}catch (Exception e)
{fail(e.getMessage());}
}

@Test(timeout=60000)
public void testSetB_Voidfloat_ST_1(){
try {
 test.setB(0.1f)
;
 }
 catch (Exception e) {
 fail(e.getMessage());
}
}
}