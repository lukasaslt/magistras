package lt.evaluator.component;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import lt.evaluator.data.EModel;
import lt.evaluator.data.EOperation;
import lt.evaluator.data.EProperty;
import lt.evaluator.data.EResult;
import lt.evaluator.data.EType;
import lt.evaluator.data.OCLConstraint;
import lt.evaluator.util.OCLUtil;
import tudresden.ocl20.pivot.essentialocl.expressions.impl.OperationCallExpImpl;

public class ResultCalculator {
	
	public EResult calculate(EModel eModel) {
		if (eModel == null)
			return null;
		
		EResult evaluationResult = new EResult();
		evaluationResult.setModelName(eModel.getName());
		
		int maxResult = getMaxResult(eModel);
		double result = getResult(eModel);
		
		if (maxResult > 0)
			evaluationResult.setResult(result / maxResult * 100);
		
		return evaluationResult;
	}
	
	private int getMaxResult(EModel umlModel) {
		int max = 0;
		
		List<EType> types = umlModel.getTypes();
		for (EType umlType : types) {
			List<EProperty> properties = umlType.getProperties();
			List<EOperation> operations = umlType.getOperations();
			max += properties.size() + operations.size();
		}
		
		return max;
	}

	private double getResult(EModel umlModel) {
		double result = 0;
		
		List<EType> types = umlModel.getTypes();
		for (EType umlType : types) {
			List<EProperty> properties = umlType.getProperties();
			for (EProperty umlProperty : properties) {
				List<String> opSigns = getOperationSigns(umlProperty.getContraints());
				result += calcResult(opSigns);
			}
			
			List<EOperation> operations = umlType.getOperations();
			for (EOperation umlOperation : operations) {
				List<String> opSigns = getOperationSigns(umlOperation.getContraints());
				result += calcResult(opSigns);
			}
		}
		
		return result;
	}

	private List<String> getOperationSigns(List<OCLConstraint> contraints) {
		List<String> opSigns = new ArrayList<>();
		for (OCLConstraint oclConstraint : contraints) {
			EObject eObject = oclConstraint.getEObject(OperationCallExpImpl.class);
			String opSign = OCLUtil.getOperationCallOperator(eObject);
			opSigns.add(opSign);
		}
		return opSigns;
	}

	private double calcResult(List<String> opSigns) {
		int opSignsSize = opSigns.size();
		if (opSignsSize == 0)
			return 0;
		
		long equalSignCount = opSigns.stream().filter(e -> e.equals("=")).count();
		double equalSignResult = equalSignCount / opSignsSize;
		
		long otherSignCount = opSignsSize - equalSignCount;
		double otherSignResult = 0;
		for (int i = 1; i <= otherSignCount; i++) {
			otherSignResult += 0.5 / Math.pow(2, i-1);
		}
		otherSignResult /= opSignsSize;
		
		return equalSignResult + otherSignResult;
	}

}
