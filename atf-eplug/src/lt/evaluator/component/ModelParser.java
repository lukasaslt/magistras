package lt.evaluator.component;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import lt.evaluator.common.EvaluatorConstants;
import lt.evaluator.data.EModel;
import lt.evaluator.data.EOperation;
import lt.evaluator.data.EProperty;
import lt.evaluator.data.EType;
import lt.evaluator.data.OCLConstraint;
import lt.evaluator.util.FilenameUtil;
import tudresden.ocl20.pivot.metamodels.java.internal.model.JavaClass;
import tudresden.ocl20.pivot.metamodels.uml2.internal.model.UML2Class;
import tudresden.ocl20.pivot.metamodels.uml2.internal.model.UML2Interface;
import tudresden.ocl20.pivot.model.IModel;
import tudresden.ocl20.pivot.model.ModelAccessException;
import tudresden.ocl20.pivot.parser.ParseException;
import tudresden.ocl20.pivot.parser.SemanticException;
import tudresden.ocl20.pivot.pivotmodel.Constraint;
import tudresden.ocl20.pivot.pivotmodel.NamedElement;
import tudresden.ocl20.pivot.pivotmodel.Namespace;
import tudresden.ocl20.pivot.pivotmodel.Operation;
import tudresden.ocl20.pivot.pivotmodel.Property;
import tudresden.ocl20.pivot.pivotmodel.Type;
import tudresden.ocl20.pivot.standalone.facade.StandaloneFacade;

public class ModelParser {
	
	private static StandaloneFacade oclParser = StandaloneFacade.INSTANCE;
	
	static {
		try {
			oclParser.initialize(new URL("file:" + new File(EvaluatorConstants.RESOURCES_LOG4J_PROPERTIES_PATH).getAbsolutePath()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void parse(EModel eModel, String type, Path modelPath, Path oclPath) throws ModelAccessException, IOException, ParseException {
		if (modelPath == null)
			return;
		
		File modelFile = modelPath.toFile();
		
		IModel model = null;
		switch (FilenameUtil.getExtension(modelPath.toString())) {
		case EvaluatorConstants.UML_EXT:
			model = oclParser.loadUMLModel(modelFile, getUMLResources());
			break;
		case EvaluatorConstants.CLASS_EXT:
			model = oclParser.loadJavaModel(modelFile);
			break;
		}
		
		List<OCLConstraint> oclConstraints = new ArrayList<>();
		
		if (oclPath != null) {
			try {
				List<Constraint> constraints = oclParser.parseOclConstraints(model, oclPath.toFile());
				
				for (Constraint constraint : constraints)
					oclConstraints.add(new OCLConstraint(constraint));
			} catch (SemanticException e) {
				return;
			}
		}
		
		Namespace rootNamespace = model.getRootNamespace();
		processElements(eModel, type, oclConstraints, rootNamespace.getNestedNamespace(), rootNamespace.getOwnedType());
	}

	/**
	 * @param singleType if not null, creates model only from this single type
	 * @param eModel
	 * @param oclConstraints
	 * @param packages
	 * @param types
	 */
	private void processElements(EModel eModel, String singleType, List<OCLConstraint> oclConstraints, List<Namespace> packages, List<Type> types) {
		for (Namespace pck : packages)
			processElements(eModel, singleType, oclConstraints, pck.getNestedNamespace(), pck.getOwnedType());
		
		for (Type type : types) {
			if (type instanceof UML2Class || type instanceof UML2Interface || type instanceof JavaClass) {
				String typeName = getConstrainedElementName(type).replaceAll("::", ".");
				if ((singleType != null && !typeName.equals(singleType))
					|| typeName.equals("Object")
					|| typeName.equals("TextualRepresentation"))
					continue;
				
				EType eType = getEType(eModel, typeName);
				
				for (Property property : type.getOwnedProperty()) {
					EProperty eProperty = getEProperty(eType, property.getName());
					
					List<OCLConstraint> constraints = oclConstraints.stream().filter(e ->
						getConstrainedElementName(property).equals(e.getFullConstrainedElementName())
					).collect(Collectors.toList());
					
					eProperty.getContraints().addAll(constraints);
				}
				
				for (Operation operation : type.getOwnedOperation()) {
					EOperation eOperation = getEOperation(eType, operation.getName());
					
					List<OCLConstraint> constraints = oclConstraints.stream().filter(e ->
						getConstrainedElementName(operation).equals(e.getFullConstrainedElementName())
					).collect(Collectors.toList());
					
					eOperation.getContraints().addAll(constraints);
				}
			}
		}
	}

	private EType getEType(EModel eModel, String typeName) {
		EType eType = eModel.getTypes().stream().filter(e -> e.getName().equals(typeName)).findFirst().orElse(null);
		
		if (eType == null) {
			eType = new EType();
			eType.setName(typeName);
			eModel.getTypes().add(eType);
		}
		
		return eType;
	}
	
	private EProperty getEProperty(EType eType, String propName) {
		EProperty eProperty = eType.getProperties().stream().filter(e -> e.getName().equals(propName)).findFirst().orElse(null);
		
		if (eProperty == null) {
			eProperty = new EProperty();
			eProperty.setName(propName);
			eType.getProperties().add(eProperty);
		}
		
		return eProperty;
	}
	
	private EOperation getEOperation(EType eType, String opName) {
		EOperation eOperation = eType.getOperations().stream().filter(e -> e.getName().equals(opName)).findFirst().orElse(null);
		
		if (eOperation == null) {
			eOperation = new EOperation();
			eOperation.setName(opName);
			eType.getOperations().add(eOperation);
		}
		
		return eOperation;
	}

	/**
	 * Removes model elements name at the start 
	 * @param operation
	 * @param rootNamespace
	 * @return
	 */
	private String getConstrainedElementName(NamedElement operation) {
		StringBuilder str = new StringBuilder();
		List<String> nameList = operation.getQualifiedNameList();
		
		for (int i = 1; i < nameList.size(); i++) {
			String name = nameList.get(i);
			
			if (i > 1)
				str.append("::");
			
			str.append(name);
		}
		
		return str.toString();
	}
	
	private static File getUMLResources() {
		return new File("plugins/org.eclipse.uml2.uml.resources_5.2.0.v20170227-0935.jar"); 
	}

}
