package lt.evaluator.component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.ICompilationUnit;

import lt.evaluator.common.EvaluatorConstants;
import lt.evaluator.util.FilenameUtil;
import tudresden.ocl20.pivot.model.ModelAccessException;
import tudresden.ocl20.pivot.parser.ParseException;

public class FileFinder {
	
	private static final String[] IGNORE_DIRS = {
		EvaluatorConstants.COMPILE_DIR, 
		"target"
	};
	
	/**
	 * Pirma UML + OCL
	 * Po to prijungia Java + OCL, jei yra atlikusiu OCL failu
	 * Java failu iesko pagal OCL failo varda
	 * @param projectPath 
	 * @param projectPathStr
	 * @return
	 * @throws ModelAccessException
	 * @throws IOException
	 * @throws ParseException
	 * @throws CoreException
	 */
	public List<Path> findAll(Path projectPath, String ext) throws ModelAccessException, IOException, ParseException, CoreException {
		if (projectPath == null || ext == null)
			return null;
		
		return Files
			.walk(projectPath)
			.filter(p -> { 
				String extension = FilenameUtil.getExtension(p.toString());
				
				if (extension == null)
					return false;
				
				return extension.equals(ext) && !shouldIgnoreDir(projectPath.relativize(p));
			})
		    .collect(Collectors.toList());
	}
	
	public Path findClassFile(Path projectPath, ICompilationUnit compilationUnit) throws ModelAccessException, IOException, ParseException, CoreException {
		if (projectPath == null || compilationUnit == null)
			return null;
		
		Path compilePath = projectPath.resolve(EvaluatorConstants.COMPILE_DIR); 
		String pck = Arrays.stream(compilationUnit.getPackageDeclarations())
				.map(e -> 
					String.join("/", e.getElementName().split("\\."))
				)
				.collect(Collectors.joining("/"));
		
		Path classToLookTmp = Paths.get(
			pck,
			compilationUnit.getElementName()
		);
		
		final Path classToLook = compilePath.resolve(classToLookTmp)
				.resolveSibling(FilenameUtil.getName(classToLookTmp.getFileName().toString()) + "." + EvaluatorConstants.CLASS_EXT);
		
		try (Stream<Path> stream = Files.walk(compilePath)) {
			return stream
				.filter(p ->
					classToLook.equals(p.toAbsolutePath())
					&& !shouldIgnoreDir(compilePath.relativize(p))
				)
			    .findFirst().orElse(null);
		}
	}
	
	private boolean shouldIgnoreDir(Path checkPath) {
		return Arrays.stream(IGNORE_DIRS).anyMatch(e -> checkPath.startsWith(e));
	}

}
