package lt.evaluator.data;

import java.util.ArrayList;
import java.util.List;

public class EProperty extends EBaseElement {
	
	private List<OCLConstraint> contraints = new ArrayList<>();

	public List<OCLConstraint> getContraints() {
		return contraints;
	}

	public void setContraints(List<OCLConstraint> contraints) {
		this.contraints = contraints;
	}
	
}
