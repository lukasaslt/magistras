package lt.evaluator.data;

public abstract class EBaseElement {
	
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
