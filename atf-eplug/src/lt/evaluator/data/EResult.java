package lt.evaluator.data;

public class EResult {
	
	private String modelName;
	
	/**
	 * Percentage
	 */
	private double result;

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public double getResult() {
		return result;
	}

	public void setResult(double result) {
		this.result = result;
	}

}
