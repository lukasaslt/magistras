package lt.evaluator.data;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

import lt.evaluator.util.OCLUtil;
import tudresden.ocl20.pivot.essentialocl.expressions.impl.PropertyCallExpImpl;
import tudresden.ocl20.pivot.pivotmodel.Constraint;

public class OCLConstraint {
	
	private static final String PACKAGE_SEP = "::";
	private String property;
	private String operation;
	private String _class;
	private String _package;
	private String fullName;
	private Constraint constraint;
	private List<EObject> eobjectList;
	
	public OCLConstraint(Constraint constraint) {
		super();
		this.constraint = constraint;
		this.eobjectList = OCLUtil.getEObjectList(constraint);
		this.property = OCLUtil.getPropertyCallName(getEObject(PropertyCallExpImpl.class));
		this.operation = OCLUtil.getConstraintOperation(constraint);
		this._class = OCLUtil.getConstraintClass(constraint);
		this._package = OCLUtil.getConstraintPackage(constraint);
		this.fullName = constructFullName();
	}

	private String constructFullName() {
		String searchStr = "root";
		
		if (_package.startsWith(searchStr)) {
			_package = _package.substring(searchStr.length());
			
			if (_package.startsWith(PACKAGE_SEP))
				_package = _package.substring(PACKAGE_SEP.length());
		}
		
		return (!_package.isEmpty() ? _package.replace(".", PACKAGE_SEP) + PACKAGE_SEP : "")  
						+ _class 
						+ (operation != null ? 
							PACKAGE_SEP + operation 
							:
							(property != null ? PACKAGE_SEP + property : "")
						);
	}
	
	public String getModelType(){
		return _package + "." + _class;
	}
	
	public String getClazz() {
		return _class;
	}

	public void setClazz(String _class) {
		this._class = _class;
	}

	public String getPackage() {
		return _package;
	}

	public void setPackage(String _package) {
		this._package = _package;
	}

	public Constraint getConstraint() {
		return constraint;
	}

	public void setConstraint(Constraint constraint) {
		this.constraint = constraint;
	}

	public List<EObject> getEobjectList() {
		return eobjectList;
	}

	public void setEobjectList(List<EObject> eobjectList) {
		this.eobjectList = eobjectList;
	}
	
	public String getFullConstrainedElementName() {
		return fullName;
	}
	
	public EObject getEObject(Class<?> clazz) {
		return eobjectList.stream().filter(e -> 
			e.getClass().equals(clazz)
		).findFirst().orElse(null);
	}
	
}
