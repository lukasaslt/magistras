package lt.evaluator.data;

import java.util.ArrayList;
import java.util.List;

public class EModel extends EBaseElement {
	
	private List<EType> types = new ArrayList<>();

	public List<EType> getTypes() {
		return types;
	}

	public void setTypes(List<EType> types) {
		this.types = types;
	}

}