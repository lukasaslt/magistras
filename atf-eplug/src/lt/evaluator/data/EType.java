package lt.evaluator.data;

import java.util.ArrayList;
import java.util.List;

public class EType extends EBaseElement {

	private List<EProperty> properties = new ArrayList<>();
	private List<EOperation> operations = new ArrayList<>();

	public List<EProperty> getProperties() {
		return properties;
	}

	public void setProperties(List<EProperty> properties) {
		this.properties = properties;
	}

	public List<EOperation> getOperations() {
		return operations;
	}

	public void setOperations(List<EOperation> operations) {
		this.operations = operations;
	}

}