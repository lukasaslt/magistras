package lt.evaluator.eclipse.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import lt.evaluator.common.EvaluatorConstants;
import lt.evaluator.eclipse.views.EvaluatorView;

public class EvaluateHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent arg0) throws ExecutionException {
		handleView();
		return null;
	}

	public static void handleView() {
		try {
			IWorkbenchPage activePage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			EvaluatorView view = (EvaluatorView) activePage.findView(EvaluatorConstants.VIEW);
			
			if (view == null)
				view = (EvaluatorView) activePage.showView(EvaluatorConstants.VIEW, null, IWorkbenchPage.VIEW_CREATE);
			
			view.refresh();
			activePage.showView(EvaluatorConstants.VIEW);
		} catch (PartInitException e) {
			e.printStackTrace();
		}
	}

}
