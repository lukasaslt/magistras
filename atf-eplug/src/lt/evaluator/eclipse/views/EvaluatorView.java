package lt.evaluator.eclipse.views;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import lt.evaluator.Evaluator;
import lt.evaluator.common.EvaluatorConstants;
import lt.evaluator.data.EResult;
import lt.evaluator.provider.StyledCellLabelProviderExt;

public class EvaluatorView extends ViewPart {

	private TableViewer viewer;
	private Evaluator evaluator;
	private Object selectedElement;

	public EvaluatorView() {
		super();
		evaluator = new Evaluator();
	}

	public void setFocus() {
		viewer.getControl().setFocus();
	}

	public void createPartControl(Composite parent) {
		viewer = new TableViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);

		createColumns();

		final Table table = viewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		viewer.setContentProvider(new ArrayContentProvider());
		getSite().setSelectionProvider(viewer);
	}
	
	public void refresh() {
		viewer.setInput(getElements());
	}

	private void createColumns() {
		TableViewerColumn col = new TableViewerColumn(viewer, SWT.NONE);
		col.getColumn().setWidth(200);
		col.getColumn().setText("Model");
		col.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {
				EResult result = (EResult) element;
				return result.getModelName();
			}
		});

		TableViewerColumn col1 = new TableViewerColumn(viewer, SWT.RIGHT);
		col1.getColumn().setWidth(200);
		col1.getColumn().setText("Evaluation result");
		col1.setLabelProvider(new StyledCellLabelProviderExt() {

			@Override
			public void update(ViewerCell cell) {
				EResult result = (EResult) cell.getElement();
				cell.setText((int) (result.getResult()) + " %");
				super.update(cell);
			}
		});
	}

	private List<EResult> getElements() {
		List<EResult> elements = new ArrayList<>();

		try {
			// gali neveikt unit test, nes ant to pacio thread
			ISelectionService service = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getSelectionService();
			IStructuredSelection selection = (IStructuredSelection) service.getSelection("org.eclipse.ui.navigator.ProjectExplorer");
			selectedElement = selection.getFirstElement();
			
			if (isSelectionValid(selectedElement)) {
				try {
					ProgressMonitorDialog dialog = new ProgressMonitorDialog(getSite().getShell());
					dialog.run(true, true, new IRunnableWithProgress() {
						
						@Override
						public void run(IProgressMonitor monitor) {
							SubMonitor subMonitor = null;
							
							try {
								subMonitor = SubMonitor.convert(monitor, 100);
								
								EResult evalResult = evaluator.evaluate(selectedElement, subMonitor);
								selectedElement = null;
								elements.add(evalResult);
								
							} catch (Exception e) {
								e.printStackTrace();
								MessageDialog.openInformation(null, "Evaluator", "Model evaluation failed failed\n" + e.getMessage());
							} finally {
								subMonitor.done();
							}
						}
					});
					
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return elements;
	}

	private Shell createProgressDialog(Display display) {
		Shell shell = new Shell(Display.getDefault(), SWT.SHELL_TRIM & (~SWT.RESIZE));
		shell.setText("Evaluation progress");
		shell.setSize(350, 150);
		
		Monitor primary = display.getPrimaryMonitor();
		Rectangle displayBounds = primary.getBounds();
		Rectangle shellBounds = shell.getBounds();
		
		int x = displayBounds.x + (displayBounds.width - shellBounds.width) / 2;
		int y = (int) (displayBounds.y + (displayBounds.height - shellBounds.height) / 2.2);
		
		shell.setLocation(x, y);
		GridLayout layout = new GridLayout(1, false);
		layout.marginWidth = 20;
		layout.marginHeight = 30;
		shell.setLayout(layout);
		
		Label label = new Label(shell, SWT.CENTER);
		label.setText("Evaluation in progress...");
		FontData[] fd = label.getFont().getFontData();
		fd[0].setHeight(12);
		label.setFont( new Font(display,fd[0]));
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		label.setLayoutData(gridData);

		ProgressBar progressBar = new ProgressBar(shell, SWT.INDETERMINATE | SWT.CENTER);
		progressBar.setSize(200, 20);
		
		x = (shellBounds.width - progressBar.getBounds().width) / 2;
		y = (shellBounds.height - progressBar.getBounds().height) / 2;
		
		progressBar.setLocation(x, y);
		gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		progressBar.setLayoutData(gridData);

		shell.open();
		return shell;
	}

	private String getSelectedProjectPath() {
		ISelectionService service = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getSelectionService();
		IStructuredSelection structured = (IStructuredSelection) service
				.getSelection("org.eclipse.ui.navigator.ProjectExplorer");

		Object firstElement = structured.getFirstElement();
		if (firstElement instanceof IProject) {
			IProject project = (IProject) firstElement;
			return project.getLocation().toString();
		}
		
		if (firstElement instanceof ICompilationUnit) {
			ICompilationUnit unit = (ICompilationUnit) firstElement;
			return unit.getJavaProject().getProject().getLocation().toString();
		}

		return null;
	}
	
	private boolean isSelectionValid(Object firstElement) {
		return firstElement instanceof IProject
				|| firstElement instanceof IPackageFragment
				|| firstElement instanceof ICompilationUnit
				|| firstElement.getClass().getSimpleName().equals(EvaluatorConstants.PAPYRUS_FILE);
	}

}
