package lt.evaluator;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.internal.core.PackageFragment;

import eu.sarunas.atf.generators.tests.RandomGenerator;
import lt.evaluator.common.EvaluatorConstants;
import lt.evaluator.component.FileFinder;
import lt.evaluator.component.ModelParser;
import lt.evaluator.component.ResultCalculator;
import lt.evaluator.data.EModel;
import lt.evaluator.data.EResult;
import lt.evaluator.util.FilenameUtil;
import tudresden.ocl20.pivot.model.ModelAccessException;
import tudresden.ocl20.pivot.parser.ParseException;

public class Evaluator {
	
	private FileFinder fileFinder;
	private ModelParser modelParser;
	private ResultCalculator resultCalculator;
	
	public Evaluator() {
		fileFinder = new FileFinder();
		modelParser = new ModelParser();
		resultCalculator = new ResultCalculator();
	}
	
	public EResult evaluate(Object selectedElement, SubMonitor subMonitor) throws Exception {
		EModel eModel = new EModel();
		Path projectPath = null;
		
		subMonitor.setTaskName("Looking for model and OCL files...");
		
		subMonitor.worked(18);
		subMonitor.setTaskName("Parsing...");
		
		TimeUnit.MILLISECONDS.sleep(RandomGenerator.randomInt(500, 1500));
		
		if (selectedElement instanceof IProject) {
			IProject project = (IProject) selectedElement;
			projectPath = Paths.get(project.getLocation().toString());
			IJavaProject javaProject = JavaCore.create(project);
			
			List<Path> oclPathList = fileFinder.findAll(projectPath, EvaluatorConstants.OCL_EXT);
			
			// java
			appendFromProject(eModel, javaProject, oclPathList, projectPath);
			
			// all umls
			List<Path> umlPathList = fileFinder.findAll(projectPath, EvaluatorConstants.UML_EXT);
			for (Path umlPath : umlPathList)
				appendFromUml(eModel, umlPath, oclPathList);
			
		} else if (selectedElement instanceof PackageFragment) {
			PackageFragment pck = (PackageFragment) selectedElement;
			projectPath = Paths.get(pck.getJavaProject().getProject().getLocation().toString());
			List<Path> oclPathList = fileFinder.findAll(projectPath, EvaluatorConstants.OCL_EXT);
			
			appendFromPackage(eModel, oclPathList, pck, projectPath);
		} else if (selectedElement instanceof ICompilationUnit) {
			ICompilationUnit compUnit = (ICompilationUnit) selectedElement;
			projectPath = Paths.get(compUnit.getJavaProject().getProject().getLocation().toString());
			List<Path> oclPathList = fileFinder.findAll(projectPath, EvaluatorConstants.OCL_EXT);
			
			// single type
			String type = getSelectedElementTypeName(compUnit);
			
			appendFromCompilationUnit(eModel, oclPathList, compUnit, type, projectPath);
		} else if (selectedElement instanceof IFile) {
			// single uml
			IFile file = (IFile) selectedElement;
			
			if (!file.getFileExtension().equals(EvaluatorConstants.UML_EXT))
				return null;
			
			projectPath = Paths.get(file.getProject().getLocation().toString());
			List<Path> oclPathList = fileFinder.findAll(projectPath, EvaluatorConstants.OCL_EXT);
			Path modelPath = file.getLocation().toFile().toPath();
			appendFromUml(eModel, modelPath, oclPathList);
		} else if (selectedElement.getClass().getSimpleName().equals(EvaluatorConstants.PAPYRUS_FILE)) {
			// papyrus
			IAdaptable adaptable = (IAdaptable) selectedElement;
			IFile file = adaptable.getAdapter(IFile.class);
			Path modelPath = Paths.get(FilenameUtil.getName(file.getLocation().toFile().toString()) + "." + EvaluatorConstants.UML_EXT);
			
			projectPath = Paths.get(file.getProject().getLocation().toString());
			List<Path> oclPathList = fileFinder.findAll(projectPath, EvaluatorConstants.OCL_EXT);
			
			appendFromUml(eModel, modelPath, oclPathList);
		} else 
			return null;
		
		subMonitor.worked(25);
		subMonitor.setTaskName("Evaluating project model...");
		
		TimeUnit.MILLISECONDS.sleep(RandomGenerator.randomInt(500, 1500));
		
		eModel.setName(projectPath.getFileName().toString());
		
		// evaluate
		EResult result = resultCalculator.calculate(eModel);
		
		subMonitor.worked(57);
		subMonitor.setTaskName("Finishing...");
		
		TimeUnit.MILLISECONDS.sleep(RandomGenerator.randomInt(500, 1500));
		
		return result;
	}

	private String getSelectedElementTypeName(ICompilationUnit javaElement) {
		String type = null;
		String packageName = javaElement.getParent().getElementName();
		type = packageName + "." + FilenameUtil.getName(javaElement.getElementName());
		return type;
	}

	private void appendFromProject(EModel eModel, IJavaProject javaProject, List<Path> oclPathList, Path projectPath)
			throws JavaModelException, IOException, ModelAccessException, ParseException, CoreException {
		for (IPackageFragment pck : javaProject.getPackageFragments()) {
			if (!pck.getClass().equals(PackageFragment.class) || pck.getElementName().isEmpty())
				continue;
			
			appendFromPackage(eModel, oclPathList, pck, projectPath);
		}
	}

	private void appendFromPackage(EModel eModel, List<Path> oclPathList, IPackageFragment pck, Path projectPath) throws IOException, ModelAccessException, ParseException, CoreException {
		ICompilationUnit[] compilationUnits = pck.getCompilationUnits();
		for (ICompilationUnit compilationUnit : compilationUnits) {
			appendFromCompilationUnit(eModel, oclPathList, compilationUnit, null, projectPath);
		}
	}

	private void appendFromCompilationUnit(EModel eModel, List<Path> oclPathList, ICompilationUnit compilationUnit, String type, Path projectPath) throws ModelAccessException, IOException, ParseException, CoreException {
		Path classFile = fileFinder.findClassFile(projectPath, compilationUnit);
		
		for (Path oclPath : oclPathList)
			modelParser.parse(eModel, type, classFile, oclPath);
	}
	
	private void appendFromUml(EModel eModel, Path umlPath, List<Path> oclPathList)
			throws ModelAccessException, IOException, ParseException {
		for (Path oclPath : oclPathList)
			modelParser.parse(eModel, null, umlPath, oclPath);
	}

}
