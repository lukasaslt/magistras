package lt.evaluator.common;

import java.io.InputStream;
import java.util.Properties;

public class Config {
	
	private static Properties props;

	static {
		props = new Properties();
		loadProperties();
	}
	
	private static void loadProperties() {
		try {
			InputStream in = Config.class.getResourceAsStream("/config.properties");
			props.load(in);
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String get(Parameter param) {
	    return props.getProperty(param.getName());
	}
	
	public enum Parameter {
		
		EXEC_MUTATION_TESTS("exec.mutation.tests"),
		OUTPUT_DIR("output.dir"),
		SRC_DIR("src.dir"),
		CLASS_DIR("class.dir"),
		PITEST_LIB_CLASSPATH("pitest.lib.classpath");

	    private final String name;

	    private Parameter(String name) {
	        this.name = name;
	    }

	    private String getName() {
	        return name;
	    }
	}

}
