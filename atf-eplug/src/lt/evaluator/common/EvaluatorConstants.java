package lt.evaluator.common;

public class EvaluatorConstants {
	
	public static final String RESOURCES_LOG4J_PROPERTIES_PATH = "resources/log4j.properties";
	public static final String UML_RESOURCES_JAR_PATH = "resources/org.eclipse.uml2.uml.resources_5.4.0.v20180903-1400.jar";
	public static final String VIEW = "lt.evaluator.eclipse.views.EvaluatorView";
	public static final String CLASS_EXT = "class";
	public static final String UML_EXT = "uml";
	public static final String OCL_EXT = "ocl";
	public static final String COMPILE_DIR = "bin";
	public static final String PAPYRUS_FILE = "PapyrusFile";

}
