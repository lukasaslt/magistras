package lt.evaluator.provider;

import java.awt.Color;

import org.eclipse.jface.viewers.StyledCellLabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.TreeItem;

import lt.evaluator.data.EResult;

public class StyledCellLabelProviderExt extends StyledCellLabelProvider {
	
	private int padding = 3;
	
	@Override
	protected void paint(Event event, Object element) {
		super.paint(event, element);

		Rectangle cellBounds = getViewerCellBounds(event);
		Image image = getImage(event, cellBounds, element);
		if (image == null) {
			return;
		}
		
		// Should ideally use ViewerCell but not available unless we copy/paste
		// internal code from superclass
		if (cellBounds == null) {
			return;
		}

		event.gc.drawImage(image, cellBounds.x + padding, cellBounds.y + padding);
	}

	private Rectangle getViewerCellBounds(Event event) {
		if (event.item instanceof TableItem) {
			return ((TableItem) event.item).getBounds(event.index);
		}
		return ((TreeItem) event.item).getBounds(event.index);
	}
	
	private Image getImage(Event event, Rectangle bounds, Object element) {
		EResult result = (EResult) element;

		Color backgroundColor = Color.RED;
		Color progressColor = Color.GREEN;
		PaletteData palette = new PaletteData(0xFF, 0xFF00, 0xFF0000);
		
        int width = (int) (bounds.width * 0.8) - padding * 2;
		int height = bounds.height - padding * 2;
		
		ImageData imageData = new ImageData(width, height, 24, palette);
		
		int progressEnd = (int) (width * (result.getResult() / 100));
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                Color color = x < progressEnd ? progressColor : backgroundColor;
				imageData.setPixel(x, y, palette.getPixel(new RGB(color.getRed(), color.getGreen(), color.getBlue())));
            }
        }
        		
        return new Image(event.display, imageData);
    }

}
