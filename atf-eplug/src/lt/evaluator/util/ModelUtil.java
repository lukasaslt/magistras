package lt.evaluator.util;

import java.util.List;

import eu.sarunas.atf.meta.sut.Class;
import eu.sarunas.atf.meta.sut.Constructor;
import eu.sarunas.atf.meta.sut.Field;
import eu.sarunas.atf.meta.sut.Method;
import eu.sarunas.atf.meta.sut.Package;
import eu.sarunas.atf.meta.sut.Project;

public class ModelUtil {

	public static Class findClass(Project project, String pckg, String clazz){
		Package pack = findPackage(project,pckg);
		if(pack == null){
			return null;
		}
		
		return findClass(pack, clazz);
	}
	
	public static Package findPackage(Project project, String pckg){
		for (Package p : project.getPackages()) {
			if(p.getName().equals(pckg)){
				return p;
			}
		}
		
		return null;
	}
	
	public static Class findClass(Package pckg,String name){
		for (Class cls : pckg.getClasses()) {
			if(cls.getName().equals(name)){
				return (Class) cls;
			}
		}
		return null;
	}
	
	public static Class findClassFullName(Package pckg,String fullName){
		for (Class cls : pckg.getClasses()) {
			if(((Class)cls).getFullName().equals(fullName)){
				return (Class) cls;
			}
		}
		return null;
	}

    public static Field findField(Class cls,String name){
    	for (Field field : cls.getFields()) {
			if(field.getName().equals(name)){
				return (Field) field;
			}
		}
    	if(cls.getSuperClass() != null){
    		return findField((Class) cls.getSuperClass(), name);
    	}
    	
    	return null;
    }

    public static Method findGetter(Class cls, String fieldName){
    	String isGetter  = "is"  + fieldName;
    	String getGetter = "get" + fieldName;
    	
    	for (Method method : cls.getMethods()) {
    		if(method.getName().equalsIgnoreCase(getGetter) || method.getName().equalsIgnoreCase(isGetter)){
    			return (Method) method;
    		}
		}
    	if(cls.getSuperClass() != null){
    		return findGetter((Class) cls.getSuperClass(),fieldName);
    	}
    	
    	return null;
    }
	
	public static boolean isJavaBeanMethod(Method method){
		String name = method.getName();
		if(name.startsWith("is") || name.startsWith("get") || name.startsWith("set")){
			return true;
		}
		return false;
	}
	
	public static boolean isJavaBean(Class clazz){
		List<Method> list = clazz.getMethods();
		for (Method iMethod : list) {
		
			if(!isJavaBeanMethod(iMethod)){
				return false;
			}
		}
		return true;
	}
	
    public static boolean hasDefaultConstructor(Class clazz){
    	if(clazz.getConstructors().isEmpty()){
    		return true;
    	}
    	for (Constructor cons : clazz.getConstructors()) {
			if(cons.isDefaultConstructor()){
				return true;
			}
		}
    	return false;
    }

}
