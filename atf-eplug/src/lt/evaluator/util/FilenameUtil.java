package lt.evaluator.util;

public class FilenameUtil {
	
	public static String getName(final String fileName) {
		int i = fileName.lastIndexOf('.');
		
		if (i > 0)
		    return fileName.substring(0, i);
		    
	    return null;
    }
	
	public static String getExtension(final String fileName) {
		int i = fileName.lastIndexOf('.');
		
		if (i > 0)
		    return fileName.substring(i + 1);
		    
	    return null;
    }

}
	