package lt.atgplugin.enumeration;

public enum TestGenerator {
	
	ATG("ATG"),
	ATF("ATF");

    private final String name;       

    private TestGenerator(String s) {
        name = s;
    }

    public boolean equalsName(String otherName) {
        return name.equals(otherName);
    }

    public String toString() {
       return this.name;
    }

}
