package lt.atgplugin.popup.actions;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;

import lt.atgplugin.utils.Constants;
import lt.atgplugin.utils.Utils;
import lt.evaluator.eclipse.handlers.EvaluateHandler;

@SuppressWarnings("restriction")
public class GenerateTestCUDefaultsAction implements IObjectActionDelegate {

	/**
	 * Constructor for Action1.
	 */
	public GenerateTestCUDefaultsAction() {
		super();
	}

	private Shell shell;

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}

	List<ICompilationUnit> list = new ArrayList<ICompilationUnit>(0);

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
		ISelection selection = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getSelectionService().getSelection("org.eclipse.ui.navigator.ProjectExplorer");
		
		Utils.selectionChanged(null, selection);
		
		EvaluateHandler.handleView();
		MainGeneratorAction m = new MainGeneratorAction(list, shell);

		if (Constants.showTimes) {
			MessageDialog.openWarning(shell, "Performance",
					m.getPerformanceTimes());
			if (m.isAnyError()) {
				MessageDialog.openWarning(shell,
						"Not all tests were generated", m.getFailedClasses());
			} else {
				MessageDialog.openInformation(shell, "Tests were generated",
						"Tests were generated successfully!");

			}

		}
		list.clear();

	}
	
	public void selectionChanged(IAction action, ISelection selection) {
		list = Utils.selectionChanged(action, selection);
	}

}
