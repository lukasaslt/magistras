package lt.atgplugin.popup.actions;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPartSite;

import eu.sarunas.atf.eclipse.actions.ExecuteMutationTestsAction;
import eu.sarunas.atf.eclipse.actions.GenerateTestsAction;
import lt.atgplugin.utils.Constants;
import lt.atgplugin.utils.Utils;
import lt.atgplugin.wizards.GenerateTestWizard;
import lt.evaluator.common.Config;
import lt.evaluator.common.Config.Parameter;
import lt.evaluator.eclipse.handlers.EvaluateHandler;

@SuppressWarnings("restriction")
public class GenerateTestCUAction implements IObjectActionDelegate {

	private Shell shell;
	List<ICompilationUnit> list = new ArrayList<ICompilationUnit>();
	private IWorkbenchPartSite site;
	private ISelection selection;

	/**
	 * Constructor for Action1.
	 */
	public GenerateTestCUAction() {
		super();
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		site = targetPart.getSite();
		shell = site.getShell();

	}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
		GenerateTestWizard wizard = new GenerateTestWizard(shell);
		wizard.init(null, null);
		WizardDialog dialog = new WizardDialog(shell, wizard) {
			@Override
			protected void configureShell(Shell newShell) {
				super.configureShell(newShell);
				newShell.setSize(newShell.getSize().x, 600);
			}
		};
		
		dialog.create();
		dialog.open();
		
		if (dialog.getReturnCode() == 0) {
			if (wizard.shouldEvaluateModel())
				EvaluateHandler.handleView();
			
			GenerateTestsAction generateTestsAction = null;
			
			switch (wizard.getSelectedTestGenerator()) {
			case ATG:
				MainGeneratorAction m = new MainGeneratorAction(list, shell);

				if (Constants.showTimes) {
					MessageDialog.openWarning(shell, "Performance",
							m.getPerformanceTimes());
					if (m.isAnyError()) {
						MessageDialog.openWarning(shell,
								"Failed to generate tests", m.getFailedClasses());
					} else {
						MessageDialog.openInformation(shell, "Tests were generated",
								"Tests were generated successfully!");

					}
				}
			
				list.clear();
				break;
			case ATF:
				try {
					generateTestsAction = new GenerateTestsAction();
					generateTestsAction.execute(selection);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				// mutation tests
				boolean execMutationTests = Boolean.parseBoolean(Config.get(Parameter.EXEC_MUTATION_TESTS));
				if (execMutationTests && generateTestsAction != null) {
					while (!generateTestsAction.isDone())
						Display.getDefault().readAndDispatch();
					
					ExecuteMutationTestsAction executeMutationTestsAction = new ExecuteMutationTestsAction();
					executeMutationTestsAction.execute(selection, generateTestsAction.getTestProject(), shell);
				}
				
				break;
			default:
				break;
			}
		}
	}

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
		this.selection = selection;
		list = Utils.selectionChanged(action, selection);
	}
}
