package lt.atgplugin.wizards.pages;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.dialogs.IDialogPage;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import lt.atgplugin.enumeration.TestGenerator;
import lt.atgplugin.filters.ATGOption;
import lt.atgplugin.generator.rules.Rule;
import lt.atgplugin.wizards.helpers.ATGList;
import lt.atgplugin.wizards.helpers.ATGTable;
import lt.atgplugin.wizards.helpers.DefaultOptions;

public class GeneratorSelectionPage extends WizardPage {

	public Rule getDefault() {
		DefaultOptions.setDefaultRule(sar.getSelectedItem());
		return (Rule) sar.getSelectedItem();
	}

	public List<Rule> getSelectedOptions() {
		DefaultOptions.setDefaultRules(table.getSelectedItems());
		List<Rule> tmp = new ArrayList<Rule>(0);
		for (ATGOption o : table.getSelectedItems()) {
			tmp.add((Rule) o);
		}
		return tmp;
	}

	protected ATGTable table = null;
	protected ATGList sar = null;
	protected Shell shell;
	private Button checkBox;
	private Combo combo;
	private boolean evaluate = true;
	private TestGenerator selectedTestGenerator = TestGenerator.ATG;

	/**
	 * Constructor for SampleNewWizardPage.
	 * 
	 * @param pageName
	 */
	public GeneratorSelectionPage(ISelection selection, Shell shell) {

		super("wizardPage");

		setTitle("Generator selection page");
		setDescription("Please select generator.");
		this.shell = shell;
	}

	public GeneratorSelectionPage() {
		super("wizardPage");
		setTitle("Generator selection page");
		setDescription("Please select generator.");
	}

	/**
	 * @see IDialogPage#createControl(Composite)
	 */
	public void createControl(Composite parent) {

		GridData gridData = new GridData();
		gridData.horizontalSpan = 2;
		gridData.horizontalAlignment = SWT.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.verticalAlignment = SWT.FILL;
		gridData.grabExcessVerticalSpace = true;

		Composite container = new Composite(parent, SWT.NULL);

		GridLayout layout = new GridLayout();
		container.setLayout(layout);
		layout.numColumns = 1;
		layout.verticalSpacing = 9;

		createElement(gridData, container);
		dialogChanged();
		setControl(container);
	}

	private void createElement(GridData gridData, Composite container) {
		Label label = new Label(container, SWT.NULL);
		label.setText("Select test generator, which will be used for test generator:");
		
		combo = new Combo(container, SWT.DROP_DOWN | SWT.READ_ONLY);
		combo.setItems(new String [] {TestGenerator.ATG.toString(), TestGenerator.ATF.toString()});	
		combo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				getWizard().getContainer().updateButtons();
				selectedTestGenerator = TestGenerator.valueOf(combo.getItem(combo.getSelectionIndex()));
				
				checkBox.setVisible(isATFSelected());
			}
		});
		combo.select(0);
		
		checkBox = new Button(container, SWT.CHECK);
		checkBox.setText("Evaluate project model before generating tests");
		checkBox.setSelection(evaluate);
		checkBox.addSelectionListener(new SelectionAdapter() {
			
		    @Override
		    public void widgetSelected(SelectionEvent event) {
	            evaluate = checkBox.getSelection();
		    }
		});
		checkBox.setVisible(isATFSelected());
	}

	/**
	 * Ensures that both text fields are set.
	 */

	private void dialogChanged() {
		updateStatus(null);
	}

	private void updateStatus(String message) {
		setErrorMessage(message);
		setPageComplete(message == null);
	}
	
	public boolean isEvaluateSelected() {
		return evaluate && isATFSelected();
	}
	
	@Override
	public boolean canFlipToNextPage() {
		if (combo.getSelectionIndex() == -1)
			return false;
			
		return TestGenerator.valueOf(combo.getItem(combo.getSelectionIndex())).equals(TestGenerator.ATG);
	}
	
	public TestGenerator getSelectedTestGenerator() {
		return selectedTestGenerator;
	}
	
	private boolean isATFSelected() {
		return selectedTestGenerator == TestGenerator.ATF;
	}

}
