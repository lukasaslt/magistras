package eu.sarunas.atf.eclipse.actions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.stream.Collectors;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.internal.core.PackageFragment;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.swt.widgets.Shell;

import eu.sarunas.atf.meta.tests.TestProject;
import eu.sarunas.atf.utils.Logger;
import lt.evaluator.common.Config;
import lt.evaluator.common.Config.Parameter;

public class ExecuteMutationTestsAction extends BaseAction
{
	private static final String OUTPUT_DIR = Config.get(Parameter.OUTPUT_DIR);
	private static final String SRC_DIR = Config.get(Parameter.SRC_DIR);
	private static final String CLASS_DIR = Config.get(Parameter.CLASS_DIR);
	private static final String PITEST_LIB_CLASSPATH = Config.get(Parameter.PITEST_LIB_CLASSPATH);
	private TestProject testProject;

	public ExecuteMutationTestsAction()
	{
		super("Mutation tests execution");
	};

	@Override
	protected void executeAction(IProgressMonitor monitor) throws Throwable
	{
		Process proc = null;
		
		try {
			IJavaProject project = getJavaProject(this.selectedItem);
			String projectPathStr = project.getProject().getLocation().toString();
			String projectClassFolder = Paths.get(projectPathStr, CLASS_DIR).toString();
			String projectOutputDir = Paths.get(projectPathStr, OUTPUT_DIR).toString();
			String projectSrcDir = Paths.get(projectPathStr, SRC_DIR).toString();
			
			String targetClasses = testProject.getTestSuites().stream()
				.filter(ts -> ts.getTestCases().size() > 0)
				.map(ts ->
					ts.getTestCases().stream()
					.map(tc ->
						tc.getPackage().getName() + "." + tc.getClassName()
					).distinct().collect(Collectors.joining(","))
				).collect(Collectors.joining(","));
			
			String targetTests = testProject.getTestSuites().stream()
				.filter(ts -> ts.getTestCases().size() > 0)
				.map(ts ->
					ts.getTestCases().get(0).getPackage().getName() + "." + ts.getName()
				).collect(Collectors.joining(","));
			
			String command = "java "
					+ "-cp \"" + PITEST_LIB_CLASSPATH + ";" + projectClassFolder + "\" org.pitest.mutationtest.commandline.MutationCoverageReport "
					+ "--reportDir \"" + projectOutputDir + "\" "
					+ "--targetClasses " + targetClasses + " "
					+ "--targetTests " + targetTests + " "
					+ "--sourceDirs \"" + projectSrcDir + "\"";
			proc = Runtime.getRuntime().exec(command);
			proc.waitFor();
			output = getStandardOutputStr(proc);
			Logger.logger.info(output);
		} catch (Exception ex) {
			this.exception = ex;
			ex.printStackTrace();
		} finally {
			if (proc != null)
				proc.destroy();
			
			monitor.done();
		}
	}
	
	private IJavaProject getJavaProject(Object selection) {
		if (selection instanceof IProject) {
			IProject project = (IProject) selection;
			return JavaCore.create(project);
		} else if (selection instanceof IPackageFragment) {
			PackageFragment pck = (PackageFragment) selection;
			return pck.getJavaProject();
		} else if (selection instanceof ICompilationUnit) {
			ICompilationUnit compUnit = (ICompilationUnit) selection;
			return compUnit.getJavaProject();
		} else if (selection instanceof IType) {
			IType type = (IType) selection;
			return type.getParent().getJavaProject();
		} else if (selection instanceof IMethod) {
			IMethod method = (IMethod) selection;
			return method.getParent().getJavaProject();
		} else 
			return null;
	};

	public static String getStandardOutputStr(Process proc) throws IOException {
		BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));
		StringBuilder strBuilder = new StringBuilder();
		String s = null;
		while ((s = stdInput.readLine()) != null) {
			strBuilder.append(s);
			strBuilder.append(System.lineSeparator());
		}
		return strBuilder.toString();
	}
	
	@Override
	protected void onDone()
	{
		if (null != this.exception)
		{
			Logger.log(this.exception);

			MessageDialog.openInformation(null, this.taskName, "Mutation execution failed\n" + this.exception.getMessage());
		}
		else
		{
			if (output == null || output.isEmpty())
				return;
			
			String searchStr = "Statistics";
			String message = output.substring(output.indexOf(searchStr) + searchStr.length());
			
			message = message.replaceAll("=", "");
			message = message.replaceAll(">", "");
			
			MessageDialog.openInformation(this.shell, this.taskName, message);
		}
	};
	
	public void execute(Object selectedItem, TestProject testProject, Shell shell) {
		try {
			if (selectedItem instanceof TreeSelection) {
				org.eclipse.jface.viewers.TreeSelection t = (TreeSelection) selectedItem;
				Iterator<?> it = t.iterator();
				while (it.hasNext()) {
					Object o = it.next();
					selectedItem = o;
				}
			}
			
			this.selectedItem = selectedItem;
			this.testProject = testProject;
			this.shell = shell;
			run(null);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	private Exception exception;
	private String output;
	private Shell shell;
};
