package eu.sarunas.atf.eclipse.actions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.internal.core.PackageFragment;
import org.eclipse.jdt.junit.JUnitCore;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.TreeSelection;

import eu.sarunas.atf.eclipse.generators.EclipseTestsWriter;
import eu.sarunas.atf.eclipse.parsers.JavaProjectParser;
import eu.sarunas.atf.eclipse.utils.ProjectManager;
import eu.sarunas.atf.generators.code.java.CodeGeneratorJava;
import eu.sarunas.atf.generators.code.junit.TestTransformerJUnit;
import eu.sarunas.atf.meta.sut.Class;
import eu.sarunas.atf.meta.sut.Method;
import eu.sarunas.atf.meta.sut.Project;
import eu.sarunas.atf.meta.tests.TestProject;
import eu.sarunas.atf.meta.tests.TestSuite;
import eu.sarunas.atf.utils.Logger;

public class GenerateTestsAction extends BaseAction
{
	public GenerateTestsAction()
	{
		super("Tests generation");
	};

	@Override
	protected void executeAction(IProgressMonitor monitor) throws Throwable
	{
		if (this.selectedItem instanceof TreeSelection) {
			org.eclipse.jface.viewers.TreeSelection t = (TreeSelection) this.selectedItem;
			Iterator<?> it = t.iterator();
			while (it.hasNext()) {
				Object o = it.next();
				handleSelection(monitor, o);
			}
		} else
			handleSelection(monitor, this.selectedItem);
	}

	private void handleSelection(IProgressMonitor monitor, Object selection) {
		try
		{
			IJavaProject project = getJavaProject(selection);
			
			if (project == null) {
				Logger.logger.info("Could not retrieve Java project...");
				return;
			}
			
			JavaProjectParser parser = new JavaProjectParser();
			Project sutModel = parser.parseProject(project);
			eu.sarunas.atf.generators.tests.TestsGenerator testsGenerator = new eu.sarunas.atf.generators.tests.TestsGenerator();
			testProject = new TestProject();
			testProject.setName(project.getElementName());
			
			this.generatedTests = 0;
			this.methods = 0;
			this.classes = 0;
			
			if (selection instanceof IMethod) {
				IMethod method = (IMethod) selection;
				this.classes++;
				generateForMethod(project, sutModel, testsGenerator, testProject, method);
			} else if (selection instanceof IType) {
				IType type = (IType) selection;
				generateForClass(project, sutModel, testsGenerator, testProject, type);
			} else if (selection instanceof ICompilationUnit) {
				ICompilationUnit compUnit = (ICompilationUnit) selection;
				generateForCompilationUnit(project, sutModel, testsGenerator, testProject, compUnit);
			} else if (selection instanceof IPackageFragment) {
				PackageFragment pck = (PackageFragment) selection;
				generateForPackage(project, sutModel, testsGenerator, testProject, pck);
			} else if (selection instanceof IProject) {
				generateForProject(project, sutModel, testsGenerator, testProject);
			}
			
			for (TestSuite ts : testProject.getTestSuites())
				this.generatedTests += ts.getTestCases().size();
			
			List<String> constraintsFiles = ProjectManager.getConstraintsFiles(project);
			EclipseTestsWriter writer = new EclipseTestsWriter();
			IPath testsFolder = writer.createTestsFolder("tests", project, monitor);

			generateHelperClass(project, testsFolder, monitor);
			addHelperLibrary(project, monitor);
			addHelperFiles(project, monitor);
			
			addHelperLibrary(project, monitor, "org.emftext.access_1.2.0.201009131109.jar");
			addHelperLibrary(project, monitor, "org.emftext.commons.antlr3_2_0_1.0.0.jar");
			addHelperLibrary(project, monitor, "org.kiama.attribution_3.1.0.201101171054.jar");
			addHelperLibrary(project, monitor, "scala-library.jar");
			addHelperLibrary(project, monitor, "tudresden.ocl20.logging_3.1.0.201101171054.jar");
			addHelperLibrary(project, monitor, "tudresden.ocl20.pivot.essentialocl.standardlibrary_3.1.0.201101171054.jar");
			addHelperLibrary(project, monitor, "tudresden.ocl20.pivot.essentialocl_3.1.0.201101171054.jar");
			addHelperLibrary(project, monitor, "tudresden.ocl20.pivot.examples.pml_3.1.0.201101171055.jar");
			addHelperLibrary(project, monitor, "tudresden.ocl20.pivot.examples.simple_3.1.0.201101171055.jar");
			addHelperLibrary(project, monitor, "tudresden.ocl20.pivot.interpreter_3.1.0.201101171054.jar");
			addHelperLibrary(project, monitor, "tudresden.ocl20.pivot.language.ocl.resource.ocl_3.1.0.201101171054.jar");
			addHelperLibrary(project, monitor, "tudresden.ocl20.pivot.language.ocl.semantics_3.1.0.201101171054.jar");
			addHelperLibrary(project, monitor, "tudresden.ocl20.pivot.language.ocl.staticsemantics_3.1.0.201101171054.jar");
			addHelperLibrary(project, monitor, "tudresden.ocl20.pivot.language.ocl_3.1.0.201101171054.jar");
			addHelperLibrary(project, monitor, "tudresden.ocl20.pivot.metamodels.ecore_3.1.0.201101171054.jar");
			addHelperLibrary(project, monitor, "tudresden.ocl20.pivot.metamodels.java_3.1.0.201101171054.jar");
			addHelperLibrary(project, monitor, "tudresden.ocl20.pivot.metamodels.uml2_3.1.0.201101171054.jar");
			addHelperLibrary(project, monitor, "tudresden.ocl20.pivot.metamodels.xsd_3.1.0.201101171054.jar");
			addHelperLibrary(project, monitor, "tudresden.ocl20.pivot.model_3.1.0.201101171054.jar");
			addHelperLibrary(project, monitor, "tudresden.ocl20.pivot.modelinstance_3.1.0.201101171054.jar");
			addHelperLibrary(project, monitor, "tudresden.ocl20.pivot.modelinstancetype.ecore_3.1.0.201101171054.jar");
			addHelperLibrary(project, monitor, "tudresden.ocl20.pivot.modelinstancetype.java_3.1.0.201101171054.jar");
			addHelperLibrary(project, monitor, "tudresden.ocl20.pivot.modelinstancetype.xml_3.1.0.201101171054.jar");
			addHelperLibrary(project, monitor, "tudresden.ocl20.pivot.modelinstancetype_3.1.0.201101171054.jar");
			addHelperLibrary(project, monitor, "tudresden.ocl20.pivot.parser_3.1.0.201101171054.jar");
			addHelperLibrary(project, monitor, "tudresden.ocl20.pivot.pivotmodel.semantics_3.1.0.201101171054.jar");
			addHelperLibrary(project, monitor, "tudresden.ocl20.pivot.pivotmodel_3.1.0.201101171054.jar");
			addHelperLibrary(project, monitor, "tudresden.ocl20.pivot.standardlibrary.java_3.1.0.201101171054.jar");
			addHelperLibrary(project, monitor, "tudresden.ocl20.pivot.tools.codegen.declarativ.ocl2sql_3.1.0.201101171054.jar");
			addHelperLibrary(project, monitor, "tudresden.ocl20.pivot.tools.codegen.declarativ_3.0.0.201101171054.jar");
			addHelperLibrary(project, monitor, "tudresden.ocl20.pivot.tools.codegen.ocl2java.types_3.0.0.201101171054.jar");
			addHelperLibrary(project, monitor, "tudresden.ocl20.pivot.tools.codegen.ocl2java_3.1.0.201101171054.jar");
			addHelperLibrary(project, monitor, "tudresden.ocl20.pivot.tools.codegen_3.1.0.201101171054.jar");
			addHelperLibrary(project, monitor, "tudresden.ocl20.pivot.tools.CWM_3.0.0.201101171054.jar");
			addHelperLibrary(project, monitor, "tudresden.ocl20.pivot.tools.template.sql_3.1.0.201101171054.jar");
			addHelperLibrary(project, monitor, "tudresden.ocl20.pivot.tools.template_3.1.0.201101171054.jar");
			addHelperLibrary(project, monitor, "tudresden.ocl20.pivot.tools.transformation.pivot2sql_3.0.0.201101171054.jar");
			addHelperLibrary(project, monitor, "tudresden.ocl20.pivot.tools.transformation_3.1.0.201101171054.jar");
			addHelperLibrary(project, monitor, "org.eclipse.emf.ecore_2.6.0.v20100614-1136.jar");
			addHelperLibrary(project, monitor, "org.eclipse.emf.common_2.6.0.v20100614-1136.jar");
			addHelperLibrary(project, monitor, "org.eclipse.core.runtime_3.6.0.v20100505.jar");
			addHelperLibrary(project, monitor, "org.eclipse.osgi_3.6.0.v20100517.jar");
			addHelperLibrary(project, monitor, "org.eclipse.equinox.common_3.6.0.v20100503.jar");
			addHelperLibrary(project, monitor, "org.apache.log4j_1.2.13.v200903072027.jar");
			addHelperLibrary(project, monitor, "org.apache.commons.lang_2.3.0.v200803061910.jar");
			addHelperLibrary(project, monitor, "org.eclipse.emf.ecore.xmi_2.5.0.v20100521-1846.jar");
			
			for (TestSuite ts : testProject.getTestSuites())
			{
				TestTransformerJUnit trasnformer = new TestTransformerJUnit();
				eu.sarunas.atf.meta.sut.Class cl = trasnformer.transformTest(ts, constraintsFiles);

				CodeGeneratorJava cdgj = new CodeGeneratorJava();
				String code = cdgj.generateClass(cl);

				writer.createTestsFile(cl.getFullName(), code, project, testsFolder, monitor);
			}
		}
		catch (Exception ex)
		{
			this.exception = ex;
		}
	}

	private void generateForMethod(IJavaProject project, Project sutModel,
			eu.sarunas.atf.generators.tests.TestsGenerator testsGenerator, TestProject testProject, IMethod method)
			throws Exception {
		Method methodToTest = sutModel.findMethod(method);
		TestSuite testSuite = testsGenerator.generate(methodToTest, testProject, ProjectManager.getConstraints(project));
		Logger.logger.info(testSuite.toString());
		testProject.getTestSuites().add(testSuite);
	}

	private void generateForProject(IJavaProject project, Project sutModel,
			eu.sarunas.atf.generators.tests.TestsGenerator testsGenerator, TestProject testProject)
			throws JavaModelException, Exception {
		for (IPackageFragment pck : project.getPackageFragments()) {
			if (!pck.getClass().equals(PackageFragment.class) || pck.getElementName().isEmpty())
				continue;
			
			generateForPackage(project, sutModel, testsGenerator, testProject, pck);
		}
	}

	private void generateForPackage(IJavaProject project, Project sutModel,
			eu.sarunas.atf.generators.tests.TestsGenerator testsGenerator, TestProject testProject, IPackageFragment pck)
			throws JavaModelException, Exception {
		ICompilationUnit[] compilationUnits = pck.getCompilationUnits();
		
		for (ICompilationUnit compilationUnit : compilationUnits)
			generateForCompilationUnit(project, sutModel, testsGenerator, testProject, compilationUnit);
	}

	private void generateForCompilationUnit(IJavaProject project, Project sutModel,
			eu.sarunas.atf.generators.tests.TestsGenerator testsGenerator, TestProject testProject,
			ICompilationUnit compUnit) throws JavaModelException, Exception {
		IType[] types = compUnit.getTypes();
		
		for (IType iType : types) {
			generateForClass(project, sutModel, testsGenerator, testProject, iType);
		}
	}

	private void generateForClass(IJavaProject project, Project sutModel,
			eu.sarunas.atf.generators.tests.TestsGenerator testsGenerator, TestProject testProject, IType type)
			throws Exception {
		Class cl = sutModel.findClass(type);
		
		if (cl == null)
			return;
		
		this.classes++;
		this.methods += cl.getMethods().size();
		
		if (null != cl) {
			TestSuite ts = testsGenerator.generate(cl, testProject, ProjectManager.getConstraints(project));
			testProject.getTestSuites().add(ts);
		}
	}

	private IJavaProject getJavaProject(Object selection) {
		if (selection instanceof IProject) {
			IProject project = (IProject) selection;
			return JavaCore.create(project);
		} else if (selection instanceof IPackageFragment) {
			PackageFragment pck = (PackageFragment) selection;
			return pck.getJavaProject();
		} else if (selection instanceof ICompilationUnit) {
			ICompilationUnit compUnit = (ICompilationUnit) selection;
			return compUnit.getJavaProject();
		} else if (selection instanceof IType) {
			IType type = (IType) selection;
			return type.getParent().getJavaProject();
		} else if (selection instanceof IMethod) {
			IMethod method = (IMethod) selection;
			return method.getParent().getJavaProject();
		} else 
			return null;
	};
	
	private void generateHelperClass(IJavaProject javaProject, IPath testsFolder, IProgressMonitor monitor) throws UnsupportedEncodingException, IOException, URISyntaxException
	{
		EclipseTestsWriter writer = new EclipseTestsWriter();

		String code = "";

		try (BufferedReader reader = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream("/JUnitTemplate.txt"))))
		{
			for (String line; (line = reader.readLine()) != null;)
			{
				code += line + "\n";
			}
		}

		writer.createTestsFile("eu.sarunas.junit.TestsHelper", code, javaProject, testsFolder, monitor);
	};

	private void addHelperLibrary(IJavaProject javaProject, IProgressMonitor monitor) throws UnsupportedEncodingException, IOException, URISyntaxException, CoreException
	{
		IFolder folder = javaProject.getProject().getFolder("tests_lib");

		if (false == folder.exists())
		{
			folder.create(IResource.NONE, true, monitor);
		}

		IFile file = folder.getFile("tests-runner-helper.jar");

		if (file.exists())
		{
			file.delete(true, monitor);
		}

		file.create(this.getClass().getResourceAsStream("/tests-runner-helper.jar"), IResource.NONE, monitor);

		IClasspathEntry jUnitEntry = JavaCore.newContainerEntry(JUnitCore.JUNIT4_CONTAINER_PATH);

		IClasspathEntry[] currentEntries = javaProject.getRawClasspath();

		boolean hasRunner = false;
		boolean hasJUnit = false;

		for (IClasspathEntry entry : currentEntries)
		{
			if (true == entry.getPath().equals(file.getFullPath()))
			{
				hasRunner = true;
			}
			else if (true == entry.getPath().equals(jUnitEntry.getPath()))
			{
				hasJUnit = true;
			}
		}

		if ((false == hasJUnit) || (false == hasRunner))
		{
			List<IClasspathEntry> newEntries = new ArrayList<>();
			Collections.addAll(newEntries, currentEntries);

			if (false == hasRunner)
			{
				newEntries.add(JavaCore.newLibraryEntry(file.getFullPath(), null, null));
			}

			if (false == hasJUnit)
			{
				newEntries.add(jUnitEntry);
			}

			javaProject.setRawClasspath(newEntries.toArray(new IClasspathEntry[0]), monitor);
		}
	};
	
	private void addHelperLibrary(IJavaProject javaProject, IProgressMonitor monitor, String libraryName) throws UnsupportedEncodingException, IOException, URISyntaxException, CoreException
	{
		IFolder folder = javaProject.getProject().getFolder("tests_lib");

		if (false == folder.exists())
		{
			folder.create(IResource.NONE, true, monitor);
		}

		IFile file = folder.getFile(libraryName);

		if (file.exists())
		{
			file.delete(true, monitor);
		}

		file.create(this.getClass().getResourceAsStream("/" + libraryName), IResource.NONE, monitor);

		IClasspathEntry[] currentEntries = javaProject.getRawClasspath();

		boolean hasLibrary = false;

		for (IClasspathEntry entry : currentEntries)
		{
			if (true == entry.getPath().equals(file.getFullPath()))
			{
				hasLibrary = true;
				break;
			}
		}

		if (false == hasLibrary)
		{
			List<IClasspathEntry> newEntries = new ArrayList<>();
			Collections.addAll(newEntries, currentEntries);

			newEntries.add(JavaCore.newLibraryEntry(file.getFullPath(), null, null));

			javaProject.setRawClasspath(newEntries.toArray(new IClasspathEntry[0]), monitor);
		}
	};	
	
	private void addHelperFiles(IJavaProject javaProject, IProgressMonitor monitor) throws UnsupportedEncodingException, IOException, URISyntaxException, CoreException
	{
		/*
//		IFolder folder = javaProject.getProject().getFolder(javaProject.getProject().getLocation());		

		IFolder folder = ResourcesPlugin.getWorkspace().getRoot().getFolder(javaProject.getProject().getLocation());
		
		if (false == folder.exists())
		{
	    folder.create(IResource.NONE, true, monitor);
		}		
		
		IFile file = folder.getFile("temp.types");
		
		if (file.exists())
		{
			file.delete(true, monitor);
		}
		
		file.create(this.getClass().getResourceAsStream("/temp.types"), IResource.NONE, monitor);*/
	};		
	
	@Override
	protected void onDone()
	{
		done = true;
		if (null != this.exception)
		{
			Logger.log(this.exception);

			MessageDialog.openInformation(null, this.taskName, "Failed to generate tests\n" + this.exception.getMessage());
		}
		else
		{
			String message = "Generated tests: " + this.generatedTests;
			message += "\r\nfor Classes: " + this.classes;
			message += "\r\nfor Methods: " + this.methods;
			
			MessageDialog.openInformation(null, this.taskName, message);
		}
	};
	
	public void execute(Object selectedItem) {
		try {
			this.selectedItem = selectedItem;
			run(null);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public TestProject getTestProject() {
		return testProject;
	}

	public boolean isDone() {
		return done;
	}

	private int classes = 0;
	private int methods = 0;
	private int generatedTests = 0;
	private Exception exception;
	private TestProject testProject;
	private boolean done;
};
