package atm;

public class Bank
{
	public void transfer(Account from, Account to, Money ammount)
	{
		to.addMoney(ammount);
		from.removeMoney(ammount);
	}
	
	public int getCash() {
		return cash;
	}

	public void setCash(int cash) {
		this.cash = cash;
	}

	private int cash;
}
