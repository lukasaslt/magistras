package atm;

import java.nio.file.Paths;

public class TestSuiteMoneyGetFraction
{
	@org.junit.Test
	public void testGetFraction1() throws Throwable
	{
		atm.Money testObject = new atm.Money();

		int res = testObject.getFraction();
		java.util.ArrayList<Object> preValues0 = new java.util.ArrayList<Object>();

		eu.sarunas.atf.utils.junit.Assert.assertPostConditions(testObject, testObject.getClass().getMethod("getFraction"), preValues0, res, 
				Paths.get("resources/atm.ocl").toAbsolutePath().toString());
	}

};
