package atm;

public class ATM
{
	public void deposit(Account account, Money money)
	{
		account.addMoney(money);
	}
	
	public void withdraw(Account account, Money money)
	{
		account.removeMoney(money);
	}
	
	public int getHeldMoney() {
		return heldMoney;
	}

	public void setHeldMoney(int heldMoney) {
		this.heldMoney = heldMoney;
	}

	protected int heldMoney;
}
