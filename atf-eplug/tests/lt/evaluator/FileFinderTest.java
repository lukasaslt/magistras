package lt.evaluator;

import java.nio.file.Paths;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import lt.evaluator.common.EvaluatorConstants;
import lt.evaluator.component.FileFinder;
import lt.evaluator.util.EvaluatorTestUtil;

public class FileFinderTest {

	private static FileFinder fileFinder;
	private static IWorkspace workspace;
	private static IJavaProject javaProject;
	private static IProject project;
	private static java.nio.file.Path projectPath;
	
	@BeforeClass
	public static void beforeClass() {
		try {
			fileFinder = new FileFinder();
			projectPath = Paths.get("../test-project").toAbsolutePath().normalize();
			workspace = ResourcesPlugin.getWorkspace();
			IProgressMonitor monitor = new NullProgressMonitor();
			project = EvaluatorTestUtil.importExisitingProject(workspace, new Path(projectPath.toString()));
			project.build(IncrementalProjectBuilder.INCREMENTAL_BUILD, monitor);
			javaProject = JavaCore.create(project);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testFindAll() {
		try {
			List<java.nio.file.Path> findAll = fileFinder.findAll(projectPath, EvaluatorConstants.UML_EXT);
			Assert.assertEquals(1, findAll.size());
			java.nio.file.Path path = findAll.get(0);
			Assert.assertEquals(projectPath.resolve("resources/uml/triangle.uml"), path);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}
	
	@Test
	public void testFindAllNull() {
		try {
			List<java.nio.file.Path> findAll = fileFinder.findAll(null, null);
			Assert.assertNull(findAll);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}
	
	@Test
	public void testFindClassFile() {
		try {
			IJavaElement javaElement = javaProject.findElement(new Path("common/shapes/Triangle.java"));
			java.nio.file.Path findClassFile = fileFinder.findClassFile(projectPath, (ICompilationUnit) javaElement);
			Assert.assertNotNull(findClassFile);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}
	
	@Test
	public void testFindClassFileNull() {
		try {
			java.nio.file.Path findClassFile = fileFinder.findClassFile(null, null);
			Assert.assertNull(findClassFile);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

}