package lt.evaluator;

import java.nio.file.Paths;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import lt.evaluator.component.ModelParser;
import lt.evaluator.component.ResultCalculator;
import lt.evaluator.data.EModel;
import lt.evaluator.data.EResult;

public class ResultCalculatorTest {

	private static ModelParser modelParser;
	private static ResultCalculator resultCalculator;
	private static java.nio.file.Path projectPath;
	
	@BeforeClass
	public static void beforeClass() {
		try {
			modelParser = new ModelParser();
			resultCalculator = new ResultCalculator();
			projectPath = Paths.get("../test-project").toAbsolutePath().normalize();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testCalculate() {
		try {
			EModel eModel = new EModel();
			modelParser.parse(eModel, null, 
					projectPath.resolve("resources/uml/triangle.uml"), 
					projectPath.resolve("resources/uml/triangle.ocl"));
			
			EResult result = resultCalculator.calculate(eModel);
			Assert.assertEquals(87.5, result.getResult(), 0.5);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}
	
	@Test
	public void testCalculateNull() {
		try {
			EResult result = resultCalculator.calculate(null);
			Assert.assertNull(result);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

}