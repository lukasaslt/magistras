package lt.evaluator;

import java.nio.file.Paths;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import lt.evaluator.component.ModelParser;
import lt.evaluator.data.EModel;
import lt.evaluator.data.EOperation;
import lt.evaluator.data.EProperty;
import lt.evaluator.data.EType;
import lt.evaluator.data.OCLConstraint;

public class ModelParserTest {

	private static ModelParser modelParser;
	private static java.nio.file.Path projectPath;
	
	@BeforeClass
	public static void beforeClass() {
		try {
			modelParser = new ModelParser();
			projectPath = Paths.get("../test-project").toAbsolutePath().normalize();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testParseUml() {
		try {
			EModel eModel = new EModel();
			modelParser.parse(eModel, null, 
					projectPath.resolve("resources/uml/triangle.uml"), 
					projectPath.resolve("resources/uml/triangle.ocl"));
			
			Assert.assertEquals(1, eModel.getTypes().size());
			EType eType = eModel.getTypes().get(0);
			Assert.assertEquals("shapes.Triangle", eType.getName());
			
			Assert.assertEquals(1, eType.getOperations().size());
			Assert.assertEquals(3, eType.getProperties().size());
			
			EOperation eOperation = eType.getOperations().get(0);
			Assert.assertEquals("calculateArea", eOperation.getName());
			Assert.assertEquals(1, eOperation.getContraints().size());
			OCLConstraint oclConstraint = eOperation.getContraints().get(0);
			Assert.assertEquals("shapes::Triangle::calculateArea", oclConstraint.getFullConstrainedElementName());
			
			EProperty eProperty = eType.getProperties().get(0);
			Assert.assertEquals("a", eProperty.getName());
			Assert.assertEquals(1, eProperty.getContraints().size());
			oclConstraint = eProperty.getContraints().get(0);
			Assert.assertEquals("shapes::Triangle::a", oclConstraint.getFullConstrainedElementName());
			
			eProperty = eType.getProperties().get(1);
			Assert.assertEquals("b", eProperty.getName());
			Assert.assertEquals(1, eProperty.getContraints().size());
			oclConstraint = eProperty.getContraints().get(0);
			Assert.assertEquals("shapes::Triangle::b", oclConstraint.getFullConstrainedElementName());
			
			eProperty = eType.getProperties().get(2);
			Assert.assertEquals("c", eProperty.getName());
			Assert.assertEquals(1, eProperty.getContraints().size());
			oclConstraint = eProperty.getContraints().get(0);
			Assert.assertEquals("shapes::Triangle::c", oclConstraint.getFullConstrainedElementName());
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}
	
	@Test
	public void testParseJavaSingleType() {
		try {
			EModel eModel = new EModel();
			modelParser.parse(eModel, 
					"common.shapes.Square", 
					projectPath.resolve("bin/common/shapes/Square.class"), 
					projectPath.resolve("resources/square.ocl"));
			
			Assert.assertEquals(1, eModel.getTypes().size());
			EType eType = eModel.getTypes().get(0);
			Assert.assertEquals("common.shapes.Square", eType.getName());
			
			Assert.assertEquals(2, eType.getOperations().size());
			Assert.assertEquals(1, eType.getProperties().size());
			
			EOperation eOperation = eType.getOperations().get(0);
			Assert.assertEquals("getSize", eOperation.getName());
			
			eOperation = eType.getOperations().get(1);
			Assert.assertEquals("setSize", eOperation.getName());
			
			EProperty eProperty = eType.getProperties().get(0);
			Assert.assertEquals("size", eProperty.getName());
			Assert.assertEquals(1, eProperty.getContraints().size());
			OCLConstraint oclConstraint = eProperty.getContraints().get(0);
			Assert.assertEquals("common::shapes::Square::size", oclConstraint.getFullConstrainedElementName());
			
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}
	
	@Test
	public void testParseNull() {
		try {
			EModel eModel = new EModel();
			modelParser.parse(eModel, 
					"shapes.Square", 
					null, 
					projectPath.resolve("resources/square.ocl"));
			
			Assert.assertEquals(0, eModel.getTypes().size());
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

}