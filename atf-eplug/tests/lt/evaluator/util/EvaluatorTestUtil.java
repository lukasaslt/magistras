package lt.evaluator.util;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRunnable;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;

public class EvaluatorTestUtil {
	
	public static IProject importExisitingProject(IWorkspace workspace, IPath projectPath) throws CoreException {
	    final IProjectDescription description = workspace.loadProjectDescription(
	    projectPath.append(IPath.SEPARATOR + IProjectDescription.DESCRIPTION_FILE_NAME));
	    final IProject project = workspace.getRoot().getProject(description.getName());
	    
	    if (project.exists()) {
	        System.out.println("Project exists");
	        return project;
	    }
	    
	    IWorkspaceRunnable runnable = new IWorkspaceRunnable() {
	        public void run(IProgressMonitor monitor) throws CoreException {
	            project.create(description, monitor);
	            project.open(IResource.NONE, monitor);
	        }
	    };
	    
	    workspace.run(runnable, workspace.getRuleFactory().modifyRule(workspace.getRoot()), IResource.NONE, null);
	    return project;
	}

}
