package lt.evaluator;

import java.nio.file.Paths;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import lt.evaluator.data.EResult;
import lt.evaluator.util.EvaluatorTestUtil;

public class EvaluatorTest {

	private static Evaluator evaluator;
	private static IWorkspace workspace;
	private static SubMonitor subMonitor;
	private static IJavaProject javaProject;
	private static IProject project;
	
	@BeforeClass
	public static void beforeClass() {
		try {
			evaluator = new Evaluator();
			workspace = ResourcesPlugin.getWorkspace();
			IProgressMonitor monitor = new NullProgressMonitor();
			subMonitor = SubMonitor.convert(monitor, 100);
			project = EvaluatorTestUtil.importExisitingProject(workspace, new Path(Paths.get("../test-project").toAbsolutePath().normalize().toString()));
			project.build(IncrementalProjectBuilder.INCREMENTAL_BUILD, monitor);
			javaProject = JavaCore.create(project);
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testSelectJavaFile() {
		try {
			IJavaElement javaElement = javaProject.findElement(new Path("common/shapes/Triangle.java"));
			EResult result = evaluator.evaluate(javaElement, subMonitor);
			Assert.assertEquals(22, result.getResult(), 0.5);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}
	
	@Test
	public void testSelectJavaPackage() {
		try {
			IJavaElement javaElement = javaProject.findElement(new Path("common/shapes"));
			EResult result = evaluator.evaluate(javaElement, subMonitor);
			Assert.assertEquals(11.3, result.getResult(), 0.5);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}
	
	@Test
	public void testSelectProject() {
		try {
			EResult result = evaluator.evaluate(project, subMonitor);
			Assert.assertEquals(23, result.getResult(), 0.5);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}
	
	@Test
	public void testSelectUml() {
		try {
			IResource resource = project.findMember("resources/uml/triangle.uml");
			EResult result = evaluator.evaluate(resource, subMonitor);
			Assert.assertEquals(87.5, result.getResult(), 0.5);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}
	
	@Test
	public void testSelectInvalidFile() {
		try {
			IResource resource = project.findMember("resources/uml/triangle.ocl");
			EResult result = evaluator.evaluate(resource, subMonitor);
			Assert.assertNull(result);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

}